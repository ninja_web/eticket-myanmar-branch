<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function getSaleCount($user, $group = BUS_GROUP)
    {
        return DB::table('users')
            ->where('user_type', '=', BOAT_GROUP == $group ? BOAT_SALE : BUS_SALE)
            ->where('owner_code', '=', $user->owner_code)
            ->where('branch', '=', $user->branch)
            ->count();
    }

    public function getFinanceCount($user, $group = BUS_GROUP)
    {
        return DB::table('users')
            ->where('user_type', '=', BOAT_GROUP == $group ? BOAT_FINANCE : BUS_FINANCE)
            ->where('owner_code', '=', $user->owner_code)
            ->where('branch', '=', $user->branch)
            ->count();
    }


    public function getAgentByType()
    {

    }

}
