<?php namespace App\Http\Controllers\BoatTicket;

use App\BoatTicket\ActivatedBoatAgent;
use App\BoatTicket\BoatScheduleType;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Input;
use Laracasts\Flash\Flash;


class BoatAgentController extends Controller
{
    // TODO : Project : You can activate as you like but commission only be done from their start points. Activate start branch can get all the schedule without comission.

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($type = NONAUTHORIZED)
    {
        $boat_agent = array();
        if ($type == AUTHORIZE) {
            // checking the user Types
            // connect with agent name
            $query = DB::table('activated_boat_agents AS aba')
                ->select(array('aba.*', 'agent.name', 'agent.branch AS agent_branch', 'act_user.name as activated_by'))
                ->join('users AS agent', 'aba.agent_id', '=', 'agent.id', 'left')
                ->join('users AS act_user', 'aba.created_by', '=', 'act_user.id', 'left');
            if (BOAT_AGENT == Auth::user()->user_type) {
                $query->where('aba.activate_branch', Auth::user()->branch);
            }
            $query->where('aba.boat_owner', Auth::user()->owner_code);
            if (BOAT_BRANCH == Auth::user()->user_type) {
                $query->where('aba.activate_branch', Auth::user()->branch);
            }
            $query->groupBy('aba.agent_id');
            $boat_agent = $query->paginate(PER_PAGE);
        } else {
            // Get activate boat Agent
            $result = DB::table('activated_boat_agents')
                ->select(array('agent_code'))
                ->where('activate_branch', Auth::user()->branch)
                ->where('boat_owner', Auth::user()->owner_code)
                ->groupBy('agent_code')
                ->get();

            $activatedAgent = array();
            if ($result != null) {
                foreach ($result as $row) {
                    $activatedAgent[] = $row->agent_code;
                }
            }
            // Remove already activated
            $boat_agent = User::where('user_type', BOAT_AGENT)
                ->whereNotIn('agent_code', $activatedAgent)
                ->paginate(PER_PAGE);
        }
        $c_page = ['main' => 'boat_agent', 'sub' => ''];
        return View('boatticket.boat_agent.index', compact('c_page', 'type', 'boat_agent'));
    }

    public function activateAgent($agent_code = '')
    {
        if (Request::ajax() && !empty($agent_code)) {
            // get the agent owner code
            $agent = User::select(array('id', 'agent_code', 'branch'))
                ->where('agent_code', '=', $agent_code)
                ->where('user_type', '=', BOAT_AGENT)
                ->first();
            // check the id
            if ($agent == null) {
                return Response::json(array('key' => false));
            }

            // authoried the agent to boat group
            $act_agent = new ActivatedBoatAgent();
            $act_agent->activate_branch = Auth::user()->branch;
            $act_agent->boat_owner = Auth::user()->owner_code;
            $act_agent->created_by = Auth::user()->id;
            $act_agent->status = STATUS_ACTIVE;
            // agent id
            $act_agent->agent_id = $agent->id;
            $act_agent->agent_code = $agent_code;
            $act_agent->agent_branch = $agent->branch;
            $act_agent->save();

            if ($act_agent->id) {
                // Flash Message
                Flash::success('You have successfully activated new branch to our trusted agent.');
                return Response::json(array('key' => true));
            }
            return Response::json(array('key' => false, 'message' => 'Error Occrued!. Please try again.'));
        }
        return Response::json(array('key' => false, 'message' => 'Incomplet Action.'));
    }

    public function getCommission($boat_owner = '', $agent_code = '')
    {
        // get Agent Details
        $boat_agent = User::where('agent_code', $agent_code)
            ->where('user_type', BOAT_AGENT)
            ->first();
        // get All Schedule list
        $schedules = array();
        $org_schedules = DB::table('boat_schedule_types')
            ->select(array('id', 'total_time', 'total_branch', 'start_branch', 'end_branch', 'whole'))
            ->where('start_branch', Auth::user()->branch)
            ->where('owner_code', $boat_owner)
            ->get();
        // Generate the comission history
        foreach ($org_schedules as $scheule) {
            // get the comission history
            $history = array();
            $comission_history = DB::table('boat_agent_commission')
                ->where('boat_schedule', $scheule->id)
                ->where('agent_code', $agent_code)
                ->where('boat_owner', $boat_owner)
                ->get();
            if ($comission_history != null) {
                foreach ($comission_history as $row) {
                    $history[] = displayDate($row->start_date) . "  to  " . (!empty($row->end_date) ? displayDate($row->end_date) : " ....  ") . "  =>  " . $row->amount;
                }
            }
            $scheule->history = $history;
            $schedules[] = $scheule;
        }
        // get the schedule history
        $c_page = ['main' => 'boat_agent', 'sub' => ''];
        return View('boatticket.boat_agent.create_edit_commission', compact('c_page', 'schedules', 'boat_agent', 'boat_owner', 'agent_code'));
    }

    // Create the comission Amount
    public function postCommission(Request $request, $boat_owner = '', $agent_code = '')
    {
        $all_schedule = Input::get('schedule_type', array());
        foreach ($all_schedule as $schedule_id => $comission_amount) {
            if (ctype_digit($comission_amount) && $comission_amount > 0) {
                // get the schedule detail
                $schedule = BoatScheduleType::find($schedule_id);
                // update the latest old comission
                $old_comission = DB::table('boat_agent_commission')
                    ->where('boat_schedule', $schedule_id)
                    ->where('agent_code', $agent_code)
                    ->where('boat_owner', $boat_owner)
                    ->orderBy('id', 'desc')
                    ->first();

                // check the old comission
                if ($old_comission == null || $old_comission->start_date != Date('Y-m-d')) {
                    $new_comission = array(
                        'boat_owner' => $boat_owner,
                        'agent_code' => $agent_code,
                        'boat_schedule' => $schedule_id,
                        'start_branch' => $schedule->start_branch,
                        'end_branch' => $schedule->end_branch,
                        'amount' => $comission_amount,
                        'start_date' => Date('Y-m-d'),
                        'created_by' => Auth::user()->id,
                        'created_date' => Date('Y-m-d H:i:s'),
                        'via' => WEB
                    );
                    DB::table('boat_agent_commission')->insertGetId($new_comission);
                }
                if ($old_comission != null && $old_comission->start_date == Date('Y-m-d')) {
                    DB::table('boat_agent_commission')
                        ->where('id', $old_comission->id)
                        ->update(array('amount' => $comission_amount));
                } else if ($old_comission != null) {
                    DB::table('boat_agent_commission')
                        ->where('id', $old_comission->id)
                        ->update(array('end_date' => date('Y-m-d', time() - 60 * 60 * 24)));
                }

            }
        }
        Flash::success('Your have successfully changes the comission amount');
        return Redirect::to('boat/agent/commission/' . $boat_owner . '/' . $agent_code);
    }

}
