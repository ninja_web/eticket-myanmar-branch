<?php namespace App\Http\Controllers\BoatTicket;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BoatTicket\BoatUserRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class BoatSubAgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Builder = User::where('user_type', '=', BOAT_SUB_AGENT)
            ->where('agent_code', Auth::user()->agent_code);
        $sub_agent = $Builder->orderBy('created_at', 'asc')->paginate(PER_PAGE);
        $c_page = ['main' => 'sub_agent', 'sub' => ''];
        return View('boatticket.sub_agent.index', compact('c_page', 'sub_agent'));
    }

    public function getCreate($id = 0)
    {
        $sub_agent = User::find($id);
        $c_page = ['main' => 'sub_agent', 'sub' => ''];
        return View('boatticket.sub_agent.create_edit_sub_agent', compact('c_page', 'sub_agent'));
    }

    public function postCreate(BoatUserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        // Location (Yangon, Mandalay)
        $user->branch = $request->branch;
        $user->nrc = $request->nrc;
        $user->dob = date("Y-m-d", strtotime(str_replace('/', '-', $request->dob)));
        $user->parent_name = $request->parent_name;
        $user->phone = $request->phone;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->status = $request->status;

        if ($request->hasFile('profile')) {
            $image = $request->file('profile');
            $imageFileName = time() . '.' . $image->getClientOriginalExtension();
            /* file upload by shna */
            $upload_path = PROFILE_PATH . "/" . date("Y") . "/" . date("m") . "/";

            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }

            $ext = $request->file('profile')->getClientOriginalExtension();
            $imageName = $request->file('logo')->getClientOriginalName();
            $fileName = Date('Y-m-d-H:i:s') . "-" . $imageName;

            Input::file('profile')->move($upload_path, $fileName);

            $user->profile_path = $upload_path;
            $user->profile = $imageName;

        }

        // Type and password
        $user->agent_code = Auth::user()->agent_code;
        $user->user_type = BOAT_SUB_AGENT;
        $user->password = bcrypt(DEFAULT_PASSWORD);

        // Create by
        $user->created_by = Auth::id();
        $user->branch = Auth::user()->branch;
        $user->via = WEB;
        $user->save();
        if ($user->id) {
            // success message
            Flash::success('You have successfully created new sub agent.');
            return Redirect::to('boat/sub_agent/');
        }

    }

    public function getEdit($id)
    {
        $sub_agent = User::find($id);
        $c_page = ['main' => 'sub_agent', 'sub' => ''];
        return View('boatticket.sub_agent.create_edit_sub_agent', compact('c_page', 'sub_agent'));
    }

    public function postEdit(BoatUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;

        // Location (Yangon, Mandalay)
        $user->branch = $request->branch;
        $user->nrc = $request->nrc;
        $user->dob = date("Y-m-d", strtotime(str_replace('/', '-', $request->dob)));
        $user->parent_name = $request->parent_name;
        $user->phone = $request->phone;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->status = $request->status;

        if ($request->hasFile('profile')) {
            $image = $request->file('profile');
            $imageFileName = time() . '.' . $image->getClientOriginalExtension();
            /* file upload by shna */
            $upload_path = PROFILE_PATH . "/" . date("Y") . "/" . date("m") . "/";
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }

            $ext = $request->file('profile')->getClientOriginalExtension();
            $imageName = $request->file('logo')->getClientOriginalName();
            $fileName = Date('Y-m-d-H:i:s') . "-" . $imageName;

            Input::file('profile')->move($upload_path, $fileName);
            $user->profile_path = $upload_path;
            $user->profile = $imageName;

        }
        // Create by
        $user->created_by = Auth::id();
        $user->via = WEB;
        $user->save();
        if ($user->id) {
            // success message
            Flash::success('You have successfully edited new sub agent.');
            return Redirect::to('boat/sub_agent/');
        }
    }

}
