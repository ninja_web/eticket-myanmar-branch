<?php namespace App\Http\Controllers\BoatTicket;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class BoatController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $c_page = ['main' => 'boat', 'sub' => ''];
        return View('boatticket.boat.index', compact('c_page'));
    }

    public function getCreate($boat_id = 0)
    {
        $c_page = ['main' => 'boat', 'sub' => ''];
        return View('boatticket.boat.create_edit_boat', compact('c_page'));
    }

    public function postCreate($boat_id = 0)
    {
        # code...
    }

}
