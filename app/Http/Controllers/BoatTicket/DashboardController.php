<?php namespace App\Http\Controllers\BoatTicket;

// common
use App\BoatTicket\BoatScheduleType;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if (BOAT_ADMIN == Auth::user()->user_type) {
            return $this->adminDashboard();
        } else if (BOAT_BRANCH == Auth::user()->user_type) {
            return $this->branchDashboard();
        } else if (BOAT_SALE == Auth::user()->user_type) {
            return $this->saleDashboard();
        } else if (BOAT_AGENT == Auth::user()->user_type || BOAT_SUB_AGENT == Auth::user()->user_type) {
            return $this->agentDashboard();
        } else if (BOAT_FINANCE == Auth::user()->user_type) {
            return $this->financeDashboard();
        }
    }

    // Admi Dashboard
    public function adminDashboard()
    {
        $no_branch = User::where('user_type', BOAT_BRANCH)
            ->where('owner_code', Auth::user()->owner_code)
            ->count();
        $no_sale = User::where('user_type', BOAT_SALE)
            ->where('owner_code', Auth::user()->owner_code)
            ->count();

        $no_schedule = BoatScheduleType::where('owner_code', Auth::user()->owner_code)->count();
        $no_agent = 0;

        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('boatticket.dashboard.boat_admin', compact('c_page', 'no_branch', 'no_sale', 'no_schedule', 'no_agent'));
    }

    // Branch dashboard
    public function branchDashboard()
    {
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('boatticket.dashboard.boat_branch', compact('c_page', 'no_branch', 'no_sale', 'no_schedule', 'no_agent'));
    }

    public function saleDashboard()
    {
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('boatticket.dashboard.boat_sale', compact('c_page'));
    }

    public function financeDashboard()
    {
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('boatticket.dashboard.boat_finance', compact('c_page'));
    }

    public function agentDashboard()
    {
        $boat_lines = getAllActivatedBoatLine(Auth::user());
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('boatticket.dashboard.boat_agent', compact('c_page', 'boat_lines'));
    }
}
