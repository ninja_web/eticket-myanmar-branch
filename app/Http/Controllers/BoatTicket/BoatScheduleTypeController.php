<?php namespace App\Http\Controllers\BoatTicket;

use App;
use App\BoatTicket\BoatScheduleType;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BoatTicket\BoatScheduleTypeRequest;
use App\Http\Requests\BoatTicket\BoatTimeSequenceRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Input;
use Laracasts\Flash\Flash;
use PDF;
use Request;

class BoatScheduleTypeController extends Controller
{
    /**
     * Show the list of boat schedule type
     * @return {[type]} [description]
     */
    public function index()
    {
        $boat_schedule_type = BoatScheduleType::where('owner_code', Auth::user()->owner_code)
            ->orderBy('created_at', 'asc')
            ->paginate(PER_PAGE);
        $c_page = ['main' => 'define', 'sub' => ''];
        return View('boatticket.boat_schedule_type.index', compact('c_page', 'boat_schedule_type'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function getScheduleTypeCreate()
    {
        $c_page = ['main' => 'define', 'sub' => ''];
        return View('boatticket.boat_schedule_type.create_edit_boat_schedule_type', compact('c_page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postScheduleTypeCreate(BoatScheduleTypeRequest $request)
    {
        $boatSchedule = new BoatScheduleType();
        $boatSchedule->total_branch = Input::get('total_route');
        $boatSchedule->total_time = Input::get('hour') . ":" . Input::get('minute');
        // start and end
        $route = [];
        for ($i = 1; $i <= $request->total_route; $i++) {
            $route[] = Input::get('route_' . $i);
        }
        $boatSchedule->b_points = $request->b_points;
        $boatSchedule->d_points = $request->d_points;
        $boatSchedule->whole = json_encode($route);
        $boatSchedule->start_branch = $request->route_1;
        $boatSchedule->end_branch = Input::get('route_' . $request->total_route);

        // Authorized
        $boatSchedule->owner_code = Auth::user()->owner_code;
        $boatSchedule->created_by = Auth::user()->id;
        $boatSchedule->status = Input::get('status');
        $boatSchedule->via = WEB;
        $boatSchedule->save();

        if ($boatSchedule->id) {
            Flash::success('You have successfully created new boat schedule type.');
            return Redirect::to('boat/scheduleType/');
        }
    }

    public function getScheduleTypeEdit($id)
    {
        $boatSchedule = BoatScheduleType::where('id', $id)
            ->first();
        $c_page = ['main' => 'boat_schedule', 'sub' => ''];
        return View('boatticket.boat_schedule_type.create_edit_boat_schedule_type', compact('c_page', 'boatSchedule'));
    }

    public function postScheduleTypeSequenceCreate(BoatTimeSequenceRequest $request, $id)
    {
        $boatSchedule = BoatScheduleType::where('id', $id)->first();
        $time_sequence = $boatSchedule->time_sequence == null ? array() : json_decode($boatSchedule->time_sequence);

        // add new time sequence : check duplicate
        $time_sequence[] = date('H:i:00', strtotime($request->time_sequence));
        $boatSchedule->time_sequence = json_encode($time_sequence);
        $boatSchedule->status = Input::get('status');
        $boatSchedule->save();
        Flash::success('You have successfully created new boat sequence.');
        return Redirect::to('boat/scheduleType/' . $id . '/edit');
    }

    public function getScheduleTypeStatusToggle($id)
    {
        if (Request::ajax() && !empty($id)) {
            $schedule = BoatScheduleType::find($id);
            if (null == $schedule) {
                return json_encode(array('status' => false));
            }
            $schedule->status = $schedule->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
            $schedule->save();
            return json_encode(array('status' => true));
        }
    }

    public function destoryTimeSequence($id, $time)
    {
        if (Request::ajax() && !empty($id) && !empty($time)) {
            $schedule = BoatScheduleType::find($id);
            if (null == $schedule) {
                return json_encode(array('status' => false));
            }

            $time_sequence = json_decode($schedule->time_sequence, true);
            $time = date("H:i:00", $time);
            if (($key = array_search($time, $time_sequence)) !== false) {
                unset($time_sequence[$key]);
                $schedule->time_sequence = count($time_sequence) ? json_encode($time_sequence) : '';
                $schedule->save();
                return json_encode(array('status' => true));
            } else {

                return json_encode(array('status' => false, 'message' => 'You can not delete.Some transaction are using this time.'));
            }
        }
    }
}
