<?php namespace App\Http\Controllers\BoatTicket;

use App\BoatTicket\BoatSchedule;
use App\BoatTicket\BoatTicket;
use App\BoatTicket\BoatTicketDetail;
use App\BoatTicket\TicketModifyHistory;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Request;
use Input;
use Laracasts\Flash\Flash;
use PDF;


/**
 * Search the Ticket Pnr Number and Generate the PDF
 */
class CheckInController extends Controller
{
    public function index()
    {
        $c_page = ['main' => 'check_in', 'sub' => ''];
        return View('boatticket.check_in.index', compact('c_page'));
    }

    // TODO : Sa Lone : Please only select the require ticket no
    public function serachCheckInTicket(Request $request)
    {
        $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
            ->select('boat_tickets.*', 'boat_schedules.*', 'boat_tickets.id as ticket_id')
            ->where('boat_tickets.ticket_pnr_code', Input::get('ticket'))
            ->first();
        // passenger details
        if ($info != null) {
            $passengers = BoatTicketDetail::where('parent_id', $info->ticket_id)->get();
        } else {
            $passengers = array();
        }
        $search = true;
        $ticket_no = Input::get('ticket');
        $c_page = ['main' => 'check_in', 'sub' => ''];
        return View('boatticket.check_in.index', compact('c_page', 'info', 'search', 'ticket_no', 'passengers'));

        // $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
        //     ->select('boat_tickets.*', 'boat_tickets.id as boatticket_id', 'boat_tickets.total_seat as t_seat', 'boat_schedules.*', 'boat_tickets.id as ticket_id')
        //     ->where('boat_tickets.ticket_pnr_code', Input::get('ticket'))
        //     ->first();
        // // passenger details
        // if ($info != null) {
        //     $passengers = BoatTicketDetail::where('parent_id', $info->ticket_id)->get();
        // } else {
        //     $passengers = array();
        // }

        // $schedule = array();

        // if ($info != null) {
        //     $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_s_t.b_points', 'b_s_t.d_points', 'b_c.seat_plan'))
        //         ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
        //         ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
        //         ->where('boat_schedules.id', $info->boat_schedule)
        //         ->first();

        // }
        // $search = true;
        // $ticket_no = Input::get('ticket');
        // $c_page = ['main' => 'check_in', 'sub' => ''];
        // return View('boatticket.check_in.search', compact('c_page', 'info', 'search', 'ticket_no', 'passengers', 'schedule'));
    }

    public function generatePDF($ticket_id = 0)
    {
        if ($ticket_id > 0) {
            $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
                ->select('boat_tickets.*', 'boat_schedules.*')
                ->where('boat_tickets.id', $ticket_id)
                ->first();
            $data['info'] = $info;
            $pdf = PDF::loadView('pdf', $data);
            return $pdf->download('invoice.pdf');
        } else {
            die("Invalid Ticket ID");
        }
    }

    public function getModifyTicket($id = 0)
    {
        $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
            ->select('boat_tickets.*', 'boat_tickets.id as boatticket_id', 'boat_tickets.total_seat as t_seat', 'boat_schedules.*', 'boat_tickets.id as ticket_id')
            ->where('boat_tickets.id', $id)
            ->first();
        // passenger details

        $ticket_no = $info->ticket_pnr_code;

        if ($info != null) {

            $passengers = BoatTicketDetail::where('parent_id', $info->ticket_id)->get();

        } else {

            $passengers = array();

        }
        $schedule = array();


        if ($info != null) {

            $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_s_t.b_points', 'b_s_t.d_points', 'b_c.seat_plan'))
                ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
                ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
                ->where('boat_schedules.id', $info->boat_schedule)
                ->first();

        }
        $search = true;


        $c_page = ['main' => 'check_in', 'sub' => ''];


        return View('boatticket.check_in.search', compact('c_page', 'info', 'search', 'ticket_no', 'passengers', 'schedule'));

    }

    public function postModifyTicket(Request $request, $id = 0)
    {
        $seat = Input::get('seat');
        $boat_ticket = BoatTicket::find($id);

        $info = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_s_t.b_points', 'b_s_t.d_points', 'b_c.seat_plan'))
            ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_schedules.id', $boat_ticket->boat_schedule)
            ->first();

        if ($info->seat_plan != WITHOUT_SEAT_PLAN) {
            if (isset($seat) && count($seat)) {
                $tmp_arr = $seat;
                $ori_arr = explode(",", $boat_ticket->seat_no);

                if (count($seat) == 1) {
                    if (count($ori_arr) > 1) {
                        if (($key = array_search($seat[0], $ori_arr)) !== false) {
                            unset($ori_arr[$key]);
                        }
                        $tmp_arr = implode(",", $ori_arr);
                    } else {
                        $tmp_arr = '';
                    }
                } else {

                    $diff_arr = array_diff($ori_arr, $tmp_arr);
                    $tmp_arr = implode(",", $diff_arr);
                }


                $boat_schedule = BoatSchedule::find($boat_ticket->boat_schedule);

                if ($boat_schedule != null) {

                    $boat_schedule->take_seat_layout = $tmp_arr;
                    $boat_schedule->remain_seat = $boat_schedule->remain_seat + count($seat);
                    $boat_schedule->save();

                }

                if ($boat_ticket != null) {

                    $boat_ticket->seat_no = $tmp_arr;
                    $boat_ticket->total_seat = count($ori_arr) > 1 ? count($ori_arr) : 1;

                    $boat_ticket->save();
                }

                if (count($ori_arr) > 1) {


                    if (count($ori_arr)) {

                        for ($j = 0; $j < count($ori_arr); $j++) {

                            $detail = BoatTicketDetail::where('parent_id', $id)->first();

                            if ($detail != null) {

                                $detail->delete();
                            }


                        }

                    }
                } else {

                    $detail = BoatTicketDetail::where('parent_id', $id)->first();

                    if ($detail != null) {

                        $detail->delete();
                    }
                }

                $info = BoatTicket::find($id);

                if ($info->seat_no == "") {

                    $info->delete();
                }


                $history = new TicketModifyHistory;

                $history->modified_by = Auth::user()->id;

                $history->seat_no = implode(",", $seat);

                $history->ticket_pnr_code = $boat_ticket->ticket_pnr_code;

                $history->save();


            }

        } else {


            $seat = Input::get('no_seat');


            $boat_ticket = BoatTicket::find($id);

            if ($seat < $boat_ticket->total_seat) {

                $boat_schedule = BoatSchedule::find($boat_ticket->boat_schedule);

                $boat_schedule->remain_seat = $boat_schedule->total_seat - $seat;

                $boat_schedule->save();
            }


            $boat_ticket->total_seat = $seat;
            $boat_ticket->save();

            $info = BoatTicket::find($id);

            if ($info->boat_ticket == 0) {

                $info->delete();
            }

            $history = new TicketModifyHistory;

            $history->modified_by = Auth::user()->id;

            $history->ticket_pnr_code = $boat_ticket->ticket_pnr_code;

            $history->seat = $seat;

            $history->save();


        }


        Flash::success("Successfully updaated ticket information.");
        return redirect('boat/ticket/modify/' . $id);


    }
}
