<?php namespace App\Http\Controllers\BoatTicket;

// use Illuminate\Http\Request;
use App\BoatTicket\BoatClass;
use App\BoatTicket\BoatOwner;
use App\BoatTicket\BoatSchedule;
use App\BoatTicket\BoatScheduleType;
use App\BoatTicket\BoatTicket;
use App\BoatTicket\BoatTicketDetail;
use App\Http\Controllers\Controller;
use App\Http\Requests\BoatTicket\SelectTicketRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\BoatTicket\TicketModifyHistory;
use Laracasts\Flash\Flash;
use PDF;
use Request;
use Response;
use Str;
use Mail;

class TicketController extends Controller
{
    public function index($query_id = 0)
    {
        $scheduleTypes = array();
        $c_page = ['main' => 'ticket', 'sub' => ''];

        $inactive_list = BoatSchedule::where(array('status' => STATUS_INACTIVE))->get();


        // Boat Admin, Boat branch
        if (Auth::user()->user_type == BOAT_ADMIN) {
            $route_list = BoatScheduleType::where('owner_code', Auth::user()->owner_code)->get();
            // distinguish with branch
            $list = array();
            foreach ($route_list as $row) {
                $list[$row->start_branch][] = $row;
            }


        } // Boat Branch
        else if (Auth::user()->user_type == BOAT_BRANCH) {
            $list = BoatScheduleType::where('owner_code', Auth::user()->owner_code)
                ->where('start_branch', Auth::user()->branch)
                ->get();
            $scheduleTypes = getBoatScheduleCache('!=');
        } // Boat Sale
        else if (in_array(Auth::user()->user_type, array(BOAT_SALE))) {
            $list = BoatScheduleType::where('owner_code', Auth::user()->owner_code)
                ->where('start_branch', Auth::user()->branch)->get();
            $scheduleTypes = getBoatScheduleCache('!=');
        } // Boat Agent and Boat SubAgent
        else if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
            loadQuery($query_id);
            $branch = Input::get('branch', '');
            $branch = explode(',', $branch);
            $builder = BoatScheduleType::where('owner_code', Input::get('owner_code'));
            if (count($branch)) {
                $builder = $builder->whereIn('start_branch', $branch);
            }
            $route_list = $builder->get();
            // distinguish with branch
            $list = array();
            foreach ($route_list as $row) {
                $list[$row->start_branch][] = $row;
            }
        }
        return view('boatticket.ticket.index', compact('c_page', 'list', 'query_id', 'scheduleTypes'));
    }

    /**
     * [route Parsing Route Day By Day]
     * @param  {[type]} $schedule_type =             0 [description]
     * @param  {[type]} $time          =             0 [description]
     * @return {[type]}                [description]
     */
    public function route($schedule_type = 0, $time = 0)
    {
        $schedule = BoatScheduleType::find($schedule_type);
        // search boat list from date
        $query = DB::table('boat_schedules AS b_s')
            ->select(array('b_s.*', 'b_s_t.whole', 'b_c.class AS boat_class', 'b_c.logo', 'b_c.logo_path'))
            ->join('boat_class AS b_c', 'b_s.boat_class', '=', 'b_c.id', 'left')
            ->join('boat_schedule_types AS b_s_t', 'b_s.schedule_type', '=', 'b_s_t.id', 'left');

        $query->where('b_s.dept_date', date('Y-m-d', $time));
        $query->where('b_s.schedule_type', $schedule_type);
        // check the permission
        if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {

        }
        $schedules = $query->get();
        return View('boatticket.ticket.schedule_list', compact('c_page', 'schedules', 'schedule'));
    }

    public function searchRouteByDay(\Illuminate\Http\Request $request, $schedule_type = 0, $schedule_date = 0)
    {
        if (\Illuminate\Support\Facades\Request::isMethod('post')) {
            // check the input method
            $schedule_date = Input::get('schedule_date', date('d/m/Y'));
            $schedule_date = str_replace('/', '-', $schedule_date);
            $schedule_date = strtotime($schedule_date);

            $schedule_type = Input::get('schedule_type');
        }
        $schedule = BoatScheduleType::find($schedule_type);

        $c_page = ['main' => 'ticket', 'sub' => ''];
        return View('boatticket.ticket.route_list', compact('schedule', 'c_page', 'schedule_type', 'schedule_date'));
    }

    public function search(\Illuminate\Http\Request $request, $query_id = 0)
    {
        $query_array = array();
        if ($query_id > 0) {
            // select query
            $row = DB::table('query')
                ->where('id', $query_id)
                ->first();
            if (count($row)) {
                parse_str($row->query_string, $query_array);
            }
        }
        $search = array(
            'from' => Input::get('from', ''),
            'boat_schdule' => Input::get('boat_schdule', '')
        );

        $id = Input::get('id', '');

        $query_array = array_merge($query_array, $search);
        // check the input
        $query_id = saveQuery($query_array);

        return Redirect::to('boat/ticket/route/' . $id . "/" . $query_id);
    }

    public function searchAgnetLine($boat_owner = '')
    {
        if (empty($boat_owner)) return Redirect::to('boat/ticket/0');
        // Select all activated boat branch start list by owner code
        $query = DB::table('activated_boat_agents')
            ->select(array('activate_branch'))
            ->where('agent_code', Auth::user()->agent_code)
            ->where('boat_owner', $boat_owner)
            ->groupBy('activate_branch')
            ->get();
        $branch = array();
        foreach ($query as $row) {
            $branch[] = $row->activate_branch;
        }
        $query_array = array(
            'branch' => implode(",", $branch),
            'owner_code' => $boat_owner
        );
        $query_id = saveQuery($query_array);
        return Redirect::to('boat/ticket/' . $query_id);
    }

    /**
     * Select the Boat Schedule and Find the boat scheudle type
     **/

    public function getTicket($schedule_id = 0)
    {
        if ($schedule_id == 0 || !ctype_digit($schedule_id)) {
            // browse error
            Flash::info("Please select schedule and action");
            return Redirect::to('boat/schedule/create/');
        }
        $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_s_t.b_points', 'b_s_t.d_points', 'b_c.seat_plan'))
            ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_schedules.id', $schedule_id)
            ->first();
        if (!count($schedule)) {
            Flash::info("Please select schedule and action");
            return Redirect::to('boat/schedule/create/');
        }
        $c_page = ['main' => 'ticket', 'sub' => ''];
        return View('boatticket.ticket.select_ticket', compact('c_page', 'schedule'));
    }

    public function getTraverseTicket($boat_schedule_type, $time, $boat_order = 1)
    {
        $result = BoatSchedule::select("id")
            ->where("owner_code", Auth::user()->owner_code)
            ->where("schedule_type", $boat_schedule_type)
            ->where("dept_date", date('Y-m-d', $time))
            ->where("boat_order", $boat_order)
            ->first();
        if ($result != null && $result->id > 0) {
            return Redirect::to('boat/ticket/select/' . $result->id);
        } else {
            Flash::info("There is no schedule for that day");
            return Redirect::to('boat/ticket/');
        }
    }

    /**
     * Browse Schedule with 3 Types
     * (1) BUY
     * (2) CANCEL
     * (3) BOOK
     **/

    public function browseSchedule(SelectTicketRequest $request, $schedule_id = 0)
    {
        $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seat_plan', 'b_s_t.whole'))
            ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_schedules.id', $schedule_id)
            ->first();

        if (!count($schedule)) {
            Flash::info("Please select schedule and action");
            return Redirect::to('boat/schedule/create/');
        }


        // check the Request
        if ($request->seat_plan == WITHOUT_SEAT_PLAN) {
            $schedule->remain_seat = $schedule->remain_seat - $request->no_seat;
            $schedule->save();
        } else {
            // with seat plan
            $seat_no = $request->seat_no;
            $old_take_seat_layout = $schedule->take_seat_layout;
            if (!empty($seat_no)) {
                $seat_no = explode(",", $request->seat_no);
                $old_take_seat_layout = !empty($old_take_seat_layout) ? explode(",", $old_take_seat_layout) : array();
                $old_take_seat_layout = array_merge($old_take_seat_layout, $seat_no);
            }
            // substract remain seat
            $schedule->remain_seat = $schedule->total_seat - count($old_take_seat_layout);
            sort($old_take_seat_layout); // sort the seater
            $schedule->take_seat_layout = implode(",", $old_take_seat_layout);
            // upate the schedule
            $schedule->save();
        }

        // check the ticket
        $ticket = new BoatTicket();
        // schedule details
        // if the user is BOAT AGENT OR BOAT SUB AGENT
        if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
            $ticket->agent_code = Auth::user()->agent_code;
            $ticket->owner_code = $schedule->owner_code;
        } else {
            $ticket->owner_code = Auth::user()->owner_code;
        }

        $ticket->ticket_pnr_code = generateCode(6);
        $ticket->boat_schedule = $schedule_id;
        // only with seat plan
        $ticket->seat_no = $request->seat_plan == WITH_SEAT_PLAN ? $request->seat_no : '';
        // total seat
        $ticket->total_seat = $request->seat_plan == WITHOUT_SEAT_PLAN ? $request->no_seat : count($seat_no);

        $whole = json_decode($schedule->whole, true);
        $price = json_decode($schedule->price, true);
        $ticket->type = 'Book Ticket' == $request->btn_type ? BOOK : BUY;
        // generate the price
        $ticket->fare = $price[$request->selected_route];
        $route = explode("_", $request->selected_route);

        $ticket->from = $route[0];
        $ticket->to = $route[1];

        // user details
        $ticket->name = Auth::user()->name;
        $ticket->phone_no = Auth::user()->phone;
        $ticket->created_by = Auth::user()->id;
        $ticket->created_type = Auth::user()->user_type;
        $ticket->pay = STATUS_UNPAID;
        $ticket->save();

        if ($ticket->id) {
            // Get the request type
            if ('Book Ticket' == $request->btn_type) {
                Flash::success('You have successfully book these ticket with owner name');
            } else {
                $seat_no = $request->seat_plan == WITH_SEAT_PLAN ? explode(",", $ticket->seat_no) : array();
                // Save the Each Ticket Details
                for ($i = 0; $i < $ticket->total_seat; $i++) {
                    $details = new BoatTicketDetail;
                    $details->parent_id = $ticket->id;
                    $details->name = Auth::user()->name;
                    $details->email = Auth::user()->email;
                    $details->seat_no = $request->seat_plan == WITH_SEAT_PLAN ? $seat_no[$i] : '';
                    $details->save();
                }
                Flash::success('You have successfully buy these ticket with owner name');
            }
            return Redirect::to('/boat/ticket/passenger/' . $ticket->id);
        }
        // show error page
        Flash::error('Error Occured. Please try again');
        return Redirect::to('/boat/ticket/select/' . $schedule_id);
    }

    public function getPassengerDetails($ticket_id = 0)
    {
        // get the ticket details
        $ticket = BoatTicket::find($ticket_id);

        // update on boat schedule
        $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater', 'b_s_t.whole', 'b_s_t.b_points', 'b_s_t.d_points', 'b_c.seat_plan'))
            ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_schedules.id', $ticket->boat_schedule)
            ->first();

        $agent = array();

        if (BOOK == $ticket->type) {
            // get all agent list by branch
            $agent = DB::table('activated_boat_agents as abs')
                ->select(array('users.id', 'users.name'))
                ->join('users', 'abs.agent_id', '=', 'users.id')
                ->where('abs.boat_owner', Auth::user()->owner_code)
                ->where('users.user_type', '=', BOAT_AGENT)
                ->where('abs.activate_branch', '=', $schedule->start_branch)
                ->get();
        }


        $detail = BoatTicketDetail::where('parent_id', $ticket_id)->get();
        $c_page = ['main' => 'ticket', 'sub' => ''];
        if (count($detail)) {
            return View('boatticket.ticket.passenger_details_edit', compact('c_page', 'schedule', 'ticket', 'agent', 'detail'));
        } else {
            return View('boatticket.ticket.passenger_details', compact('c_page', 'schedule', 'ticket', 'agent'));
        }
    }

    public function postPassengerDetails(\Illuminate\Http\Request $request, $ticket_id)
    {
        // Delete Old History
        $ticket = BoatTicket::find($ticket_id);


        $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
            ->select('boat_tickets.*', 'boat_schedules.*', 'boat_tickets.id as ticket_id', 'b_c.seat_plan')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_tickets.id', $ticket_id)
            ->first();


        $owner = BoatSchedule::select(array('owner_code'))
            ->where('id', $ticket->boat_schedule)
            ->first();
        // user details
        // check the ticket type
        if ($ticket->type != BOOK) {
            BoatTicketDetail::where('parent_id', $ticket_id)->delete();
        }

        $seat_no = $ticket->seat_no;
        $tmp_seat = explode(",", $seat_no);
        $seat_count = count($tmp_seat);

        if ($request->p_type == "0") {/* Normal Booking */

            if ($info->seat_plan == WITH_SEAT_PLAN) {


                if ($seat_count > 0 && $ticket->type != BOOK) {

                    $name_prefix = $request->name_prefix;
                    $name = $request->name;
                    $email = $request->email;
                    // Nrc
                    $nrc_prefix = $request->nrc_prefix;
                    $nrc_state = $request->nrc_state;
                    $nrc_nation = $request->nrc_nation;
                    $nrc_no = $request->nrc_no;
                    $phone_no = $request->phone_no;
                    $country = $request->hidd_country;
                    $passport = $request->passport;
                    $email = $request->email;

                    for ($i = 0; $i < $seat_count; $i++) {
                        $details = new BoatTicketDetail;
                        $details->parent_id = $ticket->id;
                        $details->name_prefix = $ticket->type == BOOK ? $name_prefix : $name_prefix[$i];
                        $details->name = $ticket->type == BOOK ? $name : $name[$i];
                        $details->nrc_prefix = $nrc_prefix[$i];
                        $details->nrc_state = $nrc_state[$i];
                        $details->nrc_nation = $nrc_nation[$i];
                        $details->nrc_no = $nrc_no[$i];
                        $details->seat_no = $tmp_seat[$i];

                        $details->country = $country[$i];
                        $details->passport = $passport[$i];
                        $details->save();
                    }

                } else {


                    $ticket = BoatTicket::find($ticket_id);


                    $ticket->pay = $request->pay;
                    $ticket->email = $request->email;
                    $ticket->name_prefix = $request->buy_name_prefix;
                    $ticket->name = $request->buy_name;
                    $ticket->remark = $request->remark;
                    $ticket->discount = $request->discount;
                    $ticket->agent_code = $request->agent;


                    $ticket->save();

                    Flash::success('Yout have successfully Booked.');

                    return redirect('boat/ticket/passenger/' . $ticket_id);

                }

            } else {/* Without Seat Plan */


                if ($ticket->type != BOOK) {

                    $seat_count = $ticket->total_seat;

                    $name_prefix = $request->name_prefix;
                    $name = $request->name;
                    $email = $request->email;
                    // Nrc
                    $nrc_prefix = $request->nrc_prefix;
                    $nrc_state = $request->nrc_state;
                    $nrc_nation = $request->nrc_nation;
                    $nrc_no = $request->nrc_no;
                    $phone_no = $request->phone_no;
                    $country = $request->hidd_country;
                    $passport = $request->passport;
                    $email = $request->email;

                    for ($j = 0; $j < $seat_count; $j++) {

                        $details = new BoatTicketDetail;
                        $details->parent_id = $ticket->id;
                        $details->name_prefix = $ticket->type == BOOK ? $name_prefix : $name_prefix[$j];
                        $details->name = $ticket->type == BOOK ? $name : $name[$j];
                        $details->nrc_prefix = $nrc_prefix[$j];
                        $details->nrc_state = $nrc_state[$j];
                        $details->nrc_nation = $nrc_nation[$j];
                        $details->nrc_no = $nrc_no[$j];
                        // $details->seat_no = $tmp_seat[$i];

                        $details->country = $country[$j];
                        $details->passport = $passport[$j];

                        $details->save();
                    }
                } else {

                    $ticket = BoatTicket::find($ticket_id);


                    $ticket->pay = $request->pay;
                    $ticket->email = $request->email;
                    $ticket->name_prefix = $request->buy_name_prefix;
                    $ticket->name = $request->buy_name;
                    $ticket->remark = $request->remark;
                    $ticket->discount = $request->discount;
                    $ticket->agent_code = $request->agent;


                    $ticket->save();

                    Flash::success('Yout have successfully Booked.');

                    return redirect('boat/ticket/passenger/' . $ticket_id);


                }
            }

        } else {/* Booking with Upload file */
            if ($request->hasFile('file')) {
                $upload_path = PASSENGER_PATH . "/" . date("Y") . "/" . date("m") . "/";
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0755, true);
                    file_put_contents($upload_path . "/index.html", "");
                }

                $ext = $request->file('file')->getClientOriginalExtension();
                $imageName = $request->file('file')->getClientOriginalName();

                $fileName = Date('Y-m-d-H-i-s') . "-" . $imageName;
                $fileName = preg_replace('/\s+/', '-', $fileName);

                Input::file('file')->move($upload_path, $fileName);

                /* Creating Normal */
                $ticket->upload_type = $request->p_type;
                $ticket->file_name = $fileName;
                $ticket->file_path = $upload_path;
            }
        }
        $ticket->email = $request->email;
        $ticket->phone_no = $request->phone_no;
        $ticket->remark = $request->remark;
        $ticket->pay = $request->pay;
        // get the Owner Code From Schedule

        $ticket->owner_code = $owner->owner_code;

        // discount amount
        $ticket->discount = $request->discount;

        // Update the name if the ticket type is BOOK
        if ($ticket->type == BOOK) {

            $ticket->name_prefix = $request->buy_name_prefix;
            $ticket->name = $request->buy_name;
        }

        $ticket->save();

        if ($ticket->id) {

            Flash::success('Yout have successfully update the ticket passenger details');
            // go to ticket details
            // return Redirect::to('boat/ticket/orders/' . $ticket->boat_schedule);
            return Redirect::to('boat/ticket/details/' . $ticket->id);
        }
        // show the error
        Flash::error('Error Occured.Please try again.');
        return Redirect::to('boat/ticket/browse/' . $ticket->boat_schedule)->withInput();
    }

    public function getTicketDetails($ticket_id = 0)
    {
        $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
            ->select('boat_tickets.*', 'boat_schedules.*', 'boat_tickets.id as ticket_id', 'b_c.seat_plan')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_tickets.id', $ticket_id)
            ->first();
        // passenger details
        $passengers = BoatTicketDetail::where('parent_id', $ticket_id)->get();


        $search = true;
        $ticket_no = Input::get('ticket');
        $c_page = ['main' => 'check_in', 'sub' => ''];


        return View('boatticket.check_in.index', compact('c_page', 'info', 'search', 'ticket_no', 'passengers'));
    }

    public function cancelTicket($cancel_id, $cancelType = STATUS_TICKET_CANCEL)
    {
        if (!in_array($cancelType, array(STATUS_TICKET_CANCEL, STATUS_BOOK_CANCEL))) {
            return json_encode(array('status' => false));
        }

        if (Request::ajax() && !empty($cancel_id)) {

            $ticket = BoatTicket::find($cancel_id);


            if (null == $ticket) {
                return json_encode(array('status' => false));
            }

            /* Deleteing BoatTicketDetail Records */

            BoatTicketDetail::where('parent_id', $ticket->id)->delete();

            /* Adding New Record for Canel Ticket */

            $history = new TicketModifyHistory;

            $history->modified_by = Auth::user()->id;
            $history->ticket_pnr_code = $ticket->ticket_pnr_code;
            $history->seat_no = $cancelType;
            $history->previous = "Canelling Ticket";
            $history->next = "Canelling Ticket";

            $history->save();

            $seat_no = explode(",", $ticket->seat_no);


            // get the boat schedule details
            //  dd($ticket);
            $boatSchedule = BoatSchedule::find($ticket->boat_schedule);

            // check the boat Schedule
            if (null == $boatSchedule) {

                return json_encode(array('status' => false));
            }
            // check the cancel type
            if (STATUS_BOOK_CANCEL == $cancelType) {
                // booking ticket cancel
                $book_seat = explode(",", $boatSchedule->book_seat_layout);
                $book_seat = implode(",", array_diff($book_seat, $seat_no));
                // update on boatSchedule
                $boatSchedule->book_seat_layout = $book_seat;
            }
            $take_seat_layout = explode(",", $boatSchedule->take_seat_layout);
            $take_seat_layout = implode(",", array_diff($take_seat_layout, $seat_no));
            // update on boatSchedule
            $boatSchedule->take_seat_layout = $take_seat_layout;

            // update on remaining seat
            if ($ticket->type == WITHOUT_SEAT_PLAN) {

                $boatSchedule->remain_seat += $ticket->total_seat;
            } else {

                $boatSchedule->remain_seat += count($seat_no);
            }


            // delete the ticket
            $ticket->delete();
            // update on boat Schedule
            $boatSchedule->save();

            Flash::success('You have successfully cancel these ticket.');
            return json_encode(array('status' => 'true'));
        }
        return json_encode(array('status' => false));
    }

    // activate the ticket of booking records
    public function activateTicket(Request $request, $ticket_id = 0)
    {
        if (Request::ajax() && !empty($ticket_id)) {
            // get the ticket id
            $ticket = BoatTicket::find($ticket_id);
            if (null == $ticket) {
                return json_encode(array('status' => false));
            }
            $boatSchedule = BoatSchedule::find($ticket->boat_schedule);
            if (null == $boatSchedule) {
                return json_encode(array('status' => false));
            }

            $seat_no = explode(",", $ticket->seat_no);

            $book_seat = explode(",", $boatSchedule->book_seat_layout);
            $book_seat = implode(",", array_diff($book_seat, $seat_no));
            // update on boatSchedule
            $boatSchedule->book_seat_layout = $book_seat;

            // update the ticket
            $ticket->type = BUY;
            $ticket->save();

            $boatSchedule->save();
            Flash::success('You have successfully buy these ticket.');
            return json_encode(array('status' => true));
        }
        return json_encode(array('status' => false));
    }

    public function orderDetails($schedule_id = 0)
    {
        if (0 == $schedule_id) {
            return;
        }
        // get order details by ticket
        $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_c.seat_plan'))
            ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_schedules.id', $schedule_id)
            ->first();
        // TODO :: Sithu : Check for variou type
        $query = BoatTicket::select(array('boat_tickets.*', 'users.name as agent'))
            ->join('users', 'boat_tickets.created_by', '=', 'users.id', 'left');
        if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
            $query->where('boat_tickets.agent_code', Auth::user()->agent_code);
        } else {
            $query->where('boat_tickets.owner_code', Auth::user()->owner_code);
        }
        // SALE, SUB AGENT Can only see what they bought
        if (in_array(Auth::user()->user_type, array(BOAT_SUB_AGENT))) {
            $query->where('boat_tickets.created_by', Auth::user()->id);
            $query->where('boat_tickets.created_type', Auth::user()->user_type);
        }

        $tickets = $query->where('boat_tickets.boat_schedule', $schedule_id)->orderBy('created_at', 'DESC')->get();
        $c_page = ['main' => 'ticket', 'sub' => ''];

        return View('boatticket.ticket.order_details', compact('c_page', 'schedule', 'tickets'));
    }

    /**
     * [getTicketPrint description]
     * @param  {[type]} $ticket_id =             0 [description]
     * @return {[type]}            [description]
     */
    public function getTicketPrint($ticket_id = 0)
    {
        // Generate the Owner Code
        $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
            ->select('boat_tickets.*', 'boat_tickets.created_at as b_created_at', 'boat_schedules.*', 'boat_tickets.id as ticket_id')
            ->where('boat_tickets.id', $ticket_id)
            ->first();


        $owner = BoatOwner::where('owner_code', $info->owner_code)->first();


        if ($info != null) {
            $boat_class = BoatClass::find($info->boat_class);
            $boat_schedule_type = BoatScheduleType::where('id', $info->schedule_type)->first();

            // passenger details
            $passengers = BoatTicketDetail::where('parent_id', $ticket_id)->get();
            $search = true;
            $ticket_no = Input::get('ticket');
            $c_page = ['main' => 'check_in', 'sub' => ''];

            $pdf = PDF::loadView('pdf.invoice', compact('owner', 'info', 'passengers', 'ticket_no', 'boat_schedule_type', 'boat_class'));
            return $pdf->stream($info->name . "-" . date('Y-m-d-H-i-s') . ".pdf");
        } else {
            die("Invalid Url.Please go back.");
        }
    }

    public function getFileDownload($id = 0)
    {
        $info = BoatTicket::find($id);
        return Response::download($info->file_path . $info->file_name);
    }

    public function getTicketDetailsFile($id = 0)
    {
        $info = BoatTicket::find($id);
        return Response::download($info->file_path . $info->file_name);
    }


    public function getSendMail(\Illuminate\Http\Request $request, $ticket_id = 0)
    {


        $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
            ->select('boat_tickets.*', 'boat_schedules.*', 'boat_tickets.created_at as b_created_at', 'boat_tickets.id as ticket_id')
            ->where('boat_tickets.id', $ticket_id)
            ->first();
        $owner = BoatOwner::where('owner_code', $info->owner_code)->first();


        if ($info != null) {
            $boat_class = BoatClass::find($info->boat_class);
            $boat_schedule_type = BoatScheduleType::where('id', $info->schedule_type)->first();

            // passenger details
            $passengers = BoatTicketDetail::where('parent_id', $ticket_id)->get();
            $search = true;
            $ticket_no = Input::get('ticket');
            $c_page = ['main' => 'check_in', 'sub' => ''];

            // $pdf = PDF::loadView('pdf.invoice', compact('owner', 'info', 'passengers', 'ticket_no', 'boat_schedule_type', 'boat_class'));
            // dreturn $pdf->stream($info->name . "-" . date('Y-m-d-H-i-s') . ".pdf");

            $data = array(

                'owner' => $owner,
                'info' => $info,
                'passengers' => $passengers,
                'ticket_no' => $ticket_no,
                'boat_schedule_type' => $boat_schedule_type,
                'boat_class' => $boat_class


            );


            Mail::send('emails.invoice', $data, function ($message) use ($request, $info) {
                $message->from('info.iticketmyanmar@gmail.com', 'IticketMyanmar');

                $message->to($request->email)->subject('eTicket ' . $info->ticket_pnr_code);

                // $message->attachData($pdf->output(),"Invoice_".$info->ticket_pnr_code.".pdf");
            });


            return Response::json(array('success' => true));


        } else {
            die("Invalid Url.Please go back.");
        }

    }

}
