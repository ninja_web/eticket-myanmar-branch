<?php namespace App\Http\Controllers\BoatTicket;

use App\BoatTicket\BoatSchedule;
use App\BoatTicket\BoatTicket;
use App\BoatTicket\BoatTicketDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\BoatTicket\TicketModifyHistory;
use Input;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;

class ManageTicketController extends Controller
{
    public function index($id = 0)
    {

        $info = array();

        $search = false;

        $c_page = ['main' => 'check_in', 'sub' => ''];

        if ($id > 0) {


            $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
                ->select('boat_tickets.*', 'boat_tickets.id as boatticket_id', 'boat_tickets.total_seat as b_total_seat', 'boat_tickets.total_seat as t_seat', 'boat_schedules.*', 'boat_tickets.id as ticket_id')
                ->where('boat_tickets.id', $id)
                ->first();


            // passenger details
            if ($info != null) {

                $passengers = BoatTicketDetail::where('parent_id', $info->ticket_id)->get();

            } else {

                $passengers = array();
            }

            $schedule = array();

            if ($info != null) {
                $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_s_t.b_points', 'b_s_t.d_points', 'b_c.seat_plan'))
                    ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
                    ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
                    ->where('boat_schedules.id', $info->boat_schedule)
                    ->first();

            }

            $ticket_no = $info != "" ? $info->ticket_pnr_code : '';
            $search = $info != null ? true : false;

        }


        return View('boatticket.manage.manage_ticket', compact('c_page', 'info', 'search', 'ticket_no', 'passengers', 'schedule'));

    }

    public function getTicketList()
    {


        // get order details by ticket
        $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_c.seat_plan'))
            ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
            ->where('boat_schedules.created_by', Auth::user()->id)
            ->first();
        // TODO :: Sithu : Check for variou type
        $query = BoatTicket::select(array('boat_tickets.*', 'users.name as agent'))
            ->join('users', 'boat_tickets.created_by', '=', 'users.id', 'left')
            ->join('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule', 'left');

        if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
            $query->where('boat_tickets.agent_code', Auth::user()->agent_code);
        } else {
            $query->where('boat_tickets.owner_code', Auth::user()->owner_code);
        }
        // SALE, SUB AGENT Can only see what they bought
        if (in_array(Auth::user()->user_type, array(BOAT_SALE, BOAT_SUB_AGENT))) {
            $query->where('boat_tickets.created_by', Auth::user()->id);
            $query->where('boat_tickets.created_type', Auth::user()->user_type);
        }

        $tickets = $query->where('boat_tickets.created_by', Auth::user()->id)
            ->where('boat_schedules.dept_date', "=", date("Y-m-d"))->orderBy('created_at', 'DESC')->get();
        $c_page = ['main' => 'ticket_manage', 'sub' => ''];


        return View('boatticket.manage.ticketlist', compact('c_page', 'schedule', 'tickets'));
    }

    public function postManageTicket(\Illuminate\Http\Request $request, $id = 0)
    {

        $ticket = BoatTicket::find($id);


        if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {

            $ticket->owner_code = Auth::user()->agent_code;

        } else {

            $ticket->owner_code = Auth::user()->owner_code;

        }

        $seat_no = $request->seat_no;

        $block_seat = $request->block_seat;

        $current_seat = $request->current_seat;


        // $ticket->agent_id = Auth::user()->id;
        // only with seat plan
        $ticket->seat_no = $request->seat_plan == WITH_SEAT_PLAN ? $request->seat_no : '';
        // total seat
        $ticket->total_seat = $request->seat_plan == WITHOUT_SEAT_PLAN ? $request->no_seat : count(explode(",", $seat_no));

        $ticket->save();

        $schedule = BoatSchedule::find($request->schedule_id);

        if ($request->seat_plan != WITHOUT_SEAT_PLAN) {


            $total_seat = $block_seat . "," . $seat_no;

            $schedule->remain_seat = $schedule->total_seat - (count(explode(",", $total_seat)));
            $schedule->take_seat_layout = $total_seat;

        } else {

            $schedule->remain_seat = ($schedule->remain_seat - $request->no_seat) + $request->old_seat;
        }

        $schedule->save();


        if ($request->seat_plan != WITHOUT_SEAT_PLAN) {

            $current_count = explode(",", $current_seat);
            $new_count = explode(",", $seat_no);

            if (count($current_count) > count($new_count)) {

                $tmp_arr = array_values(array_diff($current_count, $new_count));


                for ($i = 0; $i < count($tmp_arr); $i++) {

                    $detail = BoatTicketDetail::where(array('parent_id' => $ticket->id, 'seat_no' => $tmp_arr[$i]))->orderBy('created_at', 'DESC')->first();

                    if ($detail != null) {

                        $detail->delete();
                    }
                }
            } else if (count($new_count) > count($current_count)) {

                $tmp_arr = array_values(array_diff($new_count, $current_count));

                $tmp_arr = array_values(array_diff($new_count, $current_count));


                $detail = BoatTicketDetail::where('parent_id', $ticket->id)->orderBy('created_at', 'DESC')->first();

                for ($i = 0; $i < count($tmp_arr); $i++) {


                    $boat_detail = new BoatTicketDetail;

                    $boat_detail->parent_id = $detail->parent_id;
                    $boat_detail->name = $detail->name;
                    $boat_detail->email = $detail->email;
                    $boat_detail->phone_no = $detail->phone_no;
                    $boat_detail->passport = $detail->passport;
                    $boat_detail->nrc_prefix = $detail->nrc_prefix;
                    $boat_detail->nrc_state = $detail->nrc_state;
                    $boat_detail->nrc_nation = $detail->nrc_nation;
                    $boat_detail->nrc_no = $detail->nrc_no;
                    $boat_detail->name_prefix = $detail->name_prefix;
                    $boat_detail->seat_no = $tmp_arr[$i];

                    $boat_detail->country = $detail->country;

                    $boat_detail->save();


                }

                $tmp_arr = array_values(array_diff($current_count, $new_count));

                for ($i = 0; $i < count($tmp_arr); $i++) {

                    $detail = BoatTicketDetail::where(array('parent_id' => $ticket->id, 'seat_no' => $tmp_arr[$i]))->orderBy('created_at', 'DESC')->first();

                    if ($detail != null) {

                        $detail->delete();
                    }
                }


            } else {

                $tmp_arr = array_values(array_diff($new_count, $current_count));

                $detail = BoatTicketDetail::where('parent_id', $ticket->id)->orderBy('created_at', 'DESC')->first();

                for ($i = 0; $i < count($tmp_arr); $i++) {


                    $boat_detail = new BoatTicketDetail;

                    $boat_detail->parent_id = $detail->parent_id;
                    $boat_detail->name = $detail->name;
                    $boat_detail->email = $detail->email;
                    $boat_detail->phone_no = $detail->phone_no;
                    $boat_detail->passport = $detail->passport;
                    $boat_detail->nrc_prefix = $detail->nrc_prefix;
                    $boat_detail->nrc_state = $detail->nrc_state;
                    $boat_detail->nrc_nation = $detail->nrc_nation;
                    $boat_detail->nrc_no = $detail->nrc_no;
                    $boat_detail->name_prefix = $detail->name_prefix;
                    $boat_detail->seat_no = $tmp_arr[$i];

                    $boat_detail->country = $detail->country;

                    $boat_detail->save();


                }

                $tmp_arr = array_values(array_diff($current_count, $new_count));


                for ($i = 0; $i < count($tmp_arr); $i++) {

                    $detail = BoatTicketDetail::where(array('parent_id' => $ticket->id, 'seat_no' => $tmp_arr[$i]))->orderBy('created_at', 'DESC')->first();

                    if ($detail != null) {

                        $detail->delete();
                    }
                }

            }

        }

        $history = new TicketModifyHistory;

        $history->modified_by = Auth::user()->id;

        if ($request->seat_plan != WITHOUT_SEAT_PLAN) {

            $history->previous = $current_seat;
            $history->next = $seat_no;
        } else {

            $history->previous = $request->old_seat;
            $history->next = $request->no_seat;

        }


        $history->ticket_pnr_code = $ticket->ticket_pnr_code;

        $history->save();


        Flash::success('Successfully updated ticket information.');

        return redirect('boat/ticket/manage/' . $id);

        /* Need to be done for History */


    }

    public function searchPnr(Request $request)
    {

        $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
            ->select('boat_tickets.*', 'boat_tickets.id as boatticket_id', 'boat_tickets.total_seat as t_seat', 'boat_schedules.*', 'boat_tickets.id as ticket_id')
            ->where('boat_tickets.ticket_pnr_code', Input::get('ticket'))
            ->first();
        // passenger details
        if ($info != null) {
            $passengers = BoatTicketDetail::where('parent_id', $info->ticket_id)->get();
        } else {
            $passengers = array();
        }

        $schedule = array();

        if ($info != null) {
            $schedule = BoatSchedule::select(array('boat_schedules.*', 'b_c.class', 'b_c.seater as seater', 'b_s_t.whole', 'b_s_t.b_points', 'b_s_t.d_points', 'b_c.seat_plan'))
                ->join('boat_schedule_types AS b_s_t', 'boat_schedules.schedule_type', '=', 'b_s_t.id', 'left')
                ->join('boat_class AS b_c', 'boat_schedules.boat_class', '=', 'b_c.id', 'left')
                ->where('boat_schedules.id', $info->boat_schedule)
                ->first();

        }
        $search = true;
        $ticket_no = Input::get('ticket');
        $c_page = ['main' => 'check_in', 'sub' => ''];
        return View('boatticket.manage.index', compact('c_page', 'info', 'search', 'ticket_no', 'passengers', 'schedule'));

        // $info = BoatTicket::leftjoin('boat_schedules', 'boat_schedules.id', '=', 'boat_tickets.boat_schedule')
        //     ->select('boat_tickets.*', 'boat_schedules.*', 'boat_tickets.id as ticket_id')
        //     ->where('boat_tickets.ticket_pnr_code', Input::get('ticket'))
        //     ->first();
        // // passenger details
        // if ($info != null) {
        //     $passengers = BoatTicketDetail::where('parent_id', $info->ticket_id)->get();
        // } else {
        //     $passengers = array();
        // }
        // $search = true;
        // $ticket_no = Input::get('ticket');
        // $c_page = ['main' => 'check_in', 'sub' => ''];
        // return View('boatticket.manage.index', compact('c_page', 'info', 'search', 'ticket_no', 'passengers'));
    }
}
