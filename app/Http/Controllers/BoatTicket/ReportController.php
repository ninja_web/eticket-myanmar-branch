<?php namespace App\Http\Controllers\BoatTicket;

use App\BoatTicket\BoatSchedule;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use DB;
use Excel;
use Flash;
use Illuminate\Http\Request;
use Str;
use PDF;

class ReportController extends Controller
{
    public function index($type = 'daily')
    {
        $c_page = ['main' => 'report', 'sub' => ''];
        if ($type == 'main') {
            // get schedule type
            $schedule_type = array();
            if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
                // get the activate boat agent line
                $branch = array();
                $query = DB::table('activated_boat_agents')
                    ->where('agent_code', Auth::user()->agent_code)
                    ->get();
                if (count($query)) {
                    $sql = "SELECT `id`, `start_branch`, `end_branch` FROM `boat_schedule_types` WHERE ";
                    $index = 0;
                    foreach ($query as $row) {
                        $sql .= $index > 0 ? " OR " : "";
                        $sql .= " (`owner_code` = '" . $row->boat_owner . "' AND `start_branch` = '" . $row->activate_branch . "')";
                        $index++;
                    }
                    $schedule_type = DB::select(DB::raw($sql));
                }
            } else {
                $query = DB::table('boat_schedule_types')
                    ->select(array('id', 'start_branch', 'end_branch'))
                    ->where('owner_code', Auth::user()->owner_code);
                if (in_array(Auth::user()->user_type, array(BOAT_BRANCH, BOAT_FINANCE))) {
                    $query->where('start_branch', Auth::user()->branch);
                }
                $schedule_type = $query->get();
            }
            return View('boatticket.report.main_report', compact('c_page', 'schedule_type'));
        } else {
            // Daily Sale Report
            // Branch or Sale Person
            if (Auth::user()->user_type == BOAT_FINANCE) {
                // Get Sale Person
                $sale_person = User::select(array('id', 'name', 'branch'))
                    ->where('user_type', BOAT_SALE)
                    ->where('owner_code', Auth::user()->owner_code)
                    ->get();

                return View('boatticket.report.index', compact('c_page', 'sale_person'));
            }

            $report_date = date('Y-m-d');
            $sale_person = Auth::user()->user_type == BOAT_FINANCE ? '' : Auth::user()->id;
            $report_type = '';
            $excel_data = dailySaleReport($report_date, $sale_person, $report_type);

            // Get the Sale Person detail
            $person = Auth::user()->user_type == BOAT_FINANCE ? User::find($sale_person) : Auth::user();
            $excel_data['report_person']['name'] = $person->name;
            $excel_data['report_person']['branch'] = Str::title($person->branch);

            $filter = array();
            $filter['report_date'] = date('d/m/Y');
            $filter['report_type'] = '';
            return View('boatticket.report.index', compact('c_page', 'excel_data', 'filter'));
        }
    }

    public function generateReport(Request $request, $type = '')
    {
        $header = array('No.', 'Date', "Route", "Seat", 'Ticket Price', 'Discount', 'Total Discount', 'Total');
        $data = array();
        for ($i = 0; $i < 10; $i++) {
            $data[$i] = array($i + 1, '11-11-15', 'Mandalay - Bagan', 2, 5000, '-', '-', 10000);
        }

        Excel::create('Daily Sale Report', function ($excel) use ($data, $header) {
            $excel->setCreator("Sithu")
                ->setCompany("Company")
                ->setTitle("Hello");
            $excel->sheet('Sheet 1', function ($sheet) use ($data, $header) {
                $sheet->row(1, array('DAILY DUTY SALES REPORT', '', '', '', '', '', '', ''));
                $sheet->row(1, function ($row) {
                    $row->setFontWeight('bold');
                });
                $sheet->row(2, array('11 NOV 2015', '', '', 'SALE PERSON: KO SOE', '', '', 'MANDALAY BRANCH', ''));
                $sheet->row(2, function ($row) {
                    $row->setFontWeight('bold');
                });
                $sheet->appendRow($header);
                $sheet->rows($data);
            });

        })->download('xls');
    }

    /**
     * [FunctionName description]
     * @param string $value [description]
     */

    public function searchDailyReport(Request $request)
    {
        // Generate Sale Peson List
        $sale_person = array();
        $report_date = $request->report_date;
        $sale_person = Auth::user()->user_type == BOAT_FINANCE ? $request->sale_person : Auth::user()->id;
        $report_type = $request->report_type;
        $excel_data = dailySaleReport($report_date, $sale_person, $report_type);
        // Get the Sale Person detail
        $person = Auth::user()->user_type == BOAT_FINANCE ? User::find($sale_person) : Auth::user();
        $excel_data['report_person']['name'] = $person->name;
        $excel_data['report_person']['branch'] = Str::title($person->branch);

        $filter = array();
        $filter['report_date'] = $request->report_date;
        $filter['report_type'] = $request->report_type;

        if (Auth::user()->user_type == BOAT_FINANCE) {
            // Get Sale Person
            $sale_person = User::select(array('id', 'name', 'branch'))
                ->where('user_type', BOAT_SALE)
                ->where('owner_code', Auth::user()->owner_code)
                ->get();
        }
        $c_page = ['main' => 'report', 'sub' => ''];

        return View('boatticket.report.index', compact('c_page', 'excel_data', 'filter', 'sale_person'));

    }


    public function dailySaleReport($report_date = '', $report_type = '')
    {
        // report data
        $report_date = date('Y-m-d', strtotime($report_date));
        $report_type = $report_type == 0 ? '' : $report_type;
        $sale_person = Auth::user()->id;
        $excel_data = dailySaleReport($report_date, $sale_person, $report_type);

        // Get the Sale Person detail
        $person = Auth::user()->user_type == BOAT_FINANCE ? User::find($sale_person) : Auth::user();
        $excel_data['report_person']['name'] = $person->name;
        $excel_data['report_person']['branch'] = Str::title($person->branch);
        if (!empty($excel_data['report_data'])) {
            $report_data = $excel_data['report_data'];
            $header = array('NO.', 'OWNER', 'DATE', "ROUTE", "SEAT", 'Ticket Price', 'DISCOUNT', 'Total Discount', 'TOTAL');
            $data = array();
            $index = 0;
            foreach ($report_data as $row) {
                $data[$index] = array($index + 1, $row['owner_name'], displayDate($row['dept_date']), $row['route'], $row['total_seat'], $row['price'], $row['discount'], $row['discount'], $row['total_amount']);
                $index++;
            }

            Excel::create('Daily Sale Report' . "_" . date("Y-m-d H:i:s"), function ($excel) use ($data, $excel_data, $header) {

                $excel->setCreator($excel_data['report_person']['name'])
                    ->setCompany("Company")
                    ->setTitle("Hello");

                $excel->sheet('Sheet 1', function ($sheet) use ($data, $header, $excel_data) {
                    $sheet->row(1, array('DAILY DUTY SALES REPORT', '', '', '', '', '', '', ''));
                    $sheet->row(1, function ($row) {
                        $row->setFontWeight('bold');
                    });

                    $sheet->row(2, array($excel_data['report_person']['date'], '', '', 'SALE PERSON: ' . $excel_data['report_person']['name'], '', '', $excel_data['report_person']['branch'], ''));

                    $sheet->row(2, function ($row) {
                        $row->setFontWeight('bold');
                    });


                    $sheet->appendRow($header);

                    $sheet->row(3, function ($row) {
                        $row->setFontWeight('bold');
                        $row->setBackground('#ffeb3b');
                    });

                    $sheet->rows($data);

                    $footer_index = count($data) + 8;
                    $sheet->row($footer_index, array('', '', '', 'TOTAL', $excel_data['report_brief']['total_seat'], $excel_data['report_brief']['total_amount'], '', $excel_data['report_brief']['total_discount'], $excel_data['report_brief']['total_net_amount']));

                    $sheet->row($footer_index, function ($row) {
                        $row->setFontWeight('bold');
                    });
                    $sheet->protect('password', function (\PHPExcel_Worksheet_Protection $protection) {
                        $protection->setSort(true);
                    });

                    $sheet->setHeight(3, 25);
                    $sheet->getProtection()->setPassword('password');

                });


            })->download('xls');

        } else {

            Flash::info("There is no record about your search.");
        }

        $c_page = ['main' => 'report', 'sub' => ''];

        return View('boatticket.report.index', compact('c_page', 'exce_data'));
    }

    public function scheduleList(Request $request)
    {
        $report_date = str_replace('/', '-', $request->report_date);
        $report_date = date('Y-m-d', strtotime($report_date));
        $schedule_list = DB::table('boat_schedules')
            ->where('schedule_type', $request->schedule_type)
            ->where('dept_date', $report_date)
            ->get();
        return View('boatticket.report.schedule_list', compact('schedule_list'));
    }

    /**
     * [mainSaleReport description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function mainSaleReport(Request $request, $schedule_id = 0)
    {

        // get Schedule details
        $schedule = BoatSchedule::find($schedule_id);

        $excel_data = mainSaleReport($schedule);

        $excel_data['report_brief']['schedule'] = Str::title($schedule->start_branch) . ' - ' . Str::title($schedule->end_branch) . '  ' . $schedule->boat_order . ' BOAT ';

        $excel_data['report_brief']['dept_date'] = date('d-m-Y H:i A', strtotime($schedule->dept_date . $schedule->dept_time));

        $ticket_price = json_decode($schedule->price, true);

        $excel_data['report_brief']['ticket_price'] = $ticket_price[$schedule->start_branch . '_' . $schedule->end_branch];

        //dd($excel_data);

        if (!empty($excel_data['report_data'])) {

            $header = array('NO.', 'BUYER', "TIME", "SEAT", 'TOTAL', 'DISCOUNT/Commission', 'Total Discount', 'TOTAL');

            $data = array();

            $report_data = $excel_data['report_data'];

            $index = 0;

            foreach ($report_data as $row) {

                $data[$index] = array($index + 1, $row['buyer'], date("H:i:s", strtotime($row['time'])), $row['seat_no'], $row['total_amount'], $row['discount'], $row['total_discount'], $row['total_amount']);

                $index++;
            }

            Excel::create('Main Report' . "_" . date("Y-m-d H:i:s"), function ($excel) use ($data, $excel_data, $header) {

                $excel->setCreator('')
                    ->setCompany("Company")
                    ->setTitle("Main Report");

                $excel->sheet('Sheet 1', function ($sheet) use ($data, $header, $excel_data) {

                    $sheet->row(1, array('SALES REPORT', '( Generate by Branc or Finance)', '', '', '', '', '', ''));

                    $sheet->row(1, function ($row) {

                        $row->setFontWeight('bold');

                    });

                    $sheet->row(2, array($excel_data['report_brief']['dept_date'], $excel_data['report_brief']['schedule'], '', '', '', '', '', $excel_data['report_brief']['ticket_price']));

                    $sheet->row(2, function ($row) {
                        $row->setFontWeight('bold');
                    });


                    $sheet->appendRow($header);

                    $sheet->row(3, function ($row) {

                        $row->setFontWeight('bold');
                        $row->setBackground('#ffeb3b');


                    });

                    $sheet->rows($data);

                    $footer_index = count($data) + 8;


                    $sheet->row($footer_index, array('', '', '', '', $excel_data['report_brief']['total_amount'], '', $excel_data['report_brief']['total_discount'], $excel_data['report_brief']['total_amount']));

                    $sheet->row($footer_index, function ($row) {

                        $row->setFontWeight('bold');

                    });

                    $sheet->protect('password', function (\PHPExcel_Worksheet_Protection $protection) {
                        $protection->setSort(true);

                    });

                    $sheet->setHeight(3, 25);
                    $sheet->getProtection()->setPassword('password');

                });


            })->download('xls');

        } else {

            Flash::info("There is no record about your search.");
        }

        $c_page = ['main' => 'report', 'sub' => ''];
        return View('boatticket.report.index', compact('c_page', 'exce_data'));
    }

    public function getAgentReport(Request $request)
    {
        $agentList = array();
        $ownerList = array();
        $c_page = ['main' => 'report', 'sub' => ''];
        $excel_data = array();

        if (in_array(Auth::user()->user_type, array(BOAT_ADMIN, BOAT_BRANCH))) {
            // Get Operator List
            $agentList = DB::table('activated_boat_agents')
                ->select(array('users.name', 'users.agent_code', 'users.branch'))
                ->leftJoin('users', 'activated_boat_agents.agent_id', '=', 'users.id')
                ->where('boat_owner', Auth::user()->owner_code)
                ->groupBy('users.id')
                ->get();
        } else {
            $query = DB::table('activated_boat_agents')
                ->select(array('boat_owner'))
                ->where('agent_code', Auth::user()->agent_code)
                ->groupBy('boat_owner')
                ->get();
            $ownerList = array();
            foreach ($query as $row) {
                $owner = DB::table('boat_owners')
                    ->where('owner_code', $row->boat_owner)
                    ->first();
                $ownerList[] = array('name' => $owner->name, 'code' => $owner->owner_code);
            }
        }
        if ($request->isMethod('post')) {
            $filter['start_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->start_date)));
            $filter['end_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->end_date)));

            $filter['owner'] = in_array(Auth::user()->user_type, array(BOAT_AGENT)) ? $request->owner : Auth::user()->owner_code;
            $filter['agent'] = in_array(Auth::user()->user_type, array(BOAT_AGENT)) ? Auth::user()->agent_code : $request->agent;
            $excel_data = agentReport($filter);
        }
        return View('boatticket.report.agent_agent_report', compact('c_page', 'ownerList', 'agentList', 'excel_data', 'filter'));
    }

    public function donwloadAgentReport($start_date = '', $end_date = '', $owner = '', $agent = '')
    {

        $filter = array();

        $filter['start_date'] = $start_date;
        $filter['end_date'] = $end_date;
        $filter['owner'] = $owner;
        $filter['agent'] = $agent;

        $agent = User::where('agent_code', $agent)->first();

        $agent_name = $agent->name;

        $owner = User::where('owner_code', $owner)->first();

        $owner_name = $owner->name;

        $filter['agent_name'] = $agent_name;
        $filter['owner_name'] = $owner_name;

        $result = agentReport($filter);

        $pdf = PDF::loadView('boatticket.report.agent_report_pdf', compact('result', 'filter'));
        $pdf->setPaper('a4', 'landscape');

        return $pdf->stream('Agent_Report' . "-" . date('Y-m-d-H-i-s') . ".pdf");
    }
}
