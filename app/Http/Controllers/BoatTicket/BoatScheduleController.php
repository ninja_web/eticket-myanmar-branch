<?php namespace App\Http\Controllers\BoatTicket;

use App\BoatTicket\BoatClass;
use App\BoatTicket\BoatScheduleType;
use App\BoatTicket\BoatTicket;
use App\Http\Controllers\Controller;
use App\Http\Requests\BoatTicket\BoatScheduleRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class BoatScheduleController extends Controller
{

    public function index()
    {
        $filter = array();
        $list = getBoatScheduleCache();
        $query = DB::table('boat_schedules AS b_s')
            ->select(array('b_s.*', 'b_s_t.whole', 'b_c.class AS boat_class', 'b_c.logo', 'b_c.logo_path'))
            ->join('boat_class AS b_c', 'b_s.boat_class', '=', 'b_c.id', 'left')
            ->join('boat_schedule_types AS b_s_t', 'b_s.schedule_type', '=', 'b_s_t.id', 'left');
        $query->where('b_s.dept_date', date('Y-m-d'));
        $query->where('b_s.owner_code', Auth::user()->owner_code);
        // check the permission
        if (Auth::user()->branch != '') {
            $query->where('b_s.start_branch', Auth::user()->branch);
        }
        $query->orderBy('b_s.dept_date');
        $schedules = $query->get();
        $c_page = ['main' => 'schedule', 'sub' => 'list'];
        return View('boatticket.boat_schedule.index', compact('c_page', 'schedules', 'list', 'filter'));
    }

    public function postBoatScheduleSearch(\Illuminate\Http\Request $request)
    {
        $filter['start_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $request->start_date)));
        $filter['end_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->end_date)));
        $filter['destination'] = $request->destination;

        $list = getBoatScheduleCache();
        $query = DB::table('boat_schedules AS b_s')
            ->select(array('b_s.*', 'b_s_t.whole', 'b_c.class AS boat_class', 'b_c.logo', 'b_c.logo_path'))
            ->join('boat_class AS b_c', 'b_s.boat_class', '=', 'b_c.id', 'left')
            ->join('boat_schedule_types AS b_s_t', 'b_s.schedule_type', '=', 'b_s_t.id', 'left');
        $query->where('b_s.owner_code', Auth::user()->owner_code);

        if ($filter['start_date'] != '') $query->where('b_s.dept_date', '>=', $filter['start_date']);
        if ($filter['end_date'] != '') $query->where('b_s.dept_date', '<=', $filter['end_date']);
        if ($filter['destination'] != '') $query->where('b_s.schedule_type', $filter['destination']);

        // check the permission
        if (Auth::user()->branch != '') {
            $query->where('b_s.start_branch', Auth::user()->branch);
        }
        $query->orderBy('b_s.dept_date', 'b_s.dept_time');
        $schedules = $query->get();
        $c_page = ['main' => 'schedule', 'sub' => 'list'];
        return View('boatticket.boat_schedule.index', compact('c_page', 'schedules', 'list', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $schedules = getBoatScheduleCache();
        $boat_classes = getBoatClassCacheList(Auth::user()->owner_code, Auth::user()->branch);
        $c_page = ['main' => 'schedule', 'sub' => ''];
        return View('boatticket.boat_schedule.create_edit_boat_schedule', compact('c_page', 'boat_classes', 'schedules'));
    }

    public function postCreate(BoatScheduleRequest $request)
    {

        $parent_id = null;
        $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $request->start_date)));
        $end_date = $request->end_date;
        $end_date = empty($end_date) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));
        $repeat_type = $request->repeat_type;

        $repeatScheduleRange = calculateRepeatCount($start_date, $end_date, $repeat_type);


        // calculate the destination
        $scheduleType = BoatScheduleType::where('id', $request->destination)->orderBy('id', 'desc')->first();
        $price = array();
        if ($scheduleType !== null) {
            $whole = json_decode($scheduleType['whole']);

            for ($i = 0; $i < count($whole); $i++) {
                for ($j = $i + 1; $j < count($whole); $j++) {
                    $price[$whole[$i] . '_' . $whole[$j]] = Input::get($whole[$i] . "_" . $whole[$j]);
                }
            }
        }
        $selected_route = $scheduleType->start_branch . "_" . $scheduleType->end_branch;
        $selected_price = $price[$selected_route];


        // Check the boat Class
        $boatClass = BoatClass::find($request->boat_class);


        $total_seat = $boatClass->seater;

        if (!empty($end_date) && strtotime($request->end_date) > strtotime($request->start_date)) {
            // genrate the repeat sequence
            $parent_boat_schedule = array(
                'owner_code' => Auth::user()->owner_code,
                'schedule_type' => $request->destination,
                'rep_sequence' => $request->repeat_type,
                'permission' => $request->permission,
                'boat_class' => $request->boat_class,
                'boat_order' => $request->boat_order,
                'remark' => $request->remark,
                'total_seat' => $total_seat,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'dep_time' => date('H:i:s', strtotime($request->dept_time)),
                'price' => json_encode($price),
                'start_branch' => $scheduleType->start_branch,
                'end_branch' => $scheduleType->end_branch,
                'total_branch' => $schedulesType->total_branch,
                'status' => STATUS_ACTIVE,
                'created_at' => date('Y-m-d H:i:s')
            );
            $parent_id = DB::table('parent_boat_schedules')->insertGetId($parent_boat_schedule);
        }

        // calculate the block seat
        $block_seat = $request->block_seat;

        $block_seat = !empty($block_seat) ? explode(",", $block_seat) : array();

        $remain_seat = $total_seat;
        $booked_seat = 0;

        if ($boatClass->seat_plan == WITH_SEAT_PLAN) {
            if (count($block_seat)) {
                $remain_seat = $total_seat - count($block_seat);
                $booked_seat = count($block_seat);
            }
        } else {
            $remain_seat = $total_seat - $request->no_seat;
            $booked_seat = $request->no_seat;
        }

        $block_seat_layout = count($block_seat) ? implode(",", $block_seat) : '';

        foreach ($repeatScheduleRange as $each_day) {

            $boatSchedule = array(
                'schedule_type' => $request->destination,
                'owner_code' => Auth::user()->owner_code,
                'permission' => $request->permission,
                'boat_order' => $request->boat_order,
                'remark' => $request->remark,
                // schedule details
                'parent_id' => $parent_id,
                'dept_date' => $each_day,
                'dept_time' => date('H:i:s', strtotime($request->dept_time)),
                'boat_class' => $request->boat_class,
                // schedule type details
                'start_branch' => $scheduleType->start_branch,
                'end_branch' => $scheduleType->end_branch,
                'total_branch' => $scheduleType->total_branch,
                'price' => json_encode($price),

                // calculate the remain seat
                'total_seat' => $total_seat,
                'remain_seat' => $remain_seat,
                'take_seat_layout' => $block_seat_layout,
                'block_seat_layout' => $block_seat_layout,

                // Created user
                'status' => STATUS_ACTIVE,
                'created_by' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $schedule_id = DB::table('boat_schedules')->insertGetId($boatSchedule);

            // Book the schedule

            if ($boatClass->seat_plan == WITH_SEAT_PLAN) {


                if ($request->block_seat != '') {


                    $ticket = new BoatTicket();
                    // schedule details
                    $ticket->owner_code = Auth::user()->owner_code;

                    $ticket->ticket_pnr_code = generateCode(6);
                    $ticket->boat_schedule = $schedule_id;
                    // only with seat plan
                    $ticket->seat_no = $block_seat_layout;
                    // total seat
                    $ticket->total_seat = $booked_seat;
                    $ticket->type = BOOK;
                    // generate the price
                    $ticket->fare = $selected_price;
                    $route = explode("_", $request->route);
                    // Pay and Unpaid
                    $ticket->pay = STATUS_UNPAID;

                    $ticket->from = $route[0];
                    $ticket->to = $route[1];

                    // user details
                    $ticket->name = Auth::user()->name;
                    $ticket->phone_no = Auth::user()->phone;
                    $ticket->created_by = Auth::user()->id;
                    $ticket->created_type = Auth::user()->user_type;
                    $ticket->save();
                }

            } else {

                if ($request->no_seat > 0) {

                    $ticket = new BoatTicket();
                    // schedule details
                    $ticket->owner_code = Auth::user()->owner_code;

                    $ticket->ticket_pnr_code = generateCode(6);
                    $ticket->boat_schedule = $schedule_id;
                    // only with seat plan
                    $ticket->seat_no = '';
                    // total seat
                    $ticket->total_seat = $request->no_seat;
                    $ticket->type = BOOK;
                    // generate the price
                    $ticket->fare = $selected_price;
                    $route = explode("_", $request->route);
                    // Pay and Unpaid
                    $ticket->pay = STATUS_UNPAID;

                    $ticket->from = $route[0];
                    $ticket->to = $route[1];

                    // user details
                    $ticket->name = Auth::user()->name;
                    $ticket->phone_no = Auth::user()->phone;
                    $ticket->created_by = Auth::user()->id;
                    $ticket->created_type = Auth::user()->user_type;
                    $ticket->save();

                }

            }
        }

        Flash::success('You have successfully created new boat schedule.');
        return Redirect::to('boat/schedule/create/');
    }

    public function toggleSchedule(Request $request, $type = 'enable', $id = 0)
    {
        if ($request->ajax() && !empty($id) && !empty($type)) {
            DB::table('boat_schedules')
                ->where('id', $id)
                ->update(array('status' => $type == 'disable' ? STATUS_INACTIVE : STATUS_ACTIVE));
            return json_encode(array('status' => true));
        }

    }

    public function toggleClass($boat_class)
    {
        // check the input
        $boat_class = BoatClass::find($boat_class);
        return View('boatticket.boat_schedule.boat_layout', compact('boat_class'));
    }
}
