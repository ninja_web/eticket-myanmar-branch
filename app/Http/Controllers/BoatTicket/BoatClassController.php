<?php namespace App\Http\Controllers\BoatTicket;

use App;
use App\BoatTicket\BoatClass;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BoatTicket\BoatClassRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;
use PDF;

class BoatClassController extends Controller
{

    public function pdf()
    {

        $info = array();
        $data['info'] = $info;
        $pdf = PDF::loadView('reportpdf', $data);
        return $pdf->download('report.pdf');

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $boatClass = DB::table('boat_class')
            ->select('boat_class.*', 'users.name AS user_name')
            ->join('users', 'boat_class.created_by', '=', 'users.id')
            ->where('boat_class.owner_code', '=', Auth::user()->owner_code)
            ->orderBy('boat_class.created_at', 'asc')
            ->paginate(PER_PAGE);

        $c_page = ['main' => 'define', 'sub' => 'boat_class'];
        return View('boatticket.boat_class.index', compact('c_page', 'boatClass'));
    }

    public function getCreate()
    {
        $c_page = ['main' => 'define', 'sub' => 'boat_class'];
        return View('boatticket.boat_class.create_edit_boat_class', compact('c_page'));
    }

    // TODO :: check the radio button
    public function postCreate(BoatClassRequest $request, $id = 0)
    {
        $boatClass = $id > 0 ? BoatClass::find($id) : new BoatClass();
        // check the file upload
        if ($request->hasFile('logo')) {
            $upload_path = BOAT_CLASS_PATH . "/" . date("Y") . "/" . date("m") . "/";
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }

            // Read Temporary File
            $file_name = generateCode(12) . "." . $request->file('logo')->getClientOriginalExtension();
            $img = Image::make($request->file('logo'))
                ->save($upload_path . $file_name);

            $boatClass->logo = $file_name;
            $boatClass->logo_path = $upload_path;
        }

        $boatClass->class = $request->class;
        $boatClass->type = $request->type;
        $boatClass->seat_plan = $request->seat_plan;
        $boatClass->seater = $request->seat_plan == WITHOUT_SEAT_PLAN ? $request->no_seat : $request->seater;
        $boatClass->status = $request->status;
        $boatClass->owner_code = Auth::user()->owner_code;
        $boatClass->via = WEB;
        $boatClass->created_by = Auth::user()->id;
        $boatClass->save();
        if ($id > 0) {
            Flash::success('You have successfully updated boat class.');
            return Redirect::to('boat/boatClass/' . $boatClass->id . '/edit');
        } else {
            Flash::success('You have successfully created new boat class.');
            return Redirect::to('boat/boatClass/' . $boatClass->id . '/edit');
        }
    }

    public function getEdit($id)
    {
        $boatClass = BoatClass::find($id);
        $c_page = ['main' => 'boat_class', 'sub' => ''];
        return View('boatticket.boat_class.create_edit_boat_class', compact('c_page', 'boatClass'));

    }
}
