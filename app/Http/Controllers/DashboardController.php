<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // check the all bus agent
        if (in_array(Auth::user()->user_type, array(BUS_ADMIN, BUS_BRANCH, BUS_SALE, BUS_AGENT))) {
            return Redirect::to('/bus/dashboard');
        } else if (in_array(Auth::user()->user_type, array(BOAT_ADMIN, BOAT_BRANCH, BOAT_SALE, BOAT_AGENT, BOAT_FINANCE, BOAT_SUB_AGENT))) {
            return Redirect::to('/boat/dashboard');
        }
        // show the error message
    }
}
