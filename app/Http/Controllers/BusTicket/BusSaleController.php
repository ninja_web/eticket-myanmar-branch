<?php namespace App\Http\Controllers\BusTicket;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BusTicket\BusUserRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class BusSaleController extends Controller
{
    /**
     * The attributes included in the model's JSON form.
     *
     * @var array
     */
    public function _construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Builder = User::where('user_type', '=', BUS_SALE)
            ->where('owner_code', Auth::user()->owner_code);
        if (Auth::user()->user_type == BUS_BRANCH) {
            $Builder->where('branch', Auth::user()->branch);
        }
        $sale = $Builder->orderBy('created_at', 'asc')
            ->paginate(PER_PAGE);
        $c_page = ['main' => 'staff', 'sub' => 'sale'];
        return View('busticket.sale.index', compact('c_page', 'sale'));
    }

    public function getCreate()
    {
        $c_page = ['main' => 'staff', 'sub' => 'sale'];
        return View('busticket.sale.create_edit_sale', compact('c_page'));
    }

    public function postCreate(BusUserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        // Location (Yangon, Mandalay)
        $user->branch = $request->branch;
        $user->nrc = $request->nrc;
        $user->dob = date('Y-m-d', strtotime($request->dob));
        $user->parent_name = $request->parent_name;
        $user->phone = $request->phone;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->status = $request->status;

        // check in and check out time
        $user->check_in = date('H:i:00', strtotime($request->check_in));
        $user->check_out = date('H:i:00', strtotime($request->check_out));
        // Type and password
        $user->owner_code = Auth::user()->owner_code;
        $user->user_type = BUS_SALE;
        $user->password = bcrypt(DEFAULT_PASSWORD);

        // check the file upload
        if ($request->hasFile('profile')) {
            $upload_path = PROFILE_PATH . "/" . date("Y") . "/" . date("m") . "/";
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }

            // Read Temporary File
            $file_name = generateCode(12) . "." . $request->file('profile')->getClientOriginalExtension();
            $img = Image::make($request->file('profile'))
                ->save($upload_path . $file_name);
            $user->profile = $file_name;
            $user->profile_path = $upload_path;
        }

        // Create by
        $user->created_by = Auth::id();
        $user->via = WEB;
        $user->save();
        if ($user->id) {
            // success message
            Flash::success('You have successfully created new sale.');
            return Redirect::to('bus/sale/');
        }
    }

    public function getEdit($id)
    {
        $sale = User::find($id);
        $c_page = ['main' => 'staff', 'sub' => 'sale'];
        return View('busticket.sale.create_edit_sale', compact('c_page', 'sale'));
    }

    public function postEdit(BusUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;

        // Location (Yangon, Mandalay)
        $user->branch = $request->branch;
        $user->nrc = $request->nrc;
        $user->dob = date('Y-m-d', strtotime($request->dob));
        $user->parent_name = $request->parent_name;
        $user->phone = $request->phone;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->status = $request->status;

        // check in and check out time
        $user->check_in = date('H:i:00', strtotime($request->check_in));
        $user->check_out = date('H:i:00', strtotime($request->check_out));

        // check the file upload
        if ($request->hasFile('profile')) {
            $upload_path = PROFILE_PATH . "/" . date("Y") . "/" . date("m") . "/";
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }
            // check the old file
            if (!empty($user->profile) && $user->profile_path) {
                if (is_file($user->profile_path . $user->profile)) {
                    // delete the old file
                    unlink($user->profile_path . $user->profile);
                }
            }
            // Read Temporary File
            $file_name = generateCode(12) . "." . $request->file('profile')->getClientOriginalExtension();
            $img = Image::make($request->file('profile'))
                ->save($upload_path . $file_name);
            $user->profile = $file_name;
            $user->profile_path = $upload_path;
        }

        // Create by
        $user->created_by = Auth::id();
        $user->via = WEB;
        $user->save();
        if ($user->id) {
            // success message
            Flash::success('You have successfully edited new sale.');
            return Redirect::to('bus/sale/');
        }
    }

}
