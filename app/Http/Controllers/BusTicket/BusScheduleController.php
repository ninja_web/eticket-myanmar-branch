<?php namespace App\Http\Controllers\BusTicket;

use App\BusTicket\BusClass;
use App\BusTicket\BusScheduleType;
use App\BusTicket\BusTicket;
use App\Http\Controllers\Controller;
use App\Http\Requests\BusTicket\BusScheduleRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Validator;

class BusScheduleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $c_page = ['main' => 'schedule', 'sub' => ''];
        return View('busticket.bus_schedule.index', compact('c_page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $schedules = getBusScheduleCache(Auth::user()->owner_code, Auth::user()->branch);
        $bus_classes = getBusClassCacheList(Auth::user()->owner_code, Auth::user()->branch);
        $c_page = ['main' => 'schedule', 'sub' => ''];
        return View('busticket.bus_schedule.create_edit_bus_schedule', compact('c_page', 'bus_classes', 'schedules'));
    }

    public function postCreate(BusScheduleRequest $request)
    {

        if ($request->bus_order == "Bus Order") {

            return redirect('bus/schedule/create')
                ->withErrors("You need to select Bus Order.")
                ->withInput();
        }

        if ($request->bus_class == "") {

            return redirect('bus/schedule/create')
                ->withErrors("You need to select Bus Class.")
                ->withInput();
        }

        $parent_id = null;

        $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $request->start_date)));
        $end_date = $request->end_date;
        $end_date = empty($end_date) ? null : date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));

        $repeat_type = $request->repeat_type;

        $repeatScheduleRange = calculateRepeatCount($start_date, $end_date, $repeat_type);


        // calculate the destination
        $scheduleType = BusScheduleType::where('id', $request->destination)->orderBy('id', 'desc')->first();

        $price = array();

        if ($scheduleType !== null) {
            $whole = json_decode($scheduleType['whole']);
            for ($i = 0; $i < count($whole); $i++) {
                for ($j = $i + 1; $j < count($whole); $j++) {
                    $price[$whole[$i] . '_' . $whole[$j]] = Input::get($whole[$i] . "_" . $whole[$j]);
                }
            }
        }

        $selected_route = $scheduleType->start_branch . "_" . $scheduleType->end_branch;
        $selected_price = $price[$selected_route];

        // Check the bus Class
        $busClass = BusClass::find($request->bus_class);
        $total_seat = $busClass->seater;

        $parent_id = 0;

        if (!empty($end_date) && !empty($repeat_type)) {

            // genrate the repeat sequence
            $parent_bus_schedule = array(
                'owner_code' => Auth::user()->owner_code,
                'schedule_type' => $request->destination,
                'rep_sequence' => $request->repeat_type,
                'permission' => $request->permission,
                'bus_class' => $request->bus_class,
                'bus_order' => $request->bus_order,
                'total_seat' => $total_seat,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'dep_time' => date('H:i:s', strtotime($request->dept_time)),
                'price' => json_encode($price),
                'start_branch' => $scheduleType->start_branch,
                'end_branch' => $scheduleType->end_branch,
                'total_branch' => $scheduleType->total_branch,
                'status' => STATUS_ACTIVE,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $parent_id = DB::table('parent_bus_schedules')->insertGetId($parent_bus_schedule);
        }

        $block_seat = $request->block_seat;
        $block_seat = !empty($block_seat) ? explode(",", $block_seat) : array();

        $remain_seat = 0;
        $booked_seat = 0;

        if (count($block_seat)) {

            $remain_seat = $total_seat - count($block_seat);

            $booked_seat = count($block_seat);

        } else {
            $remain_seat = $total_seat;
        }

        $str = count($block_seat) ? implode(",", $block_seat) : '';

        foreach ($repeatScheduleRange as $each_day) {

            $busSchedule = array(

                'schedule_type' => $request->destination,
                'owner_code' => Auth::user()->owner_code,
                'bus_order' => $request->bus_order,
                'permission' => $request->permission,
                'parent_id' => $parent_id,
                'dept_date' => $each_day,
                'dept_time' => date('H:i:s', strtotime($request->dept_time)),
                'bus_class' => $request->bus_class,
                'start_branch' => $scheduleType->start_branch,
                'end_branch' => $scheduleType->end_branch,
                'total_branch' => $scheduleType->total_branch,
                'price' => json_encode($price),
                'take_seat_layout' => $str,
                'total_seat' => $total_seat,
                'remain_seat' => $remain_seat,
                'status' => STATUS_ACTIVE,
                'remark' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id

            );

            $schedule_id = DB::table('bus_schedules')->insertGetId($busSchedule);

            $ticket = new BusTicket();
            // schedule details
            $ticket->owner_code = Auth::user()->owner_code;
            $ticket->agent_id = Auth::user()->id;
            $ticket->ticket_pnr_code = generateCode(6);
            $ticket->bus_schedule = $schedule_id;
            $ticket->seat_no = $str;
            $ticket->total_seat = $booked_seat;
            $ticket->type = BOOK;
            $ticket->fare = $selected_price;
            $route = explode("_", $request->route);
            $ticket->from = $route[0];
            $ticket->to = $route[1];
            $ticket->passport = '';

            // user details
            $ticket->name = Auth::user()->name;
            $ticket->phone_no = Auth::user()->phone;
            $ticket->created_by = Auth::user()->id;
            $ticket->save();

        }

        Flash::success('You have successfully created new bus schedule.');

        return Redirect::to('bus/schedule/create/');
    }

    public function toggleClass($bus_class)
    {
        // check the input
        $bus_class = BusClass::find($bus_class);
        return View('busticket.bus_schedule.car_layout', compact('bus_class'));
    }


}
