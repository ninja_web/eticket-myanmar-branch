<?php namespace App\Http\Controllers\BusTicket;

use App\BusTicket\BusScheduleType;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    // TODO :: Ninja : Need to know the details dashboard for various user level
    public function index()
    {
        if (BUS_ADMIN == Auth::user()->user_type) {
            return $this->adminDashboard();
        } else if (BUS_BRANCH == Auth::user()->user_type) {
            return $this->branchDashboard();
        } else if (BUS_SALE == Auth::user()->user_type) {
            return $this->saleDashboard();
        } else if (BUS_AGENT == Auth::user()->user_type) {
            return $this->agentDashboard();
        }
    }

    // Admi Dashboard
    protected function adminDashboard()
    {
        $no_branch = User::where('user_type', BUS_BRANCH)
            ->where('owner_code', Auth::user()->owner_code)
            ->count();
        $no_sale = User::where('user_type', BUS_SALE)
            ->where('owner_code', Auth::user()->owner_code)
            ->count();
        $no_schedule = BusScheduleType::where('owner_code', Auth::user()->owner_code)->count();
        $no_agent = 0;
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('busticket.dashboard.bus_admin', compact('c_page', 'no_branch', 'no_sale', 'no_schedule', 'no_agent'));
    }

    // Branch dashboard
    public function branchDashboard()
    {
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('busticket.dashboard.bus_branch', compact('c_page', 'no_branch', 'no_sale', 'no_schedule', 'no_agent'));
    }

    public function saleDashboard()
    {
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('busticket.dashboard.bus_sale', compact('c_page'));
    }

    public function agentDashboard()
    {
        $c_page = ['main' => 'dashboard', 'sub' => ''];
        return View('busticket.dashboard.bus_agent', compact('c_page'));
    }
}
