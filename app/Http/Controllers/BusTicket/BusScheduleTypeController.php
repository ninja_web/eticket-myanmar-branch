<?php namespace App\Http\Controllers\BusTicket;

use App;
use App\BusTicket\BusScheduleType;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BusTicket\BusScheduleTypeRequest;
use App\Http\Requests\BusTicket\BusTimeSequenceRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Input;
use Laracasts\Flash\Flash;
use PDF;
use Request;

class BusScheduleTypeController extends Controller
{
    /**
     * Show the list of bus schedule type
     * @return {[type]} [description]
     */
    public function index()
    {
        $bus_schedule_type = BusScheduleType::where('owner_code', Auth::user()->owner_code)
            ->orderBy('created_at', 'asc')
            ->paginate(PER_PAGE);
        $c_page = ['main' => 'define', 'sub' => ''];
        return View('busticket.bus_schedule_type.index', compact('c_page', 'bus_schedule_type'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function getScheduleTypeCreate()
    {
        $c_page = ['main' => 'define', 'sub' => ''];
        return View('busticket.bus_schedule_type.create_edit_bus_schedule_type', compact('c_page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postScheduleTypeCreate(BusScheduleTypeRequest $request)
    {
        $busSchedule = new BusScheduleType();
        $busSchedule->total_branch = Input::get('total_route');
        $busSchedule->total_time = Input::get('hour') . ":" . Input::get('minute');
        // start and end
        $route = [];
        for ($i = 1; $i <= $request->total_route; $i++) {
            $route[] = Input::get('route_' . $i);
        }
        $busSchedule->b_points = $request->b_points;
        $busSchedule->d_points = $request->d_points;
        $busSchedule->whole = json_encode($route);
        $busSchedule->start_branch = $request->route_1;
        $busSchedule->end_branch = Input::get('route_' . $request->total_route);

        // Authorized
        $busSchedule->owner_code = Auth::user()->owner_code;
        $busSchedule->created_by = Auth::user()->id;
        $busSchedule->status = Input::get('status');
        $busSchedule->via = WEB;
        $busSchedule->save();

        if ($busSchedule->id) {
            Flash::success('You have successfully created new bus schedule type.');
            return Redirect::to('bus/scheduleType/');
        }
    }

    public function getScheduleTypeEdit($id)
    {
        $busSchedule = BusScheduleType::where('id', $id)
            ->first();
        $c_page = ['main' => 'bus_schedule', 'sub' => ''];
        return View('busticket.bus_schedule_type.create_edit_bus_schedule_type', compact('c_page', 'busSchedule'));
    }

    public function postScheduleTypeSequenceCreate(BusTimeSequenceRequest $request, $id)
    {
        $busSchedule = BusScheduleType::where('id', $id)->first();
        $time_sequence = $busSchedule->time_sequence == null ? array() : json_decode($busSchedule->time_sequence);

        // add new time sequence : check duplicate
        $time_sequence[] = date('H:i:00', strtotime($request->time_sequence));
        $busSchedule->time_sequence = json_encode($time_sequence);
        $busSchedule->status = Input::get('status');
        $busSchedule->save();
        Flash::success('You have successfully created new bus sequence.');
        return Redirect::to('bus/scheduleType/' . $id . '/edit');
    }

    public function getScheduleTypeStatusToggle($id)
    {
        if (Request::ajax() && !empty($id)) {
            $schedule = BusScheduleType::find($id);
            if (null == $schedule) {
                return json_encode(array('status' => false));
            }
            $schedule->status = $schedule->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
            $schedule->save();
            return json_encode(array('status' => true));
        }
    }

    public function destoryTimeSequence($id, $time)
    {
        if (Request::ajax() && !empty($id) && !empty($time)) {
            $schedule = BusScheduleType::find($id);
            if (null == $schedule) {
                return json_encode(array('status' => false));
            }

            $time_sequence = json_decode($schedule->time_sequence, true);
            $time = date("H:i:00", $time);
            if (($key = array_search($time, $time_sequence)) !== false) {
                unset($time_sequence[$key]);
                $schedule->time_sequence = count($time_sequence) ? json_encode($time_sequence) : '';
                $schedule->save();
                return json_encode(array('status' => true));
            } else {

                return json_encode(array('status' => false, 'message' => 'You can not delete.Some transaction are using this time.'));
            }
        }
    }
}
