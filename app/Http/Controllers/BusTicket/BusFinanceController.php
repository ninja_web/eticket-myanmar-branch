<?php namespace App\Http\Controllers\BusTicket;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BusTicket\BusUserRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class BusFinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Builder = User::where('user_type', '=', BUS_FINANCE)
            ->where('owner_code', Auth::user()->owner_code);
        if (BUS_BRANCH == Auth::user()->user_type) {
            $Builder->where('branch', Auth::user()->branch);
        }
        $finance = $Builder->orderBy('created_at', 'asc')->paginate(PER_PAGE);
        $c_page = ['main' => 'staff', 'sub' => 'finance'];
        return View('busticket.finance.index', compact('c_page', 'finance'));
    }

    public function getCreate()
    {
        $c_page = ['main' => 'staff', 'sub' => 'finance'];
        return View('busticket.finance.create_edit_finance', compact('c_page'));
    }

    public function postCreate(BusUserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        // Location (Yangon, Mandalay)
        $user->branch = $request->branch;
        $user->nrc = $request->nrc;
        $user->dob = date('Y-m-d', strtotime($request->dob));
        $user->parent_name = $request->parent_name;
        $user->phone = $request->phone;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->status = $request->status;

        // check the file upload
        if ($request->hasFile('profile')) {
            $upload_path = PROFILE_PATH . "/" . date("Y") . "/" . date("m") . "/";
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }

            // Read Temporary File
            $file_name = generateCode(12) . "." . $request->file('profile')->getClientOriginalExtension();
            $img = Image::make($request->file('profile'))
                ->save($upload_path . $file_name);
            $user->profile = $file_name;
            $user->profile_path = $upload_path;
        }

        // check in and check out time
        $user->check_in = date('H:i:00', strtotime($request->check_in));
        $user->check_out = date('H:i:00', strtotime($request->check_out));

        // Type and password
        $user->owner_code = Auth::user()->owner_code;
        $user->user_type = BUS_FINANCE;
        $user->password = bcrypt(DEFAULT_PASSWORD);

        // Create by
        $user->created_by = Auth::id();
        $user->via = WEB;
        $user->save();
        if ($user->id) {
            // success message
            Flash::success('You have successfully created new finance.');
            return Redirect::to('bus/finance/');
        }

    }

    public function getEdit($id)
    {
        $finance = User::find($id);
        $c_page = ['main' => 'staff', 'sub' => 'finance'];
        return View('busticket.finance.create_edit_finance', compact('c_page', 'finance'));
    }

    public function postEdit(BusUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;

        // Location (Yangon, Mandalay)
        $user->branch = $request->branch;
        $user->nrc = $request->nrc;
        $user->dob = date('Y-m-d', strtotime($request->dob));
        $user->parent_name = $request->parent_name;
        $user->phone = $request->phone;
        $user->contact = $request->contact;
        $user->address = $request->address;
        $user->status = $request->status;

        // check the file upload
        if ($request->hasFile('profile')) {
            $upload_path = PROFILE_PATH . "/" . date("Y") . "/" . date("m") . "/";
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }

            // Read Temporary File
            $file_name = generateCode(12) . "." . $request->file('profile')->getClientOriginalExtension();
            $img = Image::make($request->file('profile'))
                ->save($upload_path . $file_name);
            $user->profile = $file_name;
            $user->profile_path = $upload_path;
        }

        // check in and check out time
        $user->check_in = date('H:i:00', strtotime($request->check_in));
        $user->check_out = date('H:i:00', strtotime($request->check_out));

        // Create by
        $user->created_by = Auth::id();
        $user->via = WEB;
        $user->save();
        if ($user->id) {
            // success message
            Flash::success('You have successfully edited new sale.');
            return Redirect::to('bus/finance/');
        }
    }

}
