<?php namespace App\Http\Controllers\BusTicket;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class BusController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $c_page = ['main' => 'bus', 'sub' => ''];
        return View('busticket.bus.index', compact('c_page'));
    }

    public function getCreate($bus_id = 0)
    {
        $c_page = ['main' => 'bus', 'sub' => ''];
        return View('busticket.bus.create_edit_bus', compact('c_page'));
    }

    public function postCreate($bus_id = 0)
    {
        # code...
    }

}
