<?php namespace App\Http\Controllers\BusTicket;

use App\BusTicket\BusClass;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BusTicket\BusClassRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class BusClassController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $busClass = DB::table('bus_class')
            ->select('bus_class.*', 'users.name AS user_name')
            ->join('users', 'bus_class.created_by', '=', 'users.id')
            ->where('bus_class.owner_code', '=', Auth::user()->owner_code)
            ->orderBy('bus_class.created_at', 'asc')
            ->paginate(PER_PAGE);

        $c_page = ['main' => 'bus_class', 'sub' => ''];
        return View('busticket.bus_class.index', compact('c_page', 'busClass'));
    }

    public function getCreate()
    {
        $c_page = ['main' => 'bus_class', 'sub' => ''];
        return View('busticket.bus_class.create_edit_bus_class', compact('c_page'));

    }

    public function postCreate(BusClassRequest $request)
    {
        $busClass = new BusClass();
        // check the file upload
        if ($request->hasFile('logo')) {
            $upload_path = BUS_CLASS_PATH . "/" . date("Y") . "/" . date("m") . "/";
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0755, true);
                file_put_contents($upload_path . "/index.html", "");
            }

            // Read Temporary File
            $file_name = generateCode(12) . "." . $request->file('logo')->getClientOriginalExtension();
            $img = Image::make($request->file('logo'))
                ->save($upload_path . $file_name);
            $busClass->logo = $file_name;
            $busClass->logo_path = $upload_path;
        }

        $busClass->class = $request->class;
        $busClass->seater = $request->seater;
        $busClass->status = $request->status;
        $busClass->owner_code = Auth::user()->owner_code;
        $busClass->via = WEB;
        $busClass->created_by = Auth::user()->id;
        $busClass->save();
        if ($busClass->id) {
            Flash::success('You have successfully created new bus class.');
            return Redirect::to('bus/busClass/' . $busClass->id . '/edit');
        }
    }

    public function getEdit($id)
    {
        $busClass = BusClass::find($id);
        $c_page = ['main' => 'bus_class', 'sub' => ''];
        return View('busticket.bus_class.create_edit_bus_class', compact('c_page', 'busClass'));

    }

    public function postEdit(BusClassRequest $request, $id)
    {

        $busClass = BusClass::find($id);
        $busClass->class = $request->class;
        $busClass->seater = $request->seater;
        $busClass->status = $request->status;
        $busClass->via = WEB;
        $busClass->save();
        if ($busClass->id) {
            Flash::success('You have successfully updated bus class.');
            return Redirect::to('bus/busClass/' . $busClass->id . '/edit');
        }

    }


}
