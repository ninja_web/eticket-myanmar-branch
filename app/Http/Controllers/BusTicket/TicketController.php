<?php namespace App\Http\Controllers\BusTicket;

use App\BusTicket\BusSchedule;
use App\BusTicket\BusScheduleType;
use App\BusTicket\BusTicket;
use App\Http\Controllers\Controller;
use App\Http\Requests\BusTicket\PassengerRequest;
use App\Http\Requests\BusTicket\SelectTicketRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Laracasts\Flash\Flash;

class TicketController extends Controller
{

    public function index($query_id = 0)
    {
        $c_page = ['main' => 'ticket', 'sub' => ''];
        if (Auth::user()->user_type == BUS_ADMIN) {
            $list = BusScheduleType::where('owner_code', Auth::user()->owner_code)->get();
        } else {
            $list = BusScheduleType::where('owner_code', Auth::user()->owner_code)
                ->where('start_branch', Auth::user()->branch)->get();
        }
        return view('busticket.ticket.route_list', compact('c_page', 'list'));
    }


    public function route($id = 0, $query_id = 0)
    {
        // check the query id

        $info = BusScheduleType::find($id);

        loadQuery($query_id);

        $query_array = array(

            'from' => Input::get('from', ''),
            'to' => Input::get('to', ''),
            'bus_schedule' => Input::get('bus_schedule', '')

        );
        // request the param
        $query = DB::table('bus_schedules AS b_s')
            ->select(array('b_s.*', 'b_s_t.whole', 'b_c.class AS bus_class', 'b_c.logo', 'b_c.logo_path'))
            ->join('bus_class AS b_c', 'b_s.bus_class', '=', 'b_c.id', 'left')
            ->join('bus_schedule_types AS b_s_t', 'b_s.schedule_type', '=', 'b_s_t.id', 'left')
            ->where('b_s.owner_code', Auth::user()->owner_code);

        if (in_array(Auth::user()->user_type, array(BUS_BRANCH, BUS_SALE))) {

            $query->where('b_s.start_branch', Auth::user()->branch);

        }
        // check the query string
        if (!empty($query_array['from']) && !empty($query_array['to'])) {

            $query->where('b_s.dept_date', '>=', date("Y-m-d", strtotime(str_replace('/', '-', $query_array['from']))));
            $query->where('b_s.dept_date', '<=', date("Y-m-d", strtotime(str_replace('/', '-', $query_array['to']))));

        } else if (!empty($query_array['from'])) {

            $query->where('b_s.dept_date', '=', date("Y-m-d", strtotime(str_replace('/', '-', $query_array['from']))));

        } else {

            $query->where('b_s.dept_date', date('Y-m-d'));
        }

        if (!empty($query_array['bus_schedule'])) {

            $query->where('b_s.schedule_type', $query_array['bus_schedule']);
        }

        $query->where('b_s.schedule_type', $id);

        $schedules = $query->paginate();


        $scheduleTypes = getBusScheduleCache(Auth::user()->owner_code, Auth::user()->branch);
        $busClasses = getBusClassCacheList(Auth::user()->owner_code, Auth::user()->branch);
        $c_page = ['main' => 'ticket', 'sub' => ''];

        return View('busticket.ticket.index', compact('info', 'c_page', 'schedules', 'scheduleTypes', 'busClasses', 'id'));
    }

    public function search(Request $request)
    {
        $query_array = array(
            'from' => Input::get('from', ''),
            'to' => Input::get('to', ''),
            'bus_schdule' => Input::get('bus_schdule', '')
        );
        // check the input
        $query_id = saveQuery($query_array);
        $id = Input::get('id', '');

        return Redirect::to('bus/ticket/route/' . $id . "/" . $query_id);

    }

    /**
     * Select the Bus Schedule and Find the bus scheudle type
     **/

    public function getTicket($schedule_id = 0)
    {
        if ($schedule_id == 0 || !ctype_digit($schedule_id)) {
            // browse error
            Flash::info("Please select schedule and action");
            return Redirect::to('bus/schedule/create/');
        }
        $schedule = BusSchedule::select(array('bus_schedules.*', 'b_c.class', 'b_c.seater as seat_plan', 'b_s_t.whole'))
            ->join('bus_schedule_types AS b_s_t', 'bus_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('bus_class AS b_c', 'bus_schedules.bus_class', '=', 'b_c.id', 'left')
            ->where('bus_schedules.id', $schedule_id)
            ->first();
        if (!count($schedule)) {
            Flash::info("Please select schedule and action");
            return Redirect::to('bus/schedule/create/');
        }
        $c_page = ['main' => 'ticket', 'sub' => ''];
        return View('busticket.ticket.select_ticket', compact('c_page', 'schedule'));
    }

    /**
     * Browse Schedule with 3 Types
     * (1) BUY
     * (2) CANCEL
     * (3) BOOK
     **/

    public function browseSchedule(SelectTicketRequest $request, $schedule_id = 0)
    {
        $schedule = BusSchedule::select(array('bus_schedules.*', 'b_c.class', 'b_c.seater as seat_plan', 'b_s_t.whole'))
            ->join('bus_schedule_types AS b_s_t', 'bus_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('bus_class AS b_c', 'bus_schedules.bus_class', '=', 'b_c.id', 'left')
            ->where('bus_schedules.id', $schedule_id)
            ->first();

        if (!count($schedule)) {
            Flash::info("Please select schedule and action");
            return Redirect::to('bus/schedule/create/');
        }

        $seat_no = $request->seat_no;
        if (!empty($seat_no)) {
            $seat_no = explode(",", $request->seat_no);
            $old_take_seat_layout = $schedule->take_seat_layout;
            $old_take_seat_layout = !empty($old_take_seat_layout) ? explode(",", $old_take_seat_layout) : array();
            $old_take_seat_layout = array_merge($old_take_seat_layout, $seat_no);
        }

        // substract remain seat
        $schedule->remain_seat = $schedule->total_seat - count($old_take_seat_layout);
        sort($old_take_seat_layout); // sort the seater
        $schedule->take_seat_layout = implode(",", $old_take_seat_layout);
        // upate the schedule
        $schedule->save();

        // check the ticket
        $ticket = new BusTicket();
        // schedule details
        $ticket->ticket_pnr_code = generateCode(6);
        $ticket->owner_code = Auth::user()->owner_code;
        $ticket->bus_schedule = $schedule_id;
        $ticket->seat_no = $request->seat_no;
        // total seat
        $ticket->total_seat = count($seat_no);

        $whole = json_decode($schedule->whole, true);
        $price = json_decode($schedule->price, true);
        $ticket->type = 'Book Ticket' == $request->btn_type ? BOOK : BUY;
        // generate the price
        $ticket->fare = $price[$request->selected_route];
        $route = explode("_", $request->selected_route);

        $ticket->from = $route[0];
        $ticket->to = $route[1];

        // user details
        $ticket->name = Auth::user()->name;
        $ticket->phone_no = Auth::user()->phone;
        $ticket->passport = '';
        $ticket->created_by = Auth::user()->id;
        $ticket->save();

        if ($ticket->id) {
            // Get the request type
            if ('Book Ticket' == $request->btn_type) {
                Flash::success('You have successfully book these ticket with owner name');
            } else {
                Flash::success('You have successfully buy these ticket with owner name');
            }
            return Redirect::to('/bus/ticket/passenger/' . $ticket->id);
        }
        // show error page
        Flash::error('Error Occured. Please try again');
        return Redirect::to('/bus/ticket/select/' . $schedule_id);
    }

    public function getPassengerDetails($ticket_id = 0)
    {
        // get the ticket details
        $ticket = BusTicket::find($ticket_id);

        // update on bus schedule
        $schedule = BusSchedule::select(array('bus_schedules.*', 'b_c.class', 'b_c.seater as seat_plan', 'b_s_t.whole'))
            ->join('bus_schedule_types AS b_s_t', 'bus_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('bus_class AS b_c', 'bus_schedules.bus_class', '=', 'b_c.id', 'left')
            ->where('bus_schedules.id', $ticket->bus_schedule)
            ->first();

        $agent = array();
        if (BOOK == $ticket->type) {
            // get all agent list by branch
            $agent = DB::table('activated_bus_agents as abs')
                ->select(array('users.id', 'users.name'))
                ->join('users', 'abs.agent_id', '=', 'users.id')
                ->where('abs.bus_owner', Auth::user()->owner_code)
                ->where('users.user_type', '=', BUS_AGENT)
                ->where('abs.branch', '=', $schedule->start_branch)
                ->get();
        }
        $c_page = ['main' => 'ticket', 'sub' => ''];
        return View('busticket.ticket.passenger_details', compact('c_page', 'schedule', 'ticket', 'agent'));
    }

    public function postPassengerDetails(PassengerRequest $request, $ticket_id)
    {
        $ticket = BusTicket::find($ticket_id);
        // user details
        $ticket->name_prefix = $request->name_prefix;
        $ticket->name = $request->name;
        $ticket->email = $request->email;
        // Nrc
        $ticket->nrc_prefix = $request->nrc_prefix;
        $ticket->nrc_state = $request->nrc_state;
        $ticket->nrc_nation = $request->nrc_nation;
        $ticket->nrc_no = $request->nrc_no;
        $ticket->phone_no = $request->phone_no;
        $ticket->remark = $request->remark;

        if (BOOK == $ticket->type) {
            // check the agent id
            $agent_id = $request->agent;
            if ($agent_id > 0) $ticket->agent_id = $agent_id;
        } else {
            $ticket->passport = $request->passport;
        }

        // discount amount
        $ticket->discount = $request->discount == '' ? 0 : $request->discount;
        $ticket->save();

        if ($ticket->id) {

            Flash::success('Yout have successfully update the ticket passenger details');
            return Redirect::to('bus/ticket/select/' . $ticket->bus_schedule);
        }
        // show the error
        Flash::error('Error Occured.Please try again.');
        return Redirect::to('bus/ticket/browse/' . $ticket->bus_schedule)->withInput();
    }

    public function cancelTicket($cancel_id, $cancelType = STATUS_TICKET_CANCEL)
    {
        if (!in_array($cancelType, array(STATUS_TICKET_CANCEL, STATUS_BOOK_CANCEL))) {
            return json_encode(array('status' => false));
        }

        if (Request::ajax() && !empty($cancel_id)) {
            $ticket = BusTicket::find($cancel_id);
            if (null == $ticket) {
                return json_encode(array('status' => false));
            }
            $seat_no = explode(",", $ticket->seat_no);
            // get the bus schedule details
            //  dd($ticket);
            $busSchedule = BusSchedule::find($ticket->bus_schedule);
            // check the bus Schedule
            if (null == $busSchedule) {
                return json_encode(array('status' => false));
            }
            // check the cancel type
            if (STATUS_BOOK_CANCEL == $cancelType) {
                // booking ticket cancel
                $book_seat = explode(",", $busSchedule->book_seat_layout);
                $book_seat = implode(",", array_diff($book_seat, $seat_no));
                // update on busSchedule
                $busSchedule->book_seat_layout = $book_seat;
            }
            $take_seat_layout = explode(",", $busSchedule->take_seat_layout);
            $take_seat_layout = implode(",", array_diff($take_seat_layout, $seat_no));
            // update on busSchedule
            $busSchedule->take_seat_layout = $take_seat_layout;

            // update on remaining seat
            $busSchedule->remain_seat += count($seat_no);

            // delete the ticket
            $ticket->delete();
            // update on bus Schedule
            $busSchedule->save();

            Flash::success('You have successfully cancel these ticket.');
            return json_encode(array('status' => 'true'));
        }
        return json_encode(array('status' => false));
    }

    // activate the ticket of booking records
    public function activateTicket($ticket_id = 0)
    {
        if (Request::ajax() && !empty($ticket_id)) {
            // get the ticket id
            $ticket = BusTicket::find($ticket_id);
            if (null == $ticket) {
                return json_encode(array('status' => false));
            }
            $busSchedule = BusSchedule::find($ticket->bus_schedule);
            if (null == $busSchedule) {
                return json_encode(array('status' => false));
            }

            $seat_no = explode(",", $ticket->seat_no);

            $book_seat = explode(",", $busSchedule->book_seat_layout);
            $book_seat = implode(",", array_diff($book_seat, $seat_no));
            // update on busSchedule
            $busSchedule->book_seat_layout = $book_seat;

            // update the ticket
            $ticket->type = BUY;
            $ticket->save();

            $busSchedule->save();
            Flash::success('You have successfully buy these ticket.');
            return json_encode(array('status' => true));
        }
        return json_encode(array('status' => false));
    }

    public function orderDetails($schedule_id = 0)
    {
        if (0 == $schedule_id) {
            return;
        }
        // get order details by ticket
        $schedule = BusSchedule::select(array('bus_schedules.*', 'b_c.class', 'b_c.seater as seat_plan', 'b_s_t.whole'))
            ->join('bus_schedule_types AS b_s_t', 'bus_schedules.schedule_type', '=', 'b_s_t.id', 'left')
            ->join('bus_class AS b_c', 'bus_schedules.bus_class', '=', 'b_c.id', 'left')
            ->where('bus_schedules.id', $schedule_id)
            ->first();
        $tickets = BusTicket::select(array('bus_tickets.*', 'users.name as agent'))
            ->join('users', 'bus_tickets.agent_id', '=', 'users.id', 'left')
            ->where('bus_tickets.owner_code', Auth::user()->owner_code)
            ->where('bus_tickets.bus_schedule', $schedule_id)
            ->get();
        $c_page = ['main' => 'ticket', 'sub' => ''];
        return View('busticket.ticket.order_details', compact('c_page', 'schedule', 'tickets'));
    }
}
