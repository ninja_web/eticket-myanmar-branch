<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
DB::enableQueryLog();

Route::get('/design', function () {
    return View('design');
});
Route::get('/googleb3e20d72b90ed952.html', function () {
    return View('seo');
});


Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index');
});

/**
 *  Bus Ticket Project
 **/
if (Request::is('bus/*')) {
    require __DIR__ . '/routes/bus_routes.php';
}
/**
 * Board Ticket Project
 **/
if (Request::is('boat/*')) {
    require __DIR__ . "/routes/boat_routes.php";
}

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
