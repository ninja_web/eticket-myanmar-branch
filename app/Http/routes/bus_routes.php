<?php
Route::group(['prefix' => 'bus', 'middleware' => 'auth'], function () {
    Route::pattern('id', '[0-9]+');
    Route::pattern('status', '[0-9]+');
    Route::pattern('code', '[A-Za-z0-9]+');
    Route::pattern('agent_code', '[A-Za-z0-9]+');
    Route::pattern('type', '[a-z]+');

    Route::get('dashboard', 'BusTicket\DashboardController@index');

    // Bus Schedule
    Route::get('scheduleType', 'BusTicket\BusScheduleTypeController@index');
    Route::get('scheduleType/create', 'BusTicket\BusScheduleTypeController@getScheduleTypeCreate');
    Route::post('scheduleType/create', 'BusTicket\BusScheduleTypeController@postScheduleTypeCreate');
    Route::get('scheduleType/{id}/edit', 'BusTicket\BusScheduleTypeController@getScheduleTypeEdit');
    Route::get('scheduleType/{id}/status', 'BusTicket\BusScheduleTypeController@getScheduleTypeStatusToggle');
    Route::post('scheduleType/{id}/edit', 'BusTicket\BusScheduleTypeController@postScheduleTypeEdit');
    Route::post('scheduleType/sequence/{id}', 'BusTicket\BusScheduleTypeController@postScheduleTypeSequenceCreate');
    Route::get('scheduleType/sequence/delete/{id}/{time}', 'BusTicket\BusScheduleTypeController@destoryTimeSequence');

    // Bus Types
    Route::get('busClass', 'BusTicket\BusClassController@index');
    Route::get('busClass/create', 'BusTicket\BusClassController@getCreate');
    Route::post('busClass/create', 'BusTicket\BusClassController@postCreate');
    Route::get('busClass/{id}/edit', 'BusTicket\BusClassController@getEdit');
    Route::post('busClass/{id}/edit', 'BusTicket\BusClassController@postEdit');

    // Schedule
    Route::get('schedule', 'BusTicket\BusScheduleController@index');
    Route::get('schedule/create', 'BusTicket\BusScheduleController@getCreate');
    Route::post('schedule/create', 'BusTicket\BusScheduleController@postCreate');
    Route::get('schedule/create/toggleClass/{id}', 'BusTicket\BusScheduleController@toggleClass');


    // Bus Agent
    Route::get('agent/{type?}', 'BusTicket\BusAgentController@index');
    Route::get('agent/activateAgent/{code}', 'BusTicket\BusAgentController@activateAgent');
    Route::get('agent/commission/{code}/{agent_code}', 'BusTicket\BusAgentController@getCommission');
    Route::post('agent/commission/{code}/{agent_code}', 'BusTicket\BusAgentController@postCommission');

    // Bus Branch
    Route::get('branch', 'BusTicket\BusBranchController@index');
    Route::get('branch/create', 'BusTicket\BusBranchController@getCreate');
    Route::post('branch/create', 'BusTicket\BusBranchController@postCreate');
    Route::get('branch/{id}/edit', 'BusTicket\BusBranchController@getEdit');
    Route::post('branch/{id}/edit', 'BusTicket\BusBranchController@postEdit');
    Route::get('branch/sale/{id}', 'BusTicket\BusBranchController@sale');
    Route::get('branch/finance/{id}', 'BusTicket\BusBranchController@finance');


    // Bus Staff
    Route::get('sale', 'BusTicket\BusSaleController@index');
    Route::get('sale/create', 'BusTicket\BusSaleController@getCreate');
    Route::post('sale/create', 'BusTicket\BusSaleController@postCreate');
    Route::get('sale/{id}/edit', 'BusTicket\BusSaleController@getEdit');
    Route::post('sale/{id}/edit', 'BusTicket\BusSaleController@postEdit');

    // Finance
    Route::get('finance', 'BusTicket\BusFinanceController@index');
    Route::get('finance/create', 'BusTicket\BusFinanceController@getCreate');
    Route::post('finance/create', 'BusTicket\BusFinanceController@postCreate');
    Route::get('finance/{id}/edit', 'BusTicket\BusFinanceController@getEdit');
    Route::post('finance/{id}/edit', 'BusTicket\BusFinanceController@postEdit');

    // ticket
    Route::get('ticket/{id?}', 'BusTicket\TicketController@index');
    Route::get('ticket/route/{id}/{query_id}', 'BusTicket\TicketController@route');
    Route::post('ticket/search', 'BusTicket\TicketController@search');
    Route::get('ticket/select/{id}', 'BusTicket\TicketController@getTicket');

    Route::post('ticket/browse/{id}', 'BusTicket\TicketController@browseSchedule');
    Route::get('ticket/passenger/{id}', 'BusTicket\TicketController@getPassengerDetails');
    Route::post('ticket/passenger/{id}', 'BusTicket\TicketController@postPassengerDetails');

    Route::get('ticket/cancel/{id}/{status}', 'BusTicket\TicketController@cancelTicket');
    Route::get('ticket/activate/{id}', 'BusTicket\TicketController@activateTicket');
    Route::get('ticket/orders/{id}', 'BusTicket\TicketController@orderDetails');

    // Bus
    Route::get('index', 'BusTicket\BusController@index');
    Route::get('create/{id?}', 'BusTicket\BusController@getCreate');
    Route::post('create/{id?}', 'BusTicket\BusController@postCreate');
});
