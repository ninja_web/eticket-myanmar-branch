<?php
Route::group(['prefix' => 'boat', 'middleware' => 'auth'], function () {
    Route::pattern('id', '[0-9]+');
    Route::pattern('time', '[0-9]+');
    Route::pattern('status', '[0-9]+');
    Route::pattern('code', '[A-Za-z0-9]+');
    Route::pattern('agent_code', '[A-Za-z0-9]+');
    Route::pattern('type', '[a-z]+');

    Route::get('dashboard', 'BoatTicket\DashboardController@index');

    Route::get('scheduleType', 'BoatTicket\BoatScheduleTypeController@index');
    Route::get('scheduleType/create', 'BoatTicket\BoatScheduleTypeController@getScheduleTypeCreate');
    Route::post('scheduleType/create', 'BoatTicket\BoatScheduleTypeController@postScheduleTypeCreate');
    Route::get('scheduleType/{id}/edit', 'BoatTicket\BoatScheduleTypeController@getScheduleTypeEdit');
    Route::get('scheduleType/{id}/status', 'BoatTicket\BoatScheduleTypeController@getScheduleTypeStatusToggle');
    Route::post('scheduleType/{id}/edit', 'BoatTicket\BoatScheduleTypeController@postScheduleTypeEdit');
    Route::post('scheduleType/sequence/{id}', 'BoatTicket\BoatScheduleTypeController@postScheduleTypeSequenceCreate');
    Route::get('scheduleType/sequence/delete/{id}/{time}', 'BoatTicket\BoatScheduleTypeController@destoryTimeSequence');


    // Boat Types
    Route::get('boatClass/pdf', 'BoatTicket\BoatClassController@pdf');
    Route::get('boatClass', 'BoatTicket\BoatClassController@index');
    Route::get('boatClass/create', 'BoatTicket\BoatClassController@getCreate');
    Route::post('boatClass/create', 'BoatTicket\BoatClassController@postCreate');
    Route::get('boatClass/{id}/edit', 'BoatTicket\BoatClassController@getEdit');
    Route::post('boatClass/{id}/edit', 'BoatTicket\BoatClassController@postCreate');

    // Schedule
    Route::get('schedule', 'BoatTicket\BoatScheduleController@index');

    Route::get('schedule/search', 'BoatTicket\BoatScheduleController@index');
    Route::post('schedule/search', 'BoatTicket\BoatScheduleController@postBoatScheduleSearch');

    Route::get('schedule/create', 'BoatTicket\BoatScheduleController@getCreate');
    Route::post('schedule/create', 'BoatTicket\BoatScheduleController@postCreate');
    Route::get('schedule/toggle/{type}/{id}', 'BoatTicket\BoatScheduleController@toggleSchedule');
    Route::get('schedule/create/toggleClass/{id}', 'BoatTicket\BoatScheduleController@toggleClass');

    // Boat Branch
    Route::get('branch', 'BoatTicket\BoatBranchController@index');
    Route::get('branch/create', 'BoatTicket\BoatBranchController@getCreate');
    Route::post('branch/create', 'BoatTicket\BoatBranchController@postCreate');
    Route::get('branch/{id}/edit', 'BoatTicket\BoatBranchController@getEdit');
    Route::post('branch/{id}/edit', 'BoatTicket\BoatBranchController@postEdit');
    Route::get('branch/sale/{id}', 'BoatTicket\BoatBranchController@sale');
    Route::get('branch/finance/{id}', 'BoatTicket\BoatBranchController@finance');


    // Boat Staff
    Route::get('sale', 'BoatTicket\BoatSaleController@index');
    Route::get('sale/create', 'BoatTicket\BoatSaleController@getCreate');
    Route::post('sale/create', 'BoatTicket\BoatSaleController@postCreate');
    Route::get('sale/{id}/edit', 'BoatTicket\BoatSaleController@getEdit');
    Route::post('sale/{id}/edit', 'BoatTicket\BoatSaleController@postEdit');

    // Finance
    Route::get('finance', 'BoatTicket\BoatFinanceController@index');
    Route::get('finance/create', 'BoatTicket\BoatFinanceController@getCreate');
    Route::post('finance/create', 'BoatTicket\BoatFinanceController@postCreate');
    Route::get('finance/{id}/edit', 'BoatTicket\BoatFinanceController@getEdit');
    Route::post('finance/{id}/edit', 'BoatTicket\BoatFinanceController@postEdit');

    // route
    Route::get('route/search/{id}/{time?}', 'BoatTicket\TicketController@searchRouteByDay');
    Route::post('route/search', 'BoatTicket\TicketController@searchRouteByDay');
    Route::get('route/traverse/{id}/{time}', 'BoatTicket\TicketController@route');

    // ticket
    Route::get('ticket/{id?}', 'BoatTicket\TicketController@index');
    Route::post('ticket/search/{id?}', 'BoatTicket\TicketController@search');
    Route::get('ticket/agent/search/{agent_code}', 'BoatTicket\TicketController@searchAgnetLine');
    Route::get('ticket/select/{id}', 'BoatTicket\TicketController@getTicket');
    Route::get('ticket/traverse/{id}/{time}/{status?}', 'BoatTicket\TicketController@getTraverseTicket');

    // Check In
    Route::get('ticket/checkin/', 'BoatTicket\CheckInController@index');
    Route::post('ticket/checkin/', 'BoatTicket\CheckInController@serachCheckInTicket');
    Route::get('ticket/modify/{id}', 'BoatTicket\CheckInController@getModifyTicket');
    Route::post('ticket/modify/{id}', 'BoatTicket\CheckInController@postModifyTicket');

    Route::post('ticket/browse/{id}', 'BoatTicket\TicketController@browseSchedule');
    Route::get('ticket/passenger/{id}', 'BoatTicket\TicketController@getPassengerDetails');
    Route::post('ticket/passenger/{id}', 'BoatTicket\TicketController@postPassengerDetails');
    Route::get('ticket/details/{id}', 'BoatTicket\TicketController@getTicketDetails');
    Route::get('ticket/print/{id}', 'BoatTicket\TicketController@getTicketPrint');

    Route::post('ticket/sendmail/{id}', 'BoatTicket\TicketController@getSendMail');

    Route::get('ticket/details/download/{id}', 'BoatTicket\TicketController@getTicketDetailsFile');

    Route::get('ticket/cancel/{id}/{status}', 'BoatTicket\TicketController@cancelTicket');
    Route::get('ticket/activate/{id}', 'BoatTicket\TicketController@activateTicket');
    Route::get('ticket/orders/{id}', 'BoatTicket\TicketController@orderDetails');

    Route::get('ticket/filedownload/{id}', 'BoatTicket\TicketController@getFileDownload');

    // Manage Ticket
    Route::get('ticket/manage/{id?}', 'BoatTicket\ManageTicketController@index');

    Route::get('ticket/manage/ticketlist', 'BoatTicket\ManageTicketController@getTicketList');
    Route::post('ticket/manage/{id?}', 'BoatTicket\ManageTicketController@postManageTicket');

    // Boat
    Route::get('index', 'BoatTicket\BoatController@index');
    Route::get('create/{id?}', 'BoatTicket\BoatController@getCreate');
    Route::post('create/{id?}', 'BoatTicket\BoatController@postCreate');

    // TODO : Project : Add the Boat Agent Routes (eg: Boat Line, Sub Agent)
    Route::get('agent/{type?}', 'BoatTicket\BoatAgentController@index');
    Route::get('agent/activateAgent/{code}', 'BoatTicket\BoatAgentController@activateAgent');
    Route::get('agent/commission/{code}/{agent_code}', 'BoatTicket\BoatAgentController@getCommission');
    Route::post('agent/commission/{code}/{agent_code}', 'BoatTicket\BoatAgentController@postCommission');

    Route::get('sub_agent/', 'BoatTicket\BoatSubAgentController@index');
    Route::get('sub_agent/create/{id?}', 'BoatTicket\BoatSubAgentController@getCreate');
    Route::get('sub_agent/{id}/edit', 'BoatTicket\BoatSubAgentController@getEdit');
    Route::post('sub_agent/{id}/edit', 'BoatTicket\BoatSubAgentController@postEdit');
    Route::post('sub_agent/create', 'BoatTicket\BoatSubAgentController@postCreate');


    // Report
    Route::get('report', 'BoatTicket\ReportController@index');
    Route::get('report/{type}', 'BoatTicket\ReportController@index');
    Route::post('/report/main/schedule', 'BoatTicket\ReportController@scheduleList');
    // Route::post('report/daily', 'BoatTicket\ReportController@dailySaleReport');
    Route::post('report/daily', 'BoatTicket\ReportController@searchDailyReport');
    Route::get('report/main/{id}', 'BoatTicket\ReportController@mainSaleReport');
    Route::get('report/generate', 'BoatTicket\ReportController@generateReport');

    Route::get('report/{report_date}/{report_type}', 'BoatTicket\ReportController@dailySaleReport');
    Route::get('agent_report/{start_date}/{end_date}/{owner}/{agent}', 'BoatTicket\ReportController@donwloadAgentReport');

    Route::get('report/agent_rep', 'BoatTicket\ReportController@getAgentReport');
    Route::post('report/agent_rep', 'BoatTicket\ReportController@getAgentReport');
});
