<?php namespace App\Http\Requests\BusTicket;

use App\Http\Requests\Request;

class BusUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // check the user types
        $rules = [
            'name' => 'required|min:5',
            'email' => 'required|email|unique:users,email,' . $this->getSegmentFromEnd(2) . ',id',
            'nrc' => 'required|min:5|unique:users,nrc,' . $this->getSegmentFromEnd(2) . ',id',
            'dob' => 'required',
            'parent_name' => 'required',
            'phone' => 'required',
            'contact' => '',
            'address' => 'required',
            'status' => 'required|between:0,1,'
        ];
        // BUS Branch Create and Edit
        if (in_array($this->getSegmentFromStart(1), array('branch', 'sale'))) {
            $rules['branch'] = 'required';
        }
        if (in_array($this->getSegmentFromStart(1), array('sale', 'finance'))) {
            $rules['check_in'] = 'required';
            $rules['check_out'] = 'required';
        }
        return $rules;
    }

    public function getSegmentFromEnd($position_from_end = 0)
    {
        $segments = $this->segments();
        if ($segments[sizeof($segments) - 1] == 'edit') {
            return $segments[sizeof($segments) - $position_from_end];
        }
        return 0;

    }

    public function getSegmentFromStart($postistion_from_start = 0)
    {
        $segments = $this->segments();
        if (count($segments) > $postistion_from_start) {
            return $segments[$postistion_from_start];
        }
        return 0;
    }
}
