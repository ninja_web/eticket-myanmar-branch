<?php namespace App\Http\Requests\BusTicket;

use App\BusTicket\BusScheduleType;
use App\Http\Requests\Request;

class BusScheduleRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'destination' => 'required',
            'dept_time' => 'required',
            'bus_order' => 'required',
            'start_date' => 'required'
        ];
        // check the selected destination
        $bus_schedule_type = BusScheduleType::where('id', '=', Request::input('destination'))->first();

        // check the whole trader_typprice

        // check the price
        $wholeRoute = json_decode($bus_schedule_type['whole'], true);
        if ($bus_schedule_type !== null) {
            for ($i = 0; $i < count($wholeRoute); $i++) {
                for ($j = $i + 1; $j < count($wholeRoute); $j++) {
                    $rule[$wholeRoute[$i] . "_" . $wholeRoute[$j]] = 'integer|min:1000|numeric';
                }
            }
            //  $rule[$wholeRoute[0]."_".$wholeRoute[count($wholeRoute) - 1]] = 'required|integer|min:1000|numeric';
        }
        // check the repeat
        return $rules;
    }

}
