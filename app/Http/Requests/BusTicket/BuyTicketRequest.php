<?php namespace App\Http\Requests\BusTicket;

use App\Http\Requests\Request;

class BuyTicketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'schedule_id' => 'required',
            'seat_no' => 'required',
            'selected_route' => 'required',
            'email' => 'required|min:5|email',
            'name_prefix' => 'required',
            'name' => 'required',
            'nrc_prefix' => 'required',
            'nrc_nation' => 'required',
            'nrc_no' => 'required',
            'passport' => 'required',
            'phone_no' => 'required',
            'remark' => ''
        );
        return $rules;
    }

}
