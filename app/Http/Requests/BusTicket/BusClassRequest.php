<?php namespace App\Http\Requests\BusTicket;

use App\Http\Requests\Request;

class BusClassRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // check the user types
        $rules = [
            'class' => 'required|min:5',
            'seater' => 'required',
            'status' => 'required|between:0,1,'
        ];
        return $rules;
    }

}
