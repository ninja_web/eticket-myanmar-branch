<?php namespace App\Http\Requests\BusTicket;

use App\Http\Requests\Request;

class BusScheduleTypeRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $total_route = Request::input('total_route');
        $rules['total_route'] = 'required|numeric|min:2';
        $rules['total_time'] = '';
        for ($i = 1; $i <= $total_route; $i++) {
            $rules['route_' . $i] = 'required';
        }
        return $rules;
    }

    public function getValidationInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->after(function () use ($validator) {
            // do check here

        });
        return $validator;
    }
}
