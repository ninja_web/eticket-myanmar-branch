<?php namespace App\Http\Requests\BoatTicket;

use App\Http\Requests\Request;

class PassengerRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'email' => 'min:5|email',
            'name_prefix' => '',
            'name' => 'required',
            'nrc_prefix' => '',
            'nrc_nation' => '',
            'nrc_no' => '',
            'passport' => '',
            'phone_no' => 'required',
            'remark' => ''
        );
        return $rules;
    }

}
