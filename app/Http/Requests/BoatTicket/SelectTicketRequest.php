<?php namespace App\Http\Requests\BoatTicket;

use App\Http\Requests\Request;

class SelectTicketRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'selected_route' => 'required',
            'btn_type' => 'required'
        );
        // check the schedule is without seat plan or with seat plan
        if (Request::input('seat_plan') == WITHOUT_SEAT_PLAN) {
            $rules['no_seat'] = 'required|numeric';
        } else {
            $rules['seat_no'] = 'required';
        }

        return $rules;
    }

}
