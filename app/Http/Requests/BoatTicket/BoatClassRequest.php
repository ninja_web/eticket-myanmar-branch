<?php namespace App\Http\Requests\BoatTicket;

use App\Http\Requests\Request;

class BoatClassRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // check the user types
        $rules = [
            'class' => 'required|min:5',
            'type' => 'required|min:5',
            'seat_plan' => 'required',
            'status' => 'required|between:0,1'
        ];

        if (Request::input('seat_plan') == WITHOUT_SEAT_PLAN) {
            $rules['no_seat'] = 'required';
        } else {
            $rules['seater'] = 'required';
        }
        return $rules;
    }

}
