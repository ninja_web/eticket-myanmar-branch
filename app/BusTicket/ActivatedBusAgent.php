<?php namespace App\BusTicket;

use Illuminate\Database\Eloquent\Model;

class ActivatedBusAgent extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activated_bus_agents';
}
