<?php namespace App\BusTicket;

use Illuminate\Database\Eloquent\Model;

class BusHistory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bus_histories';

}
