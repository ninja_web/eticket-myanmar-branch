<?php
function getBusScheduleCache($owner_code, $start_branch = '')
{
    $cache_key = $owner_code . "_schedule_" . $start_branch;
    if (Cache::has($cache_key)) {
        return Cache::get($cache_key);
    }
    // search the schedule type by owner_code
    $Builder = DB::table('bus_schedule_types')
        ->select(array('id', 'total_branch', 'start_branch', 'end_branch', 'time_sequence', 'whole'))
        ->where('owner_code', $owner_code);
    if (!empty($start_branch)) {
        $Builder->where('start_branch', $start_branch);
    }

    $result = $Builder->get();
    $schedules = array();
    foreach ($result as $row) {
        $schedules[$row->id] = Array(
            'id' => $row->id,
            'total_branch' => $row->total_branch,
            'start_branch' => $row->start_branch,
            'end_branch' => $row->end_branch,
            'time_sequence' => !empty($row->time_sequence) ? json_decode($row->time_sequence) : array()
        );
        // generate the whold cache
        $english_route = array();
        foreach (json_decode($row->whole) as $key) {
            $english_route[] = $key;
        }
        $schedules[$row->id]['eng_whole'] = json_decode($row->whole);
        $schedules[$row->id]['english'] = implode(" - ", $english_route);
    }
    // put on file cache
    //Cache::put($cache_key, $schedules, CACHE_TIME);
    return json_encode($schedules);
}

function getBusClassCacheList($owner_code)
{
    if (empty($owner_code)) return array();
    $cache_key = $owner_code . "_bus_class";
    if (Cache::has($cache_key)) {
        return Cache::get($cache_key);
    }

    // search the schedule type by owner code
    $result = DB::table('bus_class')
        ->select(array('id', 'class', 'seater'))
        ->where('owner_code', $owner_code)
        ->get();
    $busClass = array();
    foreach ($result as $row) {
        $busClass[$row->id] = array('id' => $row->id, 'class' => $row->class, 'seater' => $row->seater);
    }

    // put on file cache
    //Cache::put($cache_key, $busClass, CACHE_TIME);
    return $busClass;
}
