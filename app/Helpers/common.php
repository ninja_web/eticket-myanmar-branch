<?php
/**
 * Created by PhpStorm.
 * User: revotech
 * Date: 4/11/15
 * Time: 10:15 PM
 */

function displayDate($date, $format = 'j F Y')
{
    if (empty($date)) return '';
    return date($format, strtotime($date));
}

function generateCode($length)
{
    $chars = array();
    for ($i = 0; $i < 10; $i++) {
        $chars = array_merge($chars, range(0, 9), range('A', 'Z'));
        shuffle($chars);
        $start = mt_rand(0, count($chars) - $length);
        return substr(join('', $chars), $start, $length);
    }
}

function showPriceWithRoute($route, $price)
{
    $price = json_decode($price, true);

    $text = '<ul>';
    foreach ($price as $key => $value) {
        if (ctype_digit($value) && !empty($value)) {
            $route = explode("_", $key);
            $text .= '<li>' . Str::title($route[0]) . ' - ' . Str::title($route[1]) . '</li>';
        }
    }
    $text .= '</ul>';
    return $text;
}

function showPriceWithRouteBackup($route, $price)
{
    $route = json_decode($route, true);
    $price = json_decode($price, true);
    $text = '<ul>';
    for ($i = 1; $i < count($route); $i++) {
        $text .= '<li>' . Str::title($route[0]) . ' - ' . Str::title($route[$i]) . ' =>  ' . $price[$i] . '</li>';
    }
    $text .= '</ul>';
    return $text;
}

function showStatus($status = STATUS_ACTIVE)
{
    switch ($status) {
        case STATUS_ACTIVE:
            return '<i class="fa fa-check-circle text-success"></i>';
            break;
        case STATUS_INACTIVE:
            return '<i class="fa fa-times-circle-o text-danger"></i>';
            break;
    }
}

function showRouteText($whole = array(), $prettyUI = "")
{
    if (!count($whole)) return "";
    $myanmar_route = array();
    foreach ($whole as $key => $value) {
        $myanmar_route[] = Str::title($value);
    }
    return implode(!empty($prettyUI) ? $prettyUI : ' - ', $myanmar_route);
}


// Helper funciton for loading and getting the query
function saveQuery($query_array)
{
    $query_string = http_build_query($query_array);
    $hash_string = md5($query_string);

    // check if exists
    $row = DB::table('query')
        ->where('hash', $hash_string)
        ->first();
    if (count($row)) {
        return $row->id;
    }
    // save if not exists
    $result = DB::table('query')->insertGetId(array('query_string' => $query_string, 'hash' => $hash_string));
    return $result;
}

function loadQuery($query_id)
{
    $row = DB::table('query')
        ->where('id', $query_id)
        ->first();
    if (count($row)) {
        parse_str($row->query_string, $query_string);
        Input::merge($query_string);
    }
}

// Generate Price Text Box
function generatePrice($schedule = array())
{
    if (!count($schedule)) return array();
    $data = array();
    for ($i = 0; $i < $schedule; $i++) {
        for ($j = $i + 1; $j < $schedule; $j++) {
            // genrate schedule
            $data[] = $i . "_" . $j;
        }
    }
    return $data;
}


function calculatePercentage($totalSeat, $remainSeat)
{
    return number_format((($totalSeat - $remainSeat) * 100) / $totalSeat, 2);
}

function getActivateRoute($agent_id = 0, $agent_type = BUS_AGENT)
{
    if ($agent_type == BOAT_AGENT) {
        $query = DB::table('activated_boat_agents');
    } else {
        $query = DB::table('activated_boat_agents');
    }
    $result = $query->selectRaw("GROUP_CONCAT(activate_branch SEPARATOR ',') as branch")
        ->where('agent_id', $agent_id)
        ->first();

    $branch = explode(",", $result->branch);
    $branch_text = array();
    foreach ($branch as $row) {
        $branch_text[] = Str::title($row);
    }
    // remove duplicate route
    $branch_text = array_unique($branch_text);
    return implode(",", $branch_text);
}

/*
 *  calculate the time range
 * @param : dep_date => start Date
 * @param : end_date => end Date
 * @param : rep => repeat schedule => Daily, Weekly, Monthly, Yearly
 */

function calculateRepeatCount($dep_date = '', $end_date = '', $rep = '')
{
    if ($rep == '' || $end_date == '' || (strtotime($dep_date) > strtotime($end_date)))
        return array(date('Y-m-d', strtotime($dep_date)));
    // with repeat sequence
    else {
        $rep_sequence = array();
        $repeat_sequence[] = date('Y-m-d', strtotime($dep_date));

        if (strtolower($rep) == 'yearly')
            $sequence = '+ 1year'; // yearly
        elseif (strtolower($rep) == 'monthly')
            $sequence = '+ 1month'; // monthly
        elseif (strtolower($rep) == 'weekly')
            $sequence = '+ 1week'; // weekly
        else
            $sequence = '+ 1day'; // daily

        $tmp_date = $dep_date; // dep date to tmp date
        while (strtotime($end_date) >= strtotime($tmp_date . $sequence)) {
            // swap date schedule
            $tmp_date = date('Y-m-d', strtotime($tmp_date . $sequence));
            $repeat_sequence[] = $tmp_date;
        }
        return $repeat_sequence;
    }
}

function getAllActivatedBoatLine($agent)
{
    if ($agent == null) return array();
    // Get all activated boat Line
    // TODO : Check the agent or sub agent
    $query = DB::table('activated_boat_agents')
        ->where('agent_code', $agent->agent_code)
        ->groupBy('boat_owner')
        ->get();
    $owner = array();
    foreach ($query as $row) {
        $owner[] = $row->boat_owner;
    }
    return DB::table('boat_owners')
        ->select(array('id', 'name', 'status', 'owner_code', 'profile', 'profile_path'))
        ->whereIn('owner_code', $owner)
        ->get();
}

function getAllActivateBranch($owner_code, $boat_owner)
{

}


function getSubAgentCount($code, $group = BUS_GROUP)
{
    return DB::table('users')
        ->where('user_type', '=', BOAT_GROUP == $group ? BOAT_SUB_AGENT : BUS_SUB_AGENT)
        ->where('agent_code', '=', $code)
        ->count();
}
