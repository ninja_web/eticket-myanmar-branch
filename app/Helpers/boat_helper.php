<?php
// TODO :: if the user is agent, we must need to check the comission branch
use App\User;

function getBoatScheduleCache($condition = '=')
{
    /*  $cache_key = Auth::user()->owner_code . "_schedule_". Auth::user()->start_branch;
      if (Cache::has($cache_key)) {
          return Cache::get($cache_key);
      } */
    // search the schedule type by owner_code
    $Builder = DB::table('boat_schedule_types')
        ->select(array('id', 'total_branch', 'start_branch', 'end_branch', 'time_sequence', 'whole'))
        ->where('owner_code', Auth::user()->owner_code);
    if (in_array(Auth::user()->user_type, array(BOAT_SALE, BOAT_BRANCH))) {
        $Builder->where('start_branch', $condition, Auth::user()->branch);
    }
    $Builder->where('status', STATUS_ACTIVE);
    $result = $Builder->get();
    $schedules = array();
    foreach ($result as $row) {
        $schedules[$row->id] = Array(
            'id' => $row->id,
            'total_branch' => $row->total_branch,
            'start_branch' => Str::title($row->start_branch),
            'end_branch' => Str::title($row->end_branch)
            //  'time_sequence' => !empty($row->time_sequence) ? json_decode($row->time_sequence) : array()
        );
        if (!empty($row->time_sequence)) {
            foreach (json_decode($row->time_sequence) as $value) {
                $schedules[$row->id]['time_sequence'][] = date('H:i A', strtotime($value));
            }
        } else {
            $schedules[$row->id]['time_sequence'] = array();
        }
        // generate the whold cache
        $english_route = array();
        foreach (json_decode($row->whole) as $key) {
            $english_route[] = $key;
        }
        $schedules[$row->id]['eng_whole'] = json_decode($row->whole);
        $schedules[$row->id]['english'] = implode(" - ", $english_route);
    }
    // put on file cache
    //Cache::put($cache_key, $schedules, CACHE_TIME);
    return json_encode($schedules);
}

function getBoatClassCacheList($owner_code)
{
    if (empty($owner_code)) return array();
    $cache_key = $owner_code . "_boat_class";
    if (Cache::has($cache_key)) {
        return Cache::get($cache_key);
    }

    // search the schedule type by owner code
    $result = DB::table('boat_class')
        ->select(array('id', 'class', 'seater'))
        ->where('owner_code', $owner_code)
        ->get();
    $boatClass = array();
    foreach ($result as $row) {
        $boatClass[$row->id] = array('id' => $row->id, 'class' => $row->class, 'seater' => $row->seater);
    }

    // put on file cache
    //Cache::put($cache_key, $boatClass, CACHE_TIME);
    return $boatClass;
}

/**
 * [dailySaleReport description]
 * @param  string $value [description]
 * @return [type]        [description]
 */
function dailySaleReport($report_date = '', $sale_person = 0, $report_type = '')
{
    if (empty($report_date) || $sale_person == 0) return array();

    $report_date = str_replace('/', '-', $report_date);
    $report_date = date('Y-m-d', strtotime($report_date));

    $data = array(
        'report_person' => array(
            'date' => $report_date,
            'person' => $sale_person
        ),
        'report_brief' => array(
            'total_net_amount' => 0,
            'total_amount' => 0,
            'total_seat' => 0,
            'total_discount' => 0
        ),
        'report_data' => array()
    );
    // Generate the sale report data based on these day
    $query = DB::table('boat_tickets as b_t')
        ->select(array('b_s.id', 'b_s.owner_code', 'b_s.dept_date', 'b_s.dept_date', 'b_t.*'))
        ->leftJoin('boat_schedules as b_s', 'b_t.boat_schedule', '=', 'b_s.id')
        ->where('b_t.type', '=', BUY)
        ->whereDate('b_t.created_at', '=', $report_date);
    // Check the authentication
    if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
        $query->where('b_t.agent_code', Auth::user()->agent_code);
    }

    if ($sale_person != '') {
        // $query->where('b_t.created_by', $sale_person);
    }
    if ($report_type != '') {
        $query->where('b_t.pay', $report_type);
    }
    $record = $query->get();

    if (!count($record)) return $data;
    $comission_type = array();
    if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
        $comission_type = array(
            'type' => 'byAgentSide',
            'agent_code' => Auth::user()->agent_code
        );
    } else {
        $comission_type = array(
            'type' => 'byOwnerSide',
            'owner_code' => Auth::user()->owner_code
        );
    }
    $comission = getAgentCommission($comission_type);
    foreach ($record as $row) {
        $sale_record = array(
            'price' => $row->fare,
            'route' => $row->from . ' - ' . $row->to,
            'seat_no' => $row->seat_no,
            'discount' => $row->discount > 0 ? $row->discount : 0,
            'dept_date' => $row->dept_date
        );

        // Check the Comission
        if ($row->agent_code != null && array_key_exists($row->owner_code . '_' . $row->agent_code . '_' . $row->from . '_' . $row->to, $comission)) {
            foreach ($comission[$row->owner_code . '_' . $row->agent_code . '_' . $row->from . '_' . $row->to] as $discount) {
                if ($discount['start_date'] <= $row->dept_date && $discount['end_date'] <= $row->dept_date) {
                    $sale_record['discount'] += $discount['amount'];
                } else if ($discount['start_date'] <= $row->dept_date && $discount['end_date'] == null) {
                    $sale_record['discount'] += $discount['amount'];
                }
            }
        }

        // Select owner name
        if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
            $owner = DB::table('boat_owners')->select(array('name'))->where('owner_code', $row->owner_code)->first();
            $sale_record['owner_name'] = $owner->name;
        } else {
            $sale_record['owner_name'] = '';
        }

        // check the total and discount
        $sale_record['total_seat'] = $row->total_seat;
        $sale_record['total_discount'] = $sale_record['discount'] * $sale_record['total_seat'];
        $sale_record['total_amount'] = ($row->fare * $sale_record['total_seat']) - $sale_record['total_discount'];

        $data['report_data'][] = $sale_record;

        // calculate the total
        $data['report_brief']['total_amount'] += $row->fare * $sale_record['total_seat'];
        $data['report_brief']['total_net_amount'] += $sale_record['total_amount'];
        $data['report_brief']['total_seat'] += $sale_record['total_seat'];
        $data['report_brief']['total_discount'] += $sale_record['total_discount'];

    }
    return $data;
}

/**
 * [dailySaleReport description]
 * @param  string $report_date [description]
 * @param  integer $sale_person [description]
 * @param  [type]  $report_type [description]
 * @return [type]               [description]
 */
function mainSaleReport($schedule)
{
    if (empty($schedule->id) || $schedule->id == 0) return array();
    $data = array(
        'report_brief' => array(
            'total_amount' => 0,
            'total_net_amount' => 0,
            'total_seat' => 0,
            'total_discount' => 0
        ),
        'report_data' => array()
    );

    // Generate the sale report data based on these day
    $query = DB::table('boat_tickets as b_t')
        ->select(array('b_s.id', 'b_s.owner_code', 'b_s.dept_date', 'b_s.dept_date', 'b_t.*'))
        ->leftJoin('boat_schedules as b_s', 'b_t.boat_schedule', '=', 'b_s.id')
        ->where('boat_schedule', $schedule->id)
        ->where('b_t.type', '=', BUY)
        ->get();
    if (!count($query)) return $data;
    // Check the comission
    $comission_type = array(
        'start_branch' => $schedule->start_branch,
        'end_branch' => $schedule->end_branch
    );
    if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
        $comission_type['type'] = 'byAgentSide';
        $comission_type['agent_code'] = Auth::user()->agent_code;
    } else {
        $comission_type['type'] = 'byOwnerSide';
        $comission_type['owner_code'] = Auth::user()->owner_code;
    }
    $comission = getAgentCommission($comission_type);
    foreach ($query as $row) {
        $sale_record = array(
            'buyer' => '',
            'time' => date('d-m-Y H:i A', strtotime($row->created_at)),
            'price' => $row->fare,
            'seat_no' => $row->seat_no,
            'discount' => $row->discount > 0 ? $row->discount : 0
        );

        // check the commission
        if ($row->agent_code != null && array_key_exists($row->owner_code . '_' . $row->agent_code . '_' . $row->from . '_' . $row->to, $comission)) {
            foreach ($comission[$row->owner_code . '_' . $row->agent_code . '_' . $row->from . '_' . $row->to] as $discount) {
                if ($discount['start_date'] <= $row->dept_date && $discount['end_date'] <= $row->dept_date) {
                    $sale_record['discount'] += $discount['amount'];
                } else if ($discount['start_date'] <= $row->dept_date && $discount['end_date'] == null) {
                    $sale_record['discount'] += $discount['amount'];
                }
            }
        }
        // check the total and discount
        $sale_record['total_seat'] = $row->total_seat;
        $sale_record['total_discount'] = $sale_record['discount'] * $sale_record['total_seat'];
        $sale_record['total_amount'] = ($row->fare * $sale_record['total_seat']) - $sale_record['total_discount'];

        $data['report_data'][] = $sale_record;

        // calculate the total
        $data['report_brief']['total_amount'] += $row->fare * $sale_record['total_seat'];
        $data['report_brief']['total_net_amount'] += $sale_record['total_amount'];
        $data['report_brief']['total_seat'] += $sale_record['total_seat'];
        $data['report_brief']['total_discount'] += $sale_record['total_discount'];
    }
    return $data;
}

function agentReport($report_query = array())
{
    if (!count($report_query)) return array();
    $data = array(
        'report_brief' => array(
            'total_amount' => 0,
            'total_net_amount' => 0,
            'total_seat' => 0,
            'total_discount' => 0
        ),
        'report_data' => array()
    );
    $query = DB::table('boat_tickets as b_t')
        ->select(array('b_s.id', 'b_s.owner_code', 'b_s.dept_date', 'b_s.dept_date', 'b_t.*'))
        ->leftJoin('boat_schedules as b_s', 'b_t.boat_schedule', '=', 'b_s.id')
        ->whereDate('b_t.created_at', '>=', date('Y-m-d', strtotime($report_query['start_date'])))
        ->whereDate('b_t.created_at', '<=', date('Y-m-d', strtotime($report_query['end_date'])))
        ->where('b_t.type', '=', BUY)
        ->where('b_t.owner_code', $report_query['owner'])
        ->where('b_t.agent_code', $report_query['agent']);
    if (Auth::user()->user_type == BOAT_BRANCH) {
        $query->where('b_s.start_branch', Auth::user()->branch);
    }
    $record = $query->get();
    if (!count($record)) return $data;

    // Check the comission
    $comission_type = array(
        'agent_code' => $report_query['agent'],
        'owner_code' => $report_query['owner']
    );
    if (in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT))) {
        $comission_type['type'] = 'byAgentSide';
    } else {
        $comission_type['type'] = 'byOwnerSide';
    }
    $comission = getAgentCommission($comission_type);

    $tmp_owner = User::where('owner_code', $report_query['owner'])->first();
    $tmp_owner_name = $tmp_owner->name;

    $tmp_agent = User::where('agent_code', $report_query['agent'])->first();
    $tmp_agent_name = $tmp_agent->name;

    foreach ($record as $row) {
        $sale_record = array(
            'price' => $row->fare,
            'route' => $row->from . ' - ' . $row->to,
            'seat_no' => $row->seat_no,
            'discount' => $row->discount > 0 ? $row->discount : 0,
            'dept_date' => $row->dept_date
        );
        // check the commission
        if ($row->agent_code != null && array_key_exists($row->owner_code . '_' . $row->agent_code . '_' . $row->from . '_' . $row->to, $comission)) {
            foreach ($comission[$row->owner_code . '_' . $row->agent_code . '_' . $row->from . '_' . $row->to] as $discount) {
                if ($discount['start_date'] <= $row->dept_date && $discount['end_date'] <= $row->dept_date) {
                    $sale_record['discount'] += $discount['amount'];
                } else if ($discount['start_date'] <= $row->dept_date && $discount['end_date'] == null) {
                    $sale_record['discount'] += $discount['amount'];
                }
            }
        }

        // check the total and discount
        $sale_record['total_seat'] = $row->total_seat;
        $sale_record['total_discount'] = $sale_record['discount'] * $sale_record['total_seat'];
        $sale_record['total_amount'] = ($row->fare * $sale_record['total_seat']) - $sale_record['total_discount'];

        $sale_record['owner_name'] = $tmp_owner_name;
        $sale_record['agent_name'] = $tmp_agent_name;


        $data['report_data'][] = $sale_record;

        // calculate the total
        $data['report_brief']['total_amount'] += $row->fare * $sale_record['total_seat'];
        $data['report_brief']['total_net_amount'] += $sale_record['total_amount'];
        $data['report_brief']['total_seat'] += $sale_record['total_seat'];
        $data['report_brief']['total_discount'] += $sale_record['total_discount'];

    }
    return $data;
}

/**
 * [getAgentCommission description]
 * @param  {[type]} [$query_type=array(] [description]
 * @return {[type]}                      [multiple type : byOwnerCode, byAgentCode, ]
 */
function getAgentCommission($comission_type = array())
{
    if (!is_array($comission_type)) return array();
    $comission = array();
    if ($comission_type['type'] == 'byOwnerSide') {
        $sql = "SELECT *  FROM `boat_agent_commission` "
            . " WHERE `boat_owner` = '" . $comission_type['owner_code'] . "' ";
        if (array_key_exists('agent_code', $comission_type)) {
            $sql .= " AND `agent_code` = '" . $comission_type['agent_code'] . "'";
        }
    } else {
        $sql = "SELECT * FROM `boat_agent_commission` "
            . " WHERE `agent_code` = '" . $comission_type['agent_code'] . "' ";
        if (array_key_exists('owner_code', $comission_type)) {
            $sql .= " AND `boat_owner` = '" . $comission_type['owner_code'] . "'";
        }
    }

    if (isset($comission_type['start_branch'])) $sql .= " AND `start_branch` = '" . $comission_type['start_branch'] . "'";
    if (isset($comission_type['end_branch'])) $sql .= " AND `end_branch` = '" . $comission_type['end_branch'] . "'";

    $rows = DB::select(DB::raw($sql));
    foreach ($rows as $row) {
        $comission[$row->boat_owner . '_' . $row->agent_code . '_' . $row->start_branch . '_' . $row->end_branch][] = array(
            'start_date' => $row->start_date,
            'end_date' => $row->end_date,
            'amount' => $row->amount
        );
    }
    return $comission;
}
