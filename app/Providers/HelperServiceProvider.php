<?php
/**
 * Created by PhpStorm.
 * User: revotech
 * Date: 4/11/15
 * Time: 10:12 PM
 */

namespace app\Providers;


use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function register()
    {
        foreach (glob(app_path() . '/Helpers/*.php') as $file_name) {
            require_once($file_name);
        }
    }

}
