<?php namespace App\BoatTicket;

use Illuminate\Database\Eloquent\Model;

class TicketModifyHistory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ticket_modify_histories';

}
