<?php namespace App\BoatTicket;

use Illuminate\Database\Eloquent\Model;

class BoatTicket extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boat_tickets';


    public function user()
    {

        return $this->hasOne('App\User', 'id', 'created_by');
    }

}
