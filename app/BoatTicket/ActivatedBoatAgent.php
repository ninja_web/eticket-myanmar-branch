<?php namespace App\BoatTicket;

use Illuminate\Database\Eloquent\Model;

class ActivatedBoatAgent extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activated_boat_agents';
}
