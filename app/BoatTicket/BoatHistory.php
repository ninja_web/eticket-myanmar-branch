<?php namespace App\BoatTicket;

use Illuminate\Database\Eloquent\Model;

class BoatHistory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boat_histories';

}
