// Customize the js
function ShowImagePreview(files, canvas) {
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
        //alert('The File APIs are not fully supported in this browser.');
        return false;
    }
    if (typeof FileReader === "undefined") {
        //alert("Filereader undefined!");
        return false;
    }

    var file = files[0];
    if (!(/image/i).test(file.type)) {
        // alert("File is not an image.");
        return false;
    }
    reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image;
        img.id = canvas;
        img.onload = UpdatePreviewCanvas;
        img.src = event.target.result;
    }
    reader.readAsDataURL(file);
}

// Show Edit Preview Image
function editImagePreview(id, url, width, height) {
    var canvas = document.getElementById(id + "_canvas");
    $("#" + id + "_canvas").show();
    $("#" + id + "_upload").hide();
    var context = canvas.getContext("2d");
    var world = new Object();
    world.width = width;
    world.height = height;
    canvas.width = world.width;
    canvas.height = world.height;
    var WidthDif = width - world.width;
    var HeightDif = height - world.height;
    var Scale = 0.0;
    if (WidthDif > HeightDif) {
        Scale = world.width / width;
    }
    else {
        Scale = world.height / height;
    }
    if (Scale > 1)
        Scale = 1;
    var UseWidth = Math.floor(width * Scale);
    var UseHeight = Math.floor(height * Scale);
    var x = Math.floor((world.width - UseWidth) / 2);
    var y = Math.floor((world.height - UseHeight) / 2);
    var img = new Image();
    img.src = url;
    img.onload = function () {
        context.drawImage(img, x, y, UseWidth, UseHeight);
    };
}

function UpdatePreviewCanvas() {
    var width = 200;
    var height = 200;
    var img = this;
    var canvas = document.getElementById(img.id + "_canvas");
    console.log(img.id);
    // show the canvas
    $('#' + img.id + "_canvas").show();
    // hide the upload area
    $("#" + img.id + "_upload").hide();
    if (typeof canvas === "undefined"
        || typeof canvas.getContext === "undefined")
        return;
    var context = canvas.getContext('2d');
    var world = new Object();
    world.width = canvas.offsetWidth;
    world.height = canvas.offsetHeight;
    canvas.width = world.width;
    canvas.height = world.height;
    if (typeof img === "undefined")
        return;
    var WidthDif = img.width - world.width;
    var HeightDif = img.height - world.height;
    var Scale = 0.0;
    if (WidthDif > HeightDif) {
        Scale = world.width / img.width;
    }
    else {
        Scale = world.height / img.height;
    }
    if (Scale > 1)
        Scale = 1;
    var UseWidth = Math.floor(img.width * Scale);
    var UseHeight = Math.floor(img.height * Scale);
    var x = Math.floor((world.width - UseWidth) / 2);
    var y = Math.floor((world.height - UseHeight) / 2);
    context.drawImage(img, x, y, UseWidth, UseHeight);
    // hide the upload form
}
