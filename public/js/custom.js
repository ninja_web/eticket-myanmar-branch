$(function () {
    // disable all autocomplete
    $("input").attr("autocomplete", "off");

    // chosen select
    $('.chosen-select').chosen();

    /* 
     /* future= can only select just future date
     /* past=can only select just past date
     /* date=can only select both future and past
     */
    $('.date-picker-container .input-group.future').datepicker({
        clearBtn: true,
        calendarWeeks: true,
        autoclose: true,
        enddate: new Date(),
        startDate: "0d",
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });
    $('.date-picker-container .input-group.past').datepicker({
        clearBtn: true,
        calendarWeeks: true,
        autoclose: true,
        endDate: "0d",
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });

    $('.date-picker-container .input-group.date').datepicker({
        clearBtn: true,
        calendarWeeks: true,
        autoclose: true,
        endDate: "0d",
        todayHighlight: true,
        format: "dd/mm/yyyy"
    });

});

function generatePDF(tableName) {
    var pdf = new jsPDF('p', 'pt', 'letter');
    source = $('#' + tableName)[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, { // y coord
            'width': margins.width, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        },

        function (dispose) {
            // dispose: object with X, Y of the last line add to the PDF
            //          this allow the insertion of new lines after html
            pdf.save(tableName + '.pdf');
        }, margins);
}
/* input number */

$(document).on('click', '.btn-number', function (e) {
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type = $(this).attr('data-type');
    var input = $("input[name='" + fieldName + "']");

    var total = $("input[name='hidd_without_total']").val();

    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if (type == 'minus') {

            if (currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if (parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

            $("span#remain_seat").text(total - input.val());

        } else if (type == 'plus') {

            if (currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if (parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

            $("span#remain_seat").text(total - input.val());

        }
    } else {
        input.val(0);
    }
});
$(document).on('focusin', '.input-number', function () {
    $(this).data('oldValue', $(this).val());
});
$(document).on('change', '.input-number', function () {

    minValue = parseInt($(this).attr('min'));
    maxValue = parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if (valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if (valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
});

$(document).on("keydown", ".input-number", function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


$(function () {

    // Create two variable with the names of the months and days in an array
    var monthNames = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

    // Create a newDate() object
    var newDate = new Date();
    // Extract the current date from Date object
    newDate.setDate(newDate.getDate());
    // Output the day, date, month and year
    $('#cur-time').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

    setInterval(function () {
        // Create a newDate() object and extract the seconds of the current time on the visitor's
        var hours = new Date().getHours();
        // Add a leading zero to seconds value
        $("#cur-sec").html((hours < 12 ? "AM" : "PM"));
    }, 1000);

    setInterval(function () {
        // Create a newDate() object and extract the minutes of the current time on the visitor's
        var minutes = new Date().getMinutes();
        // Add a leading zero to the minutes value
        $("#cur-min").html((minutes < 10 ? "0" : "") + minutes);
    }, 1000);

    setInterval(function () {
        // Create a newDate() object and extract the hours of the current time on the visitor's
        var hours = new Date().getHours();
        // Add a leading zero to the hours value
        hours = hours > 12 ? hours - 12 : hours;
        $("#cur-hours").html((hours < 10 ? "0" : "") + hours);
    }, 1000);
});


var $ctrKey = false;
function numberOnly(event) {
    var key = event.keyCode;
    return ((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || (event.shiftKey && event.ctrlKey && key == 37 ) || (event.shiftKey && key == 36) || (event.ctrlKey && key == 86) || key == 8 || key == 189 || key == 9 || key == 32 || key == 16 || key == 37 || key == 39 );
};

/* Adding Custom Alert System */

function messagebar_toggle($msg, $type, $time) {

    $("body .message-bar").remove();
    var $html = '<div class="message-bar fixed-top">';
    $html += '<div class="alert message-alert ' + $type + '">';
    $html += '<div class="status-label ' + $type + '">';
    $html += '<i class="fa fa-check"></i>';
    $html += '</div>';
    $html += '<span>' + $msg + '</span>';
    $html += '<button type="button" class="close" id="message_bar_close">&times;</button>';
    $html += '</div></div>';

    $("body").append($html);
    $('.message-bar').animate({'top': '0px'}, 1000);
    $('.message-bar').delay($time).animate({'top': '-200px'}, 1000);

}

/* Message bar animate fade out */
$(document).on("click", "#message_bar_close", function () {

    $("body .message-bar").remove();

});