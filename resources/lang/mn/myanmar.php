<?php
// TODO :: Project : Need to add more myanmar routes
return [
    /*
    |--------------------------------------------------------------------------
    | Online Bus Ticket Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the online bus ticket system
    |
    */

    'bus_ticket' => 'ကားလက္မွတ္',
    /**
     * Site Admin
     */
    'site_admin' => '',
    'admin_staff' => 'ဝန္ထမ္း',
    /**
     * Bus Group
     */
    'bus_owner' => 'ကားဂိတ္',
    'bus_admin' => 'ဂိတ္ပိုုင္',
    'branch' => 'ဂိတ္မွဴ း',
    'branch_new' => 'ဂိတ္မွဴ း အသစ္ထည့္ရန္',
    'sale' => 'အေရာင္းဝန္ထမ္း',
    'sale_new' => 'အေရာင္းဝန္ထမ္း အသစ္ထည့္ရန္',
    'finance' => 'စာရင္းကိုုင္',
    'finance_new' => 'စာရင္းကိုုင္ အသစ္ထည့္ရန္',
    'driver' => '',
    'spare' => '',
    'bus_attendance' => '',

    /**
     * Bus Agent Group
     */
    'bus_agent' => 'ကိုုယ္စားလွယ္',
    'bus_sub_agent' => '',
    /**
     * Board Group
     */
    'boat_ticket' => 'သေဘၤာလက္မွတ္',
    'boat_admin' => 'သေဘၤာဂိတ္',
    'boat_staff' => '',
    'boat_finance' => '',

    /**
     * Board Agent Group
     */
    'boat_agent' => 'ကိုုယ္စားလွယ္',
    'boat_sub_agent' => '',
    /* shna */
    'boat_type' => 'သေဘၤာအမ်ိုးအစား',
    'boat_type_new' => 'သေဘၤာအမ်ိုးအစားထည့္ရန္',
    'boat_schedule_type' => 'သေဘၤာခရီးစဥ္မ်ား',
    'boat_schedule_type_new' => 'ခရီးစဥ္ထည့္ရန္',
    'boat_schedule' => 'သေဘၤာခရီးစဥ္',


    /**
     * General
     */
    'setting' => 'ထိန္းခ်ဳပ္ခန္း',
    'website' => 'ဝက္ဆိုုဒ္',
    'tablet' => 'တတ္ဘလက္',
    'schedule' => 'ခရီးစဥ္',
    'bus_type' => 'ကားအမ်ဳိးအစား',
    /*shna*/
    'destination' => 'သြားမည့္ခရီစဥ္',
    'time' => 'အခ်ိန္',
    'start_date' => 'စတင္ထြက္ခြာမည့္ေန့',
    'boat_class' => 'သေဘၤာအမ်ိုးအစား',
    'boat_order' => 'သေဘၤာအမွတ္စဥ္',
    'class_name' => 'အမ်ိုးအစား',
    'seater_type' => 'ထိုင္ခံုအမ်ိုးအစား',
    'total_seater_plan' => 'စုစုေပါင္းထိုင္ခံုအမ်ိုးအစား',
    'total' => 'စုစုေပါင္း',
    'taken_seat_numbers' => 'ဝယ္ယူျပီးေသာ ခံုနံပါတ္မ်ား',
    'selected_seat' => 'ေရြးခ်ယ္ျပီးေသာခံု',
    'name' => 'အမည္',
    'nrc' => 'မွတ္ပံုတင္နံပါတ္',
    'passport' => 'ပက္စပို့စ္',
    'phone_no' => 'ဖုန္းနံပါတ္',
    'remark' => 'မွတ္ခ်က္',
    'discount' => 'ေလ်ွာ့ေစ်း',
    'schedule' => 'ခရီးစဥ္',
    'please_select_seat' => 'ေက်ဇူးျပု၍ လက္မွတ္ဝယ္ရန္ ခံုမ်ားကိုေရြးပါ',
    'price' => 'ေစ်းနွုန္း',
    'no_of_seat' => 'ထိုင္ခံုအေရအတြက္',
    'permission' => 'Permission',

    /**
     * Form Name
     */
    'require_fields' => 'လိုုအပ္ေသာအခ်က္အလက္မ်ား',
    'name' => 'အမည္',
    'phone' => 'ဖုုန္းနံပါတ္',
    'nrc' => 'မွတ္ပံုုတင္နံပါတ္',
    'phone_1' => 'ဖုုန္းနံပါတ္ (၁)',
    'phone_2' => 'ဖုုန္းနံပါတ္ (၂)',
    'dob' => 'ေမြးေန႕',
    'email' => 'အီးေမးလ္ လိပ္စာ',
    'parent_name' => 'မိဘအမည္',
    'blood_type' => 'ေသြးအမ်ိဳးအစား',
    'due_date' => '',
    'status' => 'အေျခအေန',
    'address' => 'ေနရပ္လိပ္စာ',
    'license_photo' => '',
    'contact' => 'ဆက္သြယ္ရန္',
    'branch_name' => 'ရံုုးခဲြ',
    'total_seater' => '',
    'class' => '',
    'total_route' => 'စုစုေပါင္း ခရီးစဥ္',
    'total_time' => 'စုစုေပါင္း ျကာခ်ိန္',
    'boating_points' => 'ထြက္ခြာရာ ေနရာ',
    'dropping_points' => 'ဆိုက္ေရာက္ ေနရာ',
    'whole_route' => 'ခရီးစဥ္',


    // bus admin
    'define_bus_schedule_type' => 'ကားခရီးစဥ္သတ္မွတ္ရန္',
    'define_bus_type' => 'ကားအမ်ဳိးအစားသတ္မွတ္ရန္',
    'create_schedule' => 'ခရီးစဥ္ထည္႕ရန္',
    'create_staff' => 'ဝန္ထမ္း',
    'create_agent' => 'အေရာင္းကိုုယ္စားလွယ္',
    'ticket' => 'လက္မွတ္',
    'review_booking' => 'လက္မွတ္စာရင္းျပန္ၾကည္႕ရန္',
    'create_bus_type' => 'ကားစာရင္းသြင္းရန္',
    'assign_bus' => 'Assign Bus',
    'main_report' => 'မိန္းခ်ဳပ္စာရင္းမ်ား',
    'sale_report' => 'ေရာင္းအားစာရင္း',
    'check_in' => 'Check In',
    'report' => 'စာရင္းမ်ား',
    'c_in_c_out' => 'ဝင္ခ်ိန္ / ထြက္ခ်ိန္',

    // boat admoin
    'define_boat_schedule_type' => 'Board Schedule Type',
    'define_boat_type' => 'Board Type',
    'assign_boat' => 'Assign Board',

    'logout' => 'ထြက္ရန္',

    'yangon' => 'ရန္ကုုန္',
    'mawlamyaing' => 'ေမာ္လျမိဳင္',
    'pharan' => 'ဘားအံ',
    'shwebo' => 'ေရႊဘိုု',
    'mandalay' => '',
    'mudone' => 'မုုဒံုု',
    'kockkayate' => 'ေကာ႕ကရိတ္',
    'kawlin' => 'ေကာ္လင္း',
    'pyinoolwin' => ' ျပင္ဦးလြင္',
    'thanphyuzayat' => 'သံျဖဴဇရပ္',
    'myawade' => 'ျမဝတီ',
    'hopin' => 'ဟိုုပင္',
    'kyautmae' => 'ေက်ာက္မဲ',
    'htarwal' => 'ထားဝယ္',
    'taungkyi' => 'ေတာင္ၾကီး',
    'moekaung' => 'မိုုးေကာင္း',
    'moenyin' => 'မိုုညွင္း',
    'thipaw' => 'သီေပါ',
    'bagan' => 'ပုုဂံ',
    'nyaungoo' => 'ေညာင္ဦး',
    'hopone' => 'ဟိုုပံုုး',
    'intaw' => 'အင္းေတာ္',
    'larshow' => 'လားရိူး',
    'kyautpantaung' => 'ေက်ာက္ပန္းေတာင္း',
    'pathein' => 'ပုုသိမ္',
    'myitkyinar' => ' ျမစ္ၾကီးနား',
    'musel' => 'မူဆယ္',
    'pakhoteku' => '',
    'chaungthar' => 'ေခ်ာင္းသာ',
    'bamaw' => 'ဗန္းေမာ္',
    'minbu' => 'မင္းဘူး',
    'ngwsaung' => 'ေငြေဆာင္',
    'naypyitaw' => 'ေနျပည္ေတာ္',
    'yayynanchaung' => 'ေရနံေခ်ာင္း',
    'tatkone' => 'တပ္ကုုန္း',
    'yamaethin' => 'ရမည္းသင္း',
    'pyay' => ' ျပည္',
    'sittwe' => 'စစ္ေတြ',
    'kalay' => 'ကေလး',
    'taungngo' => 'ေတာင္ငူ',
    'thantwe' => 'သံတဲြ',
    'kalaywa' => 'ကေလးဝ',
    'matehtilar' => '',
    'myinchan' => 'မင္းျခံ',
    'taungngote' => 'ေတာင္ငုုတ္',
    'tamue' => 'တမူး',

    /**
     * Seater
     */
    '44_seater' => '၄၄ ခံုု',
    '45_seater' => '၄၅ ခံုု',
    '27_seater' => '၂၇ ခံုု',
    '28_seater' => '၂၈ ခံုု',
    '12_seater' => '၁၂ ခံုု',
    /**
     * General Message
     */
    'availiableSeat' => 'ရရွိႏုိင္ေသာခံု',
    'disabledSeat' => 'ေရြးခ်ယ္လို႔မရေတာ့ေသာခံု',
    'soldSeat' => 'ခရီးသည္ဝယ္ၿပီးေသာခုံ',


    /**
     * General Message
     */
    'ticket_percentate' => 'ေရာင္းလိုက္ေသာခံုုလက္မွတ္ရာခိုင္ႏွုန္း'

];
