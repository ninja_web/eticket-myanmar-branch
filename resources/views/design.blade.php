<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="{!! asset('front-end/images/favicon.png') !!}">
    <meta name="google-site-verification" content="LLavvJCdgc3CPK63jIcS5jgC12-vJnI784seavrCkRQ"/>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1"/>
    <meta name="" content=""/>
    <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes"/>
    <meta name="description" content="iTicketMyanmar::The best myanmar online highway bus ticketing system.">
    <meta name="keywords" content="iTicketMyanmar">
    <title>iTicketMyanmar::The best myanmar online highway bus ticketing system.</title>

    <!-- Note there is no responsive meta tag here -->

    <script type="text/javascript" src="{!! asset('front-end/js/jquery-1.10.2.min.js')!!}"></script>
    <script type="text/javascript" src="{!! asset('front-end/js/bootstrap.js')!!}"></script>
    <script type="text/javascript" src="{!! asset('front-end/js/jquery.bxslider.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('front-end/js/jquery.mmenu.min.js') !!}"></script>
    <!-- Bootstrap core CSS -->
    <link href="{!! asset('front-end/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('front-end/css/material.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('front-end/css/jquery.bxslider.css')!!}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{!! asset('front-end/css/style.css') !!}" rel="stylesheet">
    <link href="{!! asset('front-end/css/jquery.mmenu.css') !!}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-63366740-2', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>

<!-- header -->
<div id="page">
    <div class="header Fixed mb-version">
        <a class="btn-menu" href="#menu"></a>
        <a href="#"><img class="mb-logo" src="{!! asset('front-end/images/logo.png') !!}"></a>
    </div>
    <!-- mobile version -->
    <nav id="menu" class="mobile">
        <ul class="clearfix">
            <li><span class="mb-icon">
                        <a href="#" title="Facebook"><img src="{!! asset('front-end/images/fb-logo.jpg') !!}"></a>
                        <a href="#" title="Twistter"><img src="{!! asset('front-end/images/twistter.png') !!}"></a>
                        <a href="#" title="Pt"><img src="{!! asset('front-end/images/youtube.jpg') !!}"></a>
                        <a href="#" title="Wi Fi"><img src="{!! asset('front-end/images/instagram-logo.jpg') !!}"></a>
                    </span></li>
            <li><a href="#" title="Home"><i class="mdi-image-dehaze icon"></i>Home</a></li>
            <li><a href="#" title="Bus"><i class="mdi-maps-directions-bus icon"></i>Bus Ticket</a></li>
            <li><a href="#" title="Boat"><i class="mdi-maps-directions-ferry icon"></i>Boat Ticket</a></li>
            <li><a href="#" title="Air"><i class="mdi-maps-flight icon"></i>Air Ticket</a></li>
            <li><a href="#" title="Coach Package"><i class="mdi-action-wallet-giftcard icon"></i>Coach Package</a></li>
            <li><a href="#" title="Hotel"><i class="mdi-maps-hotel icon"></i>Hotel</a></li>
            <li><span class="ph-no"><i
                            class="mdi-communication-call"></i>&nbsp;&nbsp;959 798 000 000, 959 798 000 000</span></li>
            <li><span class="ph-no"><i class="mdi-communication-call"></i>&nbsp;&nbsp;959 798 200 200, 959 798 300 300 </span>
            </li>
        </ul>
    </nav>
    <!-- End mobile version-->
    <div class="content" id="content">
        <div class="bd-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-lg-9 col-sm-9 ">
                        <p class="p-telephone"><i class="mdi-communication-call"></i>&nbsp;&nbsp;959 798 000 000, 959
                            798 000 000, 959 798 200 200, 959 798 300 300 </p>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-3 col-social">
                        <a href="#"><img src="{!! asset('front-end/images/mm-flag.jpg') !!}" alt=""></a>
                        <a class="a-flag" href="#"><img src="{!! asset('front-end/images/en-flag.jpg') !!}" alt=""></a>
                        <a class="login" href="#" title="Login"><i class="mdi-action-account-circle"></i>&nbsp;&nbsp;Login&nbsp;&nbsp;|</a>
                        <a class="register" href="#" title="Register">&nbsp;&nbsp;Register&nbsp;</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-logo">
                        <a class="logo-brand" href="#"><img src="{!! asset('front-end/images/logo.png') !!}"></a>
                    </div>
                    <div class="col-md-9 col-lg-9 col-sm-9 col-social">
                        <a href="#" title="Facebook"><img src="{!! asset('front-end/images/fb-logo.jpg') !!}"></a>
                        <a href="#" title="Twistter"><img src="{!! asset('front-end/images/twistter.png') !!}"></a>
                        <a href="#" title="Pt"><img src="{!! asset('front-end/images/youtube.jpg') !!}"></a>
                        <a href="#" title="Wi Fi"><img src="{!! asset('front-end/images/instagram-logo.jpg') !!}"></a>

                    </div>


                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <ul class="menu-bar clearfix">
                            <li><a href="#" title="Home"><i class="mdi-image-dehaze icon"></i></i></a></li>
                            <li><a href="#" title="Bus"><i class="mdi-maps-directions-bus icon"></i>Bus Ticket</a></li>
                            <li><a href="#" title="Boat"><i class="mdi-maps-directions-ferry icon"></i>Boat Ticket</a>
                            </li>
                            <li><a href="#" title="Air"><i class="mdi-maps-flight icon"></i>Air Ticket</a></li>
                            <li><a href="#" title="Coach Package"><i class="mdi-action-wallet-giftcard icon"></i>Coach
                                    Package</a></li>
                            <li><a href="#" title="Hotel"><i class="mdi-maps-hotel icon"></i>Hotel</a></li>
                        </ul>
                        <div class="form-wrapper clearfix">
                            <h2 class="h2-title">Bus Ticket</h2>

                            <p class="bs-component">
                                <a href="#" class="btn btn-success disabled">One Way</a>
                                <a href="#" class="btn btn-success btn-raised">Return</a>
                            </p>

                            <div class="row">
                                <div class="col-md-6 col-log-6 col-sm-6 form-div">
                                    <span>From</span><br>
                                    <input class="input-text" type="text" placeholder="Depature City">
                                </div>
                                <div class="col-md-6 col-log-6 col-sm-6 form-div">
                                    <span>To</span><br>
                                    <input class="input-text" type="text" placeholder="Arrive City">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-log-6 col-sm-6 form-div">
                                    <p class="col-md-9 col-log-9 col-sm-9 form-left">
                                        <span>Deperature Date</span><br>
                                        <input class="input-text" type="text"><i class="mdi-action-event calendar"></i>
                                    </p>

                                    <p class="col-md-3 col-log-3 col-sm-3 form-right">
                                        <span>Time </span><br>
                                        <input class="input-text" type="text" placeholder="All">
                                    </p>

                                </div>
                                <div class="col-md-6 col-log-6 col-sm-6 form-div">
                                    <p class="col-md-9 col-log-9 col-sm-9 form-left">
                                        <span>Arrival Date</span><br>
                                        <input class="input-text" type="text"><i class="mdi-action-event calendar"></i>
                                    </p>

                                    <p class="col-md-3 col-log-3 col-sm-3 form-right">
                                        <span>Time </span><br>
                                        <input class="input-text" type="text" placeholder="All">
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-log-6col-sm-6 form-div">
                                    <span>Number Of Pax</span><br>

                                    <div class="sp-quantity">
                                        <div class="sp-minus fff"><span class="ddd">-</span>
                                        </div>
                                        <div class="sp-input">
                                            <input type="text" class="quntity-input" value="1"/>
                                        </div>
                                        <div class="sp-plus fff"><span class="ddd">+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-log-6 col-sm-6 form-div">
                                    <span>Search By Express Bus Name</span><br>
                                    <input class="input-text" type="text"><i class="mdi-action-search search"></i></i>
                                </div>
                            </div>

                            <div class="row" style="text-align:center">
                                <input class="btn-search btn btn-success btn-raised" type="button" value="Search">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 lf-banner">

                        <ul class="bxslider">
                            <li><img src="{!! asset('front-end/images/img4.png') !!}" title=""/></li>
                            <li><img src="{!! asset('front-end/images/img4.png') !!}" title=""/></li>
                            <li><img src="{!! asset('front-end/images/img4.png') !!}" title=""/></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(function () {
        var $menu = $('nav#menu'),
                $html = $('html, body');

        $menu.mmenu({
            dragOpen: true
        });

        var $anchor = false;
        $menu.find('li > a').on(
                'click',
                function (e) {
                    $anchor = $(this);
                }
        );

        var api = $menu.data('mmenu');
        api.bind('closed',
                function () {
                    if ($anchor) {
                        var href = $anchor.attr('href');
                        $anchor = false;

                        //	if the clicked link is linked to an anchor, scroll the page to that anchor
                        if (href.slice(0, 1) == '#') {
                            $html.animate({
                                scrollTop: $(href).offset().top
                            });
                        }
                    }
                }
        );
    });
</script>

<div class="container">

    <ul class="banner-menu px-box">
        <li><a class="a-link" title="Car Rental Service" href="#"><span class="circle-span ft-img"><img
                            class="circle-img" src="{!! asset('front-end/images/car-logo.png') !!}" alt=""></span><br>Car
                Rental Service</a></li>
        <li><a class="a-link" title="Transfer Service" href="#"><span class="circle-span sec-img"><img
                            class="circle-img" src="{!! asset('front-end/images/transfer.png') !!}" alt=""></span><br>Transfer
                Service</a></li>
        <li><a class="a-link" title="Buy Travel Insurance" href="#"><span class="circle-span trd-img"><img
                            class="circle-img" src="{!! asset('front-end/images/dollar.png') !!}" alt=""></span><br>Buy
                Travel Insurance</a></li>
        <li><a class="a-link" title="Google Play" href="#"><span class="circle-span fr-img"><img class="circle-img"
                                                                                                 src="{!! asset('front-end/images/google-play.png') !!}"
                                                                                                 alt=""></span><br>Google
                Play</a></li>
        <li><a class="a-link" title="App Store" href="#"><span class="circle-span fv-img"><img class="circle-img"
                                                                                               src="{!! asset('front-end/images/apple.png') !!}"
                                                                                               alt=""></span><br>App
                Store</a></li>
    </ul>
    <div class="row px-box">
        <div class="col-md-3 col-lg-3 col-sm-3">
            <h2 class="title-h2">Customer Service</h2>

            <div class=" bg-white">

                <ul class="ul-service">
                    <li><a href=""><i class="mdi-action-help i-service"></i>How To Use</a></li>
                    <li><a href=""><i class="mdi-action-print i-service"></i>Print A ticket</a></li>
                    <li><a href=""><i class="mdi-action-highlight-remove i-service"></i>Cancellation</a></li>
                    <li><a href=""><i class="mdi-communication-ring-volume i-service"></i>Point Reword</a></li>
                    <li><a href=""><i class="mdi-communication-contacts i-service"></i>Contact Us</a></li>
                    <li><a href=""><i class="mdi-communication-textsms i-service"></i>My Booking</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-5 col-lg-5 col-sm-5">
            <h2 class="title-h2">Offer Promotion</h2>

            <div class="bg-white">

                <ul class="bxslider">
                    <li><img src="{!! asset('front-end/images/index-slider.jpg') !!}" title=""/></li>
                    <li><img src="{!! asset('front-end/images/index-slider.jpg') !!}" title=""/></li>
                    <li><img src="{!! asset('front-end/images/index-slider.jpg') !!}" title=""/></li>
                </ul>

            </div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <h2 class="title-h2">Quick Link</h2>

            <div class="bg-white">

                <a href="javascript:void(0)" class="btn btn-primary btn-lg btn-text">Travelling in a group?<br>Coach
                    Chartering Service</a>
                <ul class="ul-link">
                    Quick Link
                    <li><a href="#">How to Purchase Ticeket</a></li>
                    <li><a href="#">Bus Terminals</a></li>
                    <li><a href="#">Boat Jetty</a></li>
                    <li><a href="#">Trip Duration</a></li>
                    <li><a href="#">Vechicle Charter</a></li>
                    <li><a href="#">Destinations</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row px-box">
        <div class="col-md-7 col-lg-7 col-sm-7">
            <h2 class="title-h2">Top Bus Routes</h2>

            <div class="bg-white">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Route</th>
                        <th>Fare Starting</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Yangon TO Mandalay</td>
                        <td>$ 45</td>
                        <td><a href="#">Book Now</a></td>
                    </tr>
                    <tr>
                        <td>Yangon TO Inlay</td>
                        <td>$ 45</td>
                        <td><a href="#">Book Now</a></td>
                    </tr>
                    <tr>
                        <td>Mandalay TO Yangon</td>
                        <td>$ 45</td>
                        <td><a href="#">Book Now</a></td>
                    </tr>
                    <tr>
                        <td>Mandalay TO Yangon</td>
                        <td>$ 45</td>
                        <td><a href="#">Book Now</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-5 col-lg-5 col-sm-5">
            <h2 class="title-h2">Customer Testimonals</h2>

            <div class="bg-white">
                <blockquote><p>Lorem ipsum dolor met.Lorem ipsum dolor met.Lorem ipsum dolor met.Lorem ipsum dolor
                        met.Lorem ipsum dolor met.
                        Lorem ipsum dolor met.Lorem ipsum dolor met.Lorem ipsum dolor met.Lorem ipsum dolor met.Lorem
                        ipsum dolor met.
                    </p></blockquote>
                <div class="carousel-info clearfix">
                    <img class="pull-left" src="{!! asset('front-end/images/img1-small.jpg') !!}" alt="">

                    <div class="pull-left ">
                        <span class="testimonials-name clearfix">Kate Ford</span><br>
                        <span class="testimonials-post clearfix">Commercial Director</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row index-wrapper px-box">
        <h2 class="title-h2 center-text">Our Partner </h2>
        <ul class="bxslider-logo">
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/slid.png') !!}"/></li>
        </ul>
        <ul class="accept" col-md-8 col-lg-8 col-sm-8>
            <li><h2 class="title-h2 ">We Accept:</h2></li>
            <li><img src="{!! asset('front-end/images/paypal.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/visa.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/dbs.png') !!}"/></li>
            <li><img src="{!! asset('front-end/images/mastercad.png') !!}"/></li>
        </ul>
    </div>
</div>
<!-- /container -->
<footer>
    <div class="container">

    </div>
    <div class="footer-wrapper">
        <div class="container">
            <div class="row footer-menu">
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>About Us</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Contact Us</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>FAQ</a></p>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>News</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Bus Operator</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Boat Operator</a></p>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Payment Security</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Promotions</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Site Map</a></p>

                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Payment Service</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>Terms And Conditions</a></p>

                    <p><a href="#"><i class="mdi-hardware-gamepad ft-icon"></i>User Agreement</a></p>

                </div>
            </div>
        </div>
    </div>
    <div class="copy-righ">
        <div class="container">
            <p class="footer-p">© 2015 Asia Beverage Co., Ltd</p>
        </div>
    </div>
</footer>
<script type="text/javascript">
    $(".ddd").on("click", function () {

        var $button = $(this);
        var oldValue = $button.closest('.sp-quantity').find("input.quntity-input").val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.closest('.sp-quantity').find("input.quntity-input").val(newVal);

    });
    $(document).ready(function () {
        console.log("ready!");
        $('.bxslider').bxSlider({
            mode: 'fade',
            captions: true,
            auto: true
        });
        $('.bxslider-logo').bxSlider({
            minSlides: 4,
            maxSlides: 5,
            slideWidth: 220,
            slideMargin: 10
        });
    });
</script>
</body>
</html>
