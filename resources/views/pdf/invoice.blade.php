<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">

        table, table tr {
            border-collapse: collapse;
        }

        .boder {
            border: 1px solid #ddd;
        }

        table.border td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>

<table style="width:680px;margin:0 auto;">
    <tr>
        <td style="width:50%">
            <div style="border:1px solid #ddd;width:200px;height:100px;text-align:center">
                @if(isset($owner))

                    <img src="{!!'http://iticketmyanmar.net/'.$owner->profile_path.$owner->profile!!}"
                         style="width:100%;height:100%">
                @endif
            </div>
        </td>
        <td style="width:50%">

        </td>
    </tr>
    <tr>
        <td colspan="2">

            <table style="width:100%;margin-top:10px">
                <tr>
                    <td style="width:55%">
                        <strong>{!!$owner->name!!}</strong>
                    </td>
                    <td style="text-aligh:right;width:20%">
                        <p><strong style="margin-bottom:10px;">Booking Reference</strong></p>
                        <div style="border:1px solid #000;padding:10px 20px;text-align:center;width:80px">
                            <strong>{!!$info->ticket_pnr_code!!}</strong>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="border" style="width:100%;margin-top:30px">
                <tr>
                    <td style="width:100%">
                        <h3 style="padding:5px;margin:0px;">Travel Itinerary</h3>

                    </td>
                </tr>
            </table>


            <table class="border" style="width:100%">

                <tr>
                <!-- <td style="width:20%">Reserved On : {!!date('Y-M-d H:i',strtotime($info->created_at))!!}</td> -->
                    <td style="width:25%"><span style="padding:8px;display:inline-block">Date</span></td>
                    <td style="width:25%"><span style="padding:8px;display:inline-block">Route</span></td>
                    <td style="width:25%"><span style="padding:8px;display:inline-block">Departing</span></td>
                    <td style="width:25%"><span style="padding:8px;display:inline-block">Arriving</span></td>
                </tr>
                <tr>
                <!-- <td style="width:20%">Reserved On : {!!date('Y-M-d H:i',strtotime($info->created_at))!!}</td> -->
                    <td style="width:25%"><span style="padding:8px;display:inline-block">
                        {!!date('Y-M-d',strtotime($info->dept_date))!!}
                    </span></td>
                    <td style="width:25%"><span style="padding:8px;display:inline-block">
                        <span style="text-transform:capitalize">{!!$info->start_branch!!}</span>
                        <i>-</i><i>></i>
                        <span style="text-transform:capitalize">{!!$info->end_branch!!}</span>
                    </span></td>
                    <td style="width:25%"><span style="padding:8px;display:inline-block">
                        {!!displayDate($info->dept_time, 'H:i A')!!}
                            <br>
                            @if(isset($boat_schedule_type) && $boat_schedule_type->b_points!='')
                                {!!$boat_schedule_type->b_points!!}
                            @endif
                    </span></td>
                    <td style="width:25%"><span style="padding:8px;display:inline-block">
                        @if(isset($boat_schedule_type) && $boat_schedule_type->d_points!='')
                                {!!$boat_schedule_type->d_points!!}
                            @endif
                    </span></td>
                </tr>

            </table>


            <table style="margin-top:20px;margin-bottom:20px;">
                <tr>
                    <td colspan="2" style="padding-bottom:20px;">
                        <strong>Important Note</strong>

                    </td>
                </tr>
                <tr>
                    <td style="width:20%">
                        Reporting Time –

                    </td>
                    <td style="width:80%">
                        River Cruise open for check-in 1 hour before departure.

                    </td>

                </tr>
                <tr>
                    <td style="width:20%"></td>
                    <td style="width:80%">
                        Check-in counter close 30 minute before river cruise departure time.
                    </td>

                </tr>
                <tr>
                    <td style="width:20%">
                        Check-in -

                    </td>
                    <td style="width:30%">
                        Proceed to the Jetty service counter to collect your boarding pass.
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        @if(isset($passengers) && count($passengers))
            <td colspan="2">

                <table class="border" style="width:100%;margin-top:30px">
                    <tr>
                        <td style="width:100%">
                            <h3 style="padding:5px;margin:0px;">Passenger Information</h3>

                        </td>
                    </tr>
                </table>

                <table class="border" style="width:100%">
                    <tr>
                        <td style="width:40%"><strong style="padding:8px;display:inline-block">Name</strong></td>
                        <td style="width:40%"><strong style="padding:8px;display:inline-block">Passport No. / NRC
                                No</strong></td>
                        <td style="width:20%"><strong style="padding:8px;display:inline-block">Seat</strong></td>
                    </tr>

                    @foreach($passengers as $passenger)
                        <tr>
                            <td>
                                <span style="padding:5px;display:inline-block">
                                @if($passenger->name_prefix!='')
                                        <?php $prefix = json_decode(NAME_PREFIX, true);?>
                                        {!!$prefix[$passenger->name_prefix]." ".$passenger->name!!}
                                    @endif
                                    <span>
                            </td>
                            <td>
                                <span style="padding:5px;display:inline-block">
                                    {!!$passenger->passport!!}
                                </span>
                            </td>
                            <td>
                                <span style="padding:5px;display:inline-block">
                                {!!$passenger->seat_no!!}
                                </span>
                            </td>
                        </tr>

                    @endforeach
                </table>

                <table class="border" style="width:100%;margin-top:5px;margin-bottom:10px;">
                    <tr>
                        <td style="width:100%">
                            <span style="padding:5px;display:inline-block">Contact – <a
                                        href="call:to{!!$info->phone_no!!}"> {!!$info->phone_no!!} </a>   <span><a
                                            href="mail:to{!!$info->email!!}"> {!!$info->email!!}</a></span></span>

                        </td>
                    </tr>
                </table>
            </td>
        @endif
    </tr>

    <tr style="margin-top:20px;">
        <td colspan="2">
            <span style="padding:5px;display:inline-block">Booked By : {!!Auth::user()->name!!}</span>
        </td>
    </tr>
    <tr>

        <td colspan="2">
            <span style="padding:5px;display:inline-block">Contact : {!!Auth::user()->name!!}
                @if(isset($info->phone_no))
                    : {!!$info->phone_no!!}
                @endif
            </span>

        </td>
    </tr>

    <tr>
        <td colspan="2">
            <span style="padding:5px;display:inline-block">Ticketed On : {!!date("d M Y H:i:A",strtotime($info->b_created_at))!!}</span>

        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="padding:5px;display:inline-block">Pricing: <strong>$ {!!$info->fare!!}</strong></span>

        </td>
    </tr>


    <tr>
        <td colspan="2">
            <p>Terms and Conditions</p>
            <p>Tickets are non-transferable and non-endorsable, and are subject to the following terms and
                conditions ( which are subject to change )</p>
        </td>
    </tr>
</table>
</body>
</html>
