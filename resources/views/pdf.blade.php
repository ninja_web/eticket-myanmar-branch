<!DOCTYPE html>
<html>
<head>
    <link href="{{ asset('/css/pdf.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <title>PDF Format</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ asset('/font-awesome-4.3.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table>
                <tr>
                    <td style="width:90px;"></td>
                    <td style="width:20px;"></td>
                    <td style="width:90px;"></td>
                </tr>
                <tr>
                    <td style="width:90px;">Date</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!date('M d Y',strtotime($info->dept_date))!!}</td>
                </tr>
                <tr>
                    <td style="width:90px;">Trip</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!$info->start_branch." --> ".$info->end_branch!!}</td>
                </tr>
                <tr>
                    <td style="width:90px;">Trip No</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!$info->ticket_pnr_code!!}</td>
                </tr>
                <tr>
                    <td style="width:90px;">Depature Time</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!$info->dept_time!!}</td>
                </tr>
                <tr>
                    <td style="width:90px;">Jetty</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;"></td>
                </tr>
                <tr>
                    <td style="width:90px;">Seat</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!$info->total_seat!!}</td>
                </tr>
                <tr>
                    <td style="width:90px;">Booking Date</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!date('M d Y',strtotime($info->created_at))!!}</td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="width:90px;"></td>
                    <td style="width:20px;"></td>
                    <td style="width:90px;"></td>
                </tr>
                <tr>
                    <td style="width:90px;">Name</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">
                        @if($info->name_prefix!='')
                            <?php $prefix = json_decode(NAME_PREFIX, true);?>
                            {!!$prefix[$info->name_prefix]." ".$info->name!!}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:90px;">Phone No</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!$info->phone_no!!}</td>
                </tr>
                <tr>
                    <td style="width:90px;">Contact</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;"></td>
                </tr>
                <tr>
                    <td style="width:90px;">Email</td>
                    <td style="width:20px;">:</td>
                    <td style="width:90px;">{!!$info->email!!}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>