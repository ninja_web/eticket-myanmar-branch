<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <!-- Checking the sidebar -->
    @if(BUS_ADMIN == Auth::user()->user_type)
        <!-- Bus Admin Sidebar -->
        @include('sidebar.sb_bus_admin')
    @elseif(BUS_BRANCH == Auth::user()->user_type)
        <!-- Bus Branch Sidebar -->
        @include('sidebar.sb_bus_branch')
    @elseif(BUS_SALE == Auth::user()->user_type)
        <!-- Bus Sale Sidebar-->
        @include('sidebar.sb_bus_sale')
    @elseif(BUS_FINANCE == Auth::user()->user_type)
        <!-- Bus Sale Sidebar-->
        @include('sidebar.sb_finance')
    @elseif(BUS_AGENT == Auth::user()->user_type)
        <!-- Bus Bus Agent Sidebar-->
        @include('sidebar.sb_bus_agent')
        <!-- -------------------------  -->
    @elseif(BOAT_ADMIN == Auth::user()->user_type)
        <!-- Board Admin Sidebar-->
        @include('sidebar.sb_boat_admin')
    @elseif(BOAT_BRANCH == Auth::user()->user_type)
        <!-- Board Branch Sidebar-->
        @include('sidebar.sb_boat_branch')
    @elseif(BOAT_SALE == Auth::user()->user_type)
        <!-- Board Sale Sidebar-->
        @include('sidebar.sb_boat_sale')
    @elseif(BOAT_FINANCE == Auth::user()->user_type)
        @include('sidebar.sb_boat_finance')
    @elseif(BOAT_AGENT == Auth::user()->user_type)
        <!-- Board Sale Sidebar-->
        @include('sidebar.sb_boat_agent')
    @elseif(BOAT_SUB_AGENT == Auth::user()->user_type)
        @include('sidebar.sb_boat_sub_agent')
    @endif
    <!-- slide menu -->
        <!-- slide menu -->

    </div>
    <!-- /.sidebar-collapse -->
</div>
