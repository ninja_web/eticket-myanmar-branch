<ul class="nav" id="side-menu">
    <li class="toggle-panel">
        <a href="{!! url('/dashboard') !!}"
           class="first-level {!! 0 == strcasecmp('dashboard', $c_page['main']) ? 'active' : ''  !!}"><i
                    class="fa fa-dashboard fa-fw sidebar-icon"></i> <span class="sidebar-text">Dashboard</span></a>

        <div class="toggle-bar">
            <a href="#" class="slide-toggle">
                <i class="fa fa-caret-left"></i>
            </a>
        </div>
    <li><a href="{{ url('bus/ticket/') }}"
           class="first-level {{ 0 == strcasecmp('ticket', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-barcode fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Ticket" !!}</span></a></li>
    <li><a href="{{ url('boat/ticket/search') }}"
           class="first-level {{ 0 == strcasecmp('website', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-line-chart fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Search Ticket</span></a></li>
    <li class="{{ 0 == strcasecmp('setting', $c_page['main']) ? 'active' : ''  }}">
        <a href="#" class="first-level"><i class="fa fa-google-wallet fa-fw sidebar-icon"></i>
            <span class="sidebar-text">Report</span>
            <span class="fa arrow"></span></a>
        <ul class="sub-menu">
            <li>
                <a href="" class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Main Report</span></a>
            </li>
            <li>
                <a href="" class="{{ 0 == strcasecmp('boat_types', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Sale Report</span></a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <li><a href="{{ url('/') }}"
           class="{{ 0 == strcasecmp('website', $c_page['main']) ? 'active' : ''  }} first-level"><i
                    class="fa fa-tablet fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Check In</span></a></li>
    <li>
        <a href="{!! url('/auth/logout') !!}" class="first-level"><i
                    class="fa fa-sign-out fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Logout" !!}</span></a>
    </li>
</ul>
