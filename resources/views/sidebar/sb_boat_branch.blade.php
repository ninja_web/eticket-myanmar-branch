<ul class="nav" id="header-nav">
    <li class="toggle-panel">
        <a href="{!! url('/boat/dashboard') !!}"
           class="first-level {!! 0 == strcasecmp('Dashboard', $c_page['main']) ? 'active' : ''  !!}"><i
                    class="fa fa-home fa-fw sidebar-icon"></i> <span class="sidebar-text"></span></a>
    </li>
    <li><a href="{{ url('boat/agent/'.AUTHORIZE) }}"
           class="first-level {{ 0 == strcasecmp('boat_agent', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-truck fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Agent</span></a>
    </li>
    <li><a href="{{ url('/boat/schedule/create') }}"
           class="first-level {{ 0 == strcasecmp('schedule', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-exchange fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Create Schedule</span></a></li>

    <li class="{{ 0 == strcasecmp('staff', $c_page['main']) ? 'active' : ''  }}">
        <a href="#" class="first-level"><i class="fa fa-male fa-fw sidebar-icon"></i>
            <span class="sidebar-text">Staff &nbsp;</span>
            <i class="fa fa-angle-down"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="{!! url('/boat/sale')!!}"
                   class="{{ 0 == strcasecmp('sale', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Sale</span></a>
            </li>
            <li>
                <a href="{!! url('/boat/finance')!!}"
                   class="{{ 0 == strcasecmp('finance', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Finance</span></a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <li><a href="{{ url('boat/ticket/') }}"
           class="first-level {{ 0 == strcasecmp('ticket', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-barcode fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Ticket</span></a></li>
    <li><a href="{!! url('boat/ticket/manage/')!!}"
           class="{{ 0 == strcasecmp('checkin', $c_page['main']) ? 'active' : ''  }} first-level"><i
                    class="fa fa-tablet fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Manage Ticket</span></a></li>
    <li class="{{ 0 == strcasecmp('setting', $c_page['main']) ? 'active' : ''  }}">
        <a href="#" class="first-level"><i class="fa fa-google-wallet fa-fw sidebar-icon"></i>
            <span class="sidebar-text">Report &nbsp;</span>
            <i class="fa fa-angle-down"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="{!! url('boat/report/daily')!!}"
                   class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Daily Sale Report</span></a>
            </li>
            <li>
                <a href="{!! url('boat/report/main')!!}"
                   class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Main Report</span></a>
            </li>
            <li>
                <a href="{!! url('boat/report/agent_rep')!!}"
                   class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Agent Report</span></a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <li>
        <a href="{!! url('/auth/logout') !!}" class="first-level"><i
                    class="fa fa-power-off fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Logout</span></a>
    </li>
</ul>
