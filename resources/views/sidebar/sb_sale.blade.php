<ul class="nav" id="header-nav">
    <li class="toggle-panel">
        <a href="{!! url('') !!}"
           class="first-level {!! 0 == strcasecmp('dashboat', $c_page['main']) ? 'active' : ''  !!}"><i
                    class="fa fa-home fa-fw sidebar-icon"></i> <span class="sidebar-text">Dashboard</span></a>


    <li><a href="{{ url('bus/ticket/') }}"
           class="first-level {{ 0 == strcasecmp('ticket', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-barcode fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Ticket" !!}</span></a></li>
    <li><a href="{{ url('bus/ticket/searchticket') }}"
           class="first-level {{ 0 == strcasecmp('website', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-line-chart fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Search Ticket</span></a></li>

    <li><a href="{{ url('/') }}"
           class="{{ 0 == strcasecmp('website', $c_page['main']) ? 'active' : ''  }} first-level"><i
                    class="fa fa-tablet fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Check In</span></a></li>
    <li>
        <a href="{!! url('/auth/logout') !!}" class="first-level"><i
                    class="fa fa-sign-out fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Logout" !!}</span></a>
    </li>
</ul>
