<ul class="nav" id="header-nav">
    <li class="toggle-panel">
    <li class="toggle-panel">
        <a href="{!! url('/boat/dashboard') !!}"
           class="first-level {!! 0 == strcasecmp('Dashboard', $c_page['main']) ? 'active' : ''  !!}"><i
                    class="fa fa-home fa-fw sidebar-icon"></i> <span class="sidebar-text"></span></a>
    </li>
    <li class="{{ 0 == strcasecmp('setting', $c_page['main']) ? 'active' : ''  }}">
        <a href="#" class="first-level"><i class="fa fa-google-wallet fa-fw sidebar-icon"></i>
            <span class="sidebar-text">Report &nbsp;</span>
            <i class="fa fa-angle-down"></i></a>
        <ul class="sub-menu">
            <li>
                <a href="{!! url('boat/report/daily')!!}"
                   class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Daily Sale Report</span></a>
            </li>
            <li>
                <a href="{!! url('boat/report/main')!!}"
                   class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Main Report</span></a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <li>
        <a href="{!! url('/auth/logout') !!}" class="first-level"><i
                    class="fa fa-power-off fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Logout" !!}</span></a>
    </li>
</ul>
