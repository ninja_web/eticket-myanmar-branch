<ul class="nav" id="header-nav">
    <li class="toggle-panel">
        <a href="{!! url('/dashboard') !!}"
           class="first-level {!! 0 == strcasecmp('dashboard', $c_page['main']) ? 'active' : ''  !!}"><i
                    class="fa fa-home fa-fw sidebar-icon"></i> <span class="sidebar-text">Dashboard</span></a>


    <li><a href="{{ url('boat/ticket/') }}"
           class="first-level {{ 0 == strcasecmp('ticket', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-barcode fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Ticket" !!}</span></a></li>
    <li class="{{ 0 == strcasecmp('setting', $c_page['main']) ? 'active' : ''  }}">
        <a href="#" class="first-level"><i class="fa fa-google-wallet fa-fw sidebar-icon"></i>
            <span class="sidebar-text">Report</span>
            <i class="fa fa-angle-down"></i>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="{!! url('boat/report/')!!}"
                   class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Daily Sale Report</span></a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <li><a href="{!! url('boat/ticket/manage/ticketlist')!!}"
           class="{{ 0 == strcasecmp('checkin', $c_page['main']) ? 'active' : ''  }} first-level"><i
                    class="fa fa-tablet fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Manage Ticket</span></a></li>
    <li>
        <a href="{!! url('/auth/logout') !!}" class="first-level"><i
                    class="fa fa-power-off fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Logout" !!}</span></a>
    </li>
</ul>
