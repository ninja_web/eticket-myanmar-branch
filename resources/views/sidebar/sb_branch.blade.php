<ul class="nav" id="header-nav">
    <li class="toggle-panel">
        <a href="{!! url('') !!}"
           class="first-level {!! 0 == strcasecmp('dashboard', $c_page['main']) ? 'active' : ''  !!}"><i
                    class="fa fa-home fa-fw sidebar-icon"></i> <span class="sidebar-text">Dashboard</span></a>


    </li>
    <li><a href="{{ url('bus/agent/'.AUTHORIZE) }}"
           class="first-level {{ 0 == strcasecmp('bus_agent', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-truck fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Agent</span></a>
    </li>
    <li><a href="{{ url('/bus/schedule/create') }}"
           class="first-level {{ 0 == strcasecmp('schedule', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-exchange fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Create Schedule</span></a></li>

    <li class="{{ 0 == strcasecmp('staff', $c_page['main']) ? 'active' : ''  }}">
        <a href="#" class="first-level"><i class="fa fa-male fa-fw sidebar-icon"></i>
            <span class="sidebar-text">Staff</span>
            <span class="fa arrow"></span></a>
        <ul class="sub-menu">
            <li>
                <a href="{!! url('/bus/sale')!!}"
                   class="{{ 0 == strcasecmp('sale', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Sale</span></a>
            </li>
            <li>
                <a href="{!! url('/bus/finance')!!}"
                   class="{{ 0 == strcasecmp('finance', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Finance</span></a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <li><a href="{{ url('bus/ticket/') }}"
           class="first-level {{ 0 == strcasecmp('ticket', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-barcode fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Ticket" !!}</span></a></li>
    <li><a href="{{ url('bus/ticket/searchticket') }}"
           class="first-level {{ 0 == strcasecmp('website', $c_page['main']) ? 'active' : ''  }}"><i
                    class="fa fa-line-chart fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Search Ticket</span></a></li>
    <li class="{{ 0 == strcasecmp('setting', $c_page['main']) ? 'active' : ''  }}">
        <a href="#" class="first-level"><i class="fa fa-google-wallet fa-fw sidebar-icon"></i>
            <span class="sidebar-text">Report</span>
            <span class="fa arrow"></span></a>
        <ul class="sub-menu">
            <li>
                <a href="" class="{{ 0 == strcasecmp('schedules', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Main Report</span></a>
            </li>
            <li>
                <a href="" class="{{ 0 == strcasecmp('boat_types', $c_page['sub']) ? 'active' : ''  }}"><span
                            class="sub-sidebar-text">Sale Report</span></a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
    <li><a href="{{ url('/') }}"
           class="{{ 0 == strcasecmp('website', $c_page['main']) ? 'active' : ''  }} first-level"><i
                    class="fa fa-tablet fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">Check In</span></a></li>
    <li>
        <a href="{!! url('/auth/logout') !!}" class="first-level"><i
                    class="fa fa-power-off fa-fw sidebar-icon"></i> <span
                    class="sidebar-text">{!! "Logout" !!}</span></a>
    </li>
</ul>
