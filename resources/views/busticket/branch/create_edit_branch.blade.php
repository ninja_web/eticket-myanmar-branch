@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Branch"!!}</a></li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>
                @if(isset($branch)) <h4>{!! $branch->name !!}</h4> @endif
                <div class="mg10 tab-list">
                    <a href="@if(isset($branch)) {!! URL('bus/branch/'.$branch->id.'/edit')!!} @else {!!  URL('bus/agent/create')!!} @endif"
                       class="btn  btn-default active"><i
                                class="fa fa-bank fa-w"></i> &nbsp;{!! "Branch"!!}</a>


                    <a href="@if(isset($branch)) {!! URL('bus/branch/sale/'.$branch->id)!!} @else {!!  URL('#')!!} @endif"
                       class="btn  btn-default"><i
                                class="fa fa-money fa-w"></i>&nbsp;{!! "Sale"!!}</a>

                    <a href="@if(isset($branch)) {!! URL('bus/branch/finance/'.$branch->id)!!} @else {!!  URL('#')!!} @endif"
                       class="btn  btn-default"><i
                                class="fa fa-database fa-w"></i>&nbsp;{!! "Finance"!!}</a>

                </div>
                <div class="col-lg-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <p><strong class="mandatory">*</strong> All fields must be required.</p>

                    <form class="form-horizontal" enctype="multipart/form-data" method="post"
                          action="@if(isset($branch)) {{ URL::to('bus/branch/'.$branch->id.'/edit')}} @else {{ URL::to('bus/branch/create/')}} @endif"
                          autocomplete="off">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <div class="col-xs-2">
                            <div class="profile-picture">
                                <canvas id="thumb_img_canvas" style="display:none"></canvas>
                                <input type="file" id="thumb_img" name="profile"
                                       onchange="return ShowImagePreview(this.files, 'thumb_img')"
                                       class="btn btn-default"/>

                                <div class="drag-panel" id="thumb_img_upload">
                                    <p>Drag &amp; Drop your photo here for</p>
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="caption">
                                    <strong>Profile Photo</strong>
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">

                                <label class="col-xs-3 control-label">{!! "Name"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="name" class="form-control"
                                           value="{{ Input::old('name', isset($branch) ? $branch->name : null) }}"
                                           required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Branch"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <select name="branch" class="form-control chosen-select">
                                        @foreach(json_decode(BUS_BRANCH_LIST, true) as $branch_name)
                                            <option value="{!! $branch_name !!}" {!! isset($branch) && $branch_name==
                                                    $branch->branch ? 'selected="selected"' : '' !!} >{!!
                                                Str::title($branch_name) !!}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">DOB<strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8 date-picker-container">
                                    <div class="input-group date past ">
                                        <input type="text" name="dob" class="form-control"
                                               value="{{ Input::old('dob', isset($branch) ? date('d/m/Y', strtotime($branch->dob))  : null) }}"
                                               requried/>
                                        <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">Phone No<strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="phone" class="form-control"
                                           value="{{ Input::old('name', isset($branch) ? $branch->phone : null) }}"
                                    >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Contact Address<strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                                            <textarea class="form-control"
                                                                      name="contact"
                                                                      required>{!! Input::old('address', isset($branch) ? $branch->contact : null) !!}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Status"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8 radio">
                                    <label class="f-arial">
                                        {!! Form::radio('status', STATUS_ACTIVE, Input::old('status', isset($branch) ?
                                        $branch->status : null) == STATUS_ACTIVE ?
                                        TRUE : FALSE )!!} Active
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="f-arial">
                                        {!! Form::radio('status', STATUS_INACTIVE, Input::old('status', isset($branch) ?
                                        $branch->status : null) ==
                                        STATUS_INACTIVE ? TRUE : FALSE )!!} In Active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-8">
                                    <input type="reset" class="btn btn-default" value="Cancel"/>
                                    <input type="submit" class="btn btn-success" value="Submit"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!!"Email"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="email" name="email" class="form-control"
                                           value="{{ Input::old('email', isset($branch) ? $branch->email : null) }}"
                                           requried/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">NRC<strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="nrc" class="form-control"
                                           value="{{ Input::old('nrc', isset($branch) ? $branch->nrc : null) }}"
                                           required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">Parent Name<strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="parent_name" class="form-control"
                                           value="{{ Input::old('parent_name', isset($branch) ? $branch->parent_name : null) }}"
                                    >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">Address<strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <textarea class="form-control"
                                              name="address"
                                              required>{!! Input::old('address', isset($branch) ? $branch->address : null) !!}</textarea>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

    <script type="text/javascript">
        var logo = "<?php echo (isset($branch) && !empty($branch->profile) && !empty($branch->profile_path)) ? asset($branch->profile_path . $branch->profile) : '';?>";
        if (logo !== '') {
            editImagePreview('thumb_img', logo, 200, 200);
        }
    </script>
@stop
