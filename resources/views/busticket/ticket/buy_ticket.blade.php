@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li class="active">Buy Ticket</li>
                        <li class="pull-right">
                            <span id="cur-time"></span>
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success btn-outline btn-sm"
                           href="{!!  url('bus/ticket/orders/'.$schedule->id) !!}">Order Details</a>

                    </div>
                    <h3>{!! $schedule->class !!}</h3>
                    <?php $seater_plan = json_decode(BUS_SEATER_PLAN, true); ?>
                    <?php $seater_plan = json_decode($seater_plan[$schedule->seat_plan], true); ?>
                    {!! Form::open(array('url' => 'bus/ticket/buy', 'method' => 'POST', 'class' => 'form-horizontal',
                    'role' => 'form'))!!}
                    {!! Form::hidden('schedule_id', $schedule->id)!!}
                    {!! Form::hidden('seat_no', Input::get('seat_no', $seat_no), array('id' => 'hid_seat_no')) !!}
                    {!! Form::hidden('selected_route', Input::get('selected_route', $selected_route))!!}
                    <?php $take_seat_layout = !empty($schedule->take_seat_layout) ? explode(",", $schedule->take_seat_layout) : array(); ?>
                    <?php $percentage = calculatePercentage($schedule->total_seat, $schedule->remain_seat);?>
                    <?php $selected_seat = explode(',', $seat_no);?>
                    <div class="col-md-12">
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar"
                                 style="width: {!! $percentage !!}%;" id="percentage">
                                <div class="bar">{!! $percentage !!}% ( Sold )</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="text-info">
                            <span class="badge">{!! count($take_seat_layout) !!}</span> tickets sold &amp;
                            <span class="badge badge-success">{!! $schedule->remain_seat !!}</span> tickets are
                            availiable.
                        </p>

                        <div class="row">
                            <div class="car-layout {!! $seater_plan['type'] !!}">
                                <div class="seat-driver img_availiable_seat"></div>
                                @foreach($seater_plan['seat'] as $key => $value)
                                    @if("middle" == $key)
                                        <ul class="row-margin middle-row">
                                            <li></li>
                                        </ul>
                                    @else
                                        <?php $rows = explode(",", $value);?>
                                        <ul class="row-margin">
                                            @foreach($rows as $seat)
                                                <li class="seat">
                                                    <?php $seat_type = in_array($seat, $take_seat_layout) ? 'disabledSeat' : 'availableSeat' ?>
                                                    <?php $seat_type = in_array($seat, $selected_seat) ? 'selectedSeat' : $seat_type ?>
                                                    <?php $seat_type .= $seat == 43 ? '' : '';?>
                                                    <a class="{!! $seat_type !!}" id="{!! $seat !!}"></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <p>Please select the seat to buy the ticket.</span></p>

                        <p>Total : <strong id="total" class="text text-success">{!! count($selected_seat)!!}</strong>
                        </p>

                        <p>Take Seat Number: <strong id="seatNo" class="text text-success">{!! $seat_no !!}</strong></p>

                        <div class="clearfix">
                            <div class="row">
                                <ul class="car seat-description">
                                    <li><i class="seat selectedSeat"></i><span>Selected Seat</span></li>
                                    <li>
                                        <i class="seat availableSeat"></i><span>{!! Lang::get('myanmar.availiableSeat')!!}</span>
                                    </li>
                                    <li>
                                        <i class="seat disabledSeat"></i><span>{!! Lang::get('myanmar.soldSeat')!!}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php $whole_route = json_decode($schedule->whole, true); ?>
                        <?php $selected_route = explode("_", $selected_route);?>
                        <?php $price = json_decode($schedule->price, true);?>
                        <p><strong class="mandatory">*</strong> All fields must be required.</p>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Schedule</label>

                            <div class="col-sm-9">{!! Lang::get('myanmar.'.$selected_route[0])!!} {!!
                                Lang::get('myanmar.'.$selected_route[1])!!} ({!!
                                $price[$selected_route[0]."_".$selected_route[1]]!!})
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name<strong
                                        class="mandatory">*</strong></label>

                            <div class="col-sm-3">
                                <select name="name_prefix" class="form-control chosen-select">
                                    @foreach(json_decode(NAME_PREFIX, true) as $key => $value)
                                        <option value="{!! $key !!}">{!! $value !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                {!! Form::text('name', Input::old('name'), array('class' => 'form-control',
                                'required' => 'reqired'))!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email<strong
                                        class="mandatory">*</strong></label>

                            <div class="col-sm-9">
                                {!! Form::email('email', Input::old('email'), array('class' => 'form-control',
                                'required' => 'reqired'))!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">NRC<strong
                                        class="mandatory">*</strong></label>

                            <div class="col-sm-1" style="width:100px">
                                <select name="nrc_prefix" class="form-control chosen-select">
                                    @foreach(json_decode(NRC_PREFIX, true) as $key => $value)
                                        <option value="{!! $key !!}">{!! $value !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2" style="width:110px">
                                {!! Form::text('nrc_state', Input::old('nrc_state'), array('class' => 'form-control',
                                'required' => 'reqired'))!!}
                            </div>
                            <div class="col-sm-1" style="width:100px">
                                <select name="nrc_nation" class="form-control chosen-select">
                                    @foreach(json_decode(NRC_NATION, true) as $key => $value)
                                        <option value="{!! $key !!}">{!! $value !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                {!! Form::text('nrc_no', Input::old('nrc_no'), array('class' => 'form-control',
                                'required' => 'reqired'))!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Passport<strong
                                        class="mandatory">*</strong></label>

                            <div class="col-sm-9">
                                {!! Form::text('passport', Input::old('passport'), array('class' => 'form-control',
                                'required' => 'reqired'))!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phone No<strong
                                        class="mandatory">*</strong></label>

                            <div class="col-sm-9">
                                {!! Form::text('phone_no', Input::old('phone_no'), array('class' => 'form-control',
                                'required' => 'reqired'))!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Remark</label>

                            <div class="col-sm-9">
                                {!! Form::textarea('remark', Input::old('remark'),array('class' => 'form-control',
                                'rows' => 5) );!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Discount</label>

                            <div class="col-sm-9">
                                {!! Form::text('discount', Input::old('discount'), array('class' => 'form-control'))!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-offset-3  col-xs-8">
                                <input type="reset" class="btn btn-default" value="Cancel"/>
                                <input type="submit" class="btn btn-success" value="Submit"/>
                            </div>
                        </div>
                    </div>
                    {!! Form::close()!!}
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
