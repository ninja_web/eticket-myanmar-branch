@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li class="active">Ticket</li>
                        <li class="pull-right">
                            <span id="cur-time"></span>
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success btn-outline btn-sm"
                           href="{!!  url('bus/ticket/select/'.$schedule->id) !!}">Browse Ticket</a>
                    </div>
                    <h3>Choose Bus Seat</h3>
                    <?php $seater_plan = json_decode(BUS_SEATER_PLAN, true); ?>
                    <?php $seater_plan = json_decode($seater_plan[$schedule->total_seat], true); ?>
                    <?php $percentage = calculatePercentage($schedule->total_seat, $schedule->remain_seat);?>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger" role="progressbar"
                             style="width: {!! $percentage !!}%;" id="percentage">
                            <div class="bar">{!! $percentage !!}%</div>
                        </div>
                    </div>
                    {!! Form::open(array('url' => 'bus/ticket/buy', 'method' => 'POST', 'class' => 'form-horizontal',
                    'role' => 'form'))!!}
                    <div class="col-md-5">
                        <?php $take_seat_layout = explode(",", $schedule->take_seat_layout); ?>
                        <div class="row">
                            <div class="car-layout {!! $seater_plan['type'] !!}">
                                <div class="seat-driver img_availiable_seat"></div>
                                @foreach($seater_plan['seat'] as $key => $value)
                                    @if("middle" == $key)
                                        <ul class="row-margin middle-row">
                                            <li></li>
                                        </ul>
                                    @else
                                        <?php $rows = explode(",", $value);?>
                                        <ul class="row-margin">
                                            @foreach($rows as $seat)
                                                <li class="seat">
                                                    <a class="@if(in_array($seat, $take_seat_layout)) {!! 'disabledSeat' !!} @else {!! 'availableSeat' !!} @endif {!! $seat == 43 ? 'seat-right' : '' !!}"
                                                       id="{!! $seat !!}"><span class="seat-no">{!! $seat !!}</span></a>

                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="row">
                                <ul class="car seat-description">
                                    <li><i class="seat selectedSeat"></i><span>Selected Seat</span></li>
                                    <li>
                                        <i class="seat availableSeat"></i><span>{!! Lang::get('myanmar.availiableSeat')!!}</span>
                                    </li>
                                    <li>
                                        <i class="seat disabledSeat"></i><span>{!! Lang::get('myanmar.soldSeat')!!}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Seat No</th>
                                <th>Total Fare</th>
                                <th>Journey</th>
                                <th>Type</th>
                                <th>Instead</th>
                                <th>Book Time</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $index = 1;?>
                            @foreach($canceldTicket as $row)
                                <tr>
                                    <td>{!! $index !!}</td>
                                    <td>{!! $row->seat_no !!}</td>
                                    <td>{!! $row->fare * $row->total_seat !!}</td>
                                    <td>{!! Lang::get('myanmar.'.$row->from) .' - '. Lang::get('myanmar.'.$row->to)!!}
                                    </td>
                                    <td><strong class="text-success">@if(BOOK == $row->type) {!!  'BOOK' !!} @else {!!
                                            'BUY' !!} @endif </td>
                                    <td> -</td>
                                    <td>{!! displayDate($row->created_at, 'H:i A') !!}</td>
                                    <td>
                                        @if(BOOK == $row->type)
                                            <a class="text-danger cancel"
                                               href="{!! url('bus/ticket/cancel/'.$row->id.'/'.STATUS_BOOK_CANCEL)!!}"
                                               onClick="javascript:void(0); return false;"><i class="fa fa-trash-o"></i>&nbsp;Cancle
                                                Booking</a>
                                        @else
                                            <a class="text-danger cancel"
                                               href="{!! url('bus/ticket/cancel/'.$row->id.'/'.STATUS_TICKET_CANCEL)!!}"
                                               onClick="javascript:void(0); return false;"><i class="fa fa-trash-o"></i>&nbsp;Cancle
                                                Ticket</a>
                                        @endif
                                    </td>
                                </tr>
                                <?php $index += 1;?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! Form::close()!!}
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

    <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
    <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
    <script type="text/javascript">
        $(function () {
            $('.cancel').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-danger',
                    title: '<div id="dia-title">Cancle Ticket</div>',
                    message: '<div id="dia-message">Are you sure you want to cancel these ticket.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-danger',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
        });
    </script>
@stop
