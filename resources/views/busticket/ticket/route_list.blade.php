@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb f-arial">
                        <li class="active">Route List</li>
                        <li class="pull-right">
                            <span id="cur-time"></span>
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>

                    @if(isset($list) && count($list))

                        <ul class="route-list">
                            <?php $index = 1;?>
                            @foreach($list as $row)

                                <li>
                                    <a href="{!!url('bus/ticket/route/'.$row->id.'/0')!!}">

                                        {!!Str::title($row->start_branch )!!} <i
                                                class="fa fa-arrow-circle-right route-link"></i> {!! Str::title($row->end_branch)!!}

                                    </a>
                                </li>
                                <?php $index++;?>
                            @endforeach

                        </ul>

                    @endif
                </div>
            </div>
        </div>
    </div>

@stop
