@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li class="active">Ticket</li>
                        <li class="pull-right">
                            <span id="cur-time"></span>
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success btn-outline btn-sm"
                           href="{!!  url('bus/ticket/orders/'.$schedule->id) !!}">Order Details</a>

                    </div>

                    <?php $seater_plan = json_decode(BUS_SEATER_PLAN, true); ?>
                    <?php $seater_plan = json_decode($seater_plan[$schedule->seat_plan], true); ?>
                    {!! Form::open(array('url' => 'bus/ticket/browse/'.$schedule->id, 'method' => 'POST', 'class' =>
                    'form-horizontal',
                    'role' => 'form'))!!}
                    {!! Form::hidden('seat_no', "", array('id' => 'hid_seat_no')) !!}
                    <?php $take_seat_layout = explode(",", $schedule->take_seat_layout);?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info overflow">
                                <div class="col-md-4">
                                    {!! showRouteText(json_decode($schedule->whole), '&nbsp;<span class="text text-success"><i
                                                class="fa fa-long-arrow-right"></i></span>&nbsp;') !!}
                                </div>
                                <div class="col-md-5 f-arial">
                                    @if(strtotime($schedule->dept_date) > strtotime(date('Y-m-d')))
                                        <span><a href="{!! url('boat/ticket/traverse/'.$schedule->schedule_type.'/'.strtotime($schedule->dept_date . '-1 day').'/'.$schedule->boat_order)!!}"><i
                                                        class="fa fa-backward"></i></a></span>&nbsp;
                                    @endif
                                    <strong class="text text-success">{!! displayDate($schedule->dept_date)!!}</strong>&nbsp;
                                    <span><a href="{!! url('boat/ticket/traverse/'.$schedule->schedule_type.'/'.strtotime($schedule->dept_date . '+ 1day').'/'.$schedule->boat_order)!!}"><i
                                                    class="fa fa-forward"></i></a></span>
                                </div>
                                <div class="col-md-3 f-arial">
                                    {!! $schedule->boat_order !!} Boat&nbsp;&nbsp;{!! displayDate($schedule->dept_time, 'H:i
                                    A')!!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <p class="text-info">
                            <span class="badge">{!!$schedule->total_seat - $schedule->remain_seat!!}</span> tickets sold
                            &amp;
                            <span class="badge">{!! $schedule->remain_seat !!}</span> tickets are availiable.
                        </p>
                        <div class="row">
                            <div class="carr-layout {!! $seater_plan['type'] !!}">
                                <div class="seat-driver availableSeat"></div>
                                @foreach($seater_plan['seat'] as $key => $value)
                                    @if("middle" == $key)
                                        <ul class="row-margin middle-row">
                                            <li></li>
                                        </ul>
                                    @else
                                        <?php $rows = explode(",", $value);?>
                                        <ul class="row-margin">
                                            @foreach($rows as $seat)
                                                <li class="seat">
                                                    @if(!empty($seat) && $seat > 0)
                                                        <?php $seat_type = in_array($seat, $take_seat_layout) ? 'disabledSeat' : 'availableSeat' ?>
                                                        <a class="{!! $seat_type !!} bus-seat" id="{!! trim($seat) !!}"><span
                                                                    class="seat-no">{!!$seat!!}</span></a>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="panel panel-default overflow mt10">
                        <div class="col-xs-6">
                            <p style="margin-top:20px;">Please select the seat to buy the ticket.</span></p>

                            <p>Total : <strong id="total" class="text text-success"></strong></p>

                            <p>Take Seat Number: <strong id="seatNo" class="text text-success"></strong></p>


                        </div>
                        <div class="col-xs-6">
                            <ul class="car seat-description">
                                <li><i class="seat selectedSeat"></i><span>Selected Seat</span></li>
                                <li>
                                    <i class="seat availableSeat"></i><span>{!! Lang::get('myanmar.availiableSeat')!!}</span>
                                </li>
                                <li>
                                    <i class="seat disabledSeat"></i><span>{!! Lang::get('myanmar.soldSeat')!!}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php $whole_route = json_decode($schedule->whole, true); ?>
                    <?php $price = json_decode($schedule->price, true);?>
                    <ul class="list-unstyled">
                        @foreach($price as $key => $value)
                            <?php $route = explode("_", $key); ?>
                            @if(!empty($value) && ctype_digit($value))
                                <li>
                                    <label for="route">
                                        <input name="selected_route" id="route" type="radio" value="{!! $key !!}"
                                               autocomplete="off" required>&nbsp;&nbsp;&nbsp;{!!
                                            Str::title($route[0])!!} <i class="fa fa-arrow-circle-right route-link"></i> {!!
                                            Str::title($route[1])!!} ({!! $value !!})
                                    </label>
                                </li>
                    @endif
                    @endforeach
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-4">
                    <input type="submit" class="btn btn-success btn-outline btn-block" name="btn_type"
                           value="Buy Ticket"/>
                </div>
                <div class="col-xs-4">
                    <input type="submit" class="btn btn-default btn-outline btn-block" name="btn_type"
                           value="Book Ticket"/>
                </div>
                <div class="col-xs-4">
                    <a href="{!!url('bus/ticket')!!}" class="btn btn-danger btn-outline btn-block">Cancel Ticket</a>
                </div>
                {!! Form::close()!!}
                <div class="clearfix"></div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </div>
    <?php $seatHistory = Input::old("seat_no", "");?>
    <?php $seatHistory = empty($seatHistory) ? array() : explode(",", $seatHistory);?>
    <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
    <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
    <script type="text/javascript">
        $(function (e) {
            var userTakeSeat = <?php echo json_encode($seatHistory); ?>;
            var total = $('#total');
            var seatNo = $("#seatNo");
            // update error
            if (userTakeSeat.length > 0) {
                userTakeSeat = userTakeSeat.sort(sortNumber);
                // auto selected
                $.each(userTakeSeat, function (row, value) {
                    $('#' + value).removeClass('availableSeat').addClass("availableSeat selectedSeat");
                });
                total.text(userTakeSeat.length + ' seats');
                seatNo.text(userTakeSeat);
            } else {
                $('#seat-no').val();
            }

            $('ul.row-margin li.seat a').click(function (e) {
                var aClass = $(this).attr("class");
                aClass = aClass.trim();
                var selectedSeatId = $(this).attr("id").trim();

                if (aClass.trim() == "availableSeat bus-seat") {

                    $(this).removeClass(aClass).addClass("availableSeat selectedSeat bus-seat");
                    userTakeSeat.push(selectedSeatId);
                    userTakeSeat = userTakeSeat.sort(sortNumber);
                    total.text(userTakeSeat.length + ' seats');
                    seatNo.text(userTakeSeat);
                } else if (aClass.trim() == "availableSeat selectedSeat") {

                    $(this).removeClass(aClass).addClass("availableSeat");
                    userTakeSeat.pop(selectedSeatId);
                    userTakeSeat = userTakeSeat.sort(sortNumber);
                    if (userTakeSeat.length > 0) {
                        total.text(userTakeSeat.length + ' seats');
                        seatNo.text(userTakeSeat);
                    } else {
                        total.text('');
                        seatNo.text("");
                    }
                }
                // adding to the cartSeat(Basket)
                $("#hid_seat_no").val(userTakeSeat);
            });

            function sortNumber(a, b) {
                return a - b;
            }

            $('.activate').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-success',
                    title: '<div id="dia-title">Cancle Ticket</div>',
                    message: '<div id="dia-message">Are you sure you want to activate these ticket.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-success',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
        })
    </script>
@stop
