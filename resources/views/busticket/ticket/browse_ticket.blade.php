@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li class="active">Ticket</li>
                        <li class="pull-right">
                            <span id="cur-time"></span>
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <h3>Choose Bus Seat</h3>
                    <?php $seater_plan = json_decode(BUS_SEATER_PLAN, true); ?>
                    <?php $seater_plan = json_decode($seater_plan[$schedule->total_seat], true); ?>
                    {!! Form::open(array('url' => 'bus/ticket/buy', 'method' => 'POST', 'class' => 'form-horizontal',
                    'role' => 'form'))!!}
                    <div class="col-md-5">
                        <div class="row">
                            <div class="car-layout {!! $seater_plan['type'] !!}">
                                <div class="seat-driver img_availiable_seat availableSeat"></div>
                                @foreach($seater_plan['seat'] as $key => $value)
                                    @if("middle" == $key)
                                        <ul class="row-margin middle-row">
                                            <li></li>
                                        </ul>
                                    @else
                                        <?php $rows = explode(",", $value);?>
                                        <ul class="row-margin">
                                            @foreach($rows as $seat)
                                                <li>
                                                    <a class="seat availiableSeat {!! $seat == 43 ? 'seat-right' : '' !!}"
                                                       id="{!! $seat !!}"></a>
                                                    <span class="seat-no">{!! $seat !!}</span>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="row">
                                <ul class="seat-description">
                                    <li><i class="seat selectedSeat"></i><span>Selected Seat</span></li>
                                    <li><i class="seat availiableSeat"></i><span>Availiable Seat</span></li>
                                    <li><i class="seat disabledSeat"></i><span>Block Seat</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php $whole_route = json_decode($schedule->whole, true); ?>
                        <table class="table table-condensed">
                            <thead>
                            <tr class="success">
                                <?php for($i = 0; $i < count($whole_route) ; $i++) { ?>
                                @if($i > 0)
                                    <th width="15%"><strong class="text-success" id="route-text-2">{!!
                                            Lang::get('myanmar.'.$whole_route[$i])!!}</strong></th>
                                    @if($i != count($whole_route) - 1)
                                        <th width="5%"><i class="fa fa-long-arrow-right"></i></th>
                                    @endif
                                @else
                                    <th colspan="2" width="15%"></th>
                                @endif
                                <?php }?>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php for($i = 0; $i < count($whole_route) ; $i++) { ?>
                                @if($i == 0)
                                    <td><strong class="text-success" id="route-text-2">{!!
                                            Lang::get('myanmar.'.$whole_route[$i])!!}</strong></td>
                                @else
                                    @if($i != count($whole_route))
                                        <td width="5%"><i class="fa fa-long-arrow-right"></i></td>
                                    @endif
                                    <td>
                                        <label>
                                            <input type="radio" name="optionsRadios" value="option3"> &nbsp;1200</label>
                                    </td>
                                @endif
                                <?php }?>
                            </tr>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-6">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    {!! Form::close()!!}
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
