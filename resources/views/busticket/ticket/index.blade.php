@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li class="active">Ticket</li>
                        <li class=""><a
                                    href="#">{!!showRouteText(json_decode($info->whole, true), SCHEDULE_SEPERATOR)!!}</a>
                        </li>
                        <li class="pull-right">
                            <span id="cur-time"></span>
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <div class="list-search-panel">
                        {!! Form::open(array('url'=> '/bus/ticket/search','class' => 'form-inline', 'role' =>
                        'form'))!!}
                        <div class="form-group date-picker-container mw-200">
                            <div class="input-group date f-arial future">
                                <input type="text" name="from" class="form-control"
                                       value="{{ Input::get('from', date('d/m/Y')) }}"
                                       requried/>
                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>

                        </div>

                        <input type="hidden" name="id" value="{!!$id!!}">
                        <label>To</label>

                        <div class="form-group date-picker-container mw-200">
                            <div class="input-group date f-arial future">
                                <input type="text" name="to" class="form-control"
                                       value="{{ Input::get('to', '') }}"
                                       requried/>
                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <select name="bus_schedule" class="form-control chosen-select">
                                <option value="">Select Destination</option>
                                @foreach(json_decode($scheduleTypes, true) as $key => $value)
                                    <option value="{!! $key !!}" <?php echo Input::get('bus_schedule') == $key ? 'selected="selected"' : ''?>>
                                        {!! Str::title($value['english']) !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="bus_class" class="form-control chosen-select" style="width:200px;">
                                <option value="">Select Bus Class</option>
                                @foreach($busClasses as $key => $value)
                                    <option value="{!! $key !!}" <?php echo Input::old('bus_class') == $key ? 'selected="selected"' : ''?>>
                                        {!! $value['class'] !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Search"/>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <table class="table  table-striped table-hover tb-bnone">
                        <thead>
                        <tr class="f-arial">

                            <th class="text-center">Bus Class</th>
                            <th class="text-center">Time</th>
                            <th class="text-center">Bus Order</th>
                            <th class="text-center">Bus Class</th>
                            <th class="text-center">Sold</th>
                            <th class="text-center">Available</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = 1;?>
                        @foreach($schedules as $schedule)
                            <tr>

                                <?php $img_path = $schedule->logo_path . $schedule->logo;?>
                                <?php $img_path = empty($img_path) ? '' : asset($img_path); ?>
                                <td class="text-center">
                                    <img src="{!! $img_path !!}" width="100" height="100" class="img">
                                </td>
                                <td class="text-center f-arial">{!! displayDate($schedule->dept_time, 'h:i A') !!}</td>
                                <td class="text-center f-arial">{!! $schedule->bus_order ." Boat "!!}</td>
                                <td class="text-center f-arial">{!! $schedule->bus_class !!}</td>
                                <td class="text-center f-arial"><span
                                            class="badge badge-danger">{!! $schedule->total_seat - $schedule->remain_seat !!}</span>
                                </td>
                                <td class="text-center f-arial"><span
                                            class="badge badge-green">{!! $schedule->remain_seat !!}</span>
                                </td>

                                <td class="text-center">
                                    <a href="{!! url('bus/ticket/select/'.$schedule->id)!!}"
                                       class="btn btn-sm  btn-success btn-outline f-arial">Browse Ticket</a>

                                </td>
                                <td class="remark">{!! $schedule->remark !!}</td>
                            </tr>
                            <?php $index += 1;?>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- End Table -->
                    <div class="clearfix"></div>
                    <div class="pag-panel">
                        <div class="pull-right">
                            {!! $schedules->render() !!}
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
