@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li class="active">Buy Ticket</li>
                        <li class="pull-right">
                            <span id="cur-time"></span>
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success btn-outline btn-sm"
                           href="{!!  url('bus/ticket/orders/'.$schedule->id) !!}">Order Details</a>

                    </div>
                    <h3>{!! $schedule->class !!}</h3>
                    <?php $seater_plan = json_decode(BUS_SEATER_PLAN, true); ?>
                    <?php $seater_plan = json_decode($seater_plan[$schedule->seat_plan], true); ?>
                    {!! Form::open(array('url' => 'bus/ticket/passenger/'.$ticket->id, 'method' => 'POST', 'class' =>
                    'form-horizontal',
                    'role' => 'form'))!!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info overflow">
                                <div class="col-md-4">
                                    {!! showRouteText(json_decode($schedule->whole), '&nbsp;<span class="text text-success"><i
                                                class="fa fa-long-arrow-right"></i></span>&nbsp;') !!}
                                </div>
                                <div class="col-md-5 f-arial">
                                    @if(strtotime($schedule->dept_date) > strtotime(date('Y-m-d')))
                                        <span><a href="{!! url('boat/ticket/traverse/'.$schedule->schedule_type.'/'.strtotime($schedule->dept_date . '-1 day').'/'.$schedule->boat_order)!!}"><i
                                                        class="fa fa-backward"></i></a></span>&nbsp;
                                    @endif
                                    <strong class="text text-success">{!! displayDate($schedule->dept_date)!!}</strong>&nbsp;
                                    <span><a href="{!! url('boat/ticket/traverse/'.$schedule->schedule_type.'/'.strtotime($schedule->dept_date . '+ 1day').'/'.$schedule->boat_order)!!}"><i
                                                    class="fa fa-forward"></i></a></span>
                                </div>
                                <div class="col-md-3 f-arial">
                                    {!! $schedule->boat_order !!} Boat&nbsp;&nbsp;{!! displayDate($schedule->dept_time, 'H:i
                                    A')!!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $take_seat_layout = !empty($schedule->take_seat_layout) ? explode(",", $schedule->take_seat_layout) : array(); ?>
                    <?php $percentage = calculatePercentage($schedule->total_seat, $schedule->remain_seat);?>
                    <?php $selected_seat = explode(',', $ticket->seat_no);?>


                    <div class="col-md-12">
                        <p class="text-info">
                            <span class="badge">{!! count($take_seat_layout) !!}</span> tickets sold &amp;
                            <span class="badge badge-success">{!! $schedule->remain_seat !!}</span> tickets are
                            availiable.
                        </p>

                        <div class="row">
                            <div class="carr-layout {!! $seater_plan['type'] !!}">
                                <div class="seat-driver availableSeat"></div>
                                @foreach($seater_plan['seat'] as $key => $value)
                                    @if("middle" == $key)
                                        <ul class="row-margin middle-row">
                                            <li></li>
                                        </ul>
                                    @else
                                        <?php $rows = explode(",", $value);?>
                                        <ul class="row-margin">
                                            @foreach($rows as $seat)
                                                <li class="seat">
                                                    @if(!empty($seat) && $seat > 0)
                                                        <?php $seat_type = in_array($seat, $take_seat_layout) ? 'disabledSeat bus-seat' : 'availableSeat bus-seat' ?>
                                                        <?php $seat_type = in_array($seat, $selected_seat) ? 'selectedSeat bus-seat' : $seat_type ?>
                                                        <a class="{!! $seat_type !!}" id="{!! $seat !!}"><span
                                                                    class="seat-no">{!!$seat!!}</span></a>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="panel panel-default overflow mt10">
                                <div class="col-xs-6">
                                    <p class="mt10">Please select the seat to buy the ticket.</span></p>

                                    <p>
                                        Total : <strong id="total"
                                                        class="text text-success">{!!count($ticket->total_seat)!!}</strong>
                                    </p>
                                    <p>
                                        Take Seat Number:
                                        <strong id="seatNo" class="text text-success">{!! $ticket->seat_no!!}</strong>
                                    </p>
                                </div>
                                <div class="col-xs-6">
                                    <ul class="car seat-description">
                                        <li><i class="seat selectedSeat"></i><span>Selected Seat</span></li>
                                        <li>
                                            <i class="seat availableSeat"></i><span>{!! Lang::get('myanmar.availiableSeat')!!}</span>
                                        </li>
                                        <li>
                                            <i class="seat disabledSeat"></i><span>{!! Lang::get('myanmar.soldSeat')!!}</span>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default mt10 overflow">
                        <div class="col-xs-12 mt10">
                            <?php $whole_route = json_decode($schedule->whole, true); ?>
                            <?php $selected_route = array($ticket->from, $ticket->to); ?>
                            <?php $price = json_decode($schedule->price, true);?>
                            <p><strong class="mandatory">*</strong> All fields must be required.</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Schedule</label>

                                <div class="col-sm-9">{!! Str::title($selected_route[0])!!} {!!
                                    Str::title($selected_route[1])!!} ({!!
                                    $price[$selected_route[0]."_".$selected_route[1]]!!})
                                </div>
                            </div>
                            @if(BUY == $ticket->type)
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Name<strong
                                                class="mandatory">*</strong></label>

                                    <div class="col-sm-3">
                                        <select name="name_prefix" class="form-control chosen-select">
                                            @foreach(json_decode(NAME_PREFIX, true) as $key => $value)
                                                <option value="{!! $key !!}">{!! $value !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::text('name', Input::old('name', $ticket->name), array('class' =>
                                        'form-control',
                                        'required' => 'reqired'))!!}
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-9">
                                    {!! Form::email('email', Input::old('email', $ticket->email), array('class' =>
                                    'form-control'))!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">NRC</label>

                                <div class="col-sm-1" style="width:100px">
                                    <select name="nrc_prefix" class="form-control chosen-select">
                                        @foreach(json_decode(NRC_PREFIX, true) as $key => $value)
                                            <option value="{!! $key !!}">{!! $value !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2" style="width:110px">
                                    {!! Form::text('nrc_state', Input::old('nrc_state', $ticket->nrc_state), array('class'
                                    => 'form-control'))!!}
                                </div>
                                <div class="col-sm-1" style="width:100px">
                                    <select name="nrc_nation" class="form-control chosen-select">
                                        @foreach(json_decode(NRC_NATION, true) as $key => $value)
                                            <option value="{!! $key !!}">{!! $value !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    {!! Form::text('nrc_no', Input::old('nrc_no', $ticket->nrc_no), array('class' =>
                                    'form-control'))!!}
                                </div>
                            </div>
                            @if(BUY == $ticket->type)
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Passport</label>

                                    <div class="col-sm-9">
                                        {!! Form::text('passport', Input::old('passport', $ticket->passport), array('class'
                                        => 'form-control'))!!}
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Phone No<strong
                                            class="mandatory">*</strong></label>

                                <div class="col-sm-9">
                                    {!! Form::text('phone_no', Input::old('phone_no', $ticket->phone_no), array('class' =>
                                    'form-control'))!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Remark</label>

                                <div class="col-sm-9">
                                    {!! Form::textarea('remark', Input::old('remark', $ticket->remark),array('class' =>
                                    'form-control',
                                    'rows' => 5) );!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Discount</label>

                                <div class="col-sm-9">
                                    {!! Form::text('discount', Input::old('discount', $ticket->discount), array('class' =>
                                    'form-control'))!!}
                                </div>
                            </div>
                            @if(BOOK == $ticket->type)
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Name<strong
                                                class="mandatory">*</strong></label>

                                    <div class="col-sm-3">
                                        <select name="name_prefix" class="form-control chosen-select">
                                            @foreach(json_decode(NAME_PREFIX, true) as $key => $value)
                                                <option value="{!! $key !!}">{!! $value !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::text('name', Input::old('name', $ticket->name), array('class' =>
                                        'form-control',
                                        'required' => 'reqired'))!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Agent</label>

                                    <div class="col-sm-9">
                                        <select class="form-control chosen-select" name="agent">
                                            <option>Select Agent</option>
                                            @foreach($agent as $row)
                                                <option value="{!! $row->id !!}" <?php echo $row->id == $ticket->agent_id ? 'selected="selected"' : '' ?>>
                                                    {!! $row->name !!}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-xs-offset-3  col-xs-8">
                                    <input type="reset" class="btn btn-default" value="Cancel"/>
                                    <input type="submit" class="btn btn-success" value="Submit"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close()!!}
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
