@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Bus Type"!!}</a></li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <p><strong class="mandatory">*</strong> All fields must be required.</p>

                    <form class="form-horizontal" enctype="multipart/form-data" method="post"
                          action="@if(isset($busClass)) {{ URL::to('bus/busClass/'.$busClass->id.'/edit')}} @else {{ URL::to('bus/busClass/create/')}} @endif"
                          accept-charset="utf-8" autocomplete="off" id="frmSchedule">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <div class="col-lg-3">
                            <div class="profile-picture">
                                <canvas id="thumb_img_canvas" style="display:none"></canvas>
                                <input type="file" id="thumb_img" name="logo"
                                       onchange="return ShowImagePreview(this.files, 'thumb_img')"
                                       class="btn btn-default"/>

                                <div class="drag-panel" id="thumb_img_upload">
                                    <p>Drag &amp; Drop your photo here for</p>
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="caption">
                                    <strong><i class="fa fa-user"></i>&nbsp;Upload Logo</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">{!! 'Class Name'!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7">
                                    <input type="text" name="class" class="form-control"
                                           value="{{ Input::old('class', isset($busClass) ? $busClass->class : null) }}"
                                           required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">{!! "Class Type" !!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7">
                                    <input type="text" name="class" class="form-control"
                                           value="{{ Input::old('class', isset($busClass) ? $busClass->class : null) }}"
                                           required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-4 control-label">{!! 'Total Seater Plan' !!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7">
                                    <select name="seater" class="form-control chosen-select" id="seater">
                                        <option value="">Select Seater</option>
                                        @foreach(json_decode(BUS_SEATER, true) as $key => $value)
                                            <option value="{!! $key !!}" {!! Input::old('seater', isset($busClass) ?
                                            $busClass->seater : '' ) == $key ? 'selected="selected"' : '' !!} >{!! Str::title($value)!!}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">{!! "Status"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7 radio">
                                    <label class="f-arial">
                                        {!! Form::radio('status', STATUS_ACTIVE, Input::old('status', isset($busClass) ?
                                        $busClass->status : null) == STATUS_ACTIVE ?
                                        TRUE : FALSE )!!} Active
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="f-arial">
                                        {!! Form::radio('status', STATUS_INACTIVE, Input::old('status', isset($busClass)
                                        ?
                                        $busClass->status : null) ==
                                        STATUS_INACTIVE ? TRUE : FALSE )!!} In Active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-4 col-xs-8">
                                    <input type="reset" class="btn btn-default" value="Cancel"/>
                                    <input type="submit" class="btn btn-success" value="Submit"/>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-5">
                        <!-- Handling the seater plan -->
                        <?php $bus_seater = json_decode(BUS_SEATER, true); ?>
                        @foreach(json_decode(BUS_SEATER_PLAN, true) as $key => $value)
                            <div id="{!! $bus_seater[$key] !!}" class="hide">
                                <?php $seater_plan = json_decode($value, true);?>
                                <h4><strong class="text-success">{!! $key !!} - Seater</strong></h4>

                                <div class="car-layout {!! $seater_plan['type'] !!}">
                                    <div class="seat-driver img_availiable_seat"></div>
                                    @foreach($seater_plan['seat'] as $key => $value)
                                        @if("middle" == $key)
                                            <ul class="row-margin middle-row">
                                                <li></li>
                                            </ul>
                                        @else
                                            <?php $rows = explode(",", $value);?>
                                            <ul class="row-margin">
                                                @foreach($rows as $seat)
                                                    <li class="seat">
                                                        @if(!empty($seat))
                                                            <a class="availableSeat"><span
                                                                        class="seat-no">{!! $seat !!}</span></a>

                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        <div class="clearfix"></div>
                        @if(isset($busClass))
                            <div class="mt10">
                                <div class="area-panel a_photo-form s-bradious">
                                    <div class="a-cover-panel s-bradius">
                                        <form action="" method="post" id="photo-upload" enctype="multipart/form-data">
                                            <input type="file" name="uploadImage[]" id="p-photo" multiple="multiple">
                                        </form>
                                        <div class="drag-info-panel">
                                            <h4 class="drag-info">Drag &amp; Drop your photo here.</h4>

                                            <p class="text-center"><label class="fa fa-file-photo-o fa-5x"
                                                                          style="color:#999;" for="img"></label></p>
                                        </div>
                                        <div class="photoloading-bar" id="loadbar">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <br/>

                                <p>Photo List</p>

                                <div id="sort-message"></div>
                                <div
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(function () {

            /* Checking Form Validation */

            $("form#frmSchedule").on("submit", function (e) {

                e.preventDefault();

                var $val = $('select[name="seater"]').val();

                if ($val.length == 0) {

                    $message = "Please Choose Total Seater Plan.";

                    messagebar_toggle($message, 'info', 4000);


                    return false;
                }


                this.submit();

            });


        });


        var logo = "<?php echo (isset($busClass) && !empty($busClass->logo) && !empty($busClass->logo_path)) ? asset($busClass->logo_path . $busClass->logo) : '';?>";
        if (logo !== '') {
            editImagePreview('thumb_img', logo, 200, 200);
        }
        $('.time-picker').timepicker({
            minuteStep: 5
        });
    </script>
    <script type="text/javascript">
        var seater_type = ['54_seater', '48_seater', '45_seater', '44_seater', '40_seater', '29_seater', '28_seater', '27_seater', '20_seater', '12_seater'];
        $('#seater').change(function () {
            var select_seat = $(this).val();
            $.each(seater_type, function (index, value) {
                if (select_seat + '_seater' == value) {
                    $('#' + select_seat + '_seater').hasClass('hide') ? $('#' + select_seat + '_seater').removeClass('hide') : '';
                } else {
                    console.log('difference');
                    $('#' + value).hasClass('hide') ? '' : $('#' + value).addClass('hide');
                }
            });
        });
    </script>
@stop
