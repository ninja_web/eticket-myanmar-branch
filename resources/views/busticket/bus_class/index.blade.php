@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid f-arial">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-taxi"></i> Bus Type</a></li>
                            <li>
                                <a class="btn btn-system f-arial" href="{{ url('/bus/busClass/create') }}"><i
                                            class="fa fa-plus"></i> New Bus Type</a>
                            </li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <table class="table table-bordered table-striped table-hover" id="tbList">
                        <thead>
                        <tr class="f-arial">
                            <th class="text-center">No</th>
                            <th class="text-center"><i class="fa fa-exchange"></i>&nbsp;&nbsp;Class Name</th>
                            <th class="text-center"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Seater Type</th>
                            <th class="text-center">Created By</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = 1;?>
                        @foreach($busClass as $class)
                            <tr>
                                <td class="text-center">{!! $index; !!}</td>
                                <td class="text-center">{!! $class->class !!}</td>
                                <td class="text-center">{!! Lang::get('myanmar.'.$class->seater.'_seater') !!}</td>
                                <td class="text-center">{!! $class->user_name !!}</td>
                                <td class="text-center">{!! displayDate($class->created_at)!!}</td>
                                <td class="text-center">{!! showStatus($class->status)!!}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-default btn-outline"
                                       href="{!! url('bus/busClass/'.$class->id.'/edit') !!}">
                                        <i class="fa fa-edit"></i> Edit</a>
                            </tr>
                            <?php $index++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
