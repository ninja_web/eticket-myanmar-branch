@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">Bus Agent Commission</a></li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <h4>{!! $bus_agent->name !!}</h4>

                    <p><strong class="mandatory">*</strong> All fields must be required.</p>
                    {!! Form::open(array('url' => 'bus/agent/commission/'.$bus_owner.'/'.$agent_code, 'method' =>
                    'POST'));!!}
                    <table class="table table-bordered table-stripped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Whole Schedule</th>
                            <th>Comission History</th>
                            <th>Start Branch</th>
                            <th>End Branch</th>
                            <th>New Commission Amount</th>
                        </tr>
                        </thead>
                        <?php $index = 1; ?>
                        <tbody>
                        @foreach($schedules as $row)
                            <tr>
                                <td>{!! $index !!}</td>
                                <td>{!! showRouteText(json_decode($row->whole, true))!!}</td>
                                <td>
                                    @if($row->history != null)
                                        <ol>
                                            @foreach($row->history as $history)
                                                <li><strong class="text-success">{!! $history !!}</strong></li>
                                            @endforeach
                                        </ol>
                                    @endif
                                </td>
                                <td>{!! Lang::get('myanmar.'.$row->start_branch)!!}</td>
                                <td>{!! Lang::get('myanmar.'.$row->end_branch)!!}</td>
                                <td><input type="number" name="schedule_type[{!! $row->id !!}]" value=""/></td>
                            </tr>
                            <?php $index++;?>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="form-group text-center">
                        <input type="reset" class="btn btn-default" value="Cancel"/>
                        <input type="submit" class="btn btn-success" value="Changes Comission"/>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop
