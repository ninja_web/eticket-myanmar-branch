@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Schedule"!!}</a></li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="list-search-panel">
                        <a class="btn btn-outline btn-success" href="{{ url('/bus/branch/create') }}"><i
                                    class="fa fa-taxi"></i>&nbsp;{!! "Schedule" !!}</a>
                    </div>
                    <form class="form-inline" enctype="multipart/form-data" method="post"
                          action="#"
                          autocomplete="off">
                        <div class="form-group date-picker-container">
                            <div class="input-group date">
                                <input type="text" name="dob" class="form-control"
                                       value="{{ Input::old('dob', '') }}"
                                       requried/>
                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        <label>To</label>

                        <div class="form-group date-picker-container">
                            <div class="input-group date">
                                <input type="text" name="dob" class="form-control"
                                       value="{{ Input::old('dob', '') }}"
                                       requried/>
                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <select name="branch" class="form-control chosen-select" style="width:200px;">
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Search"/>
                        </div
                    </form>
                </div>
            </div>
        </div>
@stop