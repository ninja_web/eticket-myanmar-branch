@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Schedule"!!}</a></li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <p><strong class="mandatory">*</strong> All fields must be required.</p>
                    <form class="form-horizontal" method="post"
                          action="@if(isset($busClass)) {{ URL::to('bus/schedule/'.$busClass->id.'/edit')}} @else {{ URL::to('bus/schedule/create/')}} @endif"
                          autocomplete="off" id="frmSchedule">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" value="" name="block_seat" id="block-seat"/>


                        <div class="col-lg-6">
                            <legend class="text-success f-arial">Bus Schedule</legend>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">{!! "Destination"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>


                                <div class="col-xs-7">
                                    <select class="form-control chosen-select" id="destination"
                                            name="destination">
                                        <option value="">Select Destination</option>
                                        @foreach(json_decode($schedules, true) as $key => $value)
                                            <option value="{!! $key !!}" <?php echo Input::old('destination') == $key ? 'selected="selected"' : ''?>>
                                                {!! Str::title($value['english']) !!}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="details" class="">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">{!! "Time"!!} <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7">
                                        <select class="form-control chosen-select" name="dept_time"
                                                id="time-sequence"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">{!! 'Bus Order' !!} <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7">
                                        <select class="form-control chosen-select" name="bus_order">
                                            <option>Bus Order</option>
                                            @foreach(range(1, 20) as $key)
                                                <option value="{!! $key !!}" {!! Input::old('bus_order') == $key ?
                                                'selected="selected"' : '' !!}>{!! $key." - Bus"!!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Permission
                                        <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7">
                                        <select class="form-control" name="permission">
                                            @<?php foreach (json_decode(SCHEDULE_PERMISSION, true) as $key => $value): ?>
                                            <option value="{!! $key !!}" {!! Input::old(
                                            'permission' ) == $key ? 'selected="selected"' : ''!!}>{!!
                                            $value!!}</option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">{!! "Start Date"!!}
                                        <strong class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7 date-picker-container">
                                        <div class="input-group date future">
                                            <input type="text" name="start_date" class="form-control"
                                                   value="{!! Input::old('start_date', isset($branch) ? $branch->dob :date('d/m/Y')) !!}"
                                                   requried/>
                                            <span class="input-group-addon"><i
                                                        class="fa fa-calendar-o"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-7 col-xs-offset-4 date-picker-container">
                                        <label class="control-label pl18">
                                            <a href="#" id="toggleRepeat" class="f-arial">
                                                <i class="fa fa-edit"></i>&nbspRepeat
                                                Schedule</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group repeat">
                                    <label class="col-xs-4 control-label">{!! 'End Date'!!} <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7 date-picker-container">
                                        <div class="input-group date future">
                                            <input type="text" name="end_date" class="form-control"
                                                   value="{{ Input::old('end_date', isset($branch) ? $branch->dob : null) }}"
                                                   requried/>
                                            <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group repeat">
                                    <label class="col-xs-4 control-label">{!! 'Repeat Option'!!}
                                        <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7">
                                        <select class="form-control" name="repeat_type">
                                            @<?php foreach (json_decode(REPEAT_TYPE, true) as $key => $value): ?>
                                            <option value="{!! $key !!}" {!! Input::old(
                                            'repeat_type' ) == $key ? 'selected="selected"' : ''!!}>{!!
                                            $value!!}</option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <legend class="text-success f-airal">Bus Class</legend>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">{!! 'Bus Class'!!} <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7">
                                        <select class="form-control chosen-select" name="bus_class" id="bus-class">
                                            <option value="">Select Bus Class</option>
                                            @foreach($bus_classes as $key => $value)
                                                <option value="{!! $key !!}">{!! $value['class'].' ('. $value['seater'].'
                                                    seater )'!!}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- Block Seat -->
                                <div class="photoloading-bar" id="loadbar"></div>
                                <div class="car-class"></div>
                                <div class="form-group">
                                    <div class="col-xs-offset-4 col-xs-8">
                                        <input type="reset" class="btn btn-default" value="Cancel"/>
                                        <input type="submit" class="btn btn-success" value="Submit"/>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-6">
                            <legend class="text-success f-arial">Price</legend>
                            <div id="whole-route"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $("form#frmSchedule").on("submit", function (e) {

                e.preventDefault();
                var $dest = $("select[name='destination']").val();

                if ($dest.length == 0) {

                    messagebar_toggle("Please choose Destination.", 'info', 4000);

                    return false;
                }

                var $dept_time = $("select[name='dept_time']").val();

                if ($dept_time.length == 0) {

                    messagebar_toggle("Please choose Time.", 'info', 4000);

                    return false;
                }

                var $bus_order = $("select[name='bus_order']").val();

                if ($bus_order == "Bus Order") {

                    messagebar_toggle("Please choose Bus Order.", 'info', 4000);

                    return false;
                }

                var $start_date = $("input[name='start_date']").val();

                if ($start_date.length == 0) {

                    messagebar_toggle("Please choose Start Date.", 'info', 4000);

                    return false;
                }

                var $bus_class = $("select[name='bus_class']").val();

                if ($bus_class.length == 0) {

                    messagebar_toggle("Please choose Bus Class.", 'info', 4000);

                    return false;
                }

                this.submit();
                // return false;
            });
        });
    </script>

    <script type="text/javascript">
        var destination = <?php echo $schedules;?>;
        $(function (e) {
            $('.repeat').hide();

            // toggle the bus class
            $('#bus-class').change(function () {
                $("#loadbar").show();
                // call the loading page
                var select_class = $(this).val();
                var url = '/bus/schedule/create/toggleClass/' + select_class;
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        $(".car-class").html(data);
                        $(".car-class").fadeIn().delay(2000);
                    },
                    error: function () {

                    }
                });
            });
            // toggle the repeat link
            $('#toggleRepeat').click(function () {
                $('.repeat').show();
            });

            // toggle the destination
            $('#destination').change(function () {
                // change the price text boxs list
                var id = $(this).val();
                // get the destination details
                var selectedRoute = destination[id];
                // update the time sequences
                $('#time-sequence').empty();
                var opt = $('<option />');
                opt.val('');
                opt.text('Select Time');
                $('#time-sequence').append(opt);
                $.each(selectedRoute['time_sequence'], function (id, value) {
                    // check model_id
                    var opt = $('<option />');
                    opt.val(value);
                    opt.text(value);
                    $('#time-sequence').append(opt);
                });
                $("#time-sequence").trigger("chosen:updated");

                // geneate the price setting
                var whole_route = "";
                var engRoute = selectedRoute['eng_whole'];
                for (var i = 0; i < selectedRoute['total_branch']; i++) {
                    for (var j = i + 1; j < selectedRoute['total_branch']; j++) {
                        whole_route += '<div class="form-group">'
                                + '<label class="col-xs-6 control-label">' + camelCase(engRoute[i]) + '&nbsp;&nbsp;<i class="fa fa-arrow-circle-right route-link"></i>&nbsp;&nbsp;' + camelCase(engRoute[j]);
                        if (i == 0 & j == selectedRoute['total_branch'] - 1) {
                            whole_route += '<strong class="mandatory">*</strong></label>';
                        } else {
                            whole_route += '</label>'
                        }
                        whole_route += '<label class="control-label colon">:</label><div class="from-control col-xs-5">';
                        if (i == 0 & j == selectedRoute['total_branch'] - 1) {
                            whole_route += '<input type="text" class="form-control" autocomplete="off" name="' + engRoute[i] + '_' + engRoute[j] + '" id="' + engRoute[i] + '_' + engRoute[j] + '" required="required" onkeydown="return numberOnly(event)">';
                            whole_route += '<input type="hidden" name="selected_route" value="0">';
                            whole_route += '<input type="hidden" name="route" value="' + engRoute[i] + '_' + engRoute[j] + '">';
                        } else {
                            whole_route += '<input type="text" class="form-control" autocomplete="off" name="' + engRoute[i] + '_' + engRoute[j] + '" id="' + engRoute[i] + '_' + engRoute[j] + '" onkeydown="return numberOnly(event)">';
                        }
                        whole_route += '</div></div>';
                    }
                }
                $('#whole-route').html(whole_route);
            });
            function camelCase(str) {
                return str.replace(/(?:^|\s)\w/g, function (match) {
                    return match.toUpperCase();
                });
            }
        });
    </script>
@stop
