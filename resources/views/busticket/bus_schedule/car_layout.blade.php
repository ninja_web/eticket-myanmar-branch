<?php
if (SEATER_54 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_54_PLAN, true);
} else if (SEATER_48 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_48_PLAN, true);
} else if (SEATER_45 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_45_PLAN, true);
} else if (SEATER_44 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_44_PLAN, true);
} else if (SEATER_40 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_40_PLAN, true);
} else if (SEATER_29 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_29_PLAN, true);
} else if (SEATER_28 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_28_PLAN, true);
} else if (SEATER_27 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_27_PLAN, true);
} else if (SEATER_20 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_20_PLAN, true);
} else if (SEATER_12 == $bus_class->seater) {
    $seater_plan = json_decode(SEATER_12_PLAN, true);
} else {
    $seater_plan = array();
}
?>
<h4 id="lbl-total-seat"><strong class="text-success f-arial">Total Availiable Seat - <span
                id="total-seat">{!! $bus_class->seater!!}</span></strong>
</h4>
<div class="row">
    <div class="col-md-12">
        <div class="carr-layout {!! $seater_plan['type'] !!}">
            <div class="seat-driver availableSeat"></div>
            @foreach($seater_plan['seat'] as $key => $value)
                @if("middle" == $key)
                    <ul class="row-margin middle-row">
                        <li></li>
                    </ul>
                @else
                    <?php $rows = explode(",", $value);?>
                    <ul class="row-margin">
                        @foreach($rows as $seat)
                            <li class="car-seat seat">
                                @if(!empty($seat) && $seat > 0)
                                    <a class="availableSeat bus-seat" id="{!!$seat!!}">
                                        <span class="seat-no">{!! $seat !!}</span>
                                    </a>

                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif
            @endforeach
        </div>
    </div>
</div>
<div class="clearfix">
    <div class="row">
        <ul class="car seat-description">
            <li><i class="seat availableSeat"></i><span>Availiable Seat</span></li>
            <li><i class="seat disabledSeat"></i><span>Block Seat</span></li>
        </ul>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var selectId = new Array();
            var totalSeat = "<?php echo $bus_class->seater?>";

            // update the seat availiable
            $('#block-seat').val('');
            $('ul.row-margin li a').click(function (e) {
                var aClass = $(this).attr("class");
                if ($(this).hasClass('bus-seat seat disabledSeat')) {
                    // remove selected seat
                    selectId.pop($(this).attr("id"));
                    $('#total-seat').html(totalSeat - selectId.length).fadeIn();
                    $(this).removeClass("seat disabledSeat").addClass("seat availableSeat");
                } else {
                    // adding the select Id to hidden value
                    selectId.push($(this).attr("id"));
                    $('#total-seat').html(totalSeat - selectId.length);
                    $(this).removeClass("seat availableSeat").addClass("seat disabledSeat");
                }
                // ading hidden value
                $("#block-seat").val(selectId);

                $("p.lbl-blocked-seat").remove();
                var $p = $("<p/>", {'class': 'lbl-blocked-seat'});
                $p.append("Blocked Seat No: ");
                var span = $("<span/>");
                span.text(selectId);
                $p.append(span);
                $("h4#lbl-total-seat").append($p);
            });
        });
    </script>
