@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Finance"!!}</a></li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div id="message-wrapper">
                        @include('flash::message')

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <p><strong class="mandatory">*</strong> All fields must be required.</p>

                    <form class="form-horizontal" enctype="multipart/form-data" method="post"
                          action="@if(isset($finance)) {{ URL::to('bus/finance/'.$finance->id.'/edit')}} @else {{ URL::to('bus/finance/create/')}} @endif"
                          accept-charset="utf-8" autocomplete="off">

                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <div class="col-lg-3">
                            <div class="profile-picture">
                                <canvas id="thumb_img_canvas" style="display:none"></canvas>
                                <input type="file" id="thumb_img" name="profile"
                                       onchange="return ShowImagePreview(this.files, 'thumb_img')"
                                       class="btn btn-default" accept="image/*"/>

                                <div class="drag-panel" id="thumb_img_upload">
                                    <p>Drag &amp; Drop your photo here for</p>
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="caption">
                                    <strong>Profile Photo</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Name"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="name" class="form-control"
                                           value="{{ Input::old('name', isset($finance) ? $finance->name : null) }}"
                                           required accept="image/*"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!!"Email"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="email" name="email" class="form-control"
                                           value="{{ Input::old('email', isset($finance) ? $finance->email : null) }}"
                                           requried/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Branch"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    @if(BUS_BRANCH == Auth::user()->user_type)
                                        <p class="form-control-static"><strong class="text text-success">{!!
                                                Str::title(Auth::user()->branch) !!}</strong></p>
                                        <input type="hidden" value="{!! Auth::user()->branch!!}" name="branch"/>
                                    @else
                                        <select name="branch" class="form-control chosen-select">
                                            @foreach(json_decode(BUS_BRANCH_LIST, true) as $branch_name)
                                                <option value="{!! $branch_name !!}" {!! isset($finance) &&
                                                        $branch_name==
                                                        $finance->branch ? 'selected="selected"' : '' !!} >{!! Str::title($branch_name)!!}
                                                </option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Check In/ Check Out"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-4">
                                    <div class="input-group input-group bootstrap-timepicker">
                                        <input type="text" name="check_in" class="form-control time-picker"
                                               value="{{ Input::old('check_in', isset($finance) ? displayDate($finance->check_in, 'h:i A') : null) }}"
                                               required/>
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <label class="control-label colon">to</label>

                                <div class="col-xs-4">
                                    <div class="input-group input-group bootstrap-timepicker">
                                        <input type="text" name="check_out" class="form-control time-picker"
                                               value="{{ Input::old('check_out', isset($finance) ? displayDate($finance->check_out, 'h:i A') : null) }}"
                                               required/>
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "NRC"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="nrc" class="form-control"
                                           value="{{ Input::old('nrc', isset($finance) ? $finance->nrc : null) }}"
                                           required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "DOB"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8 date-picker-container">
                                    <div class="input-group date past ">
                                        <input type="text" name="dob" class="form-control"
                                               value="{{ Input::old('dob', isset($finance) ? $finance->dob : null) }}"
                                               requried/>
                                        <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Parent Name"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="parent_name" class="form-control"
                                           value="{{ Input::old('parent_name', isset($finance) ? $finance->parent_name : null) }}"
                                    >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Phone No."!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="phone" class="form-control"
                                           value="{{ Input::old('name', isset($finance) ? $finance->phone : null) }}"
                                    >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Contact Address"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <textarea class="form-control"
                                              name="contact"
                                              required>{!! Input::old('address', isset($finance) ? $finance->contact : null) !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Address"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <textarea class="form-control"
                                              name="address"
                                              required>{!! Input::old('address', isset($finance) ? $finance->address : null) !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Status"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8 radio">
                                    <label>
                                        {!! Form::radio('status', STATUS_ACTIVE, Input::old('status', isset($finance) ?
                                        $finance->status : null) == STATUS_ACTIVE ?
                                        TRUE : FALSE )!!} Active
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label>
                                        {!! Form::radio('status', STATUS_INACTIVE, Input::old('status', isset($finance)
                                        ?
                                        $finance->status : null) ==
                                        STATUS_INACTIVE ? TRUE : FALSE )!!} In Active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-8">
                                    <input type="reset" class="btn btn-default" value="Cancel"/>
                                    <input type="submit" class="btn btn-success" value="Submit"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- Time Picker Css -->
    <link href="{{ asset('/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css">
    <!-- Time Picker JS -->
    <script src="{{ asset('/js/bootstrap-timepicker.js')}}"></script>
    <script type="text/javascript">
        var logo = "<?php echo (isset($finance) && !empty($finance->profile) && !empty($finance->profile_path)) ? asset($finance->profile_path . $finance->profile) : '';?>";
        if (logo !== '') {
            editImagePreview('thumb_img', logo, 200, 200);
        }
        $('.time-picker').timepicker({
            minuteStep: 5
        });
    </script>
@stop
