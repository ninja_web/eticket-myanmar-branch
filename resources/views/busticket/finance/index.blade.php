@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid f-arial">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Finance"!!}</a></li>
                            <li>
                                <a class="btn btn-system f-arial" href="{{ url('/bus/finance/create')  }}"><i
                                            class="fa fa-plus"></i> New Finance</a>
                            </li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <table class="table table-bordered table-striped table-hover" id="tbList">
                        <thead>
                        <tr class="f-arial">
                            <th class="text-center">No</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Location</th>
                            <th class="text-center">Check In / Out</th>
                            <th class="text-center">Phone No</th>
                            <th class="text-center">Main Address</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = 1;?>
                        @foreach($finance as $user)
                            <tr>
                                <td class="text-center">{!! $index !!}</td>
                                <td class="text-center">{!! $user->name !!}</td>
                                <td class="text-center">{!! Str::title($user->branch) !!}</td>
                                <td class="text-center text-success">{!! displayDate($user->check_in, 'h:i A'). ' to
                                    '.displayDate($user->check_out, 'h:i A')!!}
                                </td>
                                <td class="text-center">{!! $user->phone !!}</td>
                                <td class="text-center">{!! $user->address !!}</td>
                                <td class="text-center">{!! displayDate($user->created_at)!!}</td>
                                <td class="text-center">{!! showStatus($user->status)!!}</td>
                                <td class="text-center">
                                    <a class="btn btn-outline btn-xs btn-info"
                                       href="{!! url('bus/finance/'.$user->id.'/edit') !!}">
                                        <i class="fa fa-edit">&nbsp;</i>Edit</a>
                                </td>
                            </tr>
                            <?php $index++;?>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- End Table -->
                    <div class="clearfix"></div>
                    <div class="pag-panel">
                        <div class="pull-right">
                            {!! $finance->render() !!}
                        </div>
                    </div>
                    <!-- paging panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
