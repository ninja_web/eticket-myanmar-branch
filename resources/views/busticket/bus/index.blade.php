@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Finance"!!}</a></li>
                            <li class="pull-right">
                                <span id="cur-time"></span>
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <div class="list-search-panel">
                        <a class="btn btn-outline btn-success" href="{{ url('/bus/create') }}"><i
                                    class="fa fa-street-view"></i>&nbsp;{!! "Finance" !!}</a>
                    </div>
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr class="active">
                            <th class="text-center">No</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Location</th>
                            <th class="text-center">Check In / Out</th>
                            <th class="text-center">Phone No</th>
                            <th class="text-center">Main Address</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = 1;?>

                        </tbody>
                    </table>
                    <!-- End Table -->
                    <div class="clearfix"></div>
                    <div class="pag-panel">
                        <div class="pull-right">

                        </div>
                    </div>
                    <!-- paging panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
