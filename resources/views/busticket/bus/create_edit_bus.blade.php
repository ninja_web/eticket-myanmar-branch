@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Sale"!!}</a></li>
                            <li class="pull-right"><i></i>{!! date("j F, Y h:m A") !!}</li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <p><strong class="mandatory">*</strong> All fields must be required.</p>

                    <form class="form-horizontal" enctype="multipart/form-data" method="post"
                          action="@if(isset($bus)) {{ URL::to('bus/sale/'.$bus->id.'/edit')}} @else {{ URL::to('bus/sale/create/')}} @endif"
                          accept-charset="utf-8" autocomplete="off">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <div class="col-lg-8">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! Lang::get('busname')!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="name" class="form-control"
                                           value="{{ Input::old('name', isset($bus) ? $bus->name : null) }}"
                                           required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! Lang::get('bus number')!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="email" name="email" class="form-control"
                                           value="{{ Input::old('email', isset($bus) ? $bus->email : null) }}"
                                           requried/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! Lang::get('bus class')!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8">
                                    <input type="text" name="nrc" class="form-control"
                                           value="{{ Input::old('nrc', isset($bus) ? $bus->nrc : null) }}"
                                           required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Status"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-8 radio">
                                    <label>
                                        {!! Form::radio('status', STATUS_ACTIVE, Input::old('status', isset($bus) ?
                                        $bus->status : null) == STATUS_ACTIVE ?
                                        TRUE : FALSE )!!} Active
                                    </label> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <label>
                                        {!! Form::radio('status', STATUS_INACTIVE, Input::old('status', isset($bus) ?
                                        $bus->status : null) ==
                                        STATUS_INACTIVE ? TRUE : FALSE )!!} In Active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-8">
                                    <input type="reset" class="btn btn-default" value="Cancel"/>
                                    <input type="submit" class="btn btn-success" value="Submit"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- Time Picker Css -->
    <link href="{{ asset('/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css">
    <!-- Time Picker JS -->
    <script src="{{ asset('/js/bootstrap-timepicker.js')}}"></script>
    <script type="text/javascript">
        var logo = "<?php echo (isset($bus) && !empty($bus->profile) && !empty($bus->profile_path)) ? asset($bus->profile_path . $bus->profile) : '';?>";
        if (logo !== '') {
            editImagePreview('thumb_img', logo, 200, 200);
        }
        $('.time-picker').timepicker({
            minuteStep: 5
        });
    </script>
@stop
