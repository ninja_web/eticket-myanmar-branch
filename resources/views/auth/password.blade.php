@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <h2 class="login-header">Online Ticket System</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default login-default">
                    <div class="panel-heading login-heading text-center">
                        <div class="circle">
                            <i class="fa fa-user fa-2x"></i>
                        </div>
                    </div>
                    <div class="panel-body login-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="email" placeholder="Email" class="form-control" name="email"
                                           value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-block btn-login">
                                        Send Password Reset Link
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer login-footer">
                        <a class="btn btn-link" href="{{ url('/auth/login') }}">
                            <i class="fa fa-lock">&nbsp;&nbsp;</i>Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
