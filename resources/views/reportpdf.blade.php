<!DOCTYPE html>
<html>
<head>

    <title>PDF Format</title>
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/pdf.css') }}" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .dn {
            display: none !important;
        }

        .hidden {
            visibility: hidden;
        }

        .b-default {
            border: 1px solid #fff !important;
            border-right: 1px !important
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h4>DAILY DUTY SALES REPORT &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;R
                - 1</span></h4>
        </div>

        <div class="col-xs-12">
            <h4>11 NOV 2015 &nbsp;&nbsp;&nbsp;&nbsp;SALE PERSON: KO SOE &nbsp;&nbsp;&nbsp;MANDALAY BRANCH</h4>
        </div>
        <div class="col-xs-12">
            <table class="table table-bordered">
                <tr style="visibility:hidden">
                    <td colspan="8"></td>
                </tr>
                <tr>
                    <td>NO</td>
                    <td>DATE</td>
                    <td>ROUTE</td>
                    <td>SEAT</td>
                    <td>Ticket Price</td>
                    <td>Discount</td>
                    <td>Total Discount</td>
                    <td>Total</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nov 16 2015</td>
                    <td>4</td>
                    <td>1</td>
                    <td>1000</td>
                    <td>-</td>
                    <td>1000</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nov 16 2015</td>
                    <td>4</td>
                    <td>1</td>
                    <td>1000</td>
                    <td>-</td>
                    <td>1000</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td colspan="3" class="b-default"></td>
                    <td>1</td>
                    <td>1000</td>
                    <td>-</td>
                    <td>1000</td>
                    <td>100</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>