<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="iTicketMyanmar::The best myanmar online highway bus ticketing system.">
    <meta name="keywords" content="iTicketMyanmar">
    <title>iTicketMyanmar::The best myanmar online highway bus ticketing system.</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('/css/plugins/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('/css/sb-admin-2.css') }}" rel="stylesheet">
    <!-- style Css -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('/font-awesome-4.3.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Date Picker -->
    <link href="{{ asset('/css/datepicker3.css') }}" rel="stylesheet" type="text/css">
    <!-- jQuery -->

    <script src="{{ asset('/js/jQuery-2.1.4.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/js/bootstrap.min.js')}}"></script>

    <!-- Custom js-->
    <script src="{{ asset('/js/functions.js')}}"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-63366740-2', 'auto');
            ga('send', 'pageview');

        </script>
      -->
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top default-theme" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand f-arial white" href="{!!url()!!}">Myanmar Online Ticket System</a>
        </div>
        <div class="">

        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li>
                {!! Form::open(array('url'=> '/boat/ticket/checkin/','class' => 'form-inline', 'role' =>'form'))!!}
                <div class="form-group">
                    {!! Form::text('ticket', Input::get('ticket'), array('class' => 'form-control', 'style' => 'width:400px','placeholder' => 'Search Ticket PNR No'))!!}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info f-arial"><i class="fa fa-search"></i></button>
                </div>
                {!! Form::close()!!}
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle f-arial" data-toggle="dropdown" href="#"><i
                            class="fa fa-user"></i> {{ Auth::user()->name }}
                    <?php $user_type = json_decode(USER_TYPE, true);?>
                    ( {!! $user_type[Auth::user()->user_type]!!} )
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <!-- // TODO :: SaLone : Need to implement the user profile update case only one class and UI -->
                        <a href="{{ url('/auth/logout') }}"> <i class="fa fa-user-secret fa-fw"></i> Profile</a>
                    </li>
                    <li>
                        <a href="{{ url('/auth/logout') }}"> <i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
        </ul>
        <!-- /.navbar-top-links -->
    @include('sidebar', $c_page)
    <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
@yield('content')
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<!-- Date Picket Css-->

<script src="{{ asset('/js/bootstrap-datepicker.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ asset('/js/plugins/metisMenu/metisMenu.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ asset('/js/sb-admin-2.js')}}"></script>
<!-- This is only necessary if you do Flash::overlay('...') -->
<script src="{{ asset('/js/chosen.jquery.js')}}"></script>
<!-- This is only necessary if you do Flash::overlay('...') -->
<!-- pdf generator -->
<script src="{{ asset('/js/jspdf.debug.js')}}"></script>
<!-- Custom js-->
<script src="{{ asset('/js/custom.js')}}"></script>
<!-- This is only necessary if you do Flash::overlay('...') -->
<script>
    // notification message
    $('.alert-success').fadeIn().delay(3000).fadeOut();
    $('#fade-in').fadeIn().delay(2000);
    $('#flash-overlay-modal').modal();


</script>

</body>
</html>
