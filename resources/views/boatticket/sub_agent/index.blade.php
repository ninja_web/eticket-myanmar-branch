@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-truck"></i> Sub Agent</li>
                            <li>
                                <a class="btn btn-system f-arial" href="{{ url('/boat/sub_agent/create') }}"><i
                                            class="fa fa-plus"></i>&nbsp;{!! "New Sub Agent"!!}</a>
                            </li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr class="f-arial">
                            <th class="text-center">No</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">NRC</th>
                            <th class="text-center">Phone No</th>
                            <th class="text-center">Main Address</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = Request::get('page', 1);?>
                        <?php $index = $index > 1 ? PER_PAGE * ($index - 1) + 1 : $index;?>
                        @foreach($sub_agent as $user)
                            <tr>
                                <td class="text-center">{!! $index !!}</td>
                                <td class="text-center">{!! $user->name !!}</td>
                                <td class="text-center"><a href="{!! url('boat/sub_agent/'.$user->id.'/edit') !!}">{!!
                                        $user->email !!}</a></td>
                                <td class="text-center">{!! $user->nrc !!}</td>
                                <td class="text-center">{!! $user->phone !!}</td>
                                <td class="text-center">{!! $user->address !!}</td>
                                <td class="text-center">{!! displayDate($user->created_at)!!}</td>
                                <td class="text-center">{!! showStatus($user->status)!!}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-primary  f-arial"
                                       href="{!! url('boat/sub_agent/'.$user->id.'/edit') !!}">
                                        <i class="fa fa-edit">&nbsp;</i>Edit</a>
                                </td>
                            </tr>
                            <?php $index++;?>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- End Table -->
                    <div class="clearfix"></div>
                    <div class="pag-panel">
                        <div class="pull-right">
                            {!! $sub_agent->render() !!}
                        </div>
                    </div>
                    <!-- paging panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
