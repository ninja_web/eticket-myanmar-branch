@extends('template')
@section('content')
    @if($schedule->seat_plan == WITHOUT_SEAT_PLAN)
        <link href="{{ asset('/js/plugins/num_picker/wan-spinner.css') }}" rel="stylesheet">
        <script src="{{ asset('/js/plugins/num_picker/wan-spinner.js')}}"></script>
    @endif
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb f-arial">
                            <li>
                                <a href="{!!URL::previous()!!}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
                            </li>

                            <li class="active"><i class="fa fa-barcode"></i> Ticket</li>
                            <li class="pull-right">

                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <a class="btn btn-system  btn-sm f-arial"
                                   href="{!!  url('boat/ticket/orders/'.$schedule->id) !!}"><i class="fa fa-list"></i>
                                    Order Details</a>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger f-arial">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                </div>

                {!! Form::open(array('url' => 'boat/ticket/browse/'.$schedule->id, 'method' => 'POST', 'class' =>
                'form-horizontal',
                'role' => 'form'))!!}
                {!! Form::hidden('seat_no', "", array('id' => 'hid_seat_no')) !!}
                {!! Form::hidden('seat_plan', $schedule->seat_plan )!!}

                <div class="clearfix"></div>
                <?php $take_seat_layout = explode(",", $schedule->take_seat_layout); ?>
                <div class="col-md-6">
                    <div class="panel panel-default" style="padding-top:15px;padding-bottom:20px;">

                        <p class="text-info f-arial" style="padding-left:10px;padding-bottom:10px;">
                            Sold:
                            <span class="badge badge-info">
                              {!! $schedule->total_seat - $schedule->remain_seat!!}
                              </span>
                            <span style="margin-left:20px;"></span>
                            Availiable
                            <span class="badge badge-success">{!! $schedule->remain_seat !!}</span>

                        </p>
                        @if($schedule->seat_plan == WITHOUT_SEAT_PLAN)
                            <div class="col-md-4 panel panel-default bn">
                                <div class="panel-body without-seat-body">
                                    <div class="cricle">
                                        <strong>{!! $schedule->remain_seat !!}</strong>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-8 mt105">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">{!! Lang::get('No. of Seat')!!} <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-7">
                                        <div class="input-group">
                                          <span class="input-group-btn">
                                              <button type="button" class="btn btn-default btn-number" data-type="minus"
                                                      data-field="no_seat">
                                                  <span class="glyphicon glyphicon-minus"></span>
                                              </button>
                                          </span>
                                            <input type="text" name="no_seat" class="form-control input-number h34"
                                                   value="1" min="1" max="300">

                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-default btn-number" data-type="plus"
                                                      data-field="no_seat">
                                                  <span class="glyphicon glyphicon-plus"></span>
                                              </button>
                                          </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Boating Points:<strong
                                                class="mandatory">*</strong></label>

                                    <div class="col-xs-8 pt8">
                                        {!!$schedule->b_points!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Dropping Points:<strong
                                                class="mandatory">*</strong></label>

                                    <div class="col-xs-8 pt8">
                                        {!!$schedule->d_points!!}
                                    </div>
                                </div>
                            </div>
                        @else
                            <?php  $seater_plan = json_decode(BOAT_SEATER_PLAN, true); ?>
                            <?php  $seater_plan = json_decode($seater_plan[$schedule->seater], true); ?>
                            <?php  $seater_class = $seater_plan['type'] == 'seater-123' ? "s123_seater" : '';?>
                            <div class="{!!$seater_class!!}">
                                <div class="car-layout {!! $seater_plan['type'] !!}">
                                    <div class="seat-driver img_availiable_seat"></div>
                                    <div class="boat-style">
                                        @foreach($seater_plan['seat'] as $key => $value)
                                            @if("middle" == $value)
                                    </div>
                                    <!-- end boat style -->
                                    <ul class="row-margin middle-row">
                                        <li></li>
                                    </ul>
                                    <div class="boat-style">
                                        @else
                                            <?php $rows = explode(",", $value);?>
                                            <ul class="row-margin">
                                                @foreach($rows as $seat)
                                                    <li class="seat">

                                                        @if(!empty($seat))
                                                            <?php $seat_type = in_array($seat, $take_seat_layout) ? 'disabledSeat' : 'availableSeat' ?>
                                                            <a class="{!! $seat_type !!} select-seat"
                                                               id="{!! trim($seat) !!}">
                                                                <span class="seat-no">{!! trim($seat) !!}</span>
                                                            </a>

                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                        @endforeach
                                    </div>
                                    <!-- end boat style -->
                                    @if($seater_plan['type']=='seater-98')
                                        <div class="lble-sign">
                                            <h2>front</h2>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row mt10">
                                <div class="col-md-12">

                                </div>
                            </div>
                        @endif
                        <div class="clearfix"></div>

                        <!-- /.row -->
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel-default">
                        <div class="panel-body b1">
                            <p class="route">
                                {!! showRouteText(json_decode($schedule->whole), SCHEDULE_SEPERATOR) !!}

                                <span class="pull-right time">
                                 {!! displayDate($schedule->dept_time, 'H:i  A')!!}
                             </span>
                            </p>
                            <!-- Showing Date -->
                            <!-- End Showing Date -->
                            @if(strtotime($schedule->dept_date) > strtotime(date('Y-m-d')))
                                <span><a href="{!! url('boat/ticket/traverse/'.$schedule->schedule_type.'/'.strtotime($schedule->dept_date . '-1 day').'/'.$schedule->boat_order)!!}"><i
                                                class="fa fa-backward"></i></a></span>&nbsp;
                            @endif
                            <strong class="text-success"><span style="padding-left:18px">{!! displayDate($schedule->dept_date)!!}
                                </span></strong>
                            <strong><span class="pull-right text-info">{!! $schedule->boat_order !!}
                                    Boat&nbsp;&nbsp;</span></strong>

                            <br>
                            <br>


                            <p style="padding-left:18px">{!! $schedule->remark !!}</p>
                            <div class="overflow mb0">

                                @if($schedule->seat_plan!=WITHOUT_SEAT_PLAN)
                                    <div class="col-md-12 pt20 f-arial">
                                        <p>Take Seat Number: <strong id="seatNo" class="text text-success"></strong></p>
                                        <p>Total : <strong id="total" class="text text-success"></strong></p>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-12 mt10">
                                <?php $whole_route = json_decode($schedule->whole, true); ?>
                                <?php $price = json_decode($schedule->price, true);?>
                                <ul class="list-unstyled">
                                    @foreach($price as $key => $value)
                                        <?php $route = explode("_", $key); ?>
                                        @if(!empty($value))
                                            <li>
                                                <label>
                                                    <input name="selected_route" type="radio" class="cus-radio"
                                                           value="{!! $key !!}"
                                                           autocomplete="off" required>&nbsp;&nbsp;&nbsp;<span
                                                            class="sp-route-txt">
                                                            {!! showRouteText(array($route[0], $route[1]), SCHEDULE_SEPERATOR)!!}
                                                        ....  <span class="text-success">({!! $value !!})</span></span>
                                                </label>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>

                            <div class="col-md-4 mt10">
                                <input type="submit" class="btn btn-block btn-system  f-arial"
                                       name="btn_type"
                                       value="Buy Ticket"/>
                            </div>
                            @if (!in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT)))
                                <div class="col-md-4 mt10">
                                    <input type="submit" class="btn btn-block btn-default  f-arial"
                                           name="btn_type"
                                           value="Book Ticket"/>
                                </div>
                            @endif


                        </div>
                    </div>

                    <ul class="car seat-description p0" style="margin-top:10px;">
                        <li><i class="seat selectedSeat"></i><span>Selected Seat</span></li>
                        <li>
                            <i class="seat availableSeat"></i><span>Availiable Seat</span>
                        </li>
                        <li>
                            <i class="seat disabledSeat"></i><span>Solded Seat</span>
                        </li>
                    </ul>
                </div>

            {!! Form::close()!!}
            <!-- /.container-fluid -->
            </div>
            @if($schedule->seat_plan == WITHOUT_SEAT_PLAN)
                <script type="text/javascript">
                    $(document).ready(function () {
                        var options = {
                            maxValue: 100,
                            minValue: 0,
                            step: 1,
                            inputWidth: 200,
                            start: 0,
                            plusClick: function (val) {
                                console.log(val);
                            },
                            minusClick: function (val) {
                                console.log(val);
                            },
                            exceptionFun: function (val) {
                                console.log("excep: " + val);
                            },
                            valueChanged: function (val) {
                                console.log('change: ' + val);
                            }
                        }
                        $(".wan-spinner-1").WanSpinner(options);
                    });
                </script>
            @endif
            <?php $seatHistory = Input::old("seat_no", "");?>
            <?php $seatHistory = empty($seatHistory) ? array() : explode(",", $seatHistory);?>
            <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
            <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
            <script type="text/javascript">
                $(function (e) {
                    var userTakeSeat = <?php echo json_encode($seatHistory); ?>;
                    var total = $('#total');
                    var seatNo = $("#seatNo");

                    // update error
                    if (userTakeSeat.length > 0) {
                        userTakeSeat = userTakeSeat.sort(sortNumber);
                        // auto selected
                        $.each(userTakeSeat, function (row, value) {
                            $('#' + value).removeClass('availableSeat').addClass("availableSeat selectedSeat");
                        });
                        total.text(userTakeSeat.length + ' seat');
                        seatNo.text(userTakeSeat);
                    } else {
                        $('#seat-no').val();
                    }

                    $('ul.row-margin li.seat a').click(function (e) {

                        var aClass = $(this).attr("class");


                        aClass = aClass.trim();
                        var selectedSeatId = $(this).attr("id").trim();

                        if (aClass.trim() == "availableSeat select-seat") {
                            $(this).removeClass(aClass).addClass("availableSeat selectedSeat select-seat");
                            userTakeSeat.push(selectedSeatId);

                            userTakeSeat = userTakeSeat.sort(sortNumber);
                            total.text(userTakeSeat.length + ' seat');

                            seatNo.text(userTakeSeat);

                        } else if (aClass.trim() == "availableSeat selectedSeat select-seat") {

                            $(this).removeClass(aClass).addClass("availableSeat select-seat");
                            userTakeSeat = userTakeSeat.sort(sortNumber);
                            userTakeSeat.splice($.inArray(selectedSeatId, userTakeSeat), 1);

                            if (userTakeSeat.length > 0) {
                                total.text(userTakeSeat.length + ' seat');
                                seatNo.text("");
                                seatNo.text(userTakeSeat);
                            } else {
                                total.text('');
                                seatNo.text("");
                            }
                        }
                        // adding to the cartSeat(Basket)
                        $("#hid_seat_no").val(userTakeSeat);
                    });

                    function sortNumber(a, b) {
                        return a - b;
                    }

                    $('.activate').click(function (e) {
                        var href = this.href;
                        var dialog = new BootstrapDialog({
                            type: 'type-success',
                            title: '<div id="dia-title">Cancle Ticket</div>',
                            message: '<div id="dia-message">Are you sure you want to activate these ticket.</div>',
                            buttons: [{
                                label: 'Cancel',
                                cssClass: 'btn btn-default',
                                action: function (dialog) {
                                    dialog.close();
                                }
                            }, {
                                label: 'Yes',
                                cssClass: 'btn btn-success',
                                action: function (dialog) {
                                    $("#dia-message").text("Processing now, please wait.");
                                    $('#dia-message').css('color', '#00');
                                    $('#dia-title').text("Wait..");
                                    $.ajax({
                                        type: 'GET',
                                        url: href,
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data.status) {
                                                location.reload();
                                            } else {
                                                // show erro
                                            }
                                        }
                                    });
                                }
                            }
                            ],
                            closable: true

                        });
                        dialog.realize();
                        dialog.open();
                    });

                    $index = 0;
                    $(".seater-123 .boat-style").each(function () {
                        if ($index == 1) {
                            $(this).append("<h2>Front</h2>");
                            $(this).append("<h2>Sun Desk</h2>");
                            $(this).append("<h2>Upper Desk</h2>");
                        }
                        if ($index == 3) {
                            $(this).append("<h2 class='lower'>Lower Desk</h2>");
                        }
                        $index++;
                        if ($index == 1 || $index == 3) {
                            $(this).addClass('bnone pt30');
                        }
                        if ($index == 2 || $index == 4) {
                            $(this).addClass('mt_27 pt30');
                        }

                    });

                });
            </script>
@stop
