@extends('template')

@section('content')
    <!-- import the date -->
    <link href="{{ asset('/css/bootstrap-datepaginator.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/moment.js')}}"></script>
    <script src="{{ asset('/js/bootstrap-datepaginator.js')}}"></script>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb f-arial">
                        <li>
                            <a href="{!!url('boat/ticket')!!}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
                        </li>

                        <li><i class="fa fa-barcode"></i> Ticket</li>
                        <li>{!! showRouteText(json_decode($schedule->whole, true), SCHEDULE_SEPERATOR) !!}</li>
                        <li class="pull-right">
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                            <strong> - </strong>
                            <span id="cur-time"></span>
                        </li>
                    </ol>
                </div>
                <div class="clearfix"></div>
                <div id="paginator" style="margin-bottom:10px"></div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <div class="loader" style="display: none">
                        <div class="sk-circle">
                            <div class="sk-circle1 sk-child"></div>
                            <div class="sk-circle2 sk-child"></div>
                            <div class="sk-circle3 sk-child"></div>
                            <div class="sk-circle4 sk-child"></div>
                            <div class="sk-circle5 sk-child"></div>
                            <div class="sk-circle6 sk-child"></div>
                            <div class="sk-circle7 sk-child"></div>
                            <div class="sk-circle8 sk-child"></div>
                            <div class="sk-circle9 sk-child"></div>
                            <div class="sk-circle10 sk-child"></div>
                            <div class="sk-circle11 sk-child"></div>
                            <div class="sk-circle12 sk-child"></div>
                        </div>
                        <p style="color:#000;text-align: center;font-size:30px">
                            Searching Route List ...
                        </p>
                    </div>
                    <div id="schedule_list"></div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <script type="text/javascript">
        $(window).ready(function (event) {
            var url = "{!! url('boat/route/traverse/'.$schedule_type) !!}";
            console.log(url);

            // call first time
            paginate("{!! $schedule_date !!}");
            var options = {
                selectedDate: moment().format("YYYY/MM/DD"),
                startDate: moment().format("YYYY/MM/DD"),
                itemWidth: 100,
                selectedItemWidth: 160,
                navItemWidth: 40,
                showOffDays: false,
                onSelectedDateChanged: function (event, date) {

                    $('.loader').show();
                    $('#schedule_list').hide();
                    // call details
                    var d = new Date(date);
                    var date = d.getFullYear() + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + ("0" + d.getDate()).slice(-2);
                    // var date = new Date(d.getFullYear() + '-' + (d.getMonth()) + '-' + (d.getDate() + 1)).getTime() / 1000;
                    date = new Date(date).getTime() / 1000;
                    paginate(date);
                }
            }

            function paginate(date) {
                $.get(url + '/' + date, function (data) {
                    $('#schedule_list').html(data);
                    $('#schedule_list').show();
                    $('.loader').hide();
                });

            }

            $('#paginator').datepaginator(options);
        });
    </script>
@stop
