@if(isset($schedules) && count($schedules))
    <table class="table  table-striped table-hover tb-bnone">
        <tbody>
        <?php $index = 1;?>


        @foreach($schedules as $schedule)

            @if($schedule->status==STATUS_ACTIVE)
                <tr>
                    <?php $img_path = $schedule->logo_path . $schedule->logo;?>
                    <?php $img_path = empty($img_path) ? '' : asset($img_path); ?>
                    <td style="padding-left:10px">
                        <img src="{!! $img_path !!}" style="width:150px;height:80px" class="img-thumbnail">
                    </td>
                    <td class="text-center f-arial ticket-time">{!! displayDate($schedule->dept_time, 'h:i A') !!}</td>
                    <td class="text-center f-arial ticket-time">{!! $schedule->boat_order ." Boat "!!}</td>
                    <td class="text-center f-arial"><strong>Sold &nbsp;</strong><br><span
                                class="badge badge-system"> {!! $schedule->total_seat - $schedule->remain_seat !!}</span>
                    </td>
                    <td class="text-center f-arial"><strong>Availiable &nbsp;</strong><br><span
                                class="badge badge-green">{!! $schedule->remain_seat !!}</span>
                    </td>
                    <?php $price = json_decode($schedule->price, true);?>
                    <td>
                        <strong>Price &nbsp;</strong><br>
                        <span class="badge badge-normal">{!! $price[$schedule->start_branch.'_'.$schedule->end_branch] !!}</span>
                    </td>
                    <td class="text-center">
                        &nbsp;<a href="{!! url('boat/ticket/select/'.$schedule->id)!!}"
                                 class="btn btn-sm f-arial btn-ticket">Browse Ticket</a>
                        <br>
                        <a href="{!! url('boat/ticket/orders/'.$schedule->id)!!}"
                           class="btn btn-sm  f-arial btn-ticket">Order Detail</a>

                    </td>
                    <td class="remark">{!! $schedule->remark !!}</td>
                </tr>
                <?php $index += 1;?>
            @endif
        @endforeach
        </tbody>
    </table>
    <!-- End Table -->
@else
    <div class="alert alert-info">
        <i class="fa fa-info-circle"></i> There is no record.
    </div>
@endif
