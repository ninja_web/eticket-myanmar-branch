@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb f-arial">
                            <li>

                                <a href="{!!URL::previous()!!}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
                            </li>
                            <li class="active"><i class="fa fa-barcode"></i> Order Details</li>
                            <li class="pull-right">

                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <span><strong> - </strong></span>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <a class="btn btn-system  btn-sm f-arial"
                                   href="{!!  url('boat/ticket/select/'.$schedule->id) !!}"><i class="fa fa-flag-o"></i>
                                    Browse Ticket</a>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="alert alert-info alert-system" style="overflow:hidden;margin-bottom:5px">
                        <div class="col-md-4">
                            {!! showRouteText(json_decode($schedule->whole), SCHEDULE_SEPERATOR)!!}
                        </div>
                        <div class="col-md-3">
                            <i class="fa fa-calendar"></i>&nbsp; {!! date("F j Y", strtotime($schedule->dept_date)) . "  " . date("H:i A", strtotime($schedule->dept_time)) !!}
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-ship"></i>&nbsp; {!! $schedule->boat_order !!} Boat
                        </div>
                        <div class="col-md-3">
                            <p style="margin-bottom:0px;">
                                Sold:
                                <span class="badge badge-info">
                              {!! $schedule->total_seat - $schedule->remain_seat!!}
                              </span>
                                <span style="margin-left:20px;"></span>
                                Availiable
                                <span class="badge badge-success">{!! $schedule->remain_seat !!}</span>

                            </p>
                        </div>
                    </div>


                </div>

                <div class="col-md-12 mt10">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr class="f-arial">
                                <th>No</th>
                                <th>Ticket No</th>
                                <th>Seat No</th>
                                <th>Total Seat</th>
                                <th>Total Fare</th>
                                <th>Journey</th>
                                <th>Type</th>
                                <th>Agent</th>
                                <th>Uploaded File</th>
                                <th>Book Time</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $index = 1;?>
                            @foreach($tickets as $row)

                                <tr class="f-arial">
                                    <td>{!! $index !!}</td>
                                    <td>{!! $row->ticket_pnr_code !!}</td>
                                    <td>{!! $row->seat_no !!}</td>
                                    <td>{!! $row->total_seat !!}</td>
                                    <td>{!! $row->fare * $row->total_seat !!}</td>
                                    <td>{!! showRouteText(array($row->from, $row->to), SCHEDULE_SEPERATOR)!!}</td>
                                    <td><strong class="text-success">@if(BOOK == $row->type) {!!  'BOOK'
                                            !!} @else {!!
                                            'BUY' !!} @endif </td>
                                    <td>{!! $row->agent !!}</td>
                                    <td>

                                        @if($row->upload_type=="1")
                                            <a href="{!!url('boat/ticket/filedownload/'.$row->id)!!}"
                                               class="btn btn-xs btn-info">Download</a>
                                        @endif
                                    </td>
                                    <td>{!! displayDate($row->created_at, 'F j Y g:i A') !!}</td>
                                    <td>
                                        @if(BOOK == $row->type)
                                            <a class="buy btn btn-xs  btn-success f-arial"
                                               href="{!! url('boat/ticket/activate/'.$row->id)!!}"
                                               id="{!! url('boat/ticket/passenger/'.$row->id)!!}"
                                               onClick="javascript:void(0); return false;"><i
                                                        class="fa fa-shopping-cart"></i>&nbsp;Buy Ticket</a>
                                            @if(Auth::user()->user_type!=BOAT_AGENT)

                                                <a class="btn btn-info btn-xs  f-arial"
                                                   href="{!! url('boat/ticket/manage/'.$row->id)!!}"><i
                                                            class="fa fa-pencil"></i>&nbsp;Manage
                                                    Ticket</a>


                                                <a class="btn btn-xs  btn-danger cancel f-arial"
                                                   href="{!! url('boat/ticket/cancel/'.$row->id.'/'.STATUS_BOOK_CANCEL)!!}"
                                                   onClick="javascript:void(0); return false;"><i
                                                            class="fa fa-trash-o"></i>&nbsp;Cancel
                                                    Booking</a>
                                            @endif
                                        @else
                                            <a class=" btn btn-xs btn-success f-arial"
                                               href="{!! url('boat/ticket/passenger/'.$row->id)!!}"><i
                                                        class="fa fa-edit"></i>
                                                Passenger Details
                                            </a>

                                            @if(Auth::user()->user_type!=BOAT_AGENT)

                                                <a class="btn btn-info btn-xs  f-arial"
                                                   href="{!! url('boat/ticket/manage/'.$row->id)!!}"><i
                                                            class="fa fa-pencil"></i>&nbsp;Manage
                                                    Ticket</a>

                                                <a class="btn btn-xs  btn-danger cancel f-arial"
                                                   href="{!! url('boat/ticket/cancel/'.$row->id.'/'.STATUS_TICKET_CANCEL)!!}"
                                                   onClick="javascript:void(0); return false;"><i
                                                            class="fa fa-trash-o"></i>&nbsp;Cancel
                                                    Ticket</a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                <?php $index += 1;?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </div>

    <style>
        .alert-system {

            background-color: #3d9eab !important;
            color: #fff;
        }
    </style>

    <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
    <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
    <script type="text/javascript">
        $(function () {
            $('.cancel').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-danger',
                    title: '<div id="dia-title">Cancle Ticket</div>',
                    message: '<div id="dia-message">Are you sure you want to cancel these ticket.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-danger',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
            $('.buy').click(function (e) {
                var href = this.href;
                var passengerurl = this.id;

                var dialog = new BootstrapDialog({
                    type: 'type-success',
                    title: '<div id="dia-title">Buy Ticket</div>',
                    message: '<div id="dia-message">Are you sure you want to buy these ticket.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-success',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        // go to next status
                                        location.href = passengerurl;
                                        // location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true
                });
                dialog.realize();
                dialog.open();
            });

            $index = 0;
            $(".seater-123 .boat-style").each(function () {
                if ($index == 1) {
                    $(this).append("<h2>Front</h2>");
                    $(this).append("<h2>Sun Desk</h2>");
                    $(this).append("<h2>Upper Desk</h2>");
                }
                if ($index == 3) {
                    $(this).append("<h2 class='lower'>Lower Desk</h2>");
                }
                $index++;
                if ($index == 1 || $index == 3) {
                    $(this).addClass('bnone pt30');
                }
                if ($index == 2 || $index == 4) {
                    $(this).addClass('mt_27 pt30');
                }

            });
        });
    </script>
@stop
