@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb f-arial">
                            <li class="active"><i class="fa fa-money"></i> Buy Ticket</li>
                            <li class="pull-right">

                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <span><strong> - </strong></span>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <a class="btn btn-system  btn-sm f-arial"
                                   href="{!!  url('boat/ticket/orders/'.$schedule->id) !!}"><i class="fa fa-list"></i>
                                    Order Details</a>

                            </li>
                            <li>
                                <strong style="font-size:18px;">{!! showRouteText(json_decode($schedule->whole), SCHEDULE_SEPERATOR)!!}</strong>

                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                </div>
                {!! Form::open(array('url' => 'boat/ticket/passenger/'.$ticket->id, 'method' => 'POST', 'class' =>
                'form-horizontal',
                'role' => 'form','files'=>true))!!}
                <div class="col-md-12">
                    <div class="alert alert-info alert-system overflow">
                        <div class="col-md-2">
                            <strong class="text">{!! displayDate($schedule->dept_date)!!}</strong>

                        </div>
                        <div class="col-md-2">
                            <strong class="text">
                                {!!displayDate($schedule->dept_time, 'H:i A')!!}
                                <i class="fa fa-arrows-h"></i>
                                {!! $schedule->boat_order !!} Boat
                            </strong>

                        </div>
                    <!--  <div class="col-md-3">
                            @if(strtotime($schedule->dept_date) > strtotime(date('Y-m-d')))
                        <span><a href="{!! url('boat/ticket/traverse/'.$schedule->schedule_type.'/'.strtotime($schedule->dept_date . '-1 day').'/'.$schedule->boat_order)!!}"><i
                                                class="fa fa-backward"></i></a></span>&nbsp;
                            @endif

                            </div>
     -->


                        <div class="col-md-3">
                            <?php $take_seat_layout = explode(",", $schedule->take_seat_layout); ?>

                            Sold
                            <span class="badge badge-info" style="maring-right:20px;">
                                    {!! $schedule->seat_plan == WITHOUT_SEAT_PLAN ? $schedule->total_seat - $schedule->remain_seat : count($take_seat_layout) !!}
                                  </span>
                            Availiable
                            <span class="badge badge-success">{!! $schedule->remain_seat !!}</span>


                        </div>

                        <div class="col-md-5">
                            @if($schedule->seat_plan==WITH_SEAT_PLAN)
                                <strong>Seat No : {!! $ticket->seat_no !!} </strong>
                            @else
                                <strong>No of Seat : {!! $ticket->total_seat !!} </strong>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>

                <?php $selected_route = array($ticket->from, $ticket->to); ?>
                <?php $price = json_decode($schedule->price, true);?>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p><strong class="mandatory">*</strong> All fields must be required.</p>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">Voucher Type <strong class="mandatory">*</strong>
                                    :</label>

                                <div class="col-sm-2">
                                    <div class="radio">
                                        <label><input type="radio" name="p_type" value="0" checked>Normal</label>
                                    </div>
                                </div>
                                <!--
                                <div class="col-md-1">
                                    <div class="radio">
                                        <label><input type="radio" name="p_type" value="1">Upload</label>
                                    </div>
                                </div>
                                -->
                            </div>

                            <?php

                            if ($schedule->seat_plan == WITH_SEAT_PLAN) {
                                $seat_count = explode(",", $ticket->seat_no);
                                $seat_count = count($seat_count);

                            } else {

                                $seat_count = $ticket->total_seat;

                            }

                            $index = 1;
                            ?>

                            <div class="normal_type">
                                @if(count($seat_count) && $ticket->type != BOOK)

                                    <table class="table table-bordered">
                                        <tr>
                                            <th style="width:30%">Name</th>
                                            <th style="width:10%">Country</th>
                                            <th>NRC / Passport</th>
                                        </tr>
                                        @for($i=0;$i<$seat_count;$i++)
                                            <tr>
                                                <td>
                                                    @if(BUY == $ticket->type)
                                                        <div class="pull-left" style="width:80px;margin-right:10px">
                                                            <select name="name_prefix[]"
                                                                    class="form-control chosen-select">
                                                                @foreach(json_decode(NAME_PREFIX, true) as $key => $value)
                                                                    <option value="{!! $key !!}">{!! $value !!}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="cross-panel pull-left" style="width:150px;">
                                                            {!! Form::text('name[]', Input::old('name', $ticket->name), array('class' =>
                                                            'form-control',
                                                            'required' => 'reqired',"id"=>"input_name"))!!}
                                                            <a id="btn_clear"><i
                                                                        class="fa fa-remove text-danger"></i></a>
                                                        </div>

                                                    @endif
                                                </td>


                                                <td>
                                                    <div class="radio">
                                                        <input type="hidden" name="hidd_country[]"
                                                               id="{!!'hidd_'.$index!!}" value="foreign">

                                                        <label>
                                                            <input type="radio" class="country_radio"
                                                                   name="{!!'country_'.$index!!}"
                                                                   <?php echo $ticket->country != 'local' ? 'checked="checked"' : ''?>
                                                                   value="foreign" id="{!!$index!!}">Foreign
                                                        </label>
                                                        <br>
                                                        <label>
                                                            <input type="radio" class="country_radio"
                                                                   name="{!!'country_'.$index!!}"
                                                                   <?php echo $ticket->country == 'local' ? 'checked="checked"' : ''?>
                                                                   value="local" id="{!!$index!!}">Local
                                                        </label>
                                                        <br>

                                                    </div>

                                                </td>
                                                <td>
                                                    <div id="{!!'local_'.$index!!}" style="display:none">
                                                        <div style="width:80px" class="pull-left">
                                                            <select name="nrc_prefix[]"
                                                                    class="form-control chosen-select" id="nrc-chosen">
                                                                @foreach(json_decode(NRC_PREFIX, true) as $key => $value)
                                                                    <option value="{!! $key !!}">{!! $value !!}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div style="width:80px" class="pull-left">
                                                            {!! Form::text('nrc_state[]', Input::old('nrc_state', $ticket->nrc_state),
                                                            array('class'
                                                            => 'form-control'))!!}
                                                        </div>
                                                        <div style="width:70px" class="pull-left">
                                                            <select name="nrc_nation[]"
                                                                    class="form-control chosen-select"
                                                                    id="nrc_nation_chosen">
                                                                @foreach(json_decode(NRC_NATION, true) as $key => $value)
                                                                    <option value="{!! $key !!}">{!! $value !!}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2" style="width:150px;">
                                                            {!! Form::text('nrc_no[]', Input::old('nrc_no', $ticket->nrc_no), array('class' =>
                                                            'form-control'))!!}
                                                        </div>
                                                    </div>
                                                    <div id="{!!'foreign_'.$index!!}"
                                                         class="col-sm-5">
                                                        {!! Form::text('passport[]', Input::old('passport', $ticket->passport),
                                                        array('class'
                                                        => 'form-control mg10','Placeholder'=>'Passpord No','required'))!!}
                                                    </div>
                                                </td>
                                                <?php $index++;?>
                                            </tr>
                                        @endfor
                                    </table>
                                @endif
                            </div>

                            <div class="dn" id="passenger_file">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Choose File</label>
                                    <div class="col-sm-9">
                                        <!-- file input -->
                                        @if(isset($info) && $info->file_path!='')
                                            <input type="hidden" name="base_url" value="{!!url("/")!!}">
                                            <input type="hidden" name="file_path" value="{!!$info->file_path!!}">
                                            <input type="hidden" name="file_name" value="{!!$info->file_name!!}">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 200px; height: 150px;">
                                                    <img src="{!!asset($info->file_path.$info->file_name)!!}"
                                                         id="edit_image">
                                                </div>
                                                <div>
                                                  <span class="btn default btn-file">
                                                  <span class="btn btn-xs btn-info fileinput-new">Change image </span>
                                                  <span class="btn btn-xs btn-info fileinput-exists">Change </span>
                                                      {!! Form::file('file')!!}
                                                  </span>
                                                    <a href="javascript:;"
                                                       class="btn btn-xs btn-danger red fileinput-exists"
                                                       data-dismiss="fileinput" id="edit_remove">
                                                        Remove </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                     style="width: 200px; height: 150px;">
                                                    <img src="{!!asset('img/s-thumbnail.png')!!}"
                                                         class="img-responsive img-lthumb">
                                                </div>
                                                <div>

                                                      <span class="btn default btn-file">
                                                      <span class="btn btn-info btn-xs fileinput-new">Select File </span>
                                                      <span class="btn btn-info btn-xs fileinput-exists">Change </span>
                                                          {!! Form::file('file')!!}
                                                      </span>
                                                    <a href="javascript:;"
                                                       class="btn btn-danger btn-xs fileinput-exists"
                                                       data-dismiss="fileinput">
                                                        Remove </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Paid / UnPaid :</label>
                                <div class="col-xs-9 radio">
                                    {!! $ticket->country !!}
                                    <label>
                                        <input type="radio"
                                               name="pay"
                                               <?php echo $ticket->pay != STATUS_PAID ? 'checked="checked"' : ''?>
                                               value="{!! STATUS_PAID !!}">Paid
                                    </label>
                                    <label>&nbsp;&nbsp;&nbsp;
                                        <input type="radio"
                                               name="pay"
                                               <?php echo $ticket->pay == STATUS_UNPAID ? 'checked="checked"' : ''?>
                                               value="{!! STATUS_UNPAID!!}">UnPaid
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Email : </label>

                                <div class="col-sm-6">
                                    {!! Form::text('email', Input::old('email', $ticket->email), array('class'
                                    =>
                                    'form-control'))!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Phone No : </label>

                                <div class="col-sm-6">
                                    {!! Form::text('phone_no', Input::old('phone_no', $ticket->phone_no), array('class'
                                    =>
                                    'form-control'))!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Remark : </label>

                                <div class="col-sm-6">
                                    {!! Form::textarea('remark', Input::old('remark', $ticket->remark),array('class' =>
                                    'form-control',
                                    'rows' => 5) );!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Discount : </label>

                                <div class="col-sm-6">
                                    {!! Form::text('discount', Input::old('discount', $ticket->discount), array('class'
                                    =>
                                    'form-control'))!!}
                                </div>
                            </div>
                            @if(BOOK == $ticket->type)
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Name<strong
                                                class="mandatory">*</strong></label>

                                    <div class="col-sm-2">
                                        <select name="buy_name_prefix" class="form-control chosen-select">
                                            @foreach(json_decode(NAME_PREFIX, true) as $key => $value)
                                                <option value="{!! $key !!}">{!! $value !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        {!! Form::text('buy_name', Input::old('name', $ticket->name), array('class' =>
                                        'form-control',
                                        'required' => 'reqired'))!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Agent</label>

                                    <div class="col-sm-6">
                                        <select class="form-control chosen-select" name="agent">
                                            <option>Select Agent</option>
                                            @foreach($agent as $row)
                                                <option value="{!! $row->id !!}" <?php echo $row->id == $ticket->agent_id ? 'selected="selected"' : '' ?>>
                                                    {!! $row->name !!}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-xs-offset-2  col-xs-8 f-arial">
                                    <button type="reset" class="btn btn-default f-arial">Cancel</button>
                                    <button type="submit" class="btn btn-success f-arial">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close()!!}
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </div>
    <style>
        #btn_clear {
            cursor: pointer;
        }
    </style>

    <script type="text/javascript" src="{!!asset('/js/bootstrap-fileinput.js')!!}"></script>

    <link rel="stylesheet" type="text/css" href="{!!asset('/css/bootstrap-fileinput.css')!!}">

    <script type="text/javascript">

        $(document).on('click', '#btn_clear', function () {
            $(this).prev('#input_name').val("");
        });

        $(document).ready(function () {


            toggleCountry($('input:radio[name=country]').val());
            $('.country_radio').change(function () {

                var id = $(this).attr('id');
                toggleCountry(this.value, id);
                var hidd = "#hidd_" + id;

                $(hidd).val($(this).val());

            });

            function toggleCountry(value, id) {
                if (value == 'foreign') {

                    $("#local_" + id).hide();
                    $("#foreign_" + id).show();

                    $("#foreign_" + id + " input[name='passport[]']").attr('required', true);

                } else {

                    $("#nrc_chosen_chosen").css('width', '80');
                    $("#nrc_nation_chosen_chosen").css('width', '120');

                    $("#passport_" + id).hide();
                    $("#local_" + id).show();
                    $("#foreign_" + id).hide();

                    $("#foreign_" + id + " input[name='passport[]']").removeAttr('required');
                }
            }

            $index = 0;
            $(".seater-102 .boat-style").each(function () {
                if ($index == 1) {
                    $(this).append("<h2>Front</h2>");
                    $(this).append("<h2>Sun Desk</h2>");
                    $(this).append("<h2>Upper Desk</h2>");
                }
                if ($index == 3) {
                    $(this).append("<h2 class='lower'>Lower Desk</h2>");
                }
                $index++;
                if ($index == 1 || $index == 3) {
                    $(this).addClass('bnone pt30');
                }
                if ($index == 2 || $index == 4) {
                    $(this).addClass('mt_27 pt30');
                }

            });
        });

        $("input[name='p_type']").change(function () {

            var $value = $(this).val();

            if ($value == 1) {/* upload file */

                $("div.normal_type").hide();
                $("div#passenger_file").show();

                $("input[name='passport[]']").removeAttr('required');

            } else {

                $("div.normal_type").show();
                $("div#passenger_file").hide();
            }
        });
    </script>

@stop
