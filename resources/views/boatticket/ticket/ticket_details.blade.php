@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb f-arial">
                            <li class="active"><i class="fa fa-file-pdf-o"></i> Ticket Search </i></li>


                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class='col-md-12'>
                    <form class="form-horizontal" role="form" method="post" action="{!!url('boat/ticket/checkin')!!}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input type="text" value="{!!isset($ticket_no)?$ticket_no:''!!}" name="ticket"
                                       class="form-control f-arial" placeholder='Ticket no' required>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-success">Search</button>
                            </div>
                        </div>
                </div>
                </form>
            </div>
            @if(isset($search) && !isset($info))
                <div class="alert alert-info">No Result found</div>
            @elseif(isset($search) && !empty($info))
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6 f-arial">
                                        Ticket No : {!!$info->ticket_pnr_code!!}
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{!!url('boat/ticket/pdf',$info->ticket_id)!!}"
                                           class="btn btn-info pull-right">Generate PDF</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <h4 class="f-arial"><strong>Boat Details</strong></h4>
                                <hr>
                                <form class="form-horizontal ticket-search" role="form">
                                    <div class="row">
                                        <div class="col-md-8">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Date : </label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static pt10">{!!date('M d Y',strtotime($info->dept_date))!!}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Trip : </label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">{!!$info->start_branch." --> ".$info->end_branch!!}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Trip No: </label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">{!!$info->ticket_pnr_code!!}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Depature Time : </label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">{!!$info->dept_time!!}</p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Jetty : </label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">{!!$info->b_points!!}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Seat : </label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">{!!$info->total_seat!!}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Booking Date : </label>

                                                <div class="col-md-9">
                                                    <p class="form-control-static">{!!date('M d Y',strtotime($info->created_at))!!}</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <img class="img-responsive thumbnail"
                                                 src="{!! asset('img/default_profile_thumbnail.jpeg') !!}"
                                                 style="width:150px; height=150px;">
                                        </div>
                                    </div>

                                </form>
                                <br><br>
                                <h4 class="f-arial"><strong>Passenger Details</strong></h4>
                                <hr>
                                <form class="form-horizontal ticket-search" role="form">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name : </label>

                                        <div class="col-md-9">
                                            @if($info->name_prefix!='')
                                                <?php $prefix = json_decode(NAME_PREFIX, true);?>
                                                <p class="form-control-static">{!!$prefix[$info->name_prefix]." ".$info->name!!}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Phone No : </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">{!!$info->phone_no!!}</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Contact: </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Email : </label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">{!!$info->email!!}</p>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            @endif
        </div>
    </div>
    </div>
@stop
