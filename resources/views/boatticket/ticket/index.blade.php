@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb f-arial">
                        <li><i class="fa fa-barcode"></i> Route List</li>
                        <li class="pull-right">

                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                            <strong> - </strong>
                            <span id="cur-time"></span>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <!-- Ticket Search Control -->
                    @if (!in_array(Auth::user()->user_type, array(BOAT_ADMIN,BOAT_AGENT, BOAT_SUB_AGENT)))
                        <div class="list-search-panel">
                            {!! Form::open(array('url'=> '/boat/route/search/','class' => 'form-inline', 'role' =>'form'))!!}
                            <div class="form-group">
                                <select name="schedule_type" class="form-control chosen-select" style="width:300px;">
                                    <option value="">Select Destination</option>
                                    @foreach(json_decode($scheduleTypes, true) as $key => $value)
                                        <option value="{!! $key !!}" <?php echo Input::get('boat_schedule') == $key ? 'selected="selected"' : ''?>>
                                            {!! Str::title($value['english']) !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group date-picker-container mw-200">
                                <div class="input-group date f-arial future">
                                    <input type="text" name="schedule_date" class="form-control"
                                           value="{{ Input::get('from', date('d/m/Y')) }}"
                                           requried/>

                                    <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success f-arial">Search</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    @endif
                <!-- Check the user type  -->
                    @if(isset($list) && count($list))
                        @if (in_array(Auth::user()->user_type, array(BOAT_ADMIN ,BOAT_AGENT, BOAT_SUB_AGENT)))
                            @foreach($list as $key => $value)
                                <legend class="text-success f-arial"><?php echo Str::title($key);?></legend>
                                <ul class="route-list">
                                    <?php $index = 1;?>
                                    @foreach($value as $row)
                                        <li>
                                            <a href="{!!url('boat/route/search/'.$row->id.'/'.strtotime('now'))!!}">
                                                {!!Str::title($row->start_branch )!!} <i
                                                        class="fa fa-arrow-circle-right route-link"></i> {!! Str::title($row->end_branch)!!}

                                            </a>
                                            <?php
                                            $whole = json_decode($row->whole, true);

                                            $index = 1;
                                            ?>
                                            <p>

                                                @foreach($whole as $row)
                                                    {!!Str::title($row )!!}
                                                    @if(count($whole)!=$index)
                                                        <i class="fa fa-arrow-circle-right route-link"></i>
                                                    @endif
                                                    <?php $index++;?>
                                                @endforeach
                                            </p>
                                        </li>
                                        <?php $index++;?>
                                    @endforeach
                                </ul>
                                <div class="clearfix"></div>
                            @endforeach
                        @else
                            <ul class="route-list">
                                <?php $index = 1;?>
                                @foreach($list as $row)
                                    <li>
                                        <a href="{!!url('boat/route/search/'.$row->id.'/'.strtotime('now'))!!}">
                                            {!!Str::title($row->start_branch )!!} <i
                                                    class="fa fa-arrow-circle-right route-link"></i> {!! Str::title($row->end_branch)!!}

                                        </a>
                                        <?php
                                        $whole = json_decode($row->whole, true);
                                        $index = 1;
                                        ?>
                                        <p>
                                            @foreach($whole as $row)
                                                {!!Str::title($row )!!}
                                                @if(count($whole)!=$index)
                                                    <i class="fa fa-arrow-circle-right route-link"></i>
                                                @endif
                                                <?php $index++;?>
                                            @endforeach
                                        </p>
                                    </li>
                                    <?php $index++;?>
                                @endforeach
                            </ul>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop
