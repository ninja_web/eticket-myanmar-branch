@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">{!! "Branch"!!}</a></li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                        </ol>
                    </div>
                </div>
                @if(isset($branch)) <h4>{!! $branch->name !!}</h4> @endif
                <div class="mg10 tab-list">
                    <a href="@if(isset($branch)) {!! URL('boat/branch/'.$branch->id.'/edit')!!} @else {!!  URL('boat/agent/create')!!} @endif"
                       class="btn  btn-default"><i
                                class="fa fa-user-md fa-w"></i> &nbsp;{!! "Branch"!!}</a>
                    @if(Auth::user()->user_type != BOAT_ADMIN)
                        <a href="@if(isset($branch)) {!! URL('boat/branch/sale/'.$branch->id)!!} @else {!!  URL('#')!!} @endif"
                           class="btn  btn-default active"><i
                                    class="fa fa-users fa-w"></i>&nbsp;{!! "Sale"!!}</a>
                    @endif

                    <a href="@if(isset($branch)) {!! URL('boat/branch/finance/'.$branch->id)!!} @else {!!  URL('#')!!} @endif"
                       class="btn  btn-default"><i
                                class="fa fa-users fa-w"></i>&nbsp;{!! "Finance"!!}</a>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr class="f-arial">
                            <th class="text-center">No</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Location</th>
                            <th class="text-center">Check In / Out</th>
                            <th class="text-center">Phone No</th>
                            <th class="text-center">Main Address</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = Request::get('page', 1);?>
                        <?php $index = $index > 1 ? PER_PAGE * ($index - 1) + 1 : $index;?>
                        @foreach($sale as $user)
                            <tr>
                                <td class="text-center">{!! $index !!}</td>
                                <td class="text-center">{!! $user->name !!}</td>
                                <td class="text-center"><a href="{!! url('boat/sale/'.$user->id.'/edit') !!}">{!!
                                        $user->email !!}</a></td>
                                <td class="text-center">{!! Str::title($user->branch) !!}</td>
                                <td class="text-center text-success">{!! displayDate($user->check_in, 'h:i A'). ' to
                                    '.displayDate($user->check_out, 'h:i A')!!}
                                </td>
                                <td class="text-center">{!! $user->phone !!}</td>
                                <td class="text-center">{!! $user->address !!}</td>
                                <td class="text-center">{!! displayDate($user->created_at)!!}</td>
                                <td class="text-center">{!! showStatus($user->status)!!}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-primary "
                                       href="{!! url('boat/sale/'.$user->id.'/edit') !!}">
                                        <i class="fa fa-edit">&nbsp;</i>Edit</a>
                                </td>
                            </tr>
                            <?php $index++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
