@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-road"></i> Schedule Type</li>
                            <li>
                                <a class="btn btn-system f-arial" href="{{ url('/boat/scheduleType/create') }}"><i
                                            class="fa fa-plus"></i>&nbsp;{!! "New Schedule Type"
                            !!}</a>
                            </li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <table class="table table-bordered table-striped table-hover" id="tbList">
                        <thead>
                        <tr class="f-arial">
                            <th class="text-center">No</th>
                            <th class="text-center">Schedule</th>
                            <th class="text-center">Time Sequence</th>
                            <th class="text-center">Total Time</th>
                            <th>Boarding Points</th>
                            <th>Dropping Points</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = Request::get('page', 1);?>
                        <?php $index = $index > 1 ? PER_PAGE * ($index - 1) + 1 : $index;?>
                        @foreach($boat_schedule_type as $schedule_type)
                            <tr>
                                <td class="text-center">{!! $index !!}</td>
                                <td>{!! showRouteText(json_decode($schedule_type->whole, true), SCHEDULE_SEPERATOR)!!}
                                </td>
                                <td>
                                    <?php $seq = null != $schedule_type->time_sequence ? json_decode($schedule_type->time_sequence) : array();?>
                                    <?php $seq = implode(" , ", $seq);?>
                                    {!! $seq !!}
                                </td>
                                <td></td>
                                <td>{!! $schedule_type->b_points !!}</td>
                                <td>{!! $schedule_type->d_points !!}</td>

                                <td class="text-center">{!! showStatus($schedule_type->status)!!}</td>
                                <td class="text-center">{!! displayDate($schedule_type->created_at, 'F j Y')!!}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-primary  f-arial"
                                       href="{!! url('boat/scheduleType/'.$schedule_type->id.'/edit') !!}">
                                        <i class="fa fa-edit">&nbsp;</i>Edit</a>
                                </td>
                            </tr>
                            <?php $index++ ?>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- End Table -->
                    <div class="clearfix"></div>
                    <div class="pag-panel">
                        <div class="pull-right">
                            {!! $boat_schedule_type->render() !!}
                        </div>
                    </div>
                    <!-- paging panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop
