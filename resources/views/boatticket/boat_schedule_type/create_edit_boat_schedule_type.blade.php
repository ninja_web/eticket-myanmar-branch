@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li>
                                <a href="{!!url('boat/scheduleType')!!}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
                            </li>
                            <li>

                                <a href="">
                                    <i class="fa fa-road"></i> Create / Edit Boat Schedule Type</a></li>
                            <li class="pull-right">

                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"></strong>
                                <span><strong>-</strong></span>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <strong class="mandatory f-arial">*</strong> All fields must be required.
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="col-lg-12 no-padding f-arial">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    @if(isset($boatSchedule))
                        @if($boatSchedule->status == STATUS_ACTIVE)
                            <span class="pull-right">
                            <a class="btn btn-danger  f-arial inactivate-schedule"
                               onClick="javascript:void(0); return false;"
                               href="{{ URL::to('boat/scheduleType/'.$boatSchedule->id.'/status')}}"
                               style="margin-bottom:10px;"><i
                                        class="fa fa-trash"></i>&nbsp;&nbsp;Inactivate
                                Schedule</a>
                             </span>
                        @else
                            <span class="pull-right">
                          <a class="btn btn-success  f-arial activate-schedule"
                             onClick="javascript:void(0); return false;"
                             href="{{ URL::to('boat/scheduleType/'.$boatSchedule->id.'/status')}}"
                             style="margin-bottom:10px;"><i
                                      class="fa fa-upload"></i>&nbsp;&nbsp;Activate
                              Schedule</a>
                           </span>
                            @endif
                            @endif
                            </p>

                            <form class="form-horizontal" enctype="multipart/form-data" method="post"
                                  action="@if(isset($boatSchedule)) {{ URL::to('boat/scheduleType/'.$boatSchedule->code.'/edit')}} @else {{ URL::to('boat/scheduleType/create/')}} @endif"
                                  autocomplete="off" id="frmSchedule">
                                <!-- CSRF Token -->

                                <div class="col-md-7">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                                    <div class="form-group">
                                        <label class="col-xs-2 control-label f-arial">Total Route <strong
                                                    class="mandatory">*</strong></label>
                                        <label class="control-label colon">:</label>

                                        <div class="col-xs-4">
                                            <select class="form-control chosen-select" id="no-route"
                                                    name="total_route" <?php echo isset($boatSchedule) && $boatSchedule != null ? 'disabled' : ''?>>
                                                <?php $range = range(2, 5);?>
                                                @foreach($range as $step)
                                                    <option value="{!! $step !!}">{!! $step !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if(!isset($boatSchedule) || $boatSchedule == null)
                                            <div class="col-xs-1">
                                                <a class="btn btn-success e f-arial" id="gen-schedule">&nbsp;&nbsp;Generate
                                                    <i class="fa fa-arrow-circle-right"></i></a>

                                            </div>
                                        @endif
                                    </div>
                                    <?php
                                    $tmp_time = isset($boatSchedule) && $boatSchedule->total_time != '' ? explode(":", $boatSchedule->total_time) : array();
                                    ?>
                                    <div class="form-group">
                                        <label class="col-xs-2 control-label">Total Time<strong
                                                    class="mandatory">*</strong></label>
                                        <label class="control-label colon">:</label>

                                        <div class="col-xs-2" style="padding-right:0px;">
                                            <input type="text"
                                                   value="{!! Input::old('total_time', count($tmp_time) > 0 ? $tmp_time[0] : 0)!!}"
                                                   name="hour"
                                                   onkeydown="return numberOnly(event)" placeholder="00"
                                                   class="form-control" required maxlength="2"/>

                                        </div>
                                        <label class="control-label colon">:</label>

                                        <div class="col-xs-2" style="padding-left:0px;">
                                            <input type="text"
                                                   value="{!! Input::old('total_time', count($tmp_time) > 1 ? $tmp_time[1] : 0)!!}"
                                                   placeholder='00'
                                                   name="minute" onkeydown="return numberOnly(event)"
                                                   class="form-control" required maxlength="2"/>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-xs-2 control-label">{!! "Status"!!} <strong
                                                    class="mandatory">*</strong></label>
                                        <label class="control-label colon">:</label>

                                        <div class="col-xs-8 radio">
                                            <label>
                                                {!! Form::radio('status', STATUS_ACTIVE, Input::old('status', isset($boatSchedule) ?
                                                $boatSchedule->status : null) == STATUS_ACTIVE ?
                                                TRUE : FALSE )!!} Active
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                {!! Form::radio('status', STATUS_INACTIVE, Input::old('status', isset($boatSchedule) ?
                                                $boatSchedule->status : null) ==
                                                STATUS_INACTIVE ? TRUE : FALSE )!!} In Active
                                            </label>
                                        </div>
                                    </div>
                                    <?php $hide_status = count($errors) > 0 || isset($boatSchedule) ? '' : 'hide';?>
                                    <div class="form-group {!! $hide_status !!} route-panel">
                                        <label class="col-xs-2 control-label">Whole Route<strong
                                                    class="mandatory">*</strong></label>
                                        <label class="control-label colon">:</label>

                                        <div class="col-xs-9" id="whole-route">
                                            @if(count($errors) > 0 || Input::old('total_route', isset($boatSchedule) ? $boatSchedule->total_branch : 0) > 1)
                                                <?php $rtList = isset($boatSchedule) ? json_decode($boatSchedule->whole) : array(); ?>
                                                <?php for($i = 1; $i <= Input::old('total_route', isset($boatSchedule) ? $boatSchedule->total_branch : 0) ; $i++) { ?>
                                                <a class="btn btn-default">{!! Str::title(Input::old('route_'.$i,$rtList[$i -
                                                1]))!!}</a>
                                                @if($i != Input::old('total_route', isset($boatSchedule) ? $boatSchedule->total_branch  : 0) )
                                                    <i class="fa fa-arrow-circle-right route-link"></i></i>
                                                @endif
                                                <?php }?>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group form-inline route-panel {!! $hide_status !!}"
                                         id="route-choose">

                                        @if(count($errors) > 0 || Input::old('total_route') > 1)
                                            <?php for($i = 1; $i <= Input::old('total_route'); $i++){ ?>
                                            {!! Input::old('route_'.$i) !!}
                                            <div class="col-xs-2">
                                                <label class="control-label">{!! $i." Route " !!}</label>
                                                <select name="{!! 'route_'.$i !!}"
                                                        class="form-control chosen-select route-select"
                                                        id="{!! 'route_'.$i !!}" data-placeholder="Choose Route"
                                                        style="width:140px;">
                                                    <option value=""></option>
                                                    @foreach(json_decode(BUS_BRANCH_LIST, TRUE) as $b)
                                                        <option value="{!! $b !!}" {!! $b== Input::old('route_'.$i) ?
                                                    'selected="selected"' : '' !!}>{!! $b!!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <?php } ?>
                                        @endif
                                    </div>
                                    <div class="form-group {!! count($errors) > 0 ? '' : 'hide' !!} route-panel">
                                        <div class="col-xs-offset-2 col-xs-8">
                                            <button type="reset" class="btn btn-default f-arial">Cancel</button>
                                            <button type="submit" class="btn btn-success f-arial">Submit</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-5">

                                    <div class="form-group">
                                        <label class="col-xs-3 control-label">{!! 'Boarding Point' !!} <strong
                                                    class="mandatory">*</strong></label>
                                        <label class="control-label colon">:</label>

                                        <div class="col-xs-2">
                                            <input type="text"
                                                   value="{!!Input::old('b_points', isset($boatSchedule) ? $boatSchedule->b_points :null)!!}"
                                                   name="b_points"
                                                   class="form-control tags" data-role="tagsinput"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label">{!! 'Dropping Point' !!} <strong
                                                    class="mandatory">*</strong></label>
                                        <label class="control-label colon">:</label>

                                        <div class="col-xs-2">
                                            <input type="text"
                                                   value="{!! Input::old('d_points', isset($boatSchedule) ? $boatSchedule->d_points :null)!!}"
                                                   name="d_points"
                                                   class="form-control tags"/>

                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                            <div class="col-lg-6 @if(isset($boatSchedule)) {!! '' !!} @else {!! 'hide' !!} @endif">
                                <legend><h4 class="text-success f-arial">Time Sequence</h4></legend>
                                @if(null != isset($boatSchedule) && $boatSchedule->time_sequence)
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr class="f-arial">
                                            <th>No</th>
                                            <th><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Sequence</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1;?>
                                        @foreach(json_decode($boatSchedule->time_sequence) as $seq)
                                            <tr class="f-arial">
                                                <td>{!! $i !!}</td>
                                                <td>{!! date('h:i A', strtotime($seq)). " ( ". date('h:i', strtotime($seq))." )" !!}
                                                </td>
                                                <td><a class="btn btn-danger  f-arial btn-xs removeTime"
                                                       onClick="javascript:void(0); return false;"
                                                       href="{!! URL::to('boat/scheduleType/sequence/delete/'.$boatSchedule->id.'/'.strtotime($seq)) !!}">
                                                        <i class="fa fa-trash-o">&nbsp;&nbsp;</i>Delete</a></td>
                                            </tr>
                                            <?php $i++;?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>


                            <div class="col-lg-6 @if(isset($boatSchedule)) {!! '' !!} @else {!! 'hide' !!} @endif">
                                <legend><h4 class="text-success f-arial">Create Sequence</h4></legend>
                                <form class="form-horizontal" method="post"
                                      action="@if(isset($boatSchedule)) {!! URL::to('boat/scheduleType/sequence/'.$boatSchedule->id) !!} @else {!! Url::to("
                            ") !!}  @endif"
                                      autocomplete="off">
                                    <!-- CSRF Token -->
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                                    <div class="form-group">
                                        <label class="col-xs-3 control-label f-arial">{!! 'Time Sequence' !!} <strong
                                                    class="mandatory">*</strong></label>

                                        <div class="col-xs-4">
                                            <div class="input-group input-group bootstrap-timepicker">
                                                <input type="text" name="time_sequence" class="form-control time-picker"
                                                       value="{{ Input::old('time_sequence')  }}"
                                                       required/>
                                                <span class="input-group-addon add-on"><i
                                                            class="fa fa-clock-o"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-offset-3 col-md-4">
                                            <button type="reset" class="btn btn-default f-arial">Cancel</button>
                                            <button type="submit" class="btn btn-success f-arial">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $routeList = array();
    foreach (json_decode(BOAT_BRANCH_LIST, true) as $b) {
        $routeList[$b] = strtoupper($b);
    }
    ?>
    <!-- Tags Input -->
    <link href="{{ asset('/css/plugins/jquery.tagsinput.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('/js/plugins/jquery.tagsinput.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            // loaded the tags input
            $('.tags').tagsInput();
        });
    </script>

    <link href="{{ asset('/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css">
    <!-- Time Picker JS -->
    <script src="{{ asset('/js/bootstrap-timepicker.js')}}"></script>
    <script type="text/javascript">
        $('.time-picker').timepicker({
            minuteStep: 5
        });
        var routeList = <?php echo json_encode($routeList); ?>;
        $(function () {

            /* Checking Form Validation */

            $("form#frmSchedule").on("submit", function (e) {

                e.preventDefault();

                var $dest = $("input[name='b_points']").val();

                if ($dest.length == 0) {

                    messagebar_toggle("Please set Boarding Point.", 'info', 4000);

                    return false;
                }

                var $dest = $("input[name='d_points']").val();

                if ($dest.length == 0) {

                    messagebar_toggle("Please set Dropping Point.", 'info', 4000);

                    return false;
                }

                $(".route-panel select").each(function () {

                    var $name = $(this).attr('name');
                    var $val = $(this).val();

                    if ($val.length == 0) {

                        $message = "Please set destination for " + $name + ".";

                        messagebar_toggle($message, 'info', 4000);


                        return false;
                    }
                });

                this.submit();

            });


            $('#gen-schedule').click(function (e) {
                var no_route = $('#no-route').val();
                var whole_route = '';
                var route_select = '';
                // generate the route text
                for (var i = 1; i <= no_route; i++) {
                    whole_route += '<a class="btn btn-default" id="route-text-' + i + '">....</a>';
                    if (i != no_route) {
                        whole_route += '&nbsp;&nbsp;<i class="fa fa-arrow-circle-right route-link"></i></i>&nbsp;&nbsp;';
                    }
                }
                $('#whole-route').html(whole_route);
                $('.route-panel').hasClass('hide') ? $('.route-panel').removeClass('hide') : '';

                // generate the route drop download
                var option_list = '';
                $.each(routeList, function (index, value) {
                    option_list += "<option value='" + index + "'>" + value + "</option>";
                });
                var route = '';
                route = '<div class="col-xs-2"></div>';
                for (var i = 1; i <= no_route; i++) {
                    route += '<div class="col-xs-3">'
                            + '<label class="control-label">' + i + ' Route </label>'
                            + '<select class="form-control chosen-select route-select" data-placeholder="Choose Route" name="route_' + i + '" id="route_' + i + '" style="width:180px;"><option value=""></option>' + option_list + '</select>'
                            + '</div>';
                }

                $('#route-choose').html(route);
                // activate the chosen
                if (jQuery().chosen) {
                    $('.chosen-select').chosen({
                        allow_single_deselect: true
                    });
                    $(".chosen-select").css({minWidth: '100%', width: 'auto'});
                }
                e.preventDefault();
            });

            $(document).on('change', '#route-choose .route-select', function (e) {
                var id = $(this).attr('id');
                var route_text = typeof $('#' + id + 'option:selected').text() != 'undefined' ? $('#' + id + ' option:selected').text() : '';
                id = id.replace('route_', '');
                $('#route-text-' + id).html(route_text);
            });
        });
    </script>
    <!-- Remove Date -->
    <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
    <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
    <script type="text/javascript">
        $(function () {
            $('.removeTime').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-danger',
                    title: '<div id="dia-title">Delete Time Sequence</div>',
                    message: '<div id="dia-message">Are you sure you want to delete these time squence.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-danger',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        $('#dia-title').text("Fail");
                                        $("#dia-message").text(data.message);

                                        $(".modal .btn-danger").hide();

                                        $(".modal").removeClass("type-danger").addClass("type-warning");

                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
            // activate schedule
            $('.inactivate-schedule').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-danger',
                    title: '<div id="dia-title">Schedule Status</div>',
                    message: '<div id="dia-message">Are you sure you want to inactivate status these schedule.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-danger',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
            // in activate scheule
            $('.activate-schedule').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-success',
                    title: '<div id="dia-title">Schedule Status</div>',
                    message: '<div id="dia-message">Are you sure you want to activate these schedule.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-success',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
        });
    </script>
@stop
