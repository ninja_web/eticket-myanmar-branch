@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb f-arial">
                        <li class="active"><i class="fa fa-file-pdf-o"></i> Ticket Search</li>
                        <li class="active"><a class="btn btn-xs btn-system"
                                              href="{!!url('boat/ticket/orders/'.$info->id)!!}"><i
                                        class="fa fa-list"></i> Order Detail</a></li>
                        <li class="pull-right">

                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                            <strong> - </strong>
                            <span id="cur-time"></span>
                        </li>
                    </ol>
                </div>


                <div class="clearfix"></div>


            </div>
        </div>
    </div>
@stop
