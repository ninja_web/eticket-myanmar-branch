@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb f-arial">
                        <li class="active">Ticket Search</li>
                        <li class="pull-right">
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                            <strong> - </strong>
                            <span id="cur-time"></span>
                        </li>
                    </ol>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                </div>

                <div class="clearfix"></div>

                @if(isset($search) && !isset($info))
                    <div class="alert alert-info">No Result found</div>
                @elseif(isset($search) && !empty($info))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a href="#" class="btn btn-success btn-block">
                                                <i class="fa fa-envelope"></i>
                                                Send E-Mail
                                            </a>
                                        </div>
                                        <div class="col-md-4">

                                            <a href="#" class="btn btn-success btn-block">
                                                <i class="fa fa-comment"></i>
                                                Send SMS
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="{!!url('boat/ticket/print/'.$info->ticket_id)!!}"
                                               class="btn btn-success btn-block">
                                                <i class="fa fa-print" target="_blank"></i>
                                                Print Ticket
                                            </a>
                                        </div>

                                    <!-- <div class="col-md-6 f-arial">
                                        Ticket No : {!!$info->ticket_pnr_code!!}
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{!!url('boat/ticket/checkin/pdf',$info->ticket_id)!!}"
                                           class="btn btn-info pull-right">Generate PDF</a>
                                    </div> -->
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal ticket-search" role="form">
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>

                                            <div class="col-md-6">
                                                <p style="margin-bottom:0px;">Booking Reference:</p>
                                                <p class="booking-reference">
                                                    <strong>{!!$info->ticket_pnr_code!!}</strong>
                                                </p>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-12">

                                                @if($info->upload_type!='' && $info->upload_type==1)
                                                    <h4>You have uploaded file.</h4>
                                                    <br>
                                                    <br>
                                                @else
                                                    <table class="table table-bordered table-striped">
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Passport / NRC No</th>
                                                            <th>Seat No</th>
                                                            <th>Cotact</th>
                                                        </tr>
                                                        @if(isset($passengers) && count($passengers))
                                                            @foreach($passengers as $passenger)


                                                                <tr>
                                                                    <td>

                                                                        @if($passenger->name_prefix!='')
                                                                            <?php $prefix = json_decode(NAME_PREFIX, true);?>
                                                                            <p class="form-control-static">{!!$prefix[$passenger->name_prefix]." ".$passenger->name!!}</p>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if($passenger->country!='' && $passenger->coutnry=="foreign")
                                                                            <p>
                                                                                {!!$passenger->passport - $passenger->email!!}
                                                                            </p>
                                                                        @else

                                                                            <p>
                                                                                {!!$passenger->nrc_prefix.$passenger->nrc_state."(".$passenger->nrc_nation.")".$passenger->nrc_no!!}</p>

                                                                        @endif

                                                                    </td>
                                                                    <td><p>{!!$passenger->seat_no!!}</p></td>
                                                                    <td><p>{!!$passenger->phone_no!!}</p></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                @endif
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-12">
                                                <table class="table table-bordered table-striped">
                                                    <tr>
                                                        <th>Route</th>
                                                        <th>Date</th>
                                                        <th>Depature Time</th>
                                                        <th>Jetty</th>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>{!! showRouteText(array($info->start_branch, $info->end_branch), SCHEDULE_SEPERATOR)!!}</p>
                                                        </td>
                                                        <td>
                                                            <p>{!!date('M d Y',strtotime($info->dept_date))!!}</p>
                                                        </td>
                                                        <td><p> {!!displayDate($info->dept_time, 'H:i A')!!}</p></td>
                                                        <td></td>

                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
            </div>
        </div>
    </div>
@stop
