@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb f-arial">
                        <li class="active"><i class="fa fa-file-pdf-o"></i> Ticket Search</li>
                        <li class="active"><a class="btn btn-xs btn-system"
                                              href="{!!url('boat/ticket/orders/'.$info->id)!!}"><i
                                        class="fa fa-list"></i> Order Detail</a></li>
                        <li class="pull-right">

                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                            <strong> - </strong>
                            <span id="cur-time"></span>
                        </li>
                    </ol>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                </div>

                <div class="clearfix"></div>

                @if(isset($search) && !isset($info))
                    <div class="alert alert-info">No Result found</div>
                @elseif(isset($search) && !empty($info))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a data-toggle="modal" data-target="#myModal"
                                               class="btn btn-success btn-block">
                                                <i class="fa fa-envelope"></i>
                                                Send E-Mail
                                            </a>
                                        </div>
                                        <div class="col-md-4">

                                            <a href="#" class="btn btn-success btn-block">
                                                <i class="fa fa-comment"></i>
                                                Send SMS
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="{!!url('boat/ticket/print/'.$info->ticket_id)!!}"
                                               class="btn btn-success btn-block">
                                                <i class="fa fa-print" target="_blank"></i>
                                                Print Ticket
                                            </a>
                                        </div>

                                    <!-- <div class="col-md-6 f-arial">
                                        Ticket No : {!!$info->ticket_pnr_code!!}
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{!!url('boat/ticket/checkin/pdf',$info->ticket_id)!!}"
                                           class="btn btn-info pull-right">Generate PDF</a>
                                    </div> -->
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal ticket-search" role="form">
                                        <div class="row">
                                            <div class="col-md-8">

                                                <table class="table table-bordered table-striped">
                                                    <tr>
                                                        <th>Route</th>
                                                        <th>Date</th>
                                                        <th>Depature Time</th>
                                                        <th>Jetty</th>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>{!! showRouteText(array($info->start_branch, $info->end_branch), SCHEDULE_SEPERATOR)!!}</p>
                                                        </td>
                                                        <td>
                                                            <p>{!!date('M d Y',strtotime($info->dept_date))!!}</p>
                                                        </td>
                                                        <td><p> {!!displayDate($info->dept_time, 'H:i A')!!}</p></td>
                                                        <td></td>

                                                    </tr>
                                                </table>

                                            </div>

                                            <div class="col-md-4">
                                                <p style="margin-bottom:0px;">Booking Reference:</p>
                                                <p class="booking-reference">
                                                    <strong>{!!$info->ticket_pnr_code!!}</strong>
                                                </p>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-12">

                                                @if($info->upload_type!='' && $info->upload_type==1)
                                                    <h4>You have uploaded file.</h4>
                                                    <br>

                                                    <a href="{!!url('boat/ticket/details/download/'.$info->ticket_id)!!}"
                                                       class="btn btn-system"><i class="fa fa-download"></i> Download
                                                        Uploaded File</a>
                                                @else
                                                    <table class="table table-bordered table-striped">
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Passport / NRC No</th>
                                                            <th>Seat No</th>
                                                        </tr>

                                                        @if($info->type!=BOOK)
                                                            @if(isset($passengers) && count($passengers))
                                                                @foreach($passengers as $passenger)
                                                                    <tr>
                                                                        <td>

                                                                            @if($passenger->name_prefix!='')
                                                                                <?php $prefix = json_decode(NAME_PREFIX, true);?>
                                                                                <p class="form-control-static">{!!$prefix[$passenger->name_prefix]." ".$passenger->name!!}</p>
                                                                            @endif
                                                                        </td>
                                                                        <td>

                                                                            @if($passenger->country!='' && $passenger->country=="foreign")
                                                                                <p>
                                                                                    {!!$passenger->passport!!}
                                                                                </p>
                                                                            @else

                                                                                <p>
                                                                                    {!!$passenger->nrc_prefix.$passenger->nrc_state."(".$passenger->nrc_nation.")".$passenger->nrc_no!!}</p>

                                                                            @endif

                                                                        </td>
                                                                        <td><p>{!!$passenger->seat_no!!}</p></td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <tr>
                                                                <td>
                                                                    <?php
                                                                    $name = json_decode(NAME_PREFIX, true);
                                                                    ?>

                                                                    {!!$name[$info->name_prefix]!!}

                                                                    {!!$info->name!!}
                                                                </td>
                                                                <td>

                                                                </td>
                                                                <td>
                                                                    @if($info->seat_plan==WITHOUT_SEAT_PLAN)

                                                                        {!!$info->total_seat!!}

                                                                    @else

                                                                        {!!$info->seat_no!!}
                                                                    @endif
                                                                </td>


                                                            </tr>
                                                        @endif
                                                    </table>
                                                @endif
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Sending Email</h4>
                </div>

                {!! Form::open(array('url' => 'boat/ticket/sendmail/'.$info->ticket_id, 'method' => 'POST', 'class' =>
                      'form-horizontal frmMailForm',
                      'role' => 'form'))!!}

                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="modal-body">


                    <div class="form-group">
                        <label class="col-xs-3 control-label">Recipient mail<strong
                                    class="mandatory">*</strong></label>
                        <label class="control-label colon">:</label>

                        <div class="col-xs-8">
                            <input type="email" name="email" class="form-control" required="true"/>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success btnSubmit">Send <i class="fa fa-spin fa-refresh hide"
                                                                                    id="email_loading"></i></button>
                </div>

                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <script>

        $(document).on('submit', 'form.frmMailForm', function (e) {

            e.preventDefault();

            var $form = $(this);

            $("i#email_loading").removeClass("hide");

            $(".frmMailForm button").prop('disabled', true);


            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: 'json',
                success: function (data) {

                    location.reload();
                    if (data.success) {


                    } else {

                    }
                }
            });
        });
    </script>

@stop
