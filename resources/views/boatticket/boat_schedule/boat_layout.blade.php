<?php
$seater_plan = array();
$seater_class = "";

if (SEATER_98 == $boat_class->seater) {
    $seater_plan = json_decode(SEATER_98_PLAN, true);
    $seater_class = "s98_seater";
} else if (SEATER_123 == $boat_class->seater) {
    $seater_plan = json_decode(SEATER_123_PLAN, true);
    $seater_class = "s123_seater";
}
?>

@if(count($seater_plan) && !empty($seater_class))
    <h4 id="lbl-total-seat"><strong class="text-success f-arial">Total Availiable Seat - <span
                    id="total-seat">{!! $boat_class->seater!!}</span></strong>
    </h4>
    <div class="row {!!$seater_class!!}">
        <div class="car-layout {!! $seater_plan['type'] !!}">
            <div class="seat-driver img_availiable_seat "></div>
            <div class="boat-style">
                @foreach($seater_plan['seat'] as $key => $value)
                    @if("middle" == $value)
            </div>
            <!-- end boat style -->
            <ul class="row-margin middle-row">
                <li></li>
            </ul>
            <div class="boat-style">
                @else

                    <?php $rows = explode(",", $value);?>
                    <ul class="row-margin">
                        @foreach($rows as $seat)
                            <li class="seat">
                                @if(!empty($seat))
                                    <a class="availableSeat select-seat" id="{!! $seat !!}">
                                        <span class="seat-no">{!! $seat !!}</span>
                                    </a>

                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif
                @endforeach
            </div>
            <!-- end boat style -->
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <ul class="car seat-description" style="margin-top:15px;">
            <li><i class="seat availableSeat"></i><span>Availiable Seat</span></li>
            <li><i class="seat disabledSeat"></i><span>Block Seat</span></li>
        </ul>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var selectId = new Array();
            var totalSeat = "<?php echo $boat_class->seater?>";

            // update the seat availiable
            $('#block-seat').val('');
            $('ul.row-margin li a').click(function (e) {
                var aClass = $(this).attr("class");
                // console.log($(this).attr("id"));
                if ($(this).hasClass('seat disabledSeat')) {
                    // remove selected seat
                    selectId.pop($(this).attr("id"));
                    $('#total-seat').html(totalSeat - selectId.length).fadeIn();
                    $(this).removeClass("seat disabledSeat").addClass("seat availableSeat");
                } else {
                    // adding the select Id to hidden value
                    selectId.push($(this).attr("id"));
                    $('#total-seat').html(totalSeat - selectId.length);
                    $(this).removeClass("seat availableSeat").addClass("seat disabledSeat");
                }
                // ading hidden value
                $("#block-seat").val(selectId);

                $("p.lbl-blocked-seat").remove();
                var $p = $("<p/>", {'class': 'lbl-blocked-seat'});
                $p.append("Blocked Seat No: ");
                var span = $("<span/>");
                span.text(selectId);
                $p.append(span);
                $("h4#lbl-total-seat").append($p);
            });
        });
    </script>
@else
    <?php echo $boat_class->seater;  ?>
@endif
