@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-exchange"></i> Schedule</li>
                            <li class="pull-right">

                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <a class="btn btn-system f-arial" href="{{ url('/boat/schedule/create') }}"><i
                                            class="fa fa-plus"></i>&nbsp;Create Schedule</a>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12 no-padding" style="margin-bottom:10px;">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <form class="form-inline" enctype="multipart/form-data" method="post"
                          action="{!!url('boat/schedule/search')!!}"
                          autocomplete="off">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <label class="f-arial">Strat Date: </label>
                        <div class="form-group date-picker-container mw-200">
                            <div class="input-group date f-arial future">
                                <input type="text" name="start_date" class="form-control"
                                       value="{!! array_key_exists('start_date', $filter) ? $filter['start_date']:date('d/m/Y')!!}"
                                       requried/>

                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        <label class="f-arial">End Date: </label>
                        <div class="form-group date-picker-container mw-200">
                            <div class="input-group date f-arial future">
                                <input type="text" name="end_date" class="form-control"
                                       value="{!! array_key_exists('end_date', $filter) ?  $filter['end_date']:date('d/m/Y')!!}"
                                       requried/>

                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <select class="form-control chosen-select" id="destination"
                                    name="destination">
                                <option value="">Select Destination</option>
                                @foreach(json_decode($list, true) as $key => $value)
                                    <?php $selected = array_key_exists('destination', $filter) && $filter['destination'] == $key ? 'selected="selected"' : '';?>
                                    <option value="{!! $key !!}" {!! $selected !!}>
                                        {!! Str::title($value['english']) !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-system"><i class="fa fa-search"></i> Search</button>

                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 no-padding">
                    @if(isset($schedules) && count($schedules))
                        <table class="table table-bordered table-striped table-hover" id="tbList">
                            <thead>
                            <tr class="f-arial">
                                <th class="text-center">No.</th>
                                <th class="text-center">Schedule</th>
                                <th class="text-center">Depature Date</th>
                                <th class="text-center">Depature Time</th>
                                <th class="text-center">Boat Order</th>
                                <th class="text-center">Boat Type</th>
                                <th class="text-center">Sold Seat</th>
                                <th class="text-center">Availiable Seat</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $index = 1;?>
                            @foreach($schedules as $schedule)
                                <tr>
                                    <td class="text-center">{!! $index !!}</td>
                                    <td>{!! showRouteText(json_decode($schedule->whole, true), SCHEDULE_SEPERATOR)!!}
                                    <td class="text-center">{!! displayDate($schedule->dept_date, 'F j Y') !!}</td>
                                    <td class="text-center">{!! displayDate($schedule->dept_time, 'h:i A') !!}</td>
                                    <td class="text-center">{!! $schedule->boat_order ." Boat "!!}</td>
                                    <td class="text-center">{!! $schedule->boat_class !!}</td>
                                    <td class="text-center"><span
                                                class="badge badge-danger"> {!! $schedule->total_seat - $schedule->remain_seat !!}</span>
                                    </td>
                                    <td class="text-center"><span
                                                class="badge badge-green">{!! $schedule->remain_seat !!}</span>
                                    </td>
                                    <td class="text-center">{!! showStatus($schedule->status)!!}</td>
                                    <td class="text-center">
                                        @if($schedule->status == STATUS_ACTIVE)
                                            <a class="btn btn-sm btn-danger disable"
                                               onClick="javascript:void(0); return false;"
                                               href="{!! URL::to('boat/schedule/toggle/disable/'.$schedule->id) !!}">Disable</a>
                                        @else
                                            <a class="btn btn-sm btn-success enable"
                                               onClick="javascript:void(0); return false;"
                                               href="{!! URL::to('boat/schedule/toggle/enable/'.$schedule->id) !!}">Enable</a>
                                        @endif
                                    </td>
                                </tr>
                                <?php $index++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info mt10">
                            <i class="fa fa-info-circle"></i> There is no record.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Remove Date -->
    <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
    <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
    <script type="text/javascript">
        $(function () {
            $('.disable').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-danger',
                    title: '<div id="dia-title">Disable Boat Schedule</div>',
                    message: '<div id="dia-message">Are you sure you want to disable boat schedule?.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-danger',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
            $('.enable').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-success',
                    title: '<div id="dia-title">Enable Boat Schedule</div>',
                    message: '<div id="dia-message">Are you sure you want to enable boat schedule ?.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-success',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });

        });
    </script>
@stop
