@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">

                            <li>
                                <a href="{!!url('boat/schedule')!!}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
                            </li>

                            <li><i class="fa fa-exchange"></i> Create Schedule</li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <p><strong class="mandatory">*</strong> All fields must be required.</p>
                    <form class="form-horizontal" method="post"
                          action="@if(isset($boatClass)) {{ URL::to('boat/schedule/'.$boatClass->id.'/edit')}} @else {{ URL::to('boat/schedule/create/')}} @endif"
                          autocomplete="off" id="frmSchedule">

                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" value="" name="block_seat" id="block-seat"/>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Destination
                                                <strong class="mandatory">*</strong></label>
                                            <div class="col-xs-12" style="padding:0px;">
                                                <select class="form-control chosen-select" id="destination"
                                                        name="destination">
                                                    <option value="">Select Destination</option>
                                                    @foreach(json_decode($schedules, true) as $key => $value)
                                                        <option value="{!! $key !!}" <?php echo Input::old('destination') == $key ? 'selected="selected"' : ''?>>
                                                            {!! Str::title($value['english']) !!}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Price
                                                <strong
                                                        class="mandatory">*</strong></label>

                                            <div class="col-xs-12" style="padding:0px;">
                                                <div id="whole-route"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="details" class="">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label>Time
                                                    <strong
                                                            class="mandatory">*</strong></label>


                                                <div class="col-xs-12" style="padding:0px;">
                                                    <select class="form-control chosen-select" name="dept_time"
                                                            id="time-sequence">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">

                                                <label>Boat Order
                                                    <strong class="mandatory">*</strong></label>
                                                <div class="col-xs-12" style="padding:0px;">
                                                    <select class="form-control chosen-select" name="boat_order">
                                                        <option>Boat Order</option>
                                                        @foreach(range(1, 20) as $key)
                                                            <option value="{!! $key !!}" {!! Input::old('boat_order') ==$key
                                                            ?
                                                            'selected="selected"' : '' !!}>{!! $key." - Boat"!!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="col-md-4">

                                                <label>Permission
                                                    <strong
                                                            class="mandatory">*</strong></label>


                                                <div class="col-xs-12" style="padding:0px;">
                                                    <select class="form-control chosen-select" name="permission">
                                                        @<?php foreach (json_decode(SCHEDULE_PERMISSION, true) as $key => $value): ?>
                                                        <option value="{!! $key !!}" {!! Input::old(
                                                        'permission' ) == $key ? 'selected="selected"' : ''!!}>{!!
                                                        $value!!}</option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="col-md-6">
                                                <label>Start Date
                                                    <strong
                                                            class="mandatory">*</strong></label>


                                                <div class="col-xs-12 date-picker-container" style="padding:0px;">
                                                    <div class="input-group date future">
                                                        <input type="text" name="start_date" class="form-control"
                                                               value="{{ Input::old('start_date', isset($branch) ? $branch->dob : null) }}"
                                                               requried/>
                                                        <span class="input-group-addon"><i
                                                                    class="fa fa-calendar-o"></i></span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <label>End Date
                                                    <strong
                                                            class="mandatory">*</strong></label>


                                                <div class="col-xs-12 date-picker-container" style="padding:0px;">
                                                    <div class="input-group date future">
                                                        <input type="text" name="end_date" class="form-control"
                                                               value="{{ Input::old('end_date', isset($branch) ? $branch->dob : null) }}"
                                                               requried/>
                                                        <span class="input-group-addon"><i
                                                                    class="fa fa-calendar-o"></i></span>
                                                    </div>
                                                </div>

                                            </div>


                                            <!--  <div class="col-xs-7 col-xs-offset-4 date-picker-container">
                                                 <label class="control-label pl18">
                                                     <a href="#" id="toggleRepeat" class="f-arial">
                                                         <i class="fa fa-edit"></i>&nbspRepeat
                                                         Schedule</a>
                                                 </label>
                                             </div> -->
                                        </div>


                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>Repeat Option</label>
                                            <select class="form-control" name="repeat_type">
                                                @<?php foreach (json_decode(REPEAT_TYPE, true) as $key => $value): ?>
                                                <option value="{!! $key !!}" {!! Input::old(
                                                    'repeat_type' ) == $key ? 'selected="selected"' : ''!!}>{!!
                                                    $value!!}</option>
                                                <?php endforeach; ?>
                                            </select>

                                        </div>
                                        <div class="col-md-6">
                                            <label>Boat Class<strong
                                                        class="mandatory">*</strong></label>


                                            <div class="col-xs-12" style="padding:0px;">
                                                <select class="form-control chosen-select" name="boat_class"
                                                        id="boat-class">
                                                    <option value="">Select Boat Class</option>
                                                    @foreach($boat_classes as $key => $value)
                                                        <option value="{!! $key !!}">{!! $value['class'].' ('. $value['seater'].'
                                                            seater )'!!}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Remark</label>
                                            <div class="col-xs-12" style="
                                            padding:0px;">
                                                {!! Form::textarea('remark', '', array('class' => 'form-control', 'rows' => 2))!!}
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-md-7">
                                    <!-- Seat Here -->
                                    <div class="photoloading-bar" id="loadbar"></div>
                                    <div class="car-class"></div>

                                    <!-- End Seat Here -->

                                </div>
                                <!-- start without seat panel !-->
                                <div class="col-md-6 dn mt10" id="without-seat-plan">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="panel-body without-seat-body-sm">
                                                <div class="cricle">
                                                    <strong id="lbl_total">100</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">No. of Seat <strong
                                                    class="mandatory">*</strong></label>
                                        <label class="control-label colon">:</label>

                                        <input type="hidden" value="0" name="hidd_without_total">

                                        <div class="col-xs-4">
                                            <div class="input-group">
                                              <span class="input-group-btn">
                                                  <button type="button" class="btn btn-default btn-number"
                                                          data-type="minus" data-field="no_seat">
                                                      <span class="glyphicon glyphicon-minus"></span>
                                                  </button>
                                              </span>
                                                <input type="text" name="no_seat" class="form-control input-number h34"
                                                       value="0" min="0" max="300" autocomplete="off">

                                                <span class="input-group-btn">
                                                  <button type="button" class="btn btn-default btn-number"
                                                          data-type="plus" data-field="no_seat">
                                                      <span class="glyphicon glyphicon-plus"></span>
                                                  </button>
                                              </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">Remain Seat</label>
                                        <label class="control-label colon">:</label>
                                        <div class="col-xs-4">
                                            <span id="remain_seat">0</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- end without seat panel -->
                            </div>
                        </div>
                        <div class="col-md-12">


                            <!-- Block Seat -->

                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-xs-offset-2 col-xs-8">
                                    <button type="reset" class="btn btn-default f-arial">Cancel</button>
                                    <button type="submit" class="btn btn-success f-arial">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $("form#frmSchedule").on("submit", function (e) {

                e.preventDefault();
                var $dest = $("select[name='destination']").val();

                if ($dest.length == 0) {

                    messagebar_toggle("Please choose Destination.", 'info', 4000);

                    return false;
                }

                var $dept_time = $("select[name='dept_time']").val();

                if ($dept_time.length == 0) {

                    messagebar_toggle("Please choose Time.", 'info', 4000);

                    return false;
                }

                var $boat_order = $("select[name='boat_order']").val();

                if ($boat_order == "Boat Order") {

                    messagebar_toggle("Please choose Boat Order.", 'info', 4000);

                    return false;
                }

                var $start_date = $("input[name='start_date']").val();

                if ($start_date.length == 0) {

                    messagebar_toggle("Please choose Start Date.", 'info', 4000);

                    return false;
                }

                var $boat_class = $("select[name='boat_class']").val();

                if ($boat_class.length == 0) {

                    messagebar_toggle("Please choose Boat Class.", 'info', 4000);

                    return false;
                }

                this.submit();
                // return false;
            });
        });
    </script>
    <script type="text/javascript">
        // TODO : Ninja : Create the block_seat
        var destination = <?php echo $schedules;?>;
        $(function (e) {
            $('.repeat').hide();

            // toggle the boat class
            $('#boat-class').change(function () {
                $("#loadbar").show();
                // call the loading page
                var select_class = $(this).val();
                var url = '/boat/schedule/create/toggleClass/' + select_class;
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        if (data.length > 20) {
                            $(".car-class").html(data);
                            $(".car-class").fadeIn().delay(2000);

                            var $index = 0;
                            $(".seater-123 .boat-style").each(function () {
                                if ($index == 1) {
                                    $(this).append("<h2>Front</h2>");
                                    $(this).append("<h2>Sun Desk</h2>");
                                    $(this).append("<h2>Upper Desk</h2>");
                                }
                                if ($index == 3) {
                                    $(this).append("<h2 class='lower'>Lower Desk</h2>");
                                }
                                $index++;
                                if ($index == 1 || $index == 3) {
                                    $(this).addClass('bnone pt30');
                                }
                                if ($index == 2 || $index == 4) {
                                    $(this).addClass('mt_27 pt30');
                                }
                            });
                            $("div#without-seat-plan").addClass("dn");

                        } else {
                            $(".car-class").html("");
                            $("div#without-seat-plan").removeClass("dn");
                            $("strong#lbl_total").text(data);
                            $("input[name='hidd_without_total']").val(data);
                            $("span#remain_seat").text(data);
                        }
                    },
                    error: function () {

                    }
                });
            });
            // toggle the repeat link
            $('#toggleRepeat').click(function () {
                $('.repeat').show();
            });

            // toggle the destination
            $('#destination').change(function () {
                // change the price text boxs list
                var id = $(this).val();
                // get the destination details
                var selectedRoute = destination[id];
                // update the time sequences
                $('#time-sequence').empty();
                var opt = $('<option />');
                opt.val('');
                opt.text('Select Time');
                $('#time-sequence').append(opt);
                $.each(selectedRoute['time_sequence'], function (id, value) {
                    // check model_id
                    var opt = $('<option />');
                    opt.val(value);
                    opt.text(value);
                    $('#time-sequence').append(opt);
                });
                $("#time-sequence").trigger("chosen:updated");

                // geneate the price setting
                var whole_route = "";
                var engRoute = selectedRoute['eng_whole'];
                for (var i = 0; i < selectedRoute['total_branch']; i++) {
                    for (var j = i + 1; j < selectedRoute['total_branch']; j++) {
                        whole_route += '<div class="form-group">'
                                + '<label class="col-xs-6 control-label">' + camelCase(engRoute[i]) + '&nbsp;&nbsp;<i class="fa fa-arrow-circle-right route-link"></i>&nbsp;&nbsp;' + camelCase(engRoute[j]);
                        if (i == 0 & j == selectedRoute['total_branch'] - 1) {
                            whole_route += '<strong class="mandatory">*</strong></label>';
                        } else {
                            whole_route += '</label>'
                        }
                        whole_route += '<label class="control-label colon">:</label><div class="from-control col-xs-4">';
                        if (i == 0 & j == selectedRoute['total_branch'] - 1) {
                            whole_route += '<input type="text" class="form-control f-arial" autocomplete="off" name="' + engRoute[i] + '_' + engRoute[j] + '" id="' + engRoute[i] + '_' + engRoute[j] + '" required="required">';
                            whole_route += '<input type="hidden" name="selected_route" value="0">';
                            whole_route += '<input type="hidden" name="route" value="' + engRoute[i] + '_' + engRoute[j] + '">';
                        } else {
                            whole_route += '<input type="text" class="form-control f-arial" autocomplete="off" name="' + engRoute[i] + '_' + engRoute[j] + '" id="' + engRoute[i] + '_' + engRoute[j] + '">';
                        }
                        whole_route += '</div><div class="col-xs-1 pl0 pr0"><strong class="pt10">$</strong></div></div>';
                    }
                }
                $('#whole-route').html(whole_route);
            });
        });
        function camelCase(str) {
            return str.replace(/(?:^|\s)\w/g, function (match) {
                return match.toUpperCase();
            });
        }
        /* boat 123 style */
    </script>

    <style type="text/css">
        .pl0 {
            padding-left: 0px;
        }

        .pr0 {
            padding-right: 0px;
        }

        .pt10 {
            padding-top: 8px;
            display: inline-block;
        }

        .lbl-blocked-seat {
            padding-bottom: 15px;

        }
    </style>
@stop
