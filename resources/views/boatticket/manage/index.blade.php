@extends('template')
@section('content')
    @if(!$search)
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 no-padding">
                        <div class="bread-wrapper">
                            <ol class="breadcrumb">
                                <li><a href="">Manage Ticket</a></li>
                                <li class="pull-right">
                                    <strong id="cur-hours"> </strong>
                                    <span id="point">:</span>
                                    <strong id="cur-min"> </strong>
                                    <span id="point">:</span>
                                    <strong id="cur-sec"> </strong>
                                    <strong> - </strong>
                                    <span id="cur-time"></span>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="message-wrapper">
                            @include('flash::message')
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-12'>
                        <!-- check the user level -->
                        <div class="list-search-panel">
                            {!! Form::open(array('url'=> '/boat/ticket/manage/','class' => 'form-inline', 'role' =>'form'))!!}
                            <div class="form-group">
                                {!! Form::text('ticket', Input::get('ticket'), array('class' => 'form-control', 'style' => 'width:400px','placeholder' => 'Search Ticket PNR No'))!!}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success f-arial">Search</button>
                            </div>
                            {!! Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(isset($search) && !isset($info))
        <div id="page-wrapper">
            <div class="row">
                <div class='col-md-12'>
                    <!-- check the user level -->
                    <div class="list-search-panel">
                        {!! Form::open(array('url'=> '/boat/ticket/manage/','class' => 'form-inline', 'role' =>'form'))!!}
                        <div class="form-group">
                            {!! Form::text('ticket', Input::get('ticket'), array('class' => 'form-control', 'style' => 'width:400px','placeholder' => 'Search Ticket PNR No'))!!}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success f-arial">Search</button>
                        </div>
                        {!! Form::close()!!}
                    </div>
                </div>

                <div class="col-md-12">
                    @if(isset($search))
                        <div class="alert   alert-info" style="margin-top:30px;">No Result found</div>
                    @endif
                </div>
            </div>
        </div>
    @elseif(!isset($search))

    @elseif(isset($search) && !empty($info))

        @if($schedule->seat_plan == WITHOUT_SEAT_PLAN)
            <link href="{{ asset('/js/plugins/num_picker/wan-spinner.css') }}" rel="stylesheet">
            <script src="{{ asset('/js/plugins/num_picker/wan-spinner.js')}}"></script>
        @endif

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="bread-wrapper">
                            <ol class="breadcrumb f-arial">
                                <li class="active">{!! "Ticket"!!}</li>
                                <li class="pull-right">
                                    <span id="cur-time"></span>
                                    <strong id="cur-hours"> </strong>
                                    <span id="point">:</span>
                                    <strong id="cur-min"> </strong>
                                    <span id="point">:</span>
                                    <strong id="cur-sec"> </strong>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-12'>
                        <!-- check the user level -->
                        <div class="list-search-panel">
                            {!! Form::open(array('url'=> '/boat/ticket/manage/','class' => 'form-inline', 'role' =>'form'))!!}
                            <div class="form-group">
                                {!! Form::text('ticket', Input::get('ticket'), array('class' => 'form-control', 'style' => 'width:400px','placeholder' => 'Search Ticket PNR No'))!!}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success f-arial">Search</button>
                            </div>
                            {!! Form::close()!!}
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            @include('flash::message')

                            @if($schedule->seat_plan != WITHOUT_SEAT_PLAN)
                                <h2>Ticket Information : {!!$info->ticket_pnr_code!!}</h2>
                                <span class="text-success">Please reduce the booked ticket.</span>
                                @if($schedule->take_seat_layout!='')
                                    <?php
                                    $arr_seat = explode(",", $info->seat_no);
                                    ?>

                                    @if(count($arr_seat))

                                        <?php

                                        ?>

                                        {!! Form::open(array('url' => 'boat/ticket/modify/'.$info->boatticket_id, 'method' => 'POST', 'class' =>
                                    'form-horizontal',
                                    'role' => 'form'))!!}

                                        @for($i=0;$i<=count($arr_seat)-1;$i++)

                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label><input name="seat[]" type="checkbox"
                                                                  value="{!!$arr_seat[$i]!!}">{!!$arr_seat[$i]!!} <span
                                                                class="seat-info">( {!!$info->type==BOOK?"booked":"Bought"!!}
                                                            )</span></label>
                                                </div>
                                            </div>


                                        @endfor

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                        {!!Form::close()!!}

                                    @endif
                                @else
                                    <div class="alert alert-info">
                                        There is no seat yet.
                                    </div>
                                @endif
                            @else

                            <!-- start -->
                                <h2>Ticket Information : {!!$info->ticket_pnr_code!!}</h2>
                                <span class="text-success">Please reduce the booked ticket.</span>
                                {!! Form::open(array('url' => 'boat/ticket/modify/'.$info->boatticket_id, 'method' => 'POST', 'class' =>
                                        'form-horizontal',
                                        'role' => 'form'))!!}
                                <div style="margin-top:20px;"></div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-xs-2 control-label">{!! Lang::get('No. of Seat')!!} <strong
                                                class="mandatory">*</strong></label>
                                    <label class="control-label colon">:</label>

                                    <div class="col-xs-2">
                                        <div class="input-group">
                                          <span class="input-group-btn">
                                              <button type="button" class="btn btn-default btn-number" data-type="minus"
                                                      data-field="no_seat">
                                                  <span class="glyphicon glyphicon-minus"></span>
                                              </button>
                                          </span>
                                            <input type="text" name="no_seat" class="form-control input-number h34"
                                                   value="{!!$info->t_seat!!}" min="1" max="{!!$info->t_seat!!}"
                                                   style="border:1px solid #ddd;">

                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-default btn-number" data-type="plus"
                                                      data-field="no_seat">
                                                  <span class="glyphicon glyphicon-plus"></span>
                                              </button>
                                          </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        ( <span class="seat-info">{!!$info->type==BOOK?"booked":"Bought"!!}</span> )
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>

                                <!-- end -->

                                {!!Form::close()!!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <style>
            .seat-info {
                font-weight: normal;
                font-family: "Arial";
                color: green;

            }
        </style>
    @endif

@stop
