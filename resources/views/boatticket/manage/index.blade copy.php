@extends('template')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 no-padding">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li><a href="">Manage Ticket</a></li>
                        <li class="pull-right">
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                            <strong> - </strong>
                            <span id="cur-time"></span>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="col-md-12">
                <div id="message-wrapper">
                    @include('flash::message')
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <div class='col-md-12'>
                <!-- check the user level -->
                <div class="list-search-panel">
                    {!! Form::open(array('url'=> '/boat/ticket/manage/','class' => 'form-inline', 'role' =>'form'))!!}
                    <div class="form-group">
                        {!! Form::text('ticket', Input::get('ticket'), array('class' => 'form-control', 'style' =>
                        'width:400px','placeholder' => 'Search Ticket PNR No'))!!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success f-arial">Search</button>
                    </div>
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="message-wrapper">
                    @include('flash::message')
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        @if(isset($search) && !isset($info))
        <div class="alert alert-info">No Result found</div>
        @elseif(isset($search) && !empty($info))
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="col-md-3">
                            {!! showRouteText(array($info->start_branch, $info->end_branch), SCHEDULE_SEPERATOR)!!}
                        </div>
                        <div class="col-md-3">
                            {!!date('M d Y',strtotime($info->dept_date))!!}
                            {!!displayDate($info->dept_time, 'H:i A')!!}
                        </div>
                        Booking Reference: &nbsp;<span
                            class="text text-success">{!!$info->ticket_pnr_code!!}</span>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal ticket-search" role="form">
                            <div class="row">
                                <div class="col-md-12">
                                    @if($info->upload_type!='' && $info->upload_type==1)
                                    <h4>You have uploaded file.</h4>
                                    <br>
                                    <br>
                                    @else
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>No</th>
                                            <th>Ticket No</th>
                                            <th>Name</th>
                                            <th>Passport / NRC No</th>
                                            <th>Seat No</th>
                                            <th>Contact No</th>
                                        </tr>
                                        @if(isset($passengers) && count($passengers))
                                        <?php $i = 1; ?>
                                        @foreach($passengers as $passenger)
                                        <tr>
                                            <td>{!! $i !!}</td>
                                            <td>{!! Form::checkbox('seat_no', $passenger->seat_no, FALSE )!!} {!!
                                                $passenger->seat_no !!}
                                            </td>
                                            <td>
                                                @if($passenger->name_prefix!='')
                                                <?php $prefix = json_decode(NAME_PREFIX, true); ?>
                                                <p class="form-control-static">{!!$prefix[$passenger->name_prefix]."
                                                    ".$passenger->name!!}</p>
                                                @endif
                                            </td>
                                            <td>
                                                {!!$passenger->nrc_prefix.$passenger->nrc_state."(".$passenger->nrc_nation.")".$passenger->nrc_no!!}
                                            </td>
                                            <td><p>{!!$passenger->seat_no!!}</p></td>
                                            <td><p>{!!$passenger->phone_no!!}</p></td>
                                        </tr>
                                        <?php $i++ ?>
                                        @endforeach
                                        @endif
                                    </table>
                                    <div class="form-group">
                                        <div class="col-xs-offset-3 col-xs-8">
                                            <input type="reset" class="btn btn-default" value="Cancel"/>
                                            <input type="submit" class="btn btn-success"
                                                   value="Remove Seat"/>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @endif
    </div>

    <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
    <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
    <script type="text/javascript">
        $(function () {
            $('.cancel').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-danger',
                    title: '<div id="dia-title">Cancle Ticket</div>',
                    message: '<div id="dia-message">Are you sure you want to cancel these ticket.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-danger',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
        });
    </script>
    @stop
