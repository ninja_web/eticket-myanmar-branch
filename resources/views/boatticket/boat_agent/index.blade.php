@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href=""><i class="fa fa-truck"></i> Agent</a></li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <a href="{!!  URL('boat/agent/'.AUTHORIZE)!!}"
                                   class="btn btn-default f-arial <?php echo $type == AUTHORIZE ? 'btn-system ' : ''?>"><i
                                            class="fa fa-chain fa-w"></i>&nbsp;Activated</a>

                                @if(BOAT_BRANCH == Auth::user()->user_type)
                                    <a href="{!!  URL('boat/agent/'.NONAUTHORIZED)!!}"
                                       class="btn  btn-default f-arial  <?php echo $type == NONAUTHORIZED ? 'btn-system' : ''?>"><i
                                                class="fa fa-chain-broken fa-w"></i>&nbsp;Not Activated</a>
                                @endif
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    @if($type == AUTHORIZE)
                        <table class="table table-bordered table-stripped">
                            <thead>
                            <tr class="f-arial">
                                <th>No</th>
                                <th>Name</th>
                                <th>Sub Agent(s)</th>
                                <th>Device(s)</th>
                                <th>Agent Branch</th>
                                <th>Activated Branch</th>
                                <th>Status</th>
                                <th>Activated By</th>
                                <th>Activated Date</th>
                                @if(BOAT_BRANCH == Auth::user()->user_type)
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            <?php $index = Request::get('page', 1);?>
                            <?php $index = $index > 1 ? PER_PAGE * ($index - 1) + 1 : $index;?>
                            @foreach($boat_agent as $row)
                                <tr>
                                    <td>{!! $index !!}</td>
                                    <td>{!! $row->name !!}</td>
                                    <td class="text-center">{!! getSubAgentCount($row->agent_code, BOAT_GROUP)!!}</td>
                                    <td></td>
                                    <td>{!! Str::title($row->agent_branch) !!}</td>
                                    @if(BOAT_ADMIN == Auth::user()->user_type)
                                        <td>{!! getActivateRoute($row->agent_id, BOAT_AGENT )!!}</td>
                                    @else
                                        <td>{!! Str::title($row->activate_branch) !!}</td>
                                    @endif
                                    <td class="text-center">{!! showStatus($row->status) !!}</td>
                                    <td>{!! $row->activated_by !!}</td>
                                    <td>{!! displayDate($row->created_at) !!}</td>
                                    @if(BOAT_BRANCH == Auth::user()->user_type)
                                        <td>
                                            <a href="{!! url('boat/agent/commission/'.$row->boat_owner.'/'.$row->agent_code)!!}"><i
                                                        class="fa fa-plus"></i>&nbsp;Add Commission</a></td>
                                    @endif
                                </tr>
                                <?php $index++ ?>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <div class="pag-panel">
                            <div class="pull-right">
                                {!! $boat_agent->render() !!}
                            </div>
                        </div>
                    @else
                        <table class="table table-bordered table-stripped">
                            <thead>
                            <tr class="">
                                <th>No</th>
                                <th>Name</th>
                                <th>Sub Agent(s)</th>
                                <th>Device(s)</th>
                                <th>Code</th>
                                <th>Route</th>
                                <th>Status</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $index = Request::get('page', 1);?>
                            <?php $index = $index > 1 ? PER_PAGE * ($index - 1) + 1 : $index;?>


                            @foreach($boat_agent as $row)
                                <tr>
                                    <td>{!! $index !!}</td>
                                    <td>{!! $row->name !!}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{!! $row->agent_code !!}</td>
                                    <td>{!! Str::title($row->branch) !!}</td>
                                    <td class="text-center">{!! showStatus($row->status) !!}</td>
                                    <td>{!! displayDate($row->created_at) !!}</td>
                                    <td><a class="activate btn btn-xs  btn-success f-arial"
                                           href="{!! url('boat/agent/activateAgent/'.$row->agent_code)!!}" onClick="javascript:void(0);
                                    return false;"><i class="fa fa-upload">&nbsp;</i>Activate</a></td>
                                </tr>
                                <?php $index += 1;?>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <div class="pag-panel">
                            <div class="pull-right">
                                {!! $boat_agent->render() !!}
                            </div>
                        </div>
                        <!-- paging panel -->
                    @endif
                </div>
            </div>
        </div>
    </div>

    <link href="{!! asset('css/bootstrap-dialog.min.css')!!}" rel="stylesheet">
    <script type="text/javascript" src="{!! asset('js/bootstrap-dialog.min.js')!!}"></script>
    <script type="text/javascript">
        $(function () {
            $('.activate').click(function (e) {
                var href = this.href;
                var dialog = new BootstrapDialog({
                    type: 'type-success',
                    title: '<div id="dia-title">Activate Boat Agent</div>',
                    message: '<div id="dia-message">Are you sure you want to activate these boat agent as authorized agent.</div>',
                    buttons: [{
                        label: 'Cancel',
                        cssClass: 'btn btn-default',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        label: 'Yes',
                        cssClass: 'btn btn-success',
                        action: function (dialog) {
                            $("#dia-message").text("Processing now, please wait.");
                            $('#dia-message').css('color', '#00');
                            $('#dia-title').text("Wait..");
                            $.ajax({
                                type: 'GET',
                                url: href,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.key) {
                                        location.reload();
                                    } else {
                                        // show erro
                                    }
                                }
                            });
                        }
                    }
                    ],
                    closable: true

                });
                dialog.realize();
                dialog.open();
            });
        });
    </script>
@stop
