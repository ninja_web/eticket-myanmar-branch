@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href=""><i class="fa fa-taxi"></i> Boat Type</a></li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <a class="btn  btn-system" href="{{ url('/boat/boatClass/create') }}"> <i
                                            class="fa fa-plus"></i>&nbsp;{!!"New Boat Type" !!}</a>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                    </div>
                    <div class="list-search-panel">


                    </div>

                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr class="f-arial">
                            <th class="text-center">No</th>
                            <th>Logo</th>
                            <th class="text-center"><i class="fa fa-exchange"></i>&nbsp;&nbsp;Class Name</th>
                            <th class="text-center"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Seater Type</th>
                            <th class="text-center">Created By</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = Request::get('page', 1);?>
                        <?php $index = $index > 1 ? PER_PAGE * ($index - 1) + 1 : $index;?>
                        @foreach($boatClass as $class)
                            <tr class="f-arial">
                                <td class="text-center">{!! $index; !!}</td>
                                <?php $img_path = $class->logo_path . $class->logo;?>
                                <?php $img_path = empty($img_path) ? "" : asset($img_path);?>
                                <td class="text-center"><img class="img" src="{!! $img_path !!}" width="60"
                                                             height="40"/></td>
                                <td class="text-center">{!! $class->class !!}</td>
                                <td class="text-center">{!! $class->seater.'_seater' !!}</td>
                                <td class="text-center">{!! $class->user_name !!}</td>
                                <td class="text-center">{!! showStatus($class->status)!!}</td>
                                <td class="text-center">{!! displayDate($class->created_at)!!}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-primary f-arial "
                                       href="{!! url('boat/boatClass/'.$class->id.'/edit') !!}">
                                        <i class="fa fa-edit">&nbsp;</i>Edit</a>
                            </tr>
                            <?php $index++;?>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                    <div class="pag-panel">
                        <div class="pull-right">
                            {!! $boatClass->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
