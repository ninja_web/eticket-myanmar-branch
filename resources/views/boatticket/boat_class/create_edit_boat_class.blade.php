@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">

                            <li>
                                <a href="{!!url('boat/boatClass')!!}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
                            </li>

                            <li><a href=""><i class="fa fa-taxi"></i> Create / Edit Boat Type</a></li>
                            <li class="pull-right">

                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                            <li>
                                <strong class="mandatory">*</strong> All fields must be required.
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>


                    <form class="form-horizontal" enctype="multipart/form-data" method="post"
                          action="@if(isset($boatClass)) {{ URL::to('boat/boatClass/'.$boatClass->id.'/edit')}} @else {{ URL::to('boat/boatClass/create/')}} @endif"
                          autocomplete="off" id="frmSchedule">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>


                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="text-center">
                                    <p style="color:#999;font-size:11px;">Standard Image Size: 150px x 80px</p>
                                    <div class="profile-picture" style="margin-left:34%">
                                        <canvas id="thumb_img_canvas" style="display:none"></canvas>
                                        <input type="file" id="thumb_img" name="logo"
                                               onchange="return ShowImagePreview(this.files, 'thumb_img')"
                                               class="btn btn-default"/>

                                        <div class="drag-panel" id="thumb_img_upload">
                                            <p>Drag &amp; Drop your photo here for</p>
                                            <i class="fa fa-picture-o"></i>
                                        </div>
                                        <div class="caption">
                                            <strong><i class="fa fa-user"></i>&nbsp;Upload Logo</strong>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Class Name"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7">
                                    <input type="text" name="class" class="form-control"
                                           value="{{ Input::old('class', isset($boatClass) ? $boatClass->class : null) }}"
                                           required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!!"Class Type"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7">
                                    <input type="text" name="type" class="form-control"
                                           value="{{ Input::old('type', isset($boatClass) ? $boatClass->type : null) }}"
                                           required/>
                                </div>
                            </div>
                            <input type="hidden" value="{!!isset($boatClass)? $boatClass->seat_plan:WITH_SEAT_PLAN!!}"
                                   name="hidd_seater_type">

                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Seater Type"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7 radio">
                                    <label class="f-arial">
                                        <input type="radio" name="seat_plan"
                                               {!!isset($boatClass) && $boatClass->seat_plan==WITH_SEAT_PLAN?"checked":''!!}
                                               value="{!! WITH_SEAT_PLAN !!}">Seat Plan
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="f-arial">
                                        <input type="radio" name="seat_plan"
                                               value="{!! WITHOUT_SEAT_PLAN !!}" {!!isset($boatClass) && $boatClass->seat_plan==WITHOUT_SEAT_PLAN?"checked":''!!}>
                                        Without
                                        Seat Plan
                                    </label>
                                </div>
                            </div>

                            <div class="form-group" id="<?php echo WITH_SEAT_PLAN . "_input" ?>" style="display:none">
                                <label class="col-xs-3 control-label">Total Seater Plan
                                    <strong class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7">
                                    <select name="seater" class="form-control chosen-select" id="seater">
                                        <option value="">Select Boat Seater</option>
                                        @foreach(json_decode(BOAT_SEATER, true) as $key => $value)
                                            <option value="{!! $key !!}" {!! Input::old('seater', isset($boatClass) ?
                                            $boatClass->seater : '' ) == $key ? 'selected="selected"' : '' !!} >{!! $value!!}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="<?php echo WITHOUT_SEAT_PLAN . "_input" ?>"
                                 style="display:none">
                                <label class="col-xs-3 control-label">{!! "No of Seat"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7">
                                    <input type="text" class="form-control" name="no_seat"
                                           value="{{ Input::old('no_seat', isset($boatClass) ? $boatClass->seater : null) }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">{!! "Status"!!} <strong
                                            class="mandatory">*</strong></label>
                                <label class="control-label colon">:</label>

                                <div class="col-xs-7 radio">
                                    <label class="f-arial">
                                        {!! Form::radio('status', STATUS_ACTIVE, Input::old('status', isset($boatClass)
                                        ?
                                        $boatClass->status : null) == STATUS_ACTIVE ?
                                        TRUE : FALSE )!!} Active
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="f-arial">
                                        {!! Form::radio('status', STATUS_INACTIVE, Input::old('status',
                                        isset($boatClass)
                                        ?
                                        $boatClass->status : null) ==
                                        STATUS_INACTIVE ? TRUE : FALSE )!!} In Active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-8">
                                    <button type="reset" class="btn btn-default f-arial">Cancel</button>
                                    <button type="submit" class="btn btn-success f-arial">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="col-md-7" style="padding-left:0px;">

                        <div id="seat_plan_layout">
                            <!-- Handling the seater plan -->
                            <?php $boat_seater = json_decode(BOAT_SEATER, true); ?>
                            @foreach(json_decode(BOAT_SEATER_PLAN, true) as $key => $value)
                                <div id="{!! $boat_seater[$key] !!}" class="{!! " s".$boat_seater[$key] !!} hide">
                                    <?php $seater_plan = json_decode($value, true);?>
                                    <h4><strong class="text-success">{!! $key !!} - Seater</strong></h4>
                                    <div class="clearfix"></div>
                                    <?php  $seater_class = $seater_plan['type'] == 'seater-123' ? "s123_seater" : '';?>
                                    <div class="{!!$seater_class!!}">
                                        <div class="car-layout {!! $seater_plan['type'] !!}">
                                            <div class="seat-driver img_availiable_seat"></div>
                                            <div class="boat-style">
                                                @foreach($seater_plan['seat'] as $key => $value)
                                                    @if("middle" == $value)
                                            </div>
                                            <!-- end boat style -->
                                            <ul class="row-margin middle-row">
                                                <li></li>
                                            </ul>
                                            <div class="boat-style">
                                                @else
                                                    <?php $rows = explode(",", $value);?>
                                                    <ul class="row-margin">
                                                        @foreach($rows as $seat)
                                                            <li class="seat">
                                                                @if(!empty($seat))
                                                                    <a class="availableSeat">
                                                                        <span class="seat-no">{!! $seat !!}</span>
                                                                    </a>

                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                                @endforeach
                                            </div>
                                            <!-- end boat style -->
                                            @if($seater_plan['type']=='seater-98')
                                                <div class="lble-sign">
                                                    <h2>front</h2>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>

                    </div>

                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function () {

                    var without_seat_plan = "{!! WITHOUT_SEAT_PLAN !!}";
                    var with_seat_plan = "{!! WITH_SEAT_PLAN !!}";

                    /* Checking Form Validation */

                    $("form#frmSchedule").on("submit", function (e) {
                        e.preventDefault();

                        var $s_plan = $('input:radio[name=seat_plan]').val();
                        if ($s_plan.length == 0) {
                            $message = "Please Select Seater Type";
                            messagebar_toggle($message, 'info', 4000);
                            return false;
                        }
                        if ($s_plan == without_seat_plan) {
                            // without seater plan
                            var value = $('select#seater option:selected').val();
                        } else {
                            // with seater plan
                            var value = $('input[name="hidd_seater_type"]').val();
                        }
                        // return false;
                        this.submit();

                    });

                    var logo = "<?php echo (isset($boatClass) && !empty($boatClass->logo) && !empty($boatClass->logo_path)) ? asset($boatClass->logo_path . $boatClass->logo) : '';?>";
                    if (logo !== '') {
                        editImagePreview('thumb_img', logo, 150, 80);
                    }

                    // seat plan and without seat plan
                    toggleSeatPlan($('input[name="hidd_seater_type"]').val());

                    $('input:radio[name=seat_plan]').change(function () {
                        toggleSeatPlan(this.value);
                    });

                    function toggleSeatPlan(value) {
                        if (value == without_seat_plan) {
                            $("#" + value + "_input").toggle();
                            $("#" + with_seat_plan + "_input").fadeOut();
                            $("#seat_plan_layout").hide();
                        } else {
                            $("#" + value + "_input").show();
                            $("#seater_chosen").css('width', '270');

                            $("#" + without_seat_plan + "_input").hide();
                            $("#seat_plan_layout").show();
                        }
                    }

                    // seater
                    var seater_type = ['96_seater', '98_seater', '123_seater', '108_seater'];
                    $('#seater').change(function () {
                        var select_seat = $(this).val();
                        $.each(seater_type, function (index, value) {
                            if (select_seat + '_seater' == value) {
                                $('#' + select_seat + '_seater').hasClass('hide') ? $('#' + select_seat + '_seater').removeClass('hide') : '';
                            } else {
                                $('#' + value).hasClass('hide') ? '' : $('#' + value).addClass('hide');
                            }
                        });
                    });
                    $index = 0;
                    $(".seater-123 .boat-style").each(function () {
                        if ($index == 1) {
                            $(this).append("<h2>Front</h2>");
                            $(this).append("<h2>Sun Desk</h2>");
                            $(this).append("<h2>Upper Desk</h2>");
                        }
                        if ($index == 3) {
                            $(this).append("<h2 class='lower'>Lower Desk</h2>");
                        }
                        $index++;
                        if ($index == 1 || $index == 3) {
                            $(this).addClass('bnone pt30');
                        }
                        if ($index == 2 || $index == 4) {
                            $(this).addClass('mt_27 pt30');
                        }

                    });

                });
            </script>
@stop
