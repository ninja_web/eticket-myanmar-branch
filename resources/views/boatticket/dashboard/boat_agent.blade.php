@extends('template')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-wrapper">
                    <ol class="breadcrumb">
                        <li class="active"><i class="fa fa-home"></i> Dashboard</li>
                        <li class="pull-right">
                            <strong id="cur-hours"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-min"> </strong>
                            <span id="point">:</span>
                            <strong id="cur-sec"> </strong>
                            <strong> - </strong>
                            <span id="cur-time"></span>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-12 no-padding">
                    <div class="bs-example" data-example-id="thumbnails-with-custom-content">
                        @foreach($boat_lines as $line)
                            <?php $img_path = !empty($line->profile) && !empty($line->profile_path) ? 'http://' . MASTER_DOMAIN . $line->profile_path . $line->profile : asset('/img/default_profile_thumbnail.jpeg');?>
                            <div class="col-xs-4">
                                <a href="{!! URL('boat/ticket/agent/search', $line->owner_code)!!}">
                                    <div class="thumbnail dashboard-thumbnail">
                                        <img alt="100%x200" src="{{ $img_path }}"
                                             data-holder-rendered="true"
                                             style="width: 100%; display: block;height:220px;">
                                        <strong class="text text-success">{!! $line->name !!}</strong>
                                    </div>


                                </a>
                            </div>
                    @endforeach
                    <!-- -->
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

@stop
