<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">

        table, table tr {
            border-collapse: collapse;
        }

        .boder {
            border: 1px solid #ddd;
        }

        table.border td {
            border: 1px solid #000;
            padding: 5px;

        }

        table.center td {
            text-align: center;
        }

        tr.strong td {
            font-weight: bold;
        }

        table td {
            font-family: "Arial"
        }
    </style>
</head>
<body>

<table style="width:100%;margin:0 auto;">
    <tr>

        <td>
            <table style="width:100%;margin-bottom:20px;" class="border center">
                <tr>
                    @if(Auth::user()->user_type==BOAT_ADMIN)

                        <td>
                            {!!$filter['owner_name']!!}
                        </td>


                        <td>{!!date("d F Y",strtotime($filter['start_date']))!!}</td>
                        <td>{!!date("d F Y",strtotime($filter['end_date']))!!}</td>
                        <td>{!!$filter['agent_name']!!}</td>

                    @elseif(Auth::user()->user_type==BOAT_AGENT || Auth::user()->user_type==BOAT_SUB_AGENT )

                        <td>{!!$filter['agent_name']!!}</td>

                        <td>{!!date("d F Y",strtotime($filter['start_date']))!!}</td>
                        <td>{!!date("d F Y",strtotime($filter['end_date']))!!}</td>
                        <td>
                            {!!$filter['owner_name']!!}
                        </td>

                    @endif

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width:100%" class="border center">
                <tr class="strong">
                    <td>No</td>
                    <td>Date</td>
                    <td>Route</td>
                    <td>Seat No</td>
                    <td>Total Seat</td>
                    <td>Price</td>
                    <td>Discount</td>
                    <td>Total Discount</td>
                    <td>Net Total</td>
                </tr>
                <?php
                $list = $result['report_data'];

                $report_brief = $result['report_brief'];
                ?>
                @if(count($list))
                    <?php $index = 1;?>
                    @foreach($list as $row)

                        <tr>
                            <td>{!!$index!!}</td>
                            <td>{!!date("d F Y",strtotime($row['dept_date']))!!}</td>
                            <td>{!!$row['route']!!}</td>
                            <td>{!!$row['seat_no']==""?"-":$row['seat_no']!!}</td>
                            <td>{!!$row['total_seat']!!}</td>
                            <td>{!!$row['price']==0?"-":$row['price']!!}</td>
                            <td>{!!$row['discount']==0?"-":$row['discount']!!}</td>
                            <td>{!!$row['total_discount']==0?"-":$row['total_discount']!!}</td>
                            <td>{!!$row['total_amount']!!}</td>
                        </tr>
                        <?php $index++;?>
                    @endforeach
                    <tr>
                        <td colspan="3"></td>
                        <td>Total</td>
                        <td>{!!$report_brief['total_seat']!!}</td>
                        <td></td>
                        <td></td>
                        <td>{!!$report_brief['total_discount']==0?"-":$report_brief['total_discount']!!}</td>
                        <td>{!!$report_brief['total_amount']!!}</td>
                    </tr>
                @endif
            </table>
        </td>
    </tr>

</table>
</body>
</html>
