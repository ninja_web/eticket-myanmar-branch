@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href=""><i class="fa fa-file-pdf-o"></i> Agent Report</a></li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='col-md-12'>
                    <!-- check the user level -->
                    <div class="list-search-panel">
                        {!! Form::open(array('url'=> '/boat/report/agent_rep','class' => 'form-inline', 'role' => 'form', 'method' => 'post'))!!}
                        <label class="f-arial">Strat Date: </label>
                        <div class="form-group date-picker-container mw-200">
                            <div class="input-group date f-arial past">
                                <input type="text" name="start_date" class="form-control"
                                       value="{!!isset($filter)?$filter['start_date']:date('d/m/Y')!!}"
                                       requried/>

                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        <label class="f-arial">End Date: </label>
                        <div class="form-group date-picker-container mw-200">
                            <div class="input-group date f-arial past">
                                <input type="text" name="end_date" class="form-control"
                                       value="{!!isset($filter)?$filter['end_date']:date('d/m/Y')!!}"
                                       requried/>

                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        @if(in_array(Auth::user()->user_type, array(BOAT_ADMIN, BOAT_BRANCH)))
                            <label class="f-arial">Agent : </label>
                            <div class="form-group">
                                <select name="agent" class="form-control chosen-select" style="width:300px;">
                                    <option value="">Select Boat Agent</option>
                                    @foreach($agentList as $row)
                                        <?php $selected = isset($filter) && $filter['agent'] == $row->agent_code ? 'selected="selected"' : '';?>
                                        <option value="{!! $row->agent_code !!}" {!! $selected !!}>{!! $row->name !!} -
                                            ( {!! Str::title($row->branch) !!})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        @if(in_array(Auth::user()->user_type, array(BOAT_AGENT)))
                            <label class="f-arial">Owner : </label>
                            <div class="form-group">
                                <select name="owner" class="form-control chosen-select" style="width:300px;">
                                    <option value="">Select Boat Owner</option>
                                    @foreach($ownerList as $row)
                                        <?php $selected = isset($filter) && $filter['owner'] == $row['code'] ? 'selected="selected"' : '';?>
                                        <option value="{!! $row['code'] !!}" {!! $selected !!}>{!! $row['name'] !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group">
                            <button type="submit" class="btn btn-success f-arial">Search</button>
                        </div>
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(isset($excel_data))
                        @if(isset($excel_data['report_data']) && count($excel_data['report_data']))
                            <table class="table table-bordered">
                                <tr>
                                    <th>No</th>
                                    @if(in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT)))
                                        <th>Owner</th>
                                    @endif
                                    <th>Date</th>
                                    <th>Route</th>
                                    <th>Seat No</th>
                                    <th>Total Seat</th>
                                    <th>Price</th>
                                    <th>Discount</th>
                                    <th>Total Discount</th>
                                    <th class="text-center">Net Total</th>
                                </tr>
                                <?php

                                $list = $excel_data['report_data'];
                                $brief = $excel_data['report_brief'];
                                $index = 1;
                                ?>

                                @foreach($list as $row)

                                    <tr>
                                        <td>{!!$index!!}</td>
                                        @if(in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT)))
                                            <td>{!! $row['owner_name'] !!}</td>
                                        @endif
                                        <td>{!! displayDate($row['dept_date']) !!}</td>
                                        <td>{!! Str::title($row['route']) !!}</td>
                                        <td>{!! $row['seat_no']!!}</td>
                                        <td>{!!$row['total_seat']!!}</td>
                                        <td>{!!$row['price']!!}</td>
                                        <td>{!!$row['discount']==0?'-':$row['discount']!!}</td>
                                        <td>{!!$row['total_discount']==0?'-':$row['total_discount']!!}</td>
                                        <td class="text-right">{!!$row['total_amount']!!}</td>

                                    </tr>
                                    <?php $index++;?>
                                @endforeach
                                <tr>

                                    @if(in_array(Auth::user()->user_type, array(BOAT_AGENT, BOAT_SUB_AGENT)))
                                        <td colspan="4"></td>
                                    @else
                                        <td colspan="3"></td>
                                    @endif
                                    <td class="active"><strong>Total</strong></td>
                                    <td class="active">
                                        <strong>{!!isset($brief['total_seat'])?$brief['total_seat']:''!!}</strong></td>
                                    <td class="active"></td>
                                    <td class="active"></td>
                                    <td class="active">
                                        <strong>{!!isset($brief['total_discount'])? number_format($brief['total_discount']):''!!}
                                    </td>
                                    <td class="active text-right">
                                        <strong>{!!isset($brief['total_net_amount'])? number_format($brief['total_net_amount']):''!!}</strong>
                                    </td>
                                </tr>
                            </table>
                            <a href="{!!url('boat/agent_report/'.$filter['start_date'].'/'.$filter['end_date'].'/'.$filter['owner'].'/'.$filter['agent'])!!}"
                               class="btn btn-success"><i class="fa fa-file-pdf-o"></i>&nbsp;Generate
                                Report</a>
                        @else
                            <div class="alert alert-info">
                                <p>There is no report data yet.</p>
                            </div>
                        @endif
                    @endif

                </div>
            </div>
        </div>
        <style>
            tr th,
            tr td {
                text-align: center;
            }

            table tr:first-child {
                background-color: #efefef
            }
        </style>
@stop
