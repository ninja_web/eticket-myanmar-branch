<div class="col-lg-12 no-padding">
    @if(isset($schedule_list) && count($schedule_list))
        <ul class="route-list">
            <?php $index = 1;?>
            @foreach($schedule_list as $row)
                <li>
                    <a href="{!!url('boat/report/main/'.$row->id )!!}">
                        {!!Str::title($row->start_branch )!!} <i
                                class="fa fa-arrow-circle-right route-link"></i> {!! Str::title($row->end_branch)!!}
                        &nbsp; {!! $row->boat_order !!} ( BOAT )
                    </a>
                </li>
                <?php $index++;?>
            @endforeach
        </ul>
    @else
        There is no scheule
    @endif
</div>
