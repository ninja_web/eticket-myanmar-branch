@extends('template')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="bread-wrapper">
                        <ol class="breadcrumb">
                            <li><a href="">Main Report</a></li>
                            <li class="pull-right">
                                <strong id="cur-hours"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-min"> </strong>
                                <span id="point">:</span>
                                <strong id="cur-sec"> </strong>
                                <strong> - </strong>
                                <span id="cur-time"></span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="message-wrapper">
                        @include('flash::message')
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='col-md-12'>
                    <!-- check the user level -->
                    <div class="list-search-panel">
                        {!! Form::open(array('url'=> '/boat/report/main/schedule','class' => 'form-inline', 'role'
                        =>
                        'form', 'id' => 'searchRoute'))!!}
                        <label class="f-arial">Report Date: </label>
                        <div class="form-group date-picker-container mw-200">
                            <div class="input-group date f-arial past">
                                <input type="text" name="report_date" class="form-control"
                                       value="{{ Input::get('report_date', date('d/m/Y')) }}"
                                       requried/>

                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                            </div>
                        </div>
                        <label class="f-arial">Schedule : </label>
                        <div class="form-group">
                            <select name="schedule_type" class="form-control chosen-select" style="width:200px;">
                                @foreach($schedule_type as $row)
                                    <option value="{!! $row->id !!}">{!! Str::title($row->start_branch) . ' - ' . Str::title($row->end_branch) !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success f-arial">Generate Report</button>
                        </div>
                        {!! Form::close()!!}
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" id="schedule_list"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(function (e) {
                $('#searchRoute').submit(function (e) {
                    $.ajax({
                        type: 'POST',
                        url: this.action,
                        data: $('#searchRoute').serialize(),
                        success: function (data) {
                            $('#schedule_list').html(data);
                        }
                    });
                    e.preventDefault();
                });
            });
        </script>
@endsection
