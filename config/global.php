<?php
/**
 * Created by PhpStorm.
 * User: revotech
 * Date: 4/11/15
 * Time: 5:41 PM
 */
define('OWNER_CODE', 12);
define('AGENT_CODE', 10);

define('WEB', 'web');
define('MOBILE', 'mobile');

// Authorized
define('AUTHORIZE', 'authorize');
define('NONAUTHORIZED', 'none');

// STATUS
define('STATUS_ACTIVE', 1);
define('STATUS_INACTIVE', 2);
define('STATUS_PENDING', 3);
define('STATUS_BOOK_CANCEL', 4);
define('STATUS_TICKET_CANCEL', 5);

// Pay and UnPaid
define('STATUS_PAID', 10);
define('STATUS_UNPAID', 11);

// board ticket and bus ticket
define('BUS_GROUP', 1000);
define('BOAT_GROUP', 1001);

define('BUS_AGENT_GROUP', 1002);
define('BOAT_AGENT_GROUP', 1003);

// User Types
/**
 *  Site admin and staff
 */
define('SITE_ADMIN', 98);
define('ADMIN_STAFF', 123);

/**
 * Bus Group
 */
define('BUS_ADMIN', 103);
define('BUS_BRANCH', 104);
define('BUS_SALE', 105);
define('BUS_FINANCE', 106);
define('DRIVER', 107);
define('SPARE', 108);
define('BUS_ATTENDANCE', 109);

/**
 *  Bus Agent Group
 */

define('BUS_AGENT', 110);
define('BUS_SUB_AGENT', 111);

/**
 * Board Group
 */
define('BOAT_ADMIN', 112);
define('BOAT_BRANCH', 113);
define('BOAT_SALE', 114);
define('BOAT_STAFF', 115);
define('BOAT_FINANCE', 116);
define('BOAT_ATTENDANCE', 117);

/**
 * Board Agent Group
 */
define('BOAT_AGENT', 118);
define('BOAT_SUB_AGENT', 119);

/**
 * Type
 */
define("BUY", 1000);
define("CANCEL", 1001);
define("BOOK", 1002);

/**
 * SEAT PLAN TYPE
 */
define("WITHOUT_SEAT_PLAN", 1000);
define("WITH_SEAT_PLAN", 1001);

// Long Name
define('USER_TYPE', json_encode(
    array(
        // bus
        BUS_ADMIN => 'Bus Owner',
        BUS_BRANCH => 'Bus Branch',
        BUS_SALE => 'Bus Sale',
        BUS_FINANCE => 'Bus Finance',
        // board
        BOAT_ADMIN => 'Boat Admin',
        BOAT_BRANCH => 'Boat Branch',
        BOAT_SALE => 'Boat Sale',
        BOAT_FINANCE => 'Boat Finance',
        BOAT_AGENT => 'Boat Agent',
        BOAT_SUB_AGENT => 'Boat Sub Agent'
    )
));

// Pagination
define('PER_PAGE', 15);

// PASSWORD
define('DEFAULT_PASSWORD', 998877);

define('SCHEDULE', json_encode(
    array(
        '' => 'yangon',
        '' => 'madalay'
    )
));

// File Path
define('MASTER_DOMAIN', 'iticketmyanmar.net/');
define('PROFILE_PATH', 'uploads/profile');
define('BUS_CLASS_PATH', 'uploads/bus_class');
define('BOAT_CLASS_PATH', 'uploads/boat_class');
define('MEDIA_PATH', 'uploads/media');

define('BUS_BRANCH_LIST', json_encode(
    array(
        'yangon', 'mawlamyaing', 'pharan', 'shwebo', 'mandalay', 'mudone', 'kockkayate', 'kawlin', 'pyinoolwin', 'bamor', 'kathar', 'monywa',
        'thanphyuzayat', 'pakukku', 'khamti', 'dawe', 'tontay', 'myawade', 'hopin', 'kyautmae', 'htarwal', 'taungkyi', 'moekaung', 'moenyin', 'thipaw', 'bagan', 'nyaungoo', 'hopone', 'intaw',
        'larshow', 'kyautpantaung', 'pathein', 'myitkyinar', 'musel', 'pakhoteku', 'chaungthar', 'bamaw', 'minbu', 'ngwsaung', 'naypyitaw',
        'yayynanchaung', 'tatkone', 'yamaethin', 'pyay', 'sittwe', 'kalay', 'taungngo', 'thantwe', 'kalaywa', 'matehtilar', 'myinchan', 'taungngote',
        'tamue'
    )
));

define('BOAT_BRANCH_LIST', json_encode(
    array(
        'yangon', 'mandalay', 'mingun', 'bagan', 'naung_u','naung_shwe','bhamo', 'katha', 'pyay', 'monywa', 'pakukku', 'khamti',
        'mawlamyaing', 'dawei', 'pathein', 'tontay', 'myeink', 'dinner_river_cruise',
    )
));

define("ALL", 10);
define("GATE", 11);
define("GATE_AGENT", 12);
define("GATE_WEBSITE", 13);

define("SCHEDULE_PERMISSION", json_encode(array(
        ALL => "All",
        GATE => "Gate Only",
        GATE_AGENT => "Gate + Agent",
        GATE_WEBSITE => "Gate + Website"
    )
));

// Bus Seater Plan
define('SEATER_54', '54');
define('SEATER_48', '48');
define('SEATER_45', '45');
define('SEATER_44', '44');
define('SEATER_40', '40');
define('SEATER_29', '29');
define('SEATER_28', '28');
define('SEATER_28_ROYAL', '28');
define('SEATER_27', '27');
define('SEATER_20', '20');
define('SEATER_12', '12');

// Boat Seater Plan
define('BUS_SEATER', json_encode(
    array(
        SEATER_54 => '54 Seater',
        SEATER_48 => '48 Seater',
        SEATER_45 => '45 Seater',
        SEATER_44 => '44 Seater',
        SEATER_40 => '40 Seater',
        SEATER_29 => '29 Seater',
        SEATER_28 => '28 Seater',
        SEATER_27 => '27 Seater',
        SEATER_20 => '20 Seater',
        SEATER_12 => '12 Seater'
    )
));


define("SEATER_54_PLAN", json_encode(
    array(
        "type" => "seater-54",
        "seat" => array(
            "1" => "1D,2D,3D,4D,5D,6D,7D,,9D,10D,11D,12D,13D,14D",
            "2" => "1C,2C,3C,4C,5C,6C,7C,,9C,10C,11C,12C,13C,14C",
            "middle" => "",
            "3" => "1B,2B,3B,4B,5B,6B,7B,8B,9B,10B,11B,12B,13B,14B",
            "4" => "1A,2A,3A,4A,5A,6A,7A,8A,9A,10A,11A,12A,13A,14A"
        )
    )
));
define("SEATER_48_PLAN", json_encode(
    array(
        "type" => "seater-48",
        "seat" => array(
            "1" => "4,8,12,16,20,24,28,32,36,40,44,48",
            "2" => "3,7,11,15,19,23,27,31,35,39,43,47",
            "middle" => "",
            "3" => "2,6,10,14,18,22,26,30,34,38,42,46",
            "4" => "1,5,9,13,17,21,25,29,33,37,41,45"
        )
    )
));
define("SEATER_45_PLAN", json_encode(
    array(
        "type" => "seater-45",
        "seat" => array(
            "1" => "4,8,12,16,20,24,28,32,36,40,45",
            "2" => "3,7,11,15,19,23,27,31,35,39,44",
            "3" => "43",
            "4" => "2,6,10,14,18,22,26,30,34,38,42",
            "5" => "1,5,9,13,17,21,25,29,33,37,41"
        )
    )
));
define("SEATER_44_PLAN", json_encode(
    array(
        "type" => "seater-44",
        "seat" => array(
            "1" => "4,8,12,16,20,24,28,32,36,40,44",
            "2" => "3,7,11,15,19,23,27,31,35,39,43",
            "middle" => "",
            "3" => "2,6,10,14,18,22,26,30,34,38,42",
            "4" => "1,5,9,13,17,21,25,29,33,37,41"
        )
    )
));
define("SEATER_40_PLAN", json_encode(
    array(
        "type" => "seater-40",
        "seat" => array(
            "1" => "4,8,12,16,20,24,28,32,36,40",
            "2" => "3,7,11,15,19,23,27,31,35,39",
            "middle" => "",
            "3" => "2,6,10,14,18,22,26,30,34,38",
            "4" => "1,5,9,13,17,21,25,29,33,37"
        )
    )
));
define("SEATER_29_PLAN", json_encode(
    array(
        "type" => "seater-45",
        "seat" => array(
            "1" => "3,6,9,12,,17,20,23,26,29",
            "2" => "2,5,8,11,,16,19,22,27,28",
            "middle" => "",
            "3" => "1,4,7,10,13,14,15,18,21,24,27"
        )
    )
));
define("SEATER_28_PLAN", json_encode(
    array(
        "type" => "seater-28",
        "seat" => array(
            "1" => "3,6,9,12,15,18,21,24,28",
            "2" => "27",
            "3" => "2,5,8,11,14,17,20,23,26",
            "4" => "1,4,7,10,13,16,19,22,25"
        )
    )
));
define("SEATER_27_PLAN", json_encode(
    array(
        'type' => 'seater-27',
        "seat" => array(
            "1" => "3,6,9,12,15,18,21,24,27",
            "middle" => "",
            "2" => "2,5,8,11,14,17,20,23,26",
            "3" => "1,4,7,10,13,16,19,22,25"
        )
    )
));
define("SEATER_20_PLAN", json_encode(
    array(
        "type" => "seater-20",
        "seat" => array(
            "1" => "3,,8,11,14,18",
            "2" => "17",
            "4" => "2,5,7,10,13,16",
            "5" => "1,4,6,9,12,15"
        )
    )
));
define("SEATER_12_PLAN", json_encode(
    array(
        'type' => 'seater-12',
        'right' => '11',
        'seat' => array(
            "1" => ",5,8,12",
            "2" => "11",
            "3" => "2,4,7,10",
            "4" => "1,3,6,9"
        )
    )
));
define('BUS_SEATER_PLAN', json_encode(
    array(
        SEATER_54 => SEATER_54_PLAN,
        SEATER_48 => SEATER_48_PLAN,
        SEATER_45 => SEATER_45_PLAN,
        SEATER_44 => SEATER_44_PLAN,
        SEATER_40 => SEATER_40_PLAN,
        SEATER_29 => SEATER_29_PLAN,
        SEATER_28 => SEATER_28_PLAN,
        SEATER_27 => SEATER_27_PLAN,
        SEATER_20 => SEATER_20_PLAN,
        SEATER_12 => SEATER_12_PLAN
    )
));
// define('BUS_SEATER_PLAN', json_encode(
//     array(SEATER_44 => SEATER_44_PLAN, SEATER_45 => SEATER_45_PLAN, SEATER_27 => SEATER_27_PLAN, SEATER_28 => SEATER_28_PLAN, SEATER_12 => SEATER_12_PLAN)
// ));

// Boat Seater Plan

define('SEATER_98', 98);
define('SEATER_96', 96);
define('SEATER_123', 123);
define('BOAT_SEATER', json_encode(
    array(
        SEATER_96 => '96_seater',
        SEATER_98 => '98_seater',
        SEATER_123 => '123_seater'
    )
));

define("SEATER_96_PLAN", json_encode(
    array(
        'type' => 'seater-96',
        'seat' => array(
            "1" => ",,C10,D10,E10,F10,G10,H10,I10,J10",
            "2" => "A8,B8,C9,D9,E9,F9,G9,H9,I9,J9",
            "3" => "A7,B7,C8,D8,E8,F7,G7,H8,I8,J8",
            "4" => "middle",
            "5" => "A6,B6,C7,D7,E7,F7,G7,H7,I7,J7",
            "6" => "A5,B5,C6,D6,E6,F6,G6,H6,I6,J6",
            "7" => "A4,B4,C5,D5,E5,F5,G5,H5,I5,J5",
            "8" => "A3,B3,C4,D4,E4,F4,G4,H4,I4,J4",
            "9" => "middle",
            "10" => "A2,B2,C3,D3,E3,F2,G2,H3,I3,J3",
            "11" => "A1,B1,C2,D2,E2,F2,G2,H2,I2,J2",
            "12" => ",,C1,D1,E1,F1,G1,H1,I1,J1"
        )
    )
));
define("SEATER_98_PLAN", json_encode(
    array(
        'type' => 'seater-98',
        'seat' => array(
            "1" => "D10,E10,,,H10,I10,J10,K10",
            "2" => "B8,C8,D9,E9,F8,G8,H9,I9,J9,K9",
            "3" => "A6,B7,C7,D8,E8,F7,G7,H8,I8,J8,K8",
            "4" => "middle",
            "5" => "A5,B6,C6,D7,E7,F6,G6,H7,I7,J7,K7",
            "6" => "A4,B5,C5,D6,E6,F5,G5,H6,I6,J6,K6",
            "7" => "A3,B4,C4,D5,E5,F4,G4,H5,I5,J5,K5",
            "8" => "A2,B3,C3,D4,E4,F3,G3,H4,I4,J4,K4",
            "9" => "middle",
            "10" => "A1,B2,C2,D3,E3,F2,G2,H3,I3,J3,K3",
            "11" => "B1,C1,D2,E2,F1,G1,H2,I2,J2,K2",
            "12" => "D1,E1,,,H1,I1,J1,K1"
        )
    )
));
define("SEATER_123_PLAN", json_encode(
    array(
        "type" => "seater-123",
        "seat" => array(
            "1" => "F1,F2,F3,F4",
            "2" => "E1,E2,E3,E4",
            "3" => "D1,D2,D3,D4",
            "4" => "middle",
            "5" => ",C2,C3,C4",
            "6" => ",B2,B3,B4",
            "7" => ",A2,A3,A4",
            "8" => "middle",
            "9" => "F5,F6,F7,F8,F9,,F10,F11,F12,F13,F14,F15,F16,F17,F18,F19,F20,F21,F22,F23",
            "10" => "E5,E6,E7,E8,E9,,E10,E11,E12,E13,E14,E15,E16,E17,E18,E19,E20,E21,E22,E23",
            "11" => ",,D7,D8,,,D10,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23",
            "12" => "middle",
            "13" => "C5,C6,C7,C8,,,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,C21,C22,C23",
            "14" => "B5,B6,B7,B8,,,B10,B11,B12,B13,B14,B15,B16,B17,B18,B19,B20,B21,B22,B23",
            "15" => "A5,A6,A7,A8,,,A10,A11,A12,A13,A14,A15,A16,A17,A18,A19,A20,A21,A22,A23"
        )
    )
));

//
define('BOAT_SEATER_PLAN', json_encode(
    array(SEATER_96 => SEATER_96_PLAN, SEATER_98 => SEATER_98_PLAN, SEATER_123 => SEATER_123_PLAN)
));

// REPEAT_TYPE
define("REPEAT_TYPE", json_encode(
    array(
        '' => 'Select repeat type',
        'Daily' => 'Daily',
        'Weekly' => 'Weekly',
        'Monthly' => 'Monthly',
        'Yearly' => 'Yearly'
    )
));

/** Booking and **/
// define('NAME_PREFIX', json_encode(
//     array(
//         'mr' => 'Mr.', 'ms' => 'Mrs.', 'miss' => 'Miss.','mg' => 'ေမာင္', 'ko' => 'ကိုု', 'uncle' => 'ဦး', 'ma' => 'မ', 'anti' => 'ေဒၚ'
//     )
// ));
define('NRC_NATION', json_encode(
    array(
        'naing' => '( ႏိုုင္ )',
        'visit' => '( ဧည္႕ )',
        'whitecard' => '( ၿပဳ )'
    )
));
define('NAME_PREFIX', json_encode(
    array(
         'mr' => 'Mr.', 'ms' => 'Ms.', 'mrs' => 'Mrs.','mg' => 'Mg', 'ko' => 'Ko', 'uncle' => 'U', 'ma' => 'Ma', 'anti' => 'Daw'
    )
));
define('NRC_PREFIX', json_encode(
    array('1/' => '၁ /', '2/' => '၂/', '3/' => '၃ /', '4/' => '၄ /', '5/' => '၅ /', '6/' => '၆ /', '7/' => '၇ /', '8/' => '၈ /', '9/' => '9 /', '10/' => '၁၀ /', '11/' => '၁၁ /', '12/' => '၁၂ /', '13/' => '၁၃ /', '14/' => '၁၄ /')
));

// DEFAULT CACHE TIME
define('CACHE_TIME', 30);
define('SCHEDULE_SEPERATOR', '&nbsp;<i class="fa fa-arrow-circle-right route-link"></i>&nbsp;');

define("PASSENGER_PATH", 'uploads/passenger');
